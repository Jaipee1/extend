//
//  UIViewController+util.h
//  EXTEND
//
//  Created by Apple on 11/04/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (util)

+(UIViewController*) currentViewController;

@end
