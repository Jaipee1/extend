//
//  VideoCallViewController+UI.h
//  EXTEND
//
//  Created by Apple on 09/06/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "VideoCallViewController.h"

@interface VideoCallViewController (UIAdjustments)
- (void)setCallStatusText:(NSString *)text;

- (void)showButtons:(EButtonsBarV)buttons;

- (void)setDuration:(NSInteger)seconds;
- (void)startCallDurationTimerWithSelector:(SEL)sel;
- (void)stopCallDurationTimer;
@end
