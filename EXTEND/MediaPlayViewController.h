//
//  MediaPlayViewController.h
//  EXTEND
//
//  Created by Apple on 29/05/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MediaPlayViewController : UIViewController
@property (strong, nonatomic) NSString * MediaType;
@property (strong, nonatomic) NSString * MediaURL;
@end
