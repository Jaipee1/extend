//
//  HelperClass.m
//  EXTEND
//
//  Created by Apple on 02/03/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "HelperClass.h"

@implementation HelperClass

#pragma Mark-  check for valid email
+ (BOOL)NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}


+ (BOOL)NSStringIsValidName:(NSString *)checkString
{
    NSString *nameRegex = @"[A-Za-z]+";
    NSPredicate *nameTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", nameRegex];
    return [nameTest evaluateWithObject:checkString];
}

+ (BOOL)NSStringIsValidNumber:(NSString *)checkString
{
    NSString *nameRegex = @"[0-9]+";
    NSPredicate *nameTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", nameRegex];
    return [nameTest evaluateWithObject:checkString];
}

+ (NSString*)getValidString:(NSString *)string
{
    if ([HelperClass isValidString:string] && string.length > 0) {
        string = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    } else {
        string = @"";
    }
    return string;
}

+ (BOOL)isValidString:(NSString *)string

{
    if ([string isKindOfClass:[NSString class]] &&  ([string isKindOfClass:[NSNull class]] || string.length == 0 || [string isEqualToString:@"null"] || [string isEqualToString:@"<null>"] || [string isEqualToString:@"(null)"]))
        return NO;
    return YES;
}

+(void)getAddressFromLatLon:(double)pdblLatitude withLongitude:(double)pdblLongitude compilation:(void (^) (id response, NSError* error))handler;
{
    __block NSString * strLocation;
    CLLocation * taplocation= [[CLLocation alloc]initWithLatitude:pdblLatitude longitude:pdblLongitude];
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
    [geocoder reverseGeocodeLocation:taplocation
                   completionHandler:^(NSArray *placemarks, NSError *error) {
                       if (error){
                           NSLog(@"Geocode failed with error: %@", error);
                           return;
                       }
                       CLPlacemark *placemark = [placemarks objectAtIndex:0];
                       NSLog(@"placemark.description %@",placemark.description);
                       NSLog(@"placemark.locality %@",placemark.locality);
                       NSLog(@"placemark.subLocality %@",placemark.subLocality);
                       NSLog(@"placemark.country %@",placemark.country);
                       //placemark.ISOcountryCode);
                       
                       strLocation = [NSString stringWithFormat:@"%@, %@",[placemark subLocality],[placemark locality]];
                       
                       handler (strLocation , nil);
                       
                   }];
    //return strLocation;
}

+(void)getAddressFromLocation:(CLLocation *)location compilation:(void (^) (id response, NSError* error))handler
{

    __block NSString * strLocation;
    //CLLocation * taplocation= [[CLLocation alloc]initWithLatitude:pdblLatitude longitude:pdblLongitude];
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
    [geocoder reverseGeocodeLocation:location
                   completionHandler:^(NSArray *placemarks, NSError *error) {
                       if (error){
                           NSLog(@"Geocode failed with error: %@", error);
                           return;
                       }
                       CLPlacemark *placemark = [placemarks objectAtIndex:0];
                       NSLog(@"placemark.description %@",placemark.description);
                       NSLog(@"placemark.locality %@",placemark.locality);
                       NSLog(@"placemark.subLocality %@",placemark.subLocality);
                       NSLog(@"placemark.country %@",placemark.country);
                       //placemark.ISOcountryCode);
                       
                       strLocation = [NSString stringWithFormat:@"%@, %@",[placemark locality],[placemark ISOcountryCode]];
                       
                       handler (strLocation , nil);
                       
                   }];
}


@end
