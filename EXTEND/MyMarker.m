//
//  MyMarker.m
//  EXTEND
//
//  Created by Apple on 08/05/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "MyMarker.h"

@implementation MyMarker
-(instancetype)init
{
    self = [super init];
    if(self)
    {
        
    }
    return self;
}

-(id)initWithProfilePictureURL:(NSString *)url
{
    self = [super init];
    if(self)
    {
        self.iconView = [[[NSBundle mainBundle] loadNibNamed:@"MyMarker" owner:self options:nil] objectAtIndex:0];
        self.imageViewUser.layer.cornerRadius = self.imageViewUser.frame.size.width/2;
        self.imageViewUser.clipsToBounds = YES;
        [self.imageViewUser sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",UserProfilePictureBaseURL,url]] placeholderImage:[UIImage imageNamed:@"upload_icon"]];
    }
    return self;
}

@end
