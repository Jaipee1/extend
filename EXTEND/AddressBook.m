//
//  AddressBook.m
//  EXTEND
//
//  Created by Apple on 15/04/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "AddressBook.h"
#import "ContactUtility.h"

@implementation AddressBook




- (instancetype)initWithComplitionHandler:(void (^) (id ContactResponse, NSError* error))handler
{
    self = [super init];
    if (self) {
        
        if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined)
        {
            ABAddressBookRequestAccessWithCompletion(ABAddressBookCreateWithOptions(NULL, nil), ^(bool granted, CFErrorRef error) {
                if (granted) {
                    // First time access has been granted, add the contact
                    [self fillContacts];
                    handler(self.arrContact, nil);
                } else {
                    // User denied access
                    // Display an alert telling user the contact could not be added
                }
            });
        }
        
        /*if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusDenied ||
            ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusRestricted){
            
            UIAlertView *cantAddContactAlert = [[UIAlertView alloc] initWithTitle: @"Cannot Add Contact" message: @"You must give the app permission to add the contact first." delegate:nil cancelButtonTitle: @"OK" otherButtonTitles: nil];
             [cantAddContactAlert show];
            
            handler(self.arrContact, nil);
            
        }*/ else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized){
            [self fillContacts];
            handler(self.arrContact, nil);
        } else{
            ABAddressBookRequestAccessWithCompletion(ABAddressBookCreateWithOptions(NULL, nil), ^(bool granted, CFErrorRef error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (!granted){
                        /*UIAlertView *cantAddContactAlert = [[UIAlertView alloc] initWithTitle: @"Cannot Add Contact" message: @"You must give the app permission to add the contact first." delegate:nil cancelButtonTitle: @"OK" otherButtonTitles: nil];
                         [cantAddContactAlert show];*/
                        handler(nil, (__bridge NSError *)(error));
                        return;
                    }
                    [self fillContacts];
                });
            });
        }
    }
    return self;
}

- (void)fillContacts
{
    ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, nil);
    
    NSArray *allContacts = (__bridge NSArray *)ABAddressBookCopyArrayOfAllPeople(addressBookRef);
    for (id record in allContacts){
        ABRecordRef thisContact = (__bridge ABRecordRef)record;
        
        NSString *fName;
        NSString *lName;
        NSString *mNumber;
        NSString *hNumber;
        
        int numberCount = 0;
        
        ABMultiValueRef phonesRef = ABRecordCopyValue(thisContact, kABPersonPhoneProperty);
        
        //NSLog(@"phonesRef : %@",phonesRef);
        
        if (ABMultiValueGetCount(phonesRef) > 0) {
            // 1
            
            fName = (__bridge NSString *)ABRecordCopyValue(thisContact, kABPersonFirstNameProperty);
            lName = (__bridge NSString *)ABRecordCopyValue(thisContact, kABPersonLastNameProperty);
            
            //NSLog(@"fName: %@",fName);
            
            for (int i=0; i < ABMultiValueGetCount(phonesRef); i++)
            {
                CFStringRef phoneLabel = ABMultiValueCopyLabelAtIndex(phonesRef, i);
                CFStringRef phoneValue = ABMultiValueCopyValueAtIndex(phonesRef, i);
                
                NSString *phoneNumber = (__bridge NSString *)phoneValue;
                
                
                NSString *parsedPhoneNumber;
                
                for (int i = 0; i < phoneNumber.length; i++)
                {
                    char currentChar = [phoneNumber characterAtIndex:i];
                    if (currentChar >= '0' && currentChar <= '9') {
                        NSString *digitString = [NSString stringWithFormat:@"%c",currentChar];
                        if (parsedPhoneNumber.length == 0) {
                            parsedPhoneNumber = [NSString stringWithString:digitString];
                        }
                        else{
                            NSString * newString = [parsedPhoneNumber stringByAppendingString:digitString];
                            parsedPhoneNumber = [NSString stringWithString:newString];
                        }
                    }
                }
                
                //NSLog(@"parsedPhoneNumber: %@",parsedPhoneNumber);
                mNumber = parsedPhoneNumber;

            }
            
            
//            for (int i=0; i < ABMultiValueGetCount(phonesRef); i++) {
//                CFStringRef phoneLabel = ABMultiValueCopyLabelAtIndex(phonesRef, i);
//                CFStringRef phoneValue = ABMultiValueCopyValueAtIndex(phonesRef, i);
//                
//                NSString *phoneNumber = (__bridge NSString *)phoneValue;
            
                // parse out unwanted characters in number
//                NSString *parsedPhoneNumber;
//                for (int i = 0; i < phoneNumber.length; i++) {
//                    char currentChar = [phoneNumber characterAtIndex:i];
//                    if (currentChar >= '0' && currentChar <= '9') {
//                        NSString *digitString = [NSString stringWithFormat:@"%c",currentChar];
//                        if (parsedPhoneNumber.length == 0) {
//                            parsedPhoneNumber = [NSString stringWithString:digitString];
//                        }
//                        else{
//                            NSString * newString = [parsedPhoneNumber stringByAppendingString:digitString];
//                            parsedPhoneNumber = [NSString stringWithString:newString];
//                        }
//                    }
//                }
                
//                // check if number is foreign
//                if ((parsedPhoneNumber.length < 11)
//                    || (parsedPhoneNumber.length == 11 && [parsedPhoneNumber hasPrefix:@"1"])) {
//                    
                    numberCount++;
//                    
//                    if (CFStringCompare(phoneLabel, kABPersonPhoneMobileLabel, 0) == kCFCompareEqualTo) {
//                        mNumber = phoneNumber;
//                    }
//                    
//                    if (CFStringCompare(phoneLabel, kABHomeLabel, 0) == kCFCompareEqualTo) {
//                        hNumber = phoneNumber;
//                    }
//                }
                
//                CFRelease(phoneLabel);
//                CFRelease(phoneValue);
//            }
            
            
            // 2
            
            if (numberCount > 0 && fName != nil && ![fName isEqual:[NSNull null]] && ![fName isEqual:nil] ) {
                ContactUtility *newContact = [ContactUtility createContactWithFirst:fName Last:lName MobileNumber:mNumber HomeNumber:hNumber];
                
                if (!self.arrContact) {
                    self.arrContact = [[NSMutableArray alloc] init];
                }
                [self.arrContact addObject:newContact];
               
            }
            
        }
        
        CFRelease(phonesRef);
        
        
    }
    
    //NSLog(@" self.arrContact : %@",self.arrContact);
}


@end
