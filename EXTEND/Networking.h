//
//  Networking.h
//  Patral
//
//  Created by GST on 27/12/16.
//  Copyright © 2016 GST. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Networking : NSObject

// Main Method
+(void)dataTaskwithURL:(NSString *)url Param:(NSDictionary*)param ImageArray:(NSMutableArray *)imageArray ImageParamArray:(NSMutableArray *)imageParamArray compilation:(void (^) (id response, NSError* error))handler;

// For Send RawJson
+(void)rawJsondataTaskwithURL:(NSString *)url Param:(NSDictionary*)param ImageArray:(NSMutableArray *)imageArray ImageParamArray:(NSMutableArray *)imageParamArray compilation:(void (^) (id response, NSError* error))handler;
// For Video
+(void)dataTaskwithURL:(NSString *)url Param:(NSDictionary*)param VideoUrlArray:(NSMutableArray  *)videoUrlArray VideoPrameterArray:(NSMutableArray *)videoParameterArray compilation:(void (^) (id response, NSError* error))handler;
// For Video and Thumbnail
+(void)dataTaskwithURL:(NSString *)url Param:(NSDictionary*)param VideoUrlArray:(NSMutableArray *)videoUrlArray VideoPrameterArray:(NSMutableArray *)videoParameterArray
           ThumbnailImageArray:(NSMutableArray  *)thumbnailimageArray ThumbnaiImagePrameterArray:(NSMutableArray *)thumbnailImageParameterArray compilation:(void (^) (id response, NSError* error))handler;

#pragma For Raw Json And HUD Animation as You Need
+(void)rawJsondataTaskwithURL:(NSString *)url Param:(NSDictionary *)param ImageArray:(NSMutableArray *)imageArray ImageParamArray:(NSMutableArray *)imageParamArray WithHudAnimation:(BOOL)animation compilation:(void (^)(id, NSError *))handler;

#pragma Mark for media chat
+(void)dataTaskForChatwithURL:(NSString *)url Param:(NSDictionary*)param MediaArray:(NSMutableArray *)mediaUrlArray MediaPrameterArray:(NSMutableArray *)mediaParameterArray MediaType:(NSString*)mediaType compilation:(void (^) (id response, NSError* error))handler;

@end
