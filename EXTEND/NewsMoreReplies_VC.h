//
//  NewsMoreReplies_VC.h
//  EXTEND
//
//  Created by Apple on 18/03/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsMoreReplies_VC : UIViewController
@property (strong, nonatomic) NSString * strCommentID;
@property (strong, nonatomic) NSString * strReplyUserID;
@end
