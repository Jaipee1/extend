//
//  MapPostHistoryCell.h
//  EXTEND
//
//  Created by Apple on 09/05/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomLable.h"

@interface MapPostHistoryCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgViewPost;
@property (weak, nonatomic) IBOutlet CustomLable *lblPostTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;

@property (weak, nonatomic) IBOutlet UIButton *btnActiveDeactive;

@end
