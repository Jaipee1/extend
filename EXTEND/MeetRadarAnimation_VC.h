//
//  MeetRadarAnimation_VC.h
//  EXTEND
//
//  Created by Apple on 23/03/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PayPalMobile.h"

@interface MeetRadarAnimation_VC : UIViewController<PayPalPaymentDelegate, PayPalFuturePaymentDelegate, PayPalProfileSharingDelegate>

#pragma -------Paypal Stuff-------
@property(nonatomic, strong, readwrite) NSString *environment;
@property(nonatomic, assign, readwrite) BOOL acceptCreditCards;
@property(nonatomic, strong, readwrite) NSString *resultText;
@property(nonatomic, strong, readwrite) PayPalConfiguration *payPalConfig;

-(void)GetPlanData:(NSDictionary*)param  compilation:(void (^) (id response, NSError* error))handler;

@end
