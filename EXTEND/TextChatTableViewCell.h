//
//  TextChatTableViewCell.h
//  EXTEND
//
//  Created by Apple on 26/05/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextChatTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *tvBGView;
@property (weak, nonatomic) IBOutlet UITextView *tvMessage;
@property (nonatomic) BOOL isIncomingCell;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *trailingConstraint;
@end
