//
//  SidMenu_Cell.h
//  Events
//
//  Created by Genuine Soft Technologies on 12/21/15.
//  Copyright © 2015 Genuine Soft Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SidMenu_Cell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageLine;
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIButton *btn_enter;

@end
