//
//  SubscriptionPlanView.m
//  EXTEND
//
//  Created by Apple on 08/05/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "SubscriptionPlanView.h"

@implementation SubscriptionPlanView


-(instancetype)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame])) {
        // Initialization code
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"SubscriptionPlanView" owner:self options:nil];
        UIView *mainView = [subviewArray objectAtIndex:0];
        _btnBuySilver.layer.cornerRadius = _btnBuySilver.frame.size.height/2;
        _btnBuyGold.layer.cornerRadius = _btnBuyGold.frame.size.height/2;
        _btnBuySilver.clipsToBounds = YES;
        _btnBuyGold.clipsToBounds = YES;
        
        /*_tblSubscriptionPlans.delegate = self;
        _tblSubscriptionPlans.dataSource = self;*/
        
        [mainView layoutIfNeeded];
        [self addSubview:mainView];
    }
    return self;
}



// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}

- (IBAction)btnCancleAction:(id)sender
{
    [self removeFromSuperview];
}


@end
