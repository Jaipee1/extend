//
//  HelperClass.h
//  EXTEND
//
//  Created by Apple on 02/03/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface HelperClass : NSObject
+ (BOOL)NSStringIsValidEmail:(NSString *)checkString;
+ (BOOL)NSStringIsValidName:(NSString *)checkString;
+ (BOOL)NSStringIsValidNumber:(NSString *)checkString;
+ (NSString*)getValidString:(NSString *)string;
+ (BOOL)isValidString:(NSString *)string;
+(void)getAddressFromLatLon:(double)pdblLatitude withLongitude:(double)pdblLongitude compilation:(void (^) (id response, NSError* error))handler;

+(void)getAddressFromLocation:(CLLocation *)location compilation:(void (^) (id response, NSError* error))handler;
@end
