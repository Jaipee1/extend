//
//  Edit_Reply_Comment_VC.h
//  EXTEND
//
//  Created by Apple on 15/03/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Edit_Reply_Comment_VC : UIViewController
@property  BOOL isReply;
@property (strong, nonatomic) NSString * strCommentID;
@property (strong, nonatomic) NSString * strReplyUserID;
@property (strong, nonatomic) NSString * strComment;
@end
