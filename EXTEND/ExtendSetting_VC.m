//
//  ExtendSetting_VC.m
//  EXTEND
//
//  Created by Apple on 18/03/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "ExtendSetting_VC.h"
#import "Networking.h"
#import "UserModel.h"
#import "LocationManagerModel.h"
#import "HelperClass.h"

#import "UIViewController+util.h"

#import "MARKRangeSlider.h"
#import "UIColor+Demo.h"

static CGFloat const kViewControllerRangeSliderWidth = 290.0;
static CGFloat const kViewControllerLabelWidth = 100.0;

@interface ExtendSetting_VC ()
{
    NSString * strInterest;
    NSString * strSwipe;
    NSString * strUserFind;
    NSString * strNotifiyUserlike;
    NSString * strNotifyUserMessage;
    NSString * strDistance;
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *lblLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblLocationRange;
@property (weak, nonatomic) IBOutlet UISlider *sliderLocationRange;
@property (weak, nonatomic) IBOutlet UISwitch *switchMale;
@property (weak, nonatomic) IBOutlet UISwitch *switchFemale;
@property (weak, nonatomic) IBOutlet UILabel *lblAgeRangeMin;
@property (weak, nonatomic) IBOutlet UILabel *lblAgeRangeMax;
@property (weak, nonatomic) IBOutlet UISlider *sliderAgeRange;
@property (weak, nonatomic) IBOutlet UISlider *sliderAgeRangeMax;
@property (weak, nonatomic) IBOutlet UISwitch *switchSwipe;
@property (weak, nonatomic) IBOutlet UISwitch *switchUserfind;
@property (weak, nonatomic) IBOutlet UISwitch *switchUserLike;
@property (weak, nonatomic) IBOutlet UISwitch *switchUserMessage;
@property (strong, nonatomic) IBOutlet UIView *viewAge;


@property (nonatomic, strong) MARKRangeSlider *rangeSlider;
@property (nonatomic, strong) UILabel *label;

@end

@implementation ExtendSetting_VC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
//    CGRect contentRect = CGRectZero;
//    for (UIView *view in self.scrollView.subviews) {
//        contentRect = CGRectUnion(contentRect, view.frame);
//    }
//    self.scrollView.contentSize = contentRect.size;
//    self.scrollView.contentSize = CGSizeMake(320, 1500);
    
    [UIViewController currentViewController];
    
    NSLog(@"which ViewController is this :%@",[UIViewController currentViewController]);
    
    [self setLocation];
    [self setUpViewComponents];
    [self setup];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    CGFloat labelX = (CGRectGetWidth(self.view.frame) - kViewControllerLabelWidth) / 2;
    self.label.frame = CGRectMake(labelX, 110.0, kViewControllerLabelWidth, 20.0);
    
    CGFloat sliderX = (CGRectGetWidth(self.view.frame) - kViewControllerRangeSliderWidth) / 2;
    //self.rangeSlider.frame = CGRectMake(sliderX, CGRectGetMaxY(self.label.frame) + 20.0, 290.0, 20.0);
    self.rangeSlider.frame = self.sliderAgeRange.frame;
    self.rangeSlider.backgroundColor = [UIColor clearColor];
    //self.rangeSlider.leftThumbView.backgroundColor = self.rangeSlider.rightThumbView.backgroundColor = UIColorFromRGB(Color_NavigationBar) ;
    self.rangeSlider.leftThumbView.layer.cornerRadius = self.rangeSlider.leftThumbView.frame.size.height/2 ;
    self.rangeSlider.leftThumbView.clipsToBounds = YES;
    
    self.rangeSlider.rightThumbView.layer.cornerRadius = self.rangeSlider.rightThumbView.frame.size.height/2 ;
    self.rangeSlider.rightThumbView.clipsToBounds = YES;
}

#pragma mark - Actions

- (void)rangeSliderValueDidChange:(MARKRangeSlider *)slider
{
    [self updateRangeText];
}

#pragma mark - UI

- (void)setUpViewComponents
{
       
    // Init slider
    self.rangeSlider = [[MARKRangeSlider alloc] initWithFrame:CGRectZero];
    self.rangeSlider.backgroundColor = [UIColor backgroundColor];
    [self.rangeSlider addTarget:self
                         action:@selector(rangeSliderValueDidChange:)
               forControlEvents:UIControlEventValueChanged];
    
    [self.rangeSlider setMinValue:18 maxValue:100];
    [self.rangeSlider setLeftValue:25 rightValue:50];
    
    self.rangeSlider.minimumDistance = 1;
    
    [self updateRangeText];
    
    //[self.view addSubview:self.label];
    [self.viewAge addSubview:self.rangeSlider];
    
    self.sliderAgeRange.hidden = self.sliderAgeRangeMax.hidden = YES;
}

- (void)updateRangeText
{
    NSLog(@"%0.2f - %0.2f", self.rangeSlider.leftValue, self.rangeSlider.rightValue);
    self.label.text = [NSString stringWithFormat:@"%0.2f - %0.2f",
                       self.rangeSlider.leftValue, self.rangeSlider.rightValue];
    self.lblAgeRangeMin.text = [NSString stringWithFormat:@"%d",(int) self.rangeSlider.leftValue];
    self.lblAgeRangeMax.text = [NSString stringWithFormat:@"%d",(int)self.rangeSlider.rightValue];
}


-(void)setLocation
{
    NSLog(@"currentLocation  : %@",[[[LocationManagerModel sharedSingleton] locationManager] location]);
    
    self.lblLocation.text = nil;
    [HelperClass getAddressFromLocation:[[[LocationManagerModel sharedSingleton] locationManager] location] compilation:^(id response, NSError *error) {
        if (!error) {
            self.lblLocation.text = response;
        }
        else
        {
            self.lblLocation.text = nil;
        }
    }];
}

-(void)setup
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"MeetSettingKey"] != nil)
    {
        NSData *dictionaryData = [[NSUserDefaults standardUserDefaults] objectForKey:@"MeetSettingKey"];
        NSDictionary *dictionary = [NSKeyedUnarchiver unarchiveObjectWithData:dictionaryData];
        NSLog(@"MeetSetting dictionary :%@",dictionary);
        
        if ([[dictionary valueForKey:@"user_interest"] isEqualToString:@"male"] )
        {
            [self.switchFemale setOn:NO animated:YES];
            [self.switchMale setOn:YES animated:YES];
        }
        else if ([[dictionary valueForKey:@"user_interest"] isEqualToString:@"female"] )
        {
            [self.switchFemale setOn:YES animated:YES];
            [self.switchMale setOn:NO animated:YES];
        }
        if ([[dictionary valueForKey:@"user_interest"] isEqualToString:@"both"] )
        {
            [self.switchFemale setOn:YES animated:YES];
            [self.switchMale setOn:YES animated:YES];
        }
        
        
        if ([[dictionary valueForKey:@"user_find"] isEqualToString:@"no"] )
        {
            [self.switchUserfind setOn:NO animated:YES];
        }
        else
        {
            [self.switchUserfind setOn:YES animated:YES];
        }
        
        if ([[dictionary valueForKey:@"swip"] isEqualToString:@"no"] )
        {
            [self.switchSwipe setOn:NO animated:YES];
        }
        else
        {
            [self.switchSwipe setOn:YES animated:YES];
        }
        
        if ([[dictionary valueForKey:@"notify_user_like"] isEqualToString:@"no"] )
        {
            [self.switchUserLike setOn:NO animated:YES];
        }
        else
        {
            [self.switchUserLike setOn:YES animated:YES];
        }
        
        if ([[dictionary valueForKey:@"notify_user_message"] isEqualToString:@"no"] )
        {
            [self.switchUserMessage setOn:NO animated:YES];
        }
        else
        {
            [self.switchUserMessage setOn:YES animated:YES];
        }
        
        self.lblLocationRange.text = [NSString stringWithFormat:@"%@ km", [dictionary valueForKey:@"distance"]];
        self.lblAgeRangeMin.text = [NSString stringWithFormat:@"%@", [dictionary valueForKey:@"min_age"]];
        self.lblAgeRangeMax.text = [NSString stringWithFormat:@"%@", [dictionary valueForKey:@"max_age"]];
        
        self.sliderLocationRange.value = [[dictionary valueForKey:@"distance"] intValue];
        /*self.sliderAgeRange.value = [[dictionary valueForKey:@"min_age"] intValue];
        self.sliderAgeRangeMax.value = [[dictionary valueForKey:@"max_age"] intValue];*/
        [self.rangeSlider setLeftValue:[[dictionary valueForKey:@"min_age"] intValue] rightValue:[[dictionary valueForKey:@"max_age"] intValue]];
    }
    else
    {
        NSLog(@"Not set Meet Settings");
        
        self.sliderLocationRange.value = [_lblLocationRange.text intValue];
        self.sliderAgeRange.value = [_lblAgeRangeMin.text intValue];
        self.sliderAgeRangeMax.value = [_lblAgeRangeMax.text intValue];
        [self.switchFemale setOn:NO animated:YES];
        [self.switchMale setOn:YES animated:YES];
        [self.switchSwipe setOn:NO animated:YES];
        [self.switchUserfind setOn:NO animated:YES];
        [self.switchUserLike setOn:NO animated:YES];
        [self.switchUserMessage setOn:NO animated:YES];
        [self.rangeSlider setLeftValue:25 rightValue:50];
    }

}

-(void)viewWillAppear:(BOOL)animated
{

    self.navigationController.navigationBar.hidden = NO;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(40, 0,SCREEN_MAX_LENGTH-40, 40)];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"Raleway" size:16];
    label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    label.textAlignment = NSTextAlignmentLeft;
    label.textColor =[UIColor whiteColor];
    label.text=@"Setting";
    self.navigationItem.titleView = label;
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back_icon"]style:UIBarButtonItemStylePlain target:self action:@selector(Back)];
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    
//    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"setting_icon"]style:UIBarButtonItemStylePlain target:self action:@selector(setting)];
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(setting)];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
}
-(void)setting
{
    if (![_switchMale isOn] && ![_switchFemale isOn])
    {
        NSLog(@"Please Select Interest");
    }
    else
    {
        [self setMeetSetting];
    }
}

-(void)Back
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)sliderActions:(UISlider *)sender
{
    if (sender.tag == 1)// Location Distance
    {
        int SliderValue = (int)roundf(sender.value);
        self.lblLocationRange.text = [NSString stringWithFormat:@"%d km", SliderValue];
    }
    else if (sender.tag == 2)// Min Age
    {
        int SliderValue = (int)roundf(sender.value);
        self.lblAgeRangeMin.text = [NSString stringWithFormat:@"%d", SliderValue];
        
        if (SliderValue >= (int) roundf(_sliderAgeRangeMax.value) )
        {
            self.sliderAgeRangeMax.value = SliderValue;
            self.lblAgeRangeMax.text = [NSString stringWithFormat:@"%d", SliderValue];
        }
    }
    else if (sender.tag == 3)//Max Age
    {
        int SliderValue = (int)roundf(sender.value);
        self.lblAgeRangeMax.text = [NSString stringWithFormat:@"%d", SliderValue];
        
        if (SliderValue <= (int) roundf(_sliderAgeRange.value) )
        {
            self.sliderAgeRange.value = SliderValue;
            self.lblAgeRangeMin.text = [NSString stringWithFormat:@"%d", SliderValue];
        }
    }
}

-(IBAction)switchActions:(UISwitch *)sender
{
    if (sender.tag == 1)
    {
        if([sender isOn]){
            NSLog(@"Switch is ON");
        } else{
            NSLog(@"Switch is OFF");
        }
    }
    else if (sender.tag == 2)
    {
        if([sender isOn]){
            NSLog(@"Switch is ON");
        } else{
            NSLog(@"Switch is OFF");
        }
    }
    else if (sender.tag == 10)
    {
        if([sender isOn]){
            NSLog(@"Switch is ON");
        } else{
            NSLog(@"Switch is OFF");
        }
    }
    else if (sender.tag == 11)
    {
        if([sender isOn]){
            NSLog(@"Switch is ON");
        } else{
            NSLog(@"Switch is OFF");
        }
    }
    else if (sender.tag == 12)
    {
        if([sender isOn]){
            NSLog(@"Switch is ON");
        } else{
            NSLog(@"Switch is OFF");
        }
    }
}


-(void)setMeetSetting
{
    // {"apptoken":"d10289185f466003c450a15d300e50d5",  "user_id":"95" ,"user_interest":"male", "swip":"yes", "user_find":"yes", "notify_user_like":"yes", "notify_user_message":"yes","min_age":"18","max_age":"23","distance":"25"}
    
    if ([_switchMale isOn] && [_switchFemale isOn])
    {
        strInterest = @"both";
    }
    else if ([_switchMale isOn])
    {
        strInterest = @"male";
    }
    else if ([_switchFemale isOn])
    {
        strInterest = @"female";
    }
    
    /*if ([_switchSwipe isOn])
    {
        strSwipe = @"yes";
    }
    else
    {
        strSwipe = @"no";
    }*/
    
    if ([_switchUserfind isOn])
    {
        strUserFind = @"yes";
    }
    else
    {
        strUserFind = @"no";
    }
    
    if ([_switchUserLike isOn])
    {
        strNotifiyUserlike = @"yes";
    }
    else
    {
        strNotifiyUserlike = @"no";
    }
    
    if ([_switchUserMessage isOn])
    {
        strNotifyUserMessage = @"yes";
    }
    else
    {
        strNotifyUserMessage = @"no";
    }
    
    strDistance = [_lblLocationRange.text stringByReplacingOccurrencesOfString:@" km"withString:@""];
    
    NSMutableDictionary * dictParam =[[NSMutableDictionary alloc] init];
    [dictParam setObject:[[UserModel sharedSingleton] getUserID] forKey:@"user_id"];
    [dictParam setObject:AppToken  forKey:@"apptoken"];
    [dictParam setObject:strInterest  forKey:@"user_interest"];
    //[dictParam setObject:strSwipe  forKey:@"swip"];
    [dictParam setObject:strUserFind  forKey:@"user_find"];
    [dictParam setObject:strNotifiyUserlike  forKey:@"notify_user_like"];
    [dictParam setObject:strNotifyUserMessage  forKey:@"notify_user_message"];
    [dictParam setObject:_lblAgeRangeMin.text  forKey:@"min_age"];
    [dictParam setObject:_lblAgeRangeMax.text  forKey:@"max_age"];
    [dictParam setObject:strDistance  forKey:@"distance"];
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:dictParam];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"MeetSettingKey"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSLog(@"DictLogin:%@",dictParam);
    
    [Networking rawJsondataTaskwithURL:MeetSetting Param:dictParam ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error)
     {
         if (!error)
         {
             NSLog(@"response :%@",response);
             
             if ([[response valueForKey:@"status"]integerValue] == 1)
             {
                 [self.navigationController popViewControllerAnimated:YES];
                              }
             else if ([[response valueForKey:@"status"]integerValue] == 0)
             {
                 [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Error];
             }
             
         }
     }];
    
}

#pragma mark-  Commaon Alertview

-(void)showAlertMessage:(NSString*)message Title:(NSString *)title
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               {
                               }];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}


@end
