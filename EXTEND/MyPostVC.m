//
//  MyPostVC.m
//  EXTEND
//
//  Created by Apple on 17/04/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "MyPostVC.h"
#import "UserModel.h"

@interface MyPostVC ()
@property (nonatomic, weak) IBOutlet UITableView *tblMyPosts;
@end

@implementation MyPostVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = NO;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(40, 0,SCREEN_MAX_LENGTH-40, 40)];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"Raleway" size:16];
    label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    label.textAlignment = NSTextAlignmentLeft;
    label.textColor =[UIColor whiteColor];
    label.text=@"My Post's";
    self.navigationItem.titleView = label;
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back_icon"]style:UIBarButtonItemStylePlain target:self action:@selector(Back)];
    self.navigationItem.leftBarButtonItem = revealButtonItem;
}

-(void)Back
{
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Tableview Data Source method

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 25;    //count number of row from counting array hear cataGorry is An Array
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //static NSString *MyIdentifier = @"NotificationCell";
    
    //NotificationCell *cell = (NotificationCell *)  [self.tblMyPosts dequeueReusableCellWithIdentifier:@"NotificationCell" forIndexPath:indexPath];
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    return cell;
}



- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    NSInteger currentOffset = scrollView.contentOffset.y;
    NSInteger maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
    if (maximumOffset - currentOffset <= -40) {
        NSLog(@"reload");
        //[self showPostwithLastRecored:last_RecordCount];
    }
}


/*- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"Cell";
    //NotificationCell *cell = [self.tblMyPosts dequeueReusableCellWithIdentifier:MyIdentifier];
    //objpost = [_arr objectAtIndex:indexPath.row];
    return 300 ;//+ [self heightForLabelwithText:objpost.PostName withFont:[UIFont systemFontOfSize:15.0] withWidth:cell.lblMsgCaption.frame.size.width] ;
    
}*/

-(CGFloat )heightForLabelwithText:(NSString *)text withFont:(UIFont *)font withWidth:(CGFloat)width
{
    CGRect rect = CGRectMake(0, 0, width, CGFLOAT_MAX);
    
    UILabel * lbl = [[UILabel alloc] initWithFrame:rect];
    lbl.numberOfLines = 0;
    lbl.lineBreakMode = NSLineBreakByCharWrapping;
    lbl.font = font;
    lbl.text = text;
    [lbl sizeToFit];
    
    return lbl.frame.size.height;
}

/*-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
 {
 _viewHeader.frame = CGRectMake(0, 0, self.tblPost.frame.size.width, _viewHeader.frame.size.height);
 _viewHeader.backgroundColor = UIColorFromRGB(Color_NavigationBar);
 UIView *view = [[UIView alloc] init ];
 tableView.tableHeaderView = _viewHeader;
 return view;
 }*/

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.001/*self.viewheader.frame.size.height*/;
}

@end
