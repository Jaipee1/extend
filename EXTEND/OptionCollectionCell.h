//
//  OptionCollectionCell.h
//  EXTEND
//
//  Created by Apple on 24/03/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OptionCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgViewOptions;
@property (weak, nonatomic) IBOutlet UILabel *lblOptionTitle;

@end
