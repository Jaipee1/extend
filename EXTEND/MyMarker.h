//
//  MyMarker.h
//  EXTEND
//
//  Created by Apple on 08/05/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <GoogleMaps/GoogleMaps.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface MyMarker : GMSMarker

@property (weak, nonatomic) IBOutlet UIImageView *imageViewUser;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewPin;

@property (nonatomic, retain) NSString * Fullname;
@property (nonatomic, retain) NSString * urlPropic;
@property (nonatomic, retain) NSString * urlPost;
@property (nonatomic, retain) NSString * TitlePost;
@property (nonatomic, retain) NSString * DescriptionPost;
@property (nonatomic, retain) NSString * userID;


@property (nonatomic) CGFloat  latitude;
@property (nonatomic) CGFloat  longitude;

-(id)initWithProfilePictureURL:(NSString *)url;
@end


/*
 description = "posted for test";
 "email_id" = "ashishkumar.sharma30@gmail.com";
 "first_name" = Ashish;
 id = 95;
 image = "4041494224877.jpg";
 "last_name" = Sharma;
 "location_latitude" = "22.722344093099856";
 "location_longitude" = "75.88764242827894";
 "login_status" = 1;
 "phone_number" = 975865865895;
 "profile_picture" = "img58cd47095c171.jpg";
 "sys_date" = "2017-05-08 06:27:57";
 title = "today post";
 */
