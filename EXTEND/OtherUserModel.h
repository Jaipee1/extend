//
//  OtherUserModel.h
//  EXTEND
//
//  Created by Apple on 01/04/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OtherUserModel : NSObject

@property (nonatomic, strong) NSString * date_of_birth;
@property (nonatomic, strong) NSString * email_id;
@property (nonatomic, strong) NSString * first_name;
@property (nonatomic, strong) NSString * gender;
@property (nonatomic, strong) NSString * user_id;
@property (nonatomic, strong) NSString * image;
@property (nonatomic, strong) NSString * last_name;
@property (nonatomic, strong) NSString * location_latitude;
@property (nonatomic, strong) NSString * location_longitude;
@property (nonatomic, strong) NSString * login_status;
@property (nonatomic, strong) NSString * phone_number;

-(NSMutableArray *)arryWithOtherUserData;

@end

/*"date_of_birth" = 24;
 "email_id" = "savitakeshav35@gmail.com";
"first_name" = Keshav;
id = 132;
image = "img58cf80341772e.jpg";
"last_name" = Savita;
"location_latitude" = "22.7234565";
"location_longitude" = "75.8865718";
"login_status" = 0;
"phone_number" = 9993350346;*/
