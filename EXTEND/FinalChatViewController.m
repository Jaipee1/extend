//
//  FinalChatViewController.m
//  EXTEND
//
//  Created by Apple on 25/05/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "FinalChatViewController.h"
#import "TextChatTableViewCell.h"
#import "VideoAndImageTableViewCell.h"
#import "Networking.h"
#import "UserModel.h"
#import "ChatModel.h"
#import "MediaPlayViewController.h"
#import "CallViewController.h"
#import "VideoCallViewController.h"
#import <Sinch/Sinch.h>

#import <SDWebImage/UIImageView+WebCache.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVKit/AVKit.h>


#define kTextCellIdentifier @"TextChatTableViewCell"
#define kMediaCellIdentifier @"VideoAndImageTableViewCell"


@interface FinalChatViewController ()<UITableViewDelegate,UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate, AVAudioPlayerDelegate, UITextViewDelegate,SINCallClientDelegate>
{
    TextChatTableViewCell * cell;
    VideoAndImageTableViewCell * cell2;
    ChatModel * objChatModel;
    NSMutableArray * arrChatData;
    NSMutableArray <ChatModel *>* arrChatModelData;
}
@property (weak, nonatomic) IBOutlet UIImageView *imgViewProPic;
@property (weak, nonatomic) IBOutlet UILabel *lblUsername;
@property (weak, nonatomic) IBOutlet UITextView *tvSendMessage;

@property (weak, nonatomic) IBOutlet UITableView *tblchat;
@property (strong, nonatomic) AVPlayer * songPlayer;
@end

@implementation FinalChatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(didReceiveRemoteNotification:)
     name:UIApplicationDidReceiveRemoteNotification
     object:nil];
    
    self.client.callClient.delegate = self;
    
    [self setUI];
    // Do any additional setup after loading the view.
    
    //static NSString *CellIdentifier1 = @"TextChatTableViewCell";
    //static NSString *CellIdentifier2 = @"SpaceCell";
    
    
    NSLog(@"arrOtherUserData:%@",self.arrOtherUserData);
    
    UINib *nib = [UINib nibWithNibName:kTextCellIdentifier bundle:nil];
    [self.tblchat registerNib:nib forCellReuseIdentifier:kTextCellIdentifier];
    
    UINib *nib2 = [UINib nibWithNibName:kMediaCellIdentifier bundle:nil];
    [self.tblchat registerNib:nib2 forCellReuseIdentifier:@"VideoAndImageCell"];
    
    [self GetChatWithOtherUser];
}

-(void)setUI
{
    self.imgViewProPic.layer.cornerRadius = self.imgViewProPic.frame.size.height/2;
    self.imgViewProPic.clipsToBounds = YES;
    
    self.tvSendMessage.layer.cornerRadius = 5;
    self.tvSendMessage.clipsToBounds = YES;
    
    [self.imgViewProPic sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",UserProfilePictureBaseURL,[self.arrOtherUserData valueForKey:@"profile_image"]]] placeholderImage:[UIImage imageNamed:@"upload_icon"]];
    self.lblUsername.text = [self.arrOtherUserData valueForKey:@"name"];
    
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)GetChatWithOtherUser
{
    //{"apptoken":"d10289185f466003c450a15d300e50d5", "user_id":"170"  ,"friend_id":"54" }
    
    NSMutableDictionary * dictParam =[[NSMutableDictionary alloc] init];
    [dictParam setObject:[[UserModel sharedSingleton] getUserID] forKey:@"user_id"];
    [dictParam setObject:AppToken  forKey:@"apptoken"];
    [dictParam setObject:self.otherPersonUserID  forKey:@"friend_id"];
    NSLog(@"dictParam:%@",dictParam);
    [Networking rawJsondataTaskwithURL:ChatWithOther Param:dictParam ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error) {
        if (!error)
        {
            
            NSLog(@"ChatWithOther Data : %@",response);
            
            if ([[response valueForKey:@"status"]integerValue] == 1)
            {
                arrChatData         = [[NSMutableArray alloc] init];
                arrChatData         = [response valueForKey:@"message"];
                arrChatModelData    = [[NSMutableArray alloc] init];
                for (int i= 0; i<arrChatData.count; i++)
                {
                    objChatModel = [ChatModel new];
                    
                    objChatModel.chat_id            = [[arrChatData valueForKey:@"chat_id"] objectAtIndex:i];
                    objChatModel.chat_data          = [[arrChatData valueForKey:@"chat_data"] objectAtIndex:i];
                    objChatModel.chat_datatype      = [[arrChatData valueForKey:@"chat_datatype"] objectAtIndex:i];
                    objChatModel.chat_date          = [[arrChatData valueForKey:@"chat_date"] objectAtIndex:i];
                    objChatModel.chat_msg           = [[arrChatData valueForKey:@"chat_msg"] objectAtIndex:i];
                    objChatModel.chat_reciver       = [[arrChatData valueForKey:@"chat_reciver"] objectAtIndex:i];
                    objChatModel.chat_sender        = [[arrChatData valueForKey:@"chat_sender"] objectAtIndex:i];
                    objChatModel.name               = [[arrChatData valueForKey:@"name"] objectAtIndex:i];
                    objChatModel.profile_picture    = [[arrChatData valueForKey:@"profile_picture"] objectAtIndex:i];
                    //objChatModel.read_status = [[arrChatData valueForKey:@"chat_reciver"] objectAtIndex:i];
                    
                    [arrChatModelData addObject:objChatModel];
                }
                
                NSLog(@"arrChatModelData count: %lu",(unsigned long)arrChatModelData.count);
                
                self.tblchat.delegate   = self;
                self.tblchat.dataSource = self;
                [self.tblchat reloadData];
                NSIndexPath* ip = [NSIndexPath indexPathForRow:arrChatModelData.count-1 inSection:0];
                [self.tblchat scrollToRowAtIndexPath:ip atScrollPosition:UITableViewScrollPositionBottom animated:NO];
                
            }
            else if ([[response valueForKey:@"status"]integerValue] == 0)
            {
                [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Error];
            }
            
        }
        else
        {
            
        }
    }];
    
}

#pragma Mark-  Commaon Alertview

-(void)showAlertMessage:(NSString*)message Title:(NSString *)title
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               {
                               }];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(IBAction)backAction:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arrChatModelData.count;  //count number of row from counting array hear cataGorry is An Array
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.tag = indexPath.row;
    objChatModel = [arrChatModelData objectAtIndex:indexPath.row];
    
    if ([objChatModel.chat_datatype isEqualToString:@"text"])
    {
        cell = (TextChatTableViewCell *)  [self.tblchat dequeueReusableCellWithIdentifier:kTextCellIdentifier forIndexPath:indexPath];
        
        if ([objChatModel.chat_sender isEqualToString:[[UserModel sharedSingleton] getUserID]])
        {
            cell.tvMessage.textAlignment = NSTextAlignmentRight;
            cell.tvMessage.textColor = [UIColor blackColor];
            cell.tvBGView.backgroundColor = [UIColor lightGrayColor];
            cell.leadingConstraint.constant =  60;
            cell.trailingConstraint.constant =  8;
        }
        else
        {
            cell.tvMessage.textAlignment = NSTextAlignmentLeft;
            cell.tvMessage.textColor = [UIColor whiteColor];
            cell.tvBGView.backgroundColor = [UIColor colorWithRed:90.0/255.0 green:12.0/255.0 blue:255.0/255.0 alpha:1.0];;
            cell.trailingConstraint.constant =  60;
            cell.leadingConstraint.constant =  8;
        }
        
        cell.tvMessage.text = objChatModel.chat_msg;
        
        
        return cell;
    }
    else
    {
        cell2 = (VideoAndImageTableViewCell *)  [self.tblchat dequeueReusableCellWithIdentifier:@"VideoAndImageCell" forIndexPath:indexPath];
        
        if ([objChatModel.chat_sender isEqualToString:[[UserModel sharedSingleton] getUserID]])
        {
            cell2.viewBG.backgroundColor = [UIColor lightGrayColor];
            cell2.leadingConstraintMedia.constant =  self.view.frame.size.width - 100 - 8;
        }
        else
        {
            cell2.viewBG.backgroundColor = [UIColor colorWithRed:90.0/255.0 green:12.0/255.0 blue:255.0/255.0 alpha:1.0];
            cell2.leadingConstraintMedia.constant =  8;
        }
        
        if ([objChatModel.chat_datatype isEqualToString:@"image"])
        {
            [cell2.imgMsg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ChatPostImageBaseUrl,objChatModel.chat_data]] placeholderImage:[UIImage imageNamed:@"upload_icon"]];
            [cell2.btnMediaAction setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        }
        else if ([objChatModel.chat_datatype isEqualToString:@"audio"])
        {
            cell2.imgMsg.image = nil;
            [cell2.btnMediaAction setImage:[UIImage imageNamed:@"audio"] forState:UIControlStateNormal];
        }
        else if ([objChatModel.chat_datatype isEqualToString:@"video"])
        {
            //cell2.imgMsg.image = [UIImage imageNamed:@""];
            [cell2.imgMsg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ChatPostImageBaseUrl,objChatModel.chat_data]] placeholderImage:[UIImage imageNamed:@"upload_icon"]];
            [cell2.btnMediaAction setImage:[UIImage imageNamed:@"arrow"] forState:UIControlStateNormal];
        }
        
        cell2.btnMediaAction.tag = indexPath.row + 10000;
        [cell2.btnMediaAction addTarget:self action:@selector(btnMediaAction:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell2;
    }
    
    
}

-(IBAction)btnMediaAction:(UIButton *)sender
{
    objChatModel = [arrChatModelData objectAtIndex:sender.tag - 10000];
    
    if ([objChatModel.chat_datatype isEqualToString:@"image"])
    {
        NSString * strUrl = [NSString stringWithFormat:@"%@%@",ChatPostImageBaseUrl,objChatModel.chat_data];
        MediaPlayViewController * mediaPlay = [self.storyboard instantiateViewControllerWithIdentifier:@"MediaPlayViewController"];
        mediaPlay.MediaURL = strUrl;
        mediaPlay.MediaType = objChatModel.chat_datatype;
        [self.navigationController pushViewController:mediaPlay animated:YES];
    }
    else if ([objChatModel.chat_datatype isEqualToString:@"audio"])
    {
        //NSError *error;
        NSString * strUrl = [NSString stringWithFormat:@"%@%@",ChatPostImageBaseUrl,objChatModel.chat_data];
        
        
        /*AVPlayer *player = [[AVPlayer alloc]initWithURL:[NSURL URLWithString:strUrl]];
         self.songPlayer = player;
         [[NSNotificationCenter defaultCenter] addObserver:self
         selector:@selector(playerItemDidReachEnd:)
         name:AVPlayerItemDidPlayToEndTimeNotification
         object:[_songPlayer currentItem]];
         [self.songPlayer addObserver:self forKeyPath:@"status" options:0 context:nil];
         //[NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateProgress:) userInfo:nil repeats:YES];*/
        
        MediaPlayViewController * mediaPlay = [self.storyboard instantiateViewControllerWithIdentifier:@"MediaPlayViewController"];
        mediaPlay.MediaURL = strUrl;
        mediaPlay.MediaType = objChatModel.chat_datatype;
        [self.navigationController pushViewController:mediaPlay animated:YES];
        
        
        
    }
    else if ([objChatModel.chat_datatype isEqualToString:@"video"])
    {
        NSString * strUrl = [NSString stringWithFormat:@"%@%@",ChatPostImageBaseUrl,objChatModel.chat_data];
        MediaPlayViewController * mediaPlay = [self.storyboard instantiateViewControllerWithIdentifier:@"MediaPlayViewController"];
        mediaPlay.MediaURL = strUrl;
        mediaPlay.MediaType = objChatModel.chat_datatype;
        [self.navigationController pushViewController:mediaPlay animated:YES];
    }
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    
    if (object == _songPlayer && [keyPath isEqualToString:@"status"]) {
        if (_songPlayer.status == AVPlayerStatusFailed) {
            NSLog(@"AVPlayer Failed");
            
        } else if (_songPlayer.status == AVPlayerStatusReadyToPlay) {
            NSLog(@"AVPlayerStatusReadyToPlay");
            [self.songPlayer play];
            
            
        } else if (_songPlayer.status == AVPlayerItemStatusUnknown) {
            NSLog(@"AVPlayer Unknown");
            
        }
    }
}

- (void)playerItemDidReachEnd:(NSNotification *)notification {
    
    //  code here to play next sound file
    
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *) player successfully:(BOOL)flag
{
    
    //Called when the audio playback finishes.
    NSLog(@"Finish Audio");
    
}

- (void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error
{
    NSLog(@"error Audio : %@",error.description);
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    objChatModel = [arrChatModelData objectAtIndex:indexPath.row];
    if ([objChatModel.chat_datatype isEqualToString:@"text"])
    {
        cell = [self.tblchat dequeueReusableCellWithIdentifier:kTextCellIdentifier];
        return /*cell.contentView.frame.size.height +*/ 20 + [self heightForLabelwithText:objChatModel.chat_msg withFont:[UIFont systemFontOfSize:cell.tvMessage.font.pointSize] withWidth:cell.tvMessage.frame.size.width - 60] ;
    }
    else
    {
        cell2 = [self.tblchat dequeueReusableCellWithIdentifier:kMediaCellIdentifier];
        return  110;
    }
    
    
    
}

-(CGFloat )heightForLabelwithText:(NSString *)text withFont:(UIFont *)font withWidth:(CGFloat)width
{
    CGRect rect = CGRectMake(0, 0, width, CGFLOAT_MAX);
    
    /*UILabel * lbl = [[UILabel alloc] initWithFrame:rect];
     lbl.numberOfLines = 0;
     lbl.lineBreakMode = NSLineBreakByCharWrapping;
     lbl.font = font;
     lbl.text = text;
     [lbl sizeToFit];*/
    
    UITextView * lbl = [[UITextView alloc] initWithFrame:rect];
    //lbl.numberOfLines = 0;
    //lbl.lineBreakMode = NSLineBreakByCharWrapping;
    lbl.font = font;
    lbl.text = text;
    [lbl sizeToFit];
    
    return lbl.frame.size.height;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.001/*self.viewheader.frame.size.height*/;
}



-(void)MediaChatSendApiCallWithMediaType:(NSString *)mediaType MediaData:(id)arrMediaData;
{
    /*user_id:54
     friend_id:53
     chat_msg:description
     chat_datatype:image
     apptoken:d10289185f466003c450a15d300e50d5
     chat_data: UPLOAD FILE*/
    
    dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSLog(@"First Log");
        
        NSString * MediaType;
        MediaType = mediaType;
        
        NSMutableDictionary * dictParam =[[NSMutableDictionary alloc] init];
        [dictParam setObject:[[UserModel sharedSingleton] getUserID] forKey:@"user_id"];
        [dictParam setObject:AppToken  forKey:@"apptoken"];
        [dictParam setObject:self.otherPersonUserID  forKey:@"friend_id"];
        [dictParam setObject:MediaType  forKey:@"chat_datatype"];
        [dictParam setObject:@""  forKey:@"chat_msg"];
        
        NSMutableArray * arrMedia = [[NSMutableArray alloc] init];
        //arrMedia = arrMediaData;
        [arrMedia addObject:arrMediaData];
        NSMutableArray * arrMediaParameter = [[NSMutableArray alloc] init];
        [arrMediaParameter addObject:@"chat_data"];
        
        
        
        [Networking dataTaskForChatwithURL:MediaChatSend Param:dictParam MediaArray:arrMedia MediaPrameterArray:arrMediaParameter MediaType:MediaType compilation:^(id response, NSError *error) {
            if (!error)
            {
                
                NSLog(@"ChatWithOther Data : %@",response);
                
                if ([[response valueForKey:@"status"]integerValue] == 1)
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // Update the UI
                        
                        NSLog(@"Update the UI");
                        
                        /*
                         "chat_datatype" = image;
                         "chat_file" = "377Image.jpg";
                         "chat_msg" = "";
                         code = 1;
                         message = "message sent";
                         "message_type" = chat;
                         status = 1;
                         */
                        objChatModel = [ChatModel new];
                        
                        objChatModel.chat_id            = [response valueForKey:@"chat_id"];
                        objChatModel.chat_data          = [response valueForKey:@"chat_file"/*@"chat_data"*/] ;
                        objChatModel.chat_datatype      = [response valueForKey:@"chat_datatype"];
                        objChatModel.chat_date          = [response valueForKey:@"chat_date"] ;
                        objChatModel.chat_msg           = [response valueForKey:@"chat_msg"] ;
                        objChatModel.chat_reciver       = @"";//[response valueForKey:@"chat_reciver"] ;
                        objChatModel.chat_sender        = [[UserModel sharedSingleton] getUserID];//[response valueForKey:@"chat_sender"];
                        objChatModel.name               = @"";//[response valueForKey:@"name"];
                        objChatModel.profile_picture    = @"";//[response valueForKey:@"profile_picture"];
                        
                        [arrChatModelData addObject:objChatModel];
                        
                        self.tblchat.delegate   = self;
                        self.tblchat.dataSource = self;
                        [self.tblchat reloadData];
                        NSIndexPath* ip = [NSIndexPath indexPathForRow:arrChatModelData.count-1 inSection:0];
                        [self.tblchat scrollToRowAtIndexPath:ip atScrollPosition:UITableViewScrollPositionBottom animated:NO];
                    });
                }
                else if ([[response valueForKey:@"status"]integerValue] == 0)
                {
                    [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Error];
                }
            }
            else
            {
                
            }
            
        }];
        
        
        
        
    });
    
    NSLog(@"Second Log");
    
    
    
}

-(IBAction)AddAttactmentAction:(UIButton *)sender
{
    
    
    UIAlertController * view=   [UIAlertController
                                 alertControllerWithTitle:nil
                                 message:@"Please Select Photo / Video from"
                                 preferredStyle:UIAlertControllerStyleActionSheet];
    
    //view.view.tintColor = UIColorFromRGB(Color_NavigationBar);
    //view.view.subviews.firstObject.backgroundColor = UIColorFromRGB(Color_NavigationBar);
    UIAlertAction* Gallery = [UIAlertAction
                              actionWithTitle:@"Camera"
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                  NSString *model = [[UIDevice currentDevice] model];
                                  if ([model isEqualToString:@"iPhone Simulator"] || [model isEqualToString:@"iPad Simulator"])
                                  {
                                      UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!!!" message:@"No Camera found in Simulator!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                      [alert show];
                                  }
                                  else
                                  {
                                      UIImagePickerController * ImgPicker = [[UIImagePickerController alloc] init];
                                      ImgPicker.delegate = self;
                                      ImgPicker.allowsEditing = YES;
                                      ImgPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                                      ImgPicker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
                                      
                                      // Only For Video  MediaType
                                      
                                      //ImgPicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
                                      
                                      // Only For All (Video and Photo)  MediaType
                                      ImgPicker.mediaTypes =
                                      [UIImagePickerController availableMediaTypesForSourceType:
                                       UIImagePickerControllerSourceTypeCamera];
                                      [self presentViewController:ImgPicker animated:YES completion:nil];
                                      ImgPicker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
                                      ImgPicker.videoQuality = UIImagePickerControllerQualityTypeMedium;
                                      
                                      
                                  }
                                  
                                  
                                  
                                  //Do some thing here
                                  [view dismissViewControllerAnimated:YES completion:nil];
                                  
                              }];
    
    UIAlertAction* Camera = [UIAlertAction
                             actionWithTitle:@"Gallery"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
                                     
                                     UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                                     picker.delegate = self;
                                     picker.allowsEditing = YES;
                                     //picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                     picker.sourceType=UIImagePickerControllerSourceTypeSavedPhotosAlbum;
                                     picker.mediaTypes =
                                     [UIImagePickerController availableMediaTypesForSourceType:
                                      UIImagePickerControllerSourceTypeSavedPhotosAlbum];
                                     picker.videoQuality = UIImagePickerControllerQualityTypeMedium;
                                     
                                     //picker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
                                     
                                     [self presentViewController:picker animated:YES completion:NULL];
                                 }
                                 //Do some thing here
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    
    
    
    [view addAction:Gallery];
    [view addAction:Camera];
    [view addAction:cancel];
    [self presentViewController:view animated:YES completion:nil];
    
}
-(void)imagePickerController:(UIImagePickerController *)picker
       didFinishPickingImage:(UIImage *)img
                 editingInfo:(NSDictionary *)editingInfo

{
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [[picker parentViewController] dismissViewControllerAnimated:YES completion:nil];
    
    NSString * MediaType;
    //IMAGE = [info valueForKey:@"UIImagePickerControllerOriginalImage"];
    
    if([info[UIImagePickerControllerMediaType] isEqualToString:(__bridge NSString *)(kUTTypeImage)])
    {
        NSLog(@"Image");
        UIImage * IMAGE=[info objectForKey:UIImagePickerControllerEditedImage];
        MediaType = @"image";
        [self MediaChatSendApiCallWithMediaType:MediaType MediaData:IMAGE];
    }
    else
    {
        // IF VIDEO CAPTURED BY CAMERA (Not from Gallery) AND SEND TO SERVER AND YOU GOT ANY SERVER ERROR THAN PLZ
        // GOTO CAPABILITES SELECT BACKGROUND MODES TO SOLVE THIS SERVER ERROR.
        
        NSLog(@"Video");
        MediaType = @"video";
        NSURL * videoURL = info[UIImagePickerControllerMediaURL];
        [picker dismissViewControllerAnimated:YES completion:NULL];
        [self MediaChatSendApiCallWithMediaType:MediaType MediaData:videoURL];
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}

-(NSString *)validation
{
    NSString * alertMsg;
    
    if ([self.tvSendMessage.text isEqualToString:@"Type a message"] || self.tvSendMessage.text.length == 0)
    {
        alertMsg = @"";
    }
    
    return  alertMsg;
}

- (IBAction)btnSendAction:(id)sender
{
    if ([self.tvSendMessage.text isEqualToString:@"Type a message"] || self.tvSendMessage.text.length == 0)
    {
        return;
    }
    else
    {
        [self sendTextAndEmoji];
    }
}

-(void)sendTextAndEmoji
{
    dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        
        /*NSString * MediaType;
         MediaType = mediaType;*/
        
        //{"apptoken":"d10289185f466003c450a15d300e50d5","user_id":"95","friend_id":"130","message":"Hi"}
        
        NSMutableDictionary * dictParam =[[NSMutableDictionary alloc] init];
        [dictParam setObject:[[UserModel sharedSingleton] getUserID] forKey:@"user_id"];
        [dictParam setObject:AppToken  forKey:@"apptoken"];
        [dictParam setObject:self.otherPersonUserID  forKey:@"friend_id"];
        //[dictParam setObject:MediaType  forKey:@"chat_datatype"];
        [dictParam setObject:self.tvSendMessage.text  forKey:@"message"];
        
        
        [Networking rawJsondataTaskwithURL:TextChatSend Param:dictParam ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error) {
            if (!error)
            {
                
                NSLog(@"ChatWithOther Data : %@",response);
                
                if ([[response valueForKey:@"status"]integerValue] == 1)
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // Update the UI
                        
                        NSLog(@"Update the UI");
                        
                        /*
                         "chat_datatype" = text;
                         "chat_date" = "30-05-2017 07:49:18am";
                         "chat_file" = "";
                         "chat_id" = 221;
                         "chat_msg" = "Heyyy yaaa";
                         code = 1;
                         message = "message sent";
                         "message_type" = chat;
                         status = 1;
                         */
                        objChatModel = [ChatModel new];
                        
                        objChatModel.chat_id            = [response valueForKey:@"chat_id"];
                        objChatModel.chat_data          = [response valueForKey:@"chat_file"/*@"chat_data"*/] ;
                        objChatModel.chat_datatype      = [response valueForKey:@"chat_datatype"];
                        objChatModel.chat_date          = [response valueForKey:@"chat_date"] ;
                        objChatModel.chat_msg           = [response valueForKey:@"chat_msg"] ;
                        objChatModel.chat_reciver       = @"";//[response valueForKey:@"chat_reciver"] ;
                        objChatModel.chat_sender        = [[UserModel sharedSingleton] getUserID];//[response valueForKey:@"chat_sender"];
                        objChatModel.name               = @"";//[response valueForKey:@"name"];
                        objChatModel.profile_picture    = @"";//[response valueForKey:@"profile_picture"];
                        
                        [arrChatModelData addObject:objChatModel];
                        
                        self.tblchat.delegate   = self;
                        self.tblchat.dataSource = self;
                        [self.tblchat reloadData];
                        NSIndexPath* ip = [NSIndexPath indexPathForRow:arrChatModelData.count-1 inSection:0];
                        [self.tblchat scrollToRowAtIndexPath:ip atScrollPosition:UITableViewScrollPositionBottom animated:NO];
                        self.tvSendMessage.text = nil;
                    });
                }
                else if ([[response valueForKey:@"status"]integerValue] == 0)
                {
                    [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Error];
                }
            }
            else
            {
                
            }
            
        }];
        
        
        
        
    });
    
    NSLog(@"Second Log");
    
}

-(void)viewDidUnload {
    [[NSNotificationCenter defaultCenter]
     removeObserver:self
     name:UIApplicationDidReceiveRemoteNotification
     object:nil];
}

-(void)didReceiveRemoteNotification:(NSNotification *)notification {
    // see http://stackoverflow.com/a/2777460/305149
    if (self.isViewLoaded && self.view.window) {
        // handle the notification
        
        NSLog(@"USER Info inside: %@",notification.userInfo);
        NSMutableDictionary * dictDATA = [[NSMutableDictionary alloc]init];
        dictDATA = [[[notification.userInfo valueForKey:@"aps"] valueForKey:@"alert"] valueForKey:@"data"];
        
        objChatModel = [ChatModel new];
        
        objChatModel.chat_id            = @"";//[userInfo valueForKey:@"chat_id"];
        objChatModel.chat_data          = [dictDATA valueForKey:@"file"/*@"chat_data"*/] ;
        objChatModel.chat_datatype      = [dictDATA valueForKey:@"messagetype"];
        objChatModel.chat_date          = @"";//[userInfo valueForKey:@"chat_date"] ;
        objChatModel.chat_msg           = [dictDATA valueForKey:@"messagetext"] ;
        objChatModel.chat_reciver       = [[UserModel sharedSingleton] getUserID];//[response valueForKey:@"chat_reciver"] ;
        objChatModel.chat_sender        = [dictDATA valueForKey:@"userid"];
        objChatModel.name               = @"";//[response valueForKey:@"name"];
        objChatModel.profile_picture    = @"";//[response valueForKey:@"profile_picture"];
        
        [arrChatModelData addObject:objChatModel];
        [self.tblchat reloadData];
        NSIndexPath* ip = [NSIndexPath indexPathForRow:arrChatModelData.count-1 inSection:0];
        [self.tblchat scrollToRowAtIndexPath:ip atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    }
    
    //NSLog(@"USER Info: %@",userInfo);
}

#pragma Mark: textview delegate method

-(BOOL)textView:(UITextView *)_textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [_textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    if (textView == self.tvSendMessage && [self.tvSendMessage.text isEqualToString:@"Type a message"])
    {
        self.tvSendMessage.text = nil;
        //_tvMessageCaption.textColor = [UIColor colorWithRed:240/255.0f green:99/255.0f blue:91/255.0f alpha:1.0];
    }
    
    NSLog(@"Did begin editing");
}

-(void)textViewDidChange:(UITextView *)textView
{
    NSLog(@"Did Change");
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    NSLog(@"Did End editing");
    
    if (textView == self.tvSendMessage && [self.tvSendMessage.text isEqualToString:@""]) {
        self.tvSendMessage.text = @"Type a message";
        //_tvDestimationAddress.textColor = [UIColor colorWithRed:199/255.0f green:199/255.0f blue:205/255.0f alpha:1.0];
    }
    
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    return YES;
}


- (id<SINClient>)client {
    return [(AppDelegate *)[[UIApplication sharedApplication] delegate] client];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.client.callClient.delegate = self;
}


- (IBAction)btnAudioCallAction:(id)sender
{
    if ([self.client isStarted]) {
        
        //{"apptoken":"d10289185f466003c450a15d300e50d5","user_id":"95","friend_id":"130","call_request":"voice"}
        
        NSMutableDictionary * dictParam =[[NSMutableDictionary alloc] init];
        [dictParam setObject:[[UserModel sharedSingleton] getUserID] forKey:@"user_id"];
        [dictParam setObject:AppToken  forKey:@"apptoken"];
        [dictParam setObject:self.otherPersonUserID  forKey:@"friend_id"];
        [dictParam setObject:@"voice"  forKey:@"call_request"];
        
        
        [Networking rawJsondataTaskwithURL:CallRequest Param:dictParam ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error) {
            if (!error)
            {
                
                NSLog(@"ChatWithOther Data : %@",response);
                
                if ([[response valueForKey:@"status"]integerValue] == 1)
                {
                    NSMutableDictionary * dictCallDetail = [response valueForKey:@"data"];
                    [[NSUserDefaults standardUserDefaults]setObject:dictCallDetail forKey:@"PushCallDetail"];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // Update the UI
                        
                        if ([self.client isStarted]) {
                            id<SINCall> call = [self.client.callClient callUserWithId:self.otherPersonUserID];
                            [self performSegueWithIdentifier:@"callView" sender:call];
                        }
                        
                    });
                }
                else if ([[response valueForKey:@"status"]integerValue] == 0)
                {
                    [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Error];
                }
            }
            else
            {
                
            }
            
        }];
    }
    
    
}
- (IBAction)btnVideoCallAction:(id)sender
{
    if ([self.client isStarted]) {
        NSMutableDictionary * dictParam =[[NSMutableDictionary alloc] init];
        [dictParam setObject:[[UserModel sharedSingleton] getUserID] forKey:@"user_id"];
        [dictParam setObject:AppToken  forKey:@"apptoken"];
        [dictParam setObject:self.otherPersonUserID  forKey:@"friend_id"];
        [dictParam setObject:@"voice"  forKey:@"call_request"];
        
        
        [Networking rawJsondataTaskwithURL:CallRequest Param:dictParam ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error) {
            if (!error)
            {
                
                NSLog(@"ChatWithOther Data : %@",response);
                
                if ([[response valueForKey:@"status"]integerValue] == 1)
                {
                    NSMutableDictionary * dictCallDetail = [response valueForKey:@"data"];
                    [[NSUserDefaults standardUserDefaults]setObject:dictCallDetail forKey:@"PushCallDetail"];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // Update the UI
                        
                        if ([self.client isStarted]) {
                            id<SINCall> call = [self.client.callClient callUserVideoWithId:self.otherPersonUserID];
                            [self performSegueWithIdentifier:@"VideocallView" sender:call];
                        }
                        
                    });
                }
                else if ([[response valueForKey:@"status"]integerValue] == 0)
                {
                    [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Error];
                }
            }
            else
            {
                
            }
            
        }];
    }
    
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"VideocallView"])
    {
        VideoCallViewController *VideoCallViewController = [segue destinationViewController];
        VideoCallViewController.call = sender;
    }
    else if ([segue.identifier isEqualToString:@"callView"])
    {
        CallViewController *callViewController = [segue destinationViewController];
        callViewController.call = sender;
    }
    
    
}

#pragma mark - SINCallClientDelegate

- (void)client:(id<SINCallClient>)client didReceiveIncomingCall:(id<SINCall>)call {
    
    NSLog(@"CALL DETAIL:%@",call.details);
    NSLog(@"client DETAIL:%@",client);
    
    
    if ([call.details isVideoOffered])
    {
        [self performSegueWithIdentifier:@"VideocallView" sender:call];
    }
    else
    {
        [self performSegueWithIdentifier:@"callView" sender:call];
    }
}

- (SINLocalNotification *)client:(id<SINClient>)client localNotificationForIncomingCall:(id<SINCall>)call {
    SINLocalNotification *notification = [[SINLocalNotification alloc] init];
    notification.alertAction = @"Answer";
    notification.alertBody = [NSString stringWithFormat:@"Incoming call from %@", [call remoteUserId]];
    return notification;
}

@end



