//
//  LocationManagerModel.h
//  EXTEND
//
//  Created by Apple on 24/03/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface LocationManagerModel : NSObject <CLLocationManagerDelegate>

@property (nonatomic, strong) CLLocationManager* locationManager;

+ (LocationManagerModel *)sharedSingleton;

@end
