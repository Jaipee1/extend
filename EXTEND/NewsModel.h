//
//  NewsModel.h
//  EXTEND
//
//  Created by Apple on 28/03/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewsModel : NSObject

@property (nonatomic, retain) NSString * PostDate;
@property (nonatomic, retain) NSString * Category_ID;
@property (nonatomic, retain) NSString * Category_Name;
@property (nonatomic, retain) NSString * CommentCount;
@property (nonatomic, retain) NSString * NewsContent;
@property (nonatomic, retain) NSString * NewsImage;
@property (nonatomic, retain) NSString * News_ID;
@property (nonatomic, retain) NSString * LikeCount;
@property (nonatomic, retain) NSString * NewsTitle;
@property (nonatomic, assign) int liked;

@end


/*"add_Date" = "2017-02-22 07:41:28";
 "categories_Id" = 8;
 categoryName = "Health & Fitness";
 "comment_count" = 4;
 content = "Bad breath or halitosis is usually due to the breakdown of proteins by bacteria somewhere in the mouth. However, there are several other possible causes in the airways, oesophagus and stomach that can also lead to bad breath. If you do not brush and floss";
 "feature_Img" = "77611487749288.jpeg";
 id = 4;
 "like_count" = 1;
 "like_show" = 0;
 title = "Tips To Prevent Bad Breath";*/
