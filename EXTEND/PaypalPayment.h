//
//  PaypalPayment.h
//  EXTEND
//
//  Created by Apple on 10/05/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PayPalMobile.h"

@protocol myProtocol <NSObject>

- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController didCompletePayment:(PayPalPayment *)completedPayment;
- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController ;

@end

@interface PaypalPayment : NSObject<PayPalPaymentDelegate, PayPalFuturePaymentDelegate, PayPalProfileSharingDelegate,UIPopoverControllerDelegate>
{
    //UIViewController *aViewcontroller;
    
    void (^paymentCancelCloser)(void) ;/*= ^{
        
        NSLog(@"Integer is: %i", anInteger);   // anInteger outside variables
        
    };*/
    
    PayPalPaymentViewController *paymentViewController;
}

@property (nonatomic, weak) id <myProtocol> delegate;

#pragma -------Paypal Stuff-------
@property(nonatomic,strong) UIViewController * aViewcontroller;
@property(nonatomic, strong, readwrite) NSString *environment;
@property(nonatomic, assign, readwrite) BOOL acceptCreditCards;
@property(nonatomic, strong, readwrite) NSString *resultText;
@property(nonatomic, strong, readwrite) PayPalConfiguration *payPalConfig;




-(id)initWithViewController:(UIViewController *)viewcontroller andWithAmmount:(NSString *)ammount;

@end
