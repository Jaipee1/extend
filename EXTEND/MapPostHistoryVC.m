//
//  MapPostHistoryVC.m
//  EXTEND
//
//  Created by Apple on 08/05/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "MapPostHistoryVC.h"
#import "UserModel.h"
#import "Networking.h"
#import "MapPostModel.h"
#import "MapPostHistoryCell.h"

#import <SDWebImage/UIImageView+WebCache.h>

@interface MapPostHistoryVC ()
{
    MapPostModel * objMapPostModel;
}
@property (retain, nonatomic) NSMutableArray * arrDataMapPostModel;
@property (weak, nonatomic) IBOutlet UITableView *tblPost;

@end

@implementation MapPostHistoryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self getAllPosts];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self navigationSetup];
}

-(void)navigationSetup
{
    self.navigationController.navigationBar.hidden = NO;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(40, 0,SCREEN_MAX_LENGTH-40, 40)];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"Raleway" size:16];
    label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    label.textAlignment = NSTextAlignmentLeft;
    label.textColor =[UIColor whiteColor];
    label.text=@"History";
    self.navigationItem.titleView = label;
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back_icon"]style:UIBarButtonItemStylePlain target:self action:@selector(Back)];
    self.navigationItem.leftBarButtonItem = revealButtonItem;
}

-(void)Back
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getAllPosts
{
    NSMutableDictionary * dictParam = [[NSMutableDictionary alloc] init];
    
    [dictParam setObject:[[UserModel sharedSingleton] getUserID] forKey:@"user_id"];
    //[dictParam setObject:@"95" forKey:@"user_id"];
    [dictParam setObject:[NSNumber numberWithInteger:0] forKey:@"limit"];
    [dictParam setObject:AppToken forKey:@"apptoken"];
    
    NSLog(@"Update Dict Data :%@",dictParam);
    
    [Networking rawJsondataTaskwithURL:HistoryMapPost Param:dictParam ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error) {
        if (!error)
        {
            NSLog(@"response History Data :%@",response);
            
            if ([[response valueForKey:@"status"]integerValue] == 1)
            {
                NSMutableArray * arrHistoryData = [[NSMutableArray alloc] init];
                arrHistoryData = [response valueForKey:@"message"];
                
                self.arrDataMapPostModel = [NSMutableArray new];
                
                for (int i=0 ; i<arrHistoryData.count; i++)
                {
                    objMapPostModel                     = [MapPostModel new];
                    objMapPostModel.postID              = [[arrHistoryData valueForKey:@"id"] objectAtIndex:i];
                    objMapPostModel.descriptionPost     = [[arrHistoryData valueForKey:@"description"] objectAtIndex:i];
                    objMapPostModel.image               = [[arrHistoryData valueForKey:@"image"] objectAtIndex:i];
                    objMapPostModel.post_status         = [[arrHistoryData valueForKey:@"post_status"] objectAtIndex:i];
                    objMapPostModel.title               = [[arrHistoryData valueForKey:@"title"] objectAtIndex:i];
                    objMapPostModel.location_latitude   = [[arrHistoryData valueForKey:@"location_latitude"] objectAtIndex:i];
                    objMapPostModel.location_longitude  = [[arrHistoryData valueForKey:@"location_longitude"] objectAtIndex:i];
                    
                    [self.arrDataMapPostModel addObject:objMapPostModel];
                    
                }
                
                NSLog(@"arrDataMapPostModel: %@",self.arrDataMapPostModel);
                [self.tblPost reloadData];
            }
            else
            {
                
            }
        }
    }];
}

#pragma Mark: Tableview Delegate & datasource Method

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.arrDataMapPostModel.count;    //count number of row from counting array hear cataGorry is An Array
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    MapPostHistoryCell *cell = (MapPostHistoryCell *)  [tableView dequeueReusableCellWithIdentifier:@"MapPostHistoryCell" forIndexPath:indexPath];
    objMapPostModel = [self.arrDataMapPostModel objectAtIndex:indexPath.row];
    cell.lblPostTitle.text      = objMapPostModel.title;
    cell.lblStatus.text         = objMapPostModel.post_status;
    cell.lblDescription.text    = objMapPostModel.descriptionPost;
    [cell.imgViewPost sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",MapImagePost,objMapPostModel.image]] placeholderImage:[UIImage imageNamed:@"upload_icon"]];
    
    cell.btnActiveDeactive.tag = 1000 + indexPath.row;
    [cell.btnActiveDeactive addTarget:self action:@selector(btnActiveDeactiveAction:) forControlEvents:UIControlEventTouchUpInside];
    
    if ([objMapPostModel.post_status isEqualToString:@"Active"])
    {
        cell.lblStatus.textColor = [UIColor greenColor];
        [cell.btnActiveDeactive setTitle:@"Deactive" forState:UIControlStateNormal];
        cell.btnActiveDeactive.backgroundColor = [UIColor redColor];
    }
    else
    {
        cell.lblStatus.textColor = [UIColor redColor];
        [cell.btnActiveDeactive setTitle:@"Active" forState:UIControlStateNormal];
        cell.btnActiveDeactive.backgroundColor = [UIColor greenColor];
    }
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"MapPostHistoryCell";
    MapPostHistoryCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    objMapPostModel = [self.arrDataMapPostModel objectAtIndex:indexPath.row];
    return 170 ;//+ [self heightForLabelwithText:objpost.PostName withFont:[UIFont systemFontOfSize:15.0] withWidth:cell.lblMsgCaption.frame.size.width] ;
    
}

-(CGFloat )heightForLabelwithText:(NSString *)text withFont:(UIFont *)font withWidth:(CGFloat)width
{
    CGRect rect = CGRectMake(0, 0, width, CGFLOAT_MAX);
    
    UILabel * lbl = [[UILabel alloc] initWithFrame:rect];
    lbl.numberOfLines = 0;
    lbl.lineBreakMode = NSLineBreakByCharWrapping;
    lbl.font = font;
    lbl.text = text;
    [lbl sizeToFit];
    
    return lbl.frame.size.height;
}
/*-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
 {
 _viewHeader.frame = CGRectMake(0, 0, self.tblPost.frame.size.width, _viewHeader.frame.size.height);
 _viewHeader.backgroundColor = UIColorFromRGB(Color_NavigationBar);
 UIView *view = [[UIView alloc] init ];
 tableView.tableHeaderView = _viewHeader;
 return view;
 }*/

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.001/*self.viewheader.frame.size.height*/;
}

-(IBAction)btnActiveDeactiveAction:(UIButton *)sender
{
    //{"apptoken":"d10289185f466003c450a15d300e50d5","user_id":"54","post_id":"46"}
    
    objMapPostModel = [self.arrDataMapPostModel objectAtIndex:sender.tag - 1000];
    
    NSMutableDictionary * dictParam = [[NSMutableDictionary alloc] init];
    
    [dictParam setObject:[[UserModel sharedSingleton] getUserID] forKey:@"user_id"];
    [dictParam setObject:objMapPostModel.postID forKey:@"post_id"];
    [dictParam setObject:AppToken forKey:@"apptoken"];
    
    NSLog(@"Update Dict Data :%@",dictParam);
    NSString * strURL;
    if ([sender.titleLabel.text isEqualToString:@"Active"])
    {
        strURL = ReactiveMapPost;
    }
    else if ([sender.titleLabel.text isEqualToString:@"Deactive"])
    {
        strURL = DeactiveMapPost;
    }
    
    [Networking rawJsondataTaskwithURL:strURL Param:dictParam ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error) {
        if (!error)
        {
            NSLog(@"response History Data :%@",response);
            
            if ([[response valueForKey:@"status"]integerValue] == 1)
            {
                [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Success];
                
                /*if ([sender.titleLabel.text isEqualToString:@"Active"])
                {
                    [sender setTitle:@"Deactive" forState:UIControlStateNormal];
                    sender.backgroundColor = [UIColor redColor];
                }
                else if ([sender.titleLabel.text isEqualToString:@"Deactive"])
                {
                    [sender setTitle:@"Active" forState:UIControlStateNormal];
                    sender.backgroundColor = [UIColor greenColor];
                }*/
                
                [self getAllPosts];
                
            }
            else
            {
                [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Error];
            }
        }
    }];
    
}


#pragma Mark-  Commaon Alertview

-(void)showAlertMessage:(NSString*)message Title:(NSString *)title
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               {
                               }];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

@end
