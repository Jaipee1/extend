//
//  NewsDetailsVC.m
//  EXTEND
//
//  Created by Apple on 05/04/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "NewsDetailsVC.h"
#import "NewsCommentList_VC.h"

#import "NewsDetailCell.h"
#import "Networking.h"
#import "UserModel.h"

#import <SDWebImage/UIImageView+WebCache.h>

#define  kTableHeaderHeight  250.0//300.0f
#define  kTableHeaderCutAway  0.0//60.0f
@interface NewsDetailsVC ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate,UIGestureRecognizerDelegate>
{
    CAShapeLayer * headerMaskLayer;
    NewsModel * objNewsModel;
}
@property (strong, nonatomic) IBOutlet UITableView *tblStratchy;
@property (strong, nonatomic) IBOutlet UIView *viewHeader;
@property (strong, nonatomic) IBOutlet UIImageView *ImgViewHeader;
@property (weak,nonatomic) IBOutlet UILabel * lblNewsTitle;
@property (weak,nonatomic) IBOutlet UILabel * lblDatePosted;

@end

@implementation NewsDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //_tblStratchy.rowHeight = UITableViewAutomaticDimension;
    
    
    
    _viewHeader = _tblStratchy.tableHeaderView;
    _tblStratchy.tableHeaderView = nil;
    [_tblStratchy addSubview:_viewHeader];
    
    CGFloat effectiveHeight = kTableHeaderHeight-kTableHeaderCutAway/5;
    _tblStratchy.contentInset = UIEdgeInsetsMake(effectiveHeight, 0, 0, 0);
    _tblStratchy.contentOffset = CGPointMake(0, -effectiveHeight);
    
    headerMaskLayer = [[CAShapeLayer alloc] init];
    headerMaskLayer.fillColor = [UIColor blackColor].CGColor;
    
    _viewHeader.layer.mask = headerMaskLayer;
    [self updateHeaderView];
    
    self.tblStratchy.estimatedRowHeight = 134.0;
    self.tblStratchy.rowHeight = UITableViewAutomaticDimension;
    
    [self setUp];
    
}

-(void)setUp
{
    
    [self ShowDataOnIndex:self.index];
    [self SetGesture];
    
}
-(void)SetGesture
{
    UISwipeGestureRecognizer *gestureLeft = [[UISwipeGestureRecognizer alloc]
                                             initWithTarget:self action:@selector(handleSwipeFrom:)];
    gestureLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    gestureLeft.delegate = self;
    [self.tblStratchy addGestureRecognizer:gestureLeft];
    
    UISwipeGestureRecognizer *gestureRight = [[UISwipeGestureRecognizer alloc]
                                              initWithTarget:self action:@selector(handleSwipeFrom:)];
    gestureRight.direction = UISwipeGestureRecognizerDirectionRight;
    gestureRight.delegate = self;
    [self.tblStratchy addGestureRecognizer:gestureRight];
}

- (void)handleSwipeFrom:(UISwipeGestureRecognizer *)recognizer {
    if (recognizer.direction == UISwipeGestureRecognizerDirectionLeft)
    {
        //[self moveLeftColumnButtonPressed:nil];
        NSLog(@"Left");
        self.index ++;
        if (self.index == self.arrGetNews.count)
        {
            self.index --;
            return;
        }
        [self ShowDataOnIndex:self.index];
    }
    else if (recognizer.direction == UISwipeGestureRecognizerDirectionRight)
    {
        //[self moveRightColumnButtonPressed:nil];
        NSLog(@"Right");
        self.index --;
        if (self.index < 0)
        {
            self.index ++;
            return;
        }
        [self ShowDataOnIndex:self.index];
    }
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return YES;
}

-(void)ShowDataOnIndex:(NSInteger)index
{
    objNewsModel = [self.arrGetNews objectAtIndex:index];
    
    NSLog(@"News:%@",objNewsModel.NewsTitle);
    self.lblNewsTitle.text = objNewsModel.NewsTitle;
    self.lblDatePosted.text = objNewsModel.PostDate;
    
    [self.ImgViewHeader sd_setImageWithURL:[NSURL URLWithString:objNewsModel.NewsImage] placeholderImage:[UIImage imageNamed:@"upload_icon"]];
    [self.tblStratchy reloadData];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updateHeaderView];
    
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barTintColor = UIColorFromRGB(Color_NavigationBar);
    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(40, 0,SCREEN_MAX_LENGTH-40, 40)];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"Raleway" size:16];
    label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    label.textAlignment = NSTextAlignmentLeft;
    label.textColor =[UIColor whiteColor];
    label.text=@"News Detail";
    self.navigationItem.titleView = label;
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back_icon"]style:UIBarButtonItemStylePlain target:self action:@selector(Back)];
    self.navigationItem.leftBarButtonItem = revealButtonItem;
}

-(void)Back
{
    [self.navigationController popViewControllerAnimated:YES];
    
}


-(void)updateHeaderView
{
    
    
    CGFloat effectiveHeight = kTableHeaderHeight - kTableHeaderCutAway/5;
    CGRect headerRect = CGRectMake(0, -effectiveHeight, _tblStratchy.frame/*Bound*/.size.width, kTableHeaderHeight);
    
    
    
    if (_tblStratchy.contentOffset.y < -effectiveHeight) {
        headerRect.origin.y = _tblStratchy.contentOffset.y;
        headerRect.size.height = -_tblStratchy.contentOffset.y + kTableHeaderCutAway/5;
    }
    _viewHeader.frame = headerRect;
    
    UIBezierPath *path = [[UIBezierPath alloc]init];
    [path moveToPoint:CGPointMake(0, 0)];
    [path addLineToPoint:CGPointMake(headerRect.size.width, 0)];
    [path addLineToPoint:CGPointMake(headerRect.size.width, headerRect.size.height)];
    [path addLineToPoint:CGPointMake(0, headerRect.size.height-kTableHeaderCutAway)];
    headerMaskLayer.path = path.CGPath;
    
    CAGradientLayer *gradientMask = [CAGradientLayer layer];
    gradientMask.frame = self.ImgViewHeader.bounds;
    gradientMask.colors = @[(id)[UIColor clearColor].CGColor,
                            (id)[UIColor whiteColor].CGColor,
                            (id)[UIColor blackColor].CGColor,
                            (id)[UIColor clearColor].CGColor];
    gradientMask.locations = @[@0.0, @0.10, @0.70, @1.0];
    self.ImgViewHeader.layer.mask = gradientMask;

}



/*-(BOOL)prefersStatusBarHidden
{
    return true;
}*/
-(void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
        //self;
        [self updateHeaderView];
        [self.tblStratchy reloadData];
        
    } completion:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
        
    }];
    
    //    coordinator.animateAlongsideTransition({ (context) -> Void in
    //        [self]
    //        self.updateHeaderView()
    //        self.tableView.reloadData()
    //    }, completion: { (context) -> Void in
    //    })
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;    //count number of row from counting array hear cataGorry is An Array
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"MyIdentifier";
    
    NewsDetailCell *cell = (NewsDetailCell *) [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[NewsDetailCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:MyIdentifier] ;
    }
    objNewsModel = [self.arrGetNews objectAtIndex:self.index];
    cell.tvNews.text = objNewsModel.NewsContent;
    cell.lblLikeCount.text = objNewsModel.LikeCount;
    cell.lblCommentCount.text = objNewsModel.CommentCount;
    
    if (objNewsModel.liked == 0)
    {
        [cell.btnLikeDislike setImage:[UIImage imageNamed:@"like_icon-1"] forState:UIControlStateNormal];
    }
    else if (objNewsModel.liked == 1)
    {
        [cell.btnLikeDislike setImage:[UIImage imageNamed:@"like_hover_news"] forState:UIControlStateNormal];
    }
    
    [cell.btnLikeDislike addTarget:self action:@selector(LikeDislikeAction:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.btnWritComment.layer.cornerRadius      = 5;
    cell.btnWritComment.layer.borderWidth       = 1.5f;
    cell.btnWritComment.layer.borderColor       = [UIColor grayColor].CGColor;
    cell.btnWritComment.clipsToBounds           = YES;
    [cell.btnWritComment addTarget:self action:@selector(btnWriteCommentAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.btnShare addTarget:self action:@selector(btnShareAction:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

- (void)LikeDislikeAction:(UIButton *)sender
{
    
    NSLog(@"\n\nTap\n\n");
    
    //objNewsModel = [self.arrGetNews objectAtIndex:self.index];
    
    NSMutableDictionary * dictParam =[[NSMutableDictionary alloc] init];
    [dictParam setObject:AppToken  forKey:@"apptoken"];
    [dictParam setObject:[[UserModel sharedSingleton] getUserID]  forKey:@"user_id"];
    [dictParam setObject:objNewsModel.News_ID  forKey:@"news_id"];
    
    if (objNewsModel.liked == 1)
    {
        [dictParam setObject:@"0"  forKey:@"like"];
    }
    else
    {
        [dictParam setObject:@"1"  forKey:@"like"];
    }
    
    NSLog(@"Dict Like:%@",dictParam);
    
    [Networking rawJsondataTaskwithURL:NewsLikeDislike Param:dictParam ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error)
     {
         if (!error) {
             
             NSLog(@"Like Unlike News Data :%@",response);
             if ([[response valueForKey:@"code"] intValue] == 1)
             {
                 
                 NSLog(@"Liked Value: %d",objNewsModel.liked);
                 if (objNewsModel.liked == 1)
                 {
                     objNewsModel.liked = 0;
                     objNewsModel.LikeCount = [NSString stringWithFormat:@"%d",[objNewsModel.LikeCount intValue] - 1];
                     [sender setImage:[UIImage imageNamed:@"like_icon-1"] forState:UIControlStateNormal];
                 }
                 else
                 {
                     objNewsModel.liked = 1;
                     objNewsModel.LikeCount = [NSString stringWithFormat:@"%d",[objNewsModel.LikeCount intValue] + 1];
                     [sender setImage:[UIImage imageNamed:@"like_hover_news"] forState:UIControlStateNormal];
                 }
                 NSLog(@"Update Liked Value: %d",objNewsModel.liked);
                 
                 [self.tblStratchy reloadData];
             }
             else if ([[response valueForKey:@"code"] intValue] == 0)
             {
                 
             }
         }
     }];

}

-(void)btnWriteCommentAction:(UIButton *)sender
{
    
    NewsCommentList_VC * objNewsCommentList_VC = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsCommentList_VC"];
    
    objNewsCommentList_VC.strNewsPostID = objNewsModel.News_ID;
    objNewsCommentList_VC.strNewsCategoryID = objNewsModel.Category_ID;
    objNewsCommentList_VC.strCommentCount = objNewsModel.CommentCount;
    objNewsCommentList_VC.strLikeCount = objNewsModel.LikeCount;
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:objNewsCommentList_VC];
    [self presentViewController:nav animated:YES completion:nil];
}

-(void)btnShareAction:(UIButton *)sender
{
    //objpost = [_arr objectAtIndex:sender.tag - 1000000];
    
    NSURL *SharingUrl;

        SharingUrl = [NSURL URLWithString:objNewsModel.NewsImage];
 
    
    NSArray *objectsToShare = @[SharingUrl];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:nil];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self updateHeaderView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.001/*self.viewheader.frame.size.height*/;
}


@end


/*
 
 // border radius
 [v.layer setCornerRadius:30.0f];
 
 // border
 [v.layer setBorderColor:[UIColor lightGrayColor].CGColor];
 [v.layer setBorderWidth:1.5f];
 
 // drop shadow
 [v.layer setShadowColor:[UIColor blackColor].CGColor];
 [v.layer setShadowOpacity:0.8];
 [v.layer setShadowRadius:3.0];
 [v.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
 
 cell.previewImage.layer.masksToBounds = YES;
 
 */
