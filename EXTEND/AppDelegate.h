//
//  AppDelegate.h
//  EXTEND
//
//  Created by Apple on 02/03/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Sinch/Sinch.h>
#import <SinchService/SinchService.h>
#import <PushKit/PushKit.h>

@class GTMOAuth2Authentication;

//NSString * const UIApplicationDidReceiveRemoteNotification;

//NSString * const UIApplicationDidReceivedRemoteNotification = @"UIApplicationDidReceiveRemoteNotification";

@interface AppDelegate : UIResponder <UIApplicationDelegate,SINClientDelegate,SINCallClientDelegate,SINServiceDelegate,PKPushRegistryDelegate,SINManagedPushDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) id<SINService> sinch;
@property (strong, nonatomic) id<SINClient> client;
@property (nonatomic, readwrite, strong) id<SINManagedPush> push;

+(NSMutableArray *)getContactAuthorizationFromUser;

+(NSMutableArray *)getContacts;


@end

