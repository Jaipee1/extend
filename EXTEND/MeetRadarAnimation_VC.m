//
//  MeetRadarAnimation_VC.m
//  EXTEND
//
//  Created by Apple on 23/03/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "MeetRadarAnimation_VC.h"
#import "ExtendSetting_VC.h"
#import "REFrostedViewController.h"
#import "Networking.h"
#import "UserModel.h"
#import "OtherUserModel.h"
#import "LocationManagerModel.h"
#import "PulsingHaloLayer.h"
#import "DraggableViewBackground.h"
#import "SubscriptionPlanView.h"
#import "SubscriptionTableViewCell.h"

#import <SDWebImage/UIImageView+WebCache.h>


#define kMaxRadius 200
#define kMaxDuration 10

#define kPayPalEnvironment PayPalEnvironmentSandbox


@interface MeetRadarAnimation_VC ()<UITableViewDataSource,UITableViewDelegate>
{
    NSString * strGender;
    NSMutableArray * arrOtherUserlist;
    OtherUserModel * objOtherUserModel;
    
    NSMutableArray * arrGetAllPlans;
    SubscriptionPlanView * subscriptionView;
    NSString * strPlanID;
    UIButton * btnBackground;
}
@property (retain, nonatomic) NSMutableArray * arrOtherUserModel;

@property (nonatomic, weak) PulsingHaloLayer *halo;
@property (nonatomic, weak) IBOutlet UIImageView *beaconView;

@property (strong, nonatomic) IBOutlet UIView *viewUpdateDobandGender;
@property (strong, nonatomic) IBOutlet UIButton *btnMale;
@property (strong, nonatomic) IBOutlet UIButton *btnFemale;
@property (strong, nonatomic) IBOutlet UISlider *sliderAge;
@property (strong, nonatomic) IBOutlet UILabel *lblAge;



@end

@implementation MeetRadarAnimation_VC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupPayPalConfiguration];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(didReceivePlans:)
     name:DidReceivePlans
     object:nil];
    
    strGender = @"male";
    self.viewUpdateDobandGender.hidden = YES;
    
    self.sliderAge.value = [self.lblAge.text intValue];
    
    self.beaconView.layer.cornerRadius = self.beaconView.frame.size.height/2;
    self.beaconView.layer.borderWidth = 2;
    self.beaconView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.beaconView.clipsToBounds = YES;
    [self.beaconView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",UserProfilePictureBaseURL,[[UserModel sharedSingleton] getUserProfileImageUrl]]] placeholderImage:[UIImage imageNamed:@"upload_icon"]];
    
    // Do any additional setup after loading the view.
    // basic setup
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    // Preconnect to PayPal early
    [self setPayPalEnvironment:self.environment];
    
    [self NavigationSetup];
    [self radarAnimation];
    [self searchUsers];
}

-(void)radarAnimation
{
    PulsingHaloLayer *layer = [PulsingHaloLayer layer];
    self.halo = layer;
    [self.beaconView.superview.layer insertSublayer:self.halo below:self.beaconView.layer];
    [self setupInitialValues];
    [self viewDidLayoutSubviews];
    [self.halo start];
}

-(void)NavigationSetup
{
    self.automaticallyAdjustsScrollViewInsets = false;
    self.navigationController.navigationBar.hidden = NO;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(40, 0,SCREEN_MAX_LENGTH-40, 40)];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"Raleway" size:16];
    label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    label.textAlignment = NSTextAlignmentLeft;
    label.textColor =[UIColor whiteColor];
    label.text=@"Meet";
    self.navigationItem.titleView = label;
    
    /*UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back_icon"]style:UIBarButtonItemStylePlain target:self action:@selector(Back)];
     self.navigationItem.leftBarButtonItem = revealButtonItem;*/
    
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"setting_icon"]style:UIBarButtonItemStylePlain target:self action:@selector(setting)];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu_icon"]style:UIBarButtonItemStylePlain target:self action:@selector(showmenu)];
    self.navigationItem.leftBarButtonItem = revealButtonItem;

}

-(void)searchUsers
{
    //{"apptoken":"d10289185f466003c450a15d300e50d5", "location_latitude":"22.7234483" , "location_longitude":"75.886481" ,"distance":"5" , "user_id":"95"}
    /* "apptoken":"d10289185f466003c450a15d300e50d5",
    "user_id":"95",
    "user_interest":"male",
    "min_age":"18",
    "max_age":"40",
    "location_latitude":22.7233654,
    "location_longitude":75.8865718,
    "distance":"100"*/

    
    CGFloat latitude = [[[[LocationManagerModel sharedSingleton] locationManager] location] coordinate].latitude;
    CGFloat longitude = [[[[LocationManagerModel sharedSingleton] locationManager] location] coordinate].longitude;
    
    NSMutableDictionary * dictParam = [[NSMutableDictionary alloc] init];
    
    [dictParam setObject:[[UserModel sharedSingleton] getUserID] forKey:@"user_id"];
    [dictParam setObject:AppToken forKey:@"apptoken"];
    [dictParam setObject:@"22.719568"/*[NSString stringWithFormat:@"%f",latitude]*/ forKey:@"location_latitude"];
    [dictParam setObject:@"75.857727"/*[NSString stringWithFormat:@"%f",longitude]*/ forKey:@"location_longitude"];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"MeetSettingKey"] != nil)
    {
        NSData *dictionaryData = [[NSUserDefaults standardUserDefaults] objectForKey:@"MeetSettingKey"];
        NSDictionary *dictionary = [NSKeyedUnarchiver unarchiveObjectWithData:dictionaryData];
        NSLog(@"MeetSetting dictionary :%@",dictionary);
        
        [dictParam setObject:[dictionary valueForKey:@"distance"] forKey:@"distance"];
        [dictParam setObject:[dictionary valueForKey:@"user_interest"] forKey:@"user_interest"];
        [dictParam setObject:[dictionary valueForKey:@"min_age"] forKey:@"min_age"];
        [dictParam setObject:[dictionary valueForKey:@"max_age"] forKey:@"max_age"];
    }
    else
    {
        [dictParam setObject:@"5" forKey:@"distance"];
        [dictParam setObject:@"both" forKey:@"user_interest"];
        [dictParam setObject:@"18" forKey:@"min_age"];
        [dictParam setObject:@"50" forKey:@"max_age"];
    }
    
    NSLog(@"Meet Search  Dict Data :%@",dictParam);
    
    [Networking rawJsondataTaskwithURL:MeetSearchUsers Param:dictParam ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error)
     {
         if (!error) {
             
             NSLog(@"Meet Search Users Data :%@",response);
             if ([[response valueForKey:@"code"] intValue] == 1)
             {

                 self.arrOtherUserModel = [NSMutableArray new];
                 arrOtherUserlist = [[NSMutableArray alloc] init];
                 arrOtherUserlist =   [response valueForKey:@"user_info"];
                 
                 for (int i=0; i < arrOtherUserlist.count; i++)
                 {
                     objOtherUserModel = [OtherUserModel new];
                     
                     objOtherUserModel.first_name           = [[arrOtherUserlist valueForKey:@"first_name"]objectAtIndex:i];
                     objOtherUserModel.last_name            = [[arrOtherUserlist valueForKey:@"last_name"] objectAtIndex:i];
                     objOtherUserModel.user_id              = [[arrOtherUserlist valueForKey:@"id"] objectAtIndex:i];
                     objOtherUserModel.image                = [[arrOtherUserlist valueForKey:@"image"] objectAtIndex:i];
                     objOtherUserModel.email_id             = [[arrOtherUserlist valueForKey:@"email_id"] objectAtIndex:i];
                     objOtherUserModel.location_longitude   = [[arrOtherUserlist valueForKey:@"location_longitude"] objectAtIndex:i];
                     objOtherUserModel.location_latitude    = [[arrOtherUserlist valueForKey:@"location_latitude"] objectAtIndex:i];
                     objOtherUserModel.phone_number         = [[arrOtherUserlist valueForKey:@"phone_number"] objectAtIndex:i];
                     objOtherUserModel.gender               = [[arrOtherUserlist valueForKey:@"gender"] objectAtIndex:i];
                     objOtherUserModel.date_of_birth        = [[arrOtherUserlist valueForKey:@"date_of_birth"] objectAtIndex:i];
                     
                     [self.arrOtherUserModel addObject:objOtherUserModel];
                 }
                 
                 objOtherUserModel = [self.arrOtherUserModel objectAtIndex:0];
                 NSLog(@"Other User first name :%@",objOtherUserModel.first_name);
                 
                 // Dragable View Remove When Pop to Other ViewController
                 
                 for (DraggableViewBackground *draggableBackground in self.view.subviews)
                 {
                     if ([draggableBackground isKindOfClass:[DraggableViewBackground class]])
                     {
                         [draggableBackground removeFromSuperview];
                     }
                 }
                 
                 DraggableViewBackground *draggableBackground = [[DraggableViewBackground alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) andWithCardArray:[self.arrOtherUserModel copy]];
                 
//                 DraggableViewBackground *draggableBackground = [[DraggableViewBackground alloc]initWithFrame:self.view.frame andWithCardArray:[[NSArray alloc]initWithObjects:@"first",@"second",@"third",@"fourth",@"last", nil]];
                 //DraggableViewBackground *draggableBackground = [[DraggableViewBackground alloc]initWithFrame:CGRectMake(20, 50, 280, 300)];
                 
                 //draggableBackground.backgroundColor = [UIColor clearColor];
                 [self.view addSubview:draggableBackground];
                 
             }
             else if ([[response valueForKey:@"code"] intValue] == 0)
             {
                 if (([[response valueForKey:@"dob"] intValue] == 0))
                 {
                     
                     [self.view bringSubviewToFront:self.viewUpdateDobandGender];
                     self.viewUpdateDobandGender.hidden = NO;
                 }
                 
             }
         }
     }];
    
    
}

-(void)updateAgeAndGender
{
    //{"apptoken":"d10289185f466003c450a15d300e50d5", "dob":" ", "gender":" ", "user_id":"95"}
    
    NSMutableDictionary * dictParam = [[NSMutableDictionary alloc] init];
    
    [dictParam setObject:[[UserModel sharedSingleton] getUserID] forKey:@"user_id"];
    [dictParam setObject:AppToken forKey:@"apptoken"];
    [dictParam setObject:self.lblAge.text forKey:@"dob"];
    [dictParam setObject:strGender forKey:@"gender"];
    
    NSLog(@"Meet Search  Dict Data :%@",dictParam);
    
    [Networking rawJsondataTaskwithURL:UpdateUserGenderAndDOB Param:dictParam ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error)
     {
         if (!error) {
             
             NSLog(@"UserGenderAndDOB Data :%@",response);
             if ([[response valueForKey:@"code"] intValue] == 1)
             {
                 self.viewUpdateDobandGender.hidden = YES;
                 [self.viewUpdateDobandGender removeFromSuperview];
                 [self searchUsers];
             }
             else if ([[response valueForKey:@"code"] intValue] == 0)
             {
                 
                 
             }
         }
     }];
    
}

- (void)showmenu
{
    [self.navigationController.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    [self.frostedViewController presentMenuViewController];
}

-(void)setting
{
    
    ExtendSetting_VC * objExtendSetting_VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ExtendSetting_VC"];
    [self.navigationController pushViewController:objExtendSetting_VC animated:YES];
    //[self presentViewController:objExtendSetting_VC animated:YES completion:nil];
}

-(void)Back
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    self.halo.position = self.beaconView.center;
}

- (void)setupInitialValues {
    
    self.halo.haloLayerNumber = 6;
    self.halo.radius = self.view.frame.size.width-100; //100 ;//* kMaxRadius;
    self.halo.animationDuration = 5;// * kMaxDuration;
    UIColor *color = UIColorFromRGB(Color_NavigationBar); /*[UIColor colorWithRed:self.rSlider.value
                                                           green:self.gSlider.value
                                                           blue:self.bSlider.value
                                                           alpha:1.0];*/
    
    [self.halo setBackgroundColor:color.CGColor];
    
    //    self.countSlider.value = 5;
    //    [self countChanged:nil];
    //
    //    self.radiusSlider.value = 0.7;
    //    [self radiusChanged:nil];
    //
    //    self.durationSlider.value = 0.5;
    //    [self durationChanged:nil];
    //
    //    self.rSlider.value = 0.;
    //    self.gSlider.value = 0.455;
    //    self.bSlider.value = 0.756;
    //    [self colorChanged:nil];
}

// =============================================================================
#pragma mark - IBAction

/*- (IBAction)countChanged:(UISlider *)sender {
 
 //you can specify the number of halos by initial method or by instance property "haloLayerNumber"
 float value = floor(self.countSlider.value);
 self.halo.haloLayerNumber = value;
 self.countLabel.text = [@(value) stringValue];
 }
 
 - (IBAction)radiusChanged:(UISlider *)sender {
 
 self.halo.radius = self.radiusSlider.value * kMaxRadius;
 
 self.radiusLabel.text = [NSString stringWithFormat:@"%.0f", self.radiusSlider.value * kMaxRadius];
 }
 
 - (IBAction)durationChanged:(UISlider *)sender {
 
 self.halo.animationDuration = self.durationSlider.value * kMaxDuration;
 
 self.durationLabel.text = [NSString stringWithFormat:@"%.1f", self.durationSlider.value * kMaxDuration];
 }
 
 - (IBAction)colorChanged:(UISlider *)sender {
 
 UIColor *color = [UIColor colorWithRed:self.rSlider.value
 green:self.gSlider.value
 blue:self.bSlider.value
 alpha:1.0];
 
 [self.halo setBackgroundColor:color.CGColor];
 
 self.rLabel.text = [NSString stringWithFormat:@"%.2f", self.rSlider.value];
 self.gLabel.text = [NSString stringWithFormat:@"%.2f", self.gSlider.value];
 self.bLabel.text = [NSString stringWithFormat:@"%.2f", self.bSlider.value];
 }*/



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)sliderActions:(UISlider *)sender
{
    int SliderValue = (int)roundf(sender.value);
    self.lblAge.text = [NSString stringWithFormat:@"%d", SliderValue];
    
}


- (IBAction)btnMaleFemaleAction:(UIButton *)sender
{
    if (sender.tag == 100000)
    {
        strGender = @"male";
        [self.btnMale setImage:[UIImage imageNamed:@"radio_active_icon"] forState:UIControlStateNormal];
        [self.btnFemale setImage:[UIImage imageNamed:@"radio_icon"] forState:UIControlStateNormal];
    }
    else if (sender.tag == 100001)
    {
        strGender = @"female";
        [self.btnMale setImage:[UIImage imageNamed:@"radio_icon"] forState:UIControlStateNormal];
        [self.btnFemale setImage:[UIImage imageNamed:@"radio_active_icon"] forState:UIControlStateNormal];
    }
}


- (IBAction)btnUpdate:(id)sender
{
    
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-ddHH:mm:ss ZZZ"];
    NSDate *date1 = [f dateFromString:@""];
    
    NSDateComponents *components;
    NSInteger year;
    
    components = [[NSCalendar currentCalendar] components: NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute fromDate: date1 toDate: [NSDate date] options: 0];
    year = [components year];
    
    [self updateAgeAndGender];
}

-(void)GetPlanData:(NSDictionary *)param compilation:(void (^)(id, NSError *))handler
{
    NSLog(@"GetPlanData In Meet Radar animation View Controller:%@",param);
    arrGetAllPlans = [[NSMutableArray alloc] init];
    arrGetAllPlans = [param valueForKey:@"message"];
    //[subscriptionView.tblSubscriptionPlans reloadData];
    [self showSubscription];
    handler(nil, nil);
    UIAlertController * alertcontroller = [[UIAlertController alloc] init];
    alertcontroller = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please Buy Plan" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * OkAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self showSubscription];
        UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, 420)];
        view.backgroundColor = [UIColor yellowColor];
        [self.view addSubview:view];
        [self.view bringSubviewToFront:view];
    }];
    
    [alertcontroller addAction:OkAction];
    [self presentViewController:alertcontroller animated:YES completion:nil];
    
    /*UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Attention" message:@"Please Buy Plan" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
    [alert show];*/
}


-(void)showSubscription
{
    
    btnBackground = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    btnBackground.backgroundColor = [UIColor blackColor];
    btnBackground.alpha = 0.4f;
    [btnBackground addTarget:self action:@selector(removePopUpView) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnBackground];
    [self.view bringSubviewToFront:btnBackground];
    subscriptionView = [[SubscriptionPlanView alloc] initWithFrame:CGRectMake(0, 0, 300, 450)];
    [subscriptionView.tblSubscriptionPlans registerNib:[UINib nibWithNibName:@"SubscriptionTableViewCell" bundle:nil] forCellReuseIdentifier:@"SubscriptionCell"];
    
    subscriptionView.layer.cornerRadius = 10;
    subscriptionView.clipsToBounds = YES;
    
    subscriptionView.frame  = CGRectOffset( subscriptionView.frame, (self.view.frame.size.width - subscriptionView.frame.size.width)/2, (self.view.frame.size.height - subscriptionView.frame.size.height)/6 );
    subscriptionView.backgroundColor = [UIColor whiteColor];
    subscriptionView.transform       = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    
    /*[subscriptionView.btnBuyGold addTarget:self action:@selector(btnGoldBuyAction) forControlEvents:UIControlEventTouchUpInside];
     [subscriptionView.btnBuySilver addTarget:self action:@selector(btnSilverBuyAction) forControlEvents:UIControlEventTouchUpInside];*/
    
    [self.view addSubview:subscriptionView];
    [self.view bringSubviewToFront:subscriptionView];
    subscriptionView.hidden = NO;
    
    [UIView animateWithDuration:0.3/1.5 animations:^{
        
        subscriptionView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0);
        
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3/2 animations:^{
            
            subscriptionView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
            
        } completion:^(BOOL finished) {
            
            [UIView animateWithDuration:0.3/2 animations:^{
                subscriptionView.transform = CGAffineTransformIdentity;
                [subscriptionView layoutIfNeeded];
                
                
            }];
        }];
    }];

    
    subscriptionView.tblSubscriptionPlans.delegate = self;
    subscriptionView.tblSubscriptionPlans.dataSource = self;
    [subscriptionView.tblSubscriptionPlans reloadData];
}

-(void)removePopUpView
{
    [btnBackground removeFromSuperview];
    [subscriptionView removeFromSuperview];
}


#pragma Mark: Tableview Delegate & datasource Method

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arrGetAllPlans.count;    //count number of row from counting array hear cataGorry is An Array
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SubscriptionTableViewCell *cell = (SubscriptionTableViewCell *)  [subscriptionView.tblSubscriptionPlans dequeueReusableCellWithIdentifier:@"SubscriptionCell" forIndexPath:indexPath];
    cell.lblPlanPrice.text = [NSString stringWithFormat:@"%@ - $%@",[[arrGetAllPlans objectAtIndex:indexPath.row] valueForKey:@"plan_name"],[[arrGetAllPlans objectAtIndex:indexPath.row] valueForKey:@"plan_price"]];
    NSString * strNew= [[arrGetAllPlans objectAtIndex:indexPath.row] valueForKey:@"plan_description"];
    
    strNew = [strNew stringByReplacingOccurrencesOfString:@"-" withString:@"\n-"];
    cell.lblPlanDescription.text = strNew;
    
    cell.btnBuyPlan.tag = 100 + indexPath.row;
    [cell.btnBuyPlan addTarget:self action:@selector(btnBuyPlanAction:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"SubscriptionCell";
    //SubscriptionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    return 150 ;//+ [self heightForLabelwithText:objpost.PostName withFont:[UIFont systemFontOfSize:15.0] withWidth:cell.lblMsgCaption.frame.size.width] ;
    
}

-(CGFloat )heightForLabelwithText:(NSString *)text withFont:(UIFont *)font withWidth:(CGFloat)width
{
    CGRect rect = CGRectMake(0, 0, width, CGFLOAT_MAX);
    
    UILabel * lbl = [[UILabel alloc] initWithFrame:rect];
    lbl.numberOfLines = 0;
    lbl.lineBreakMode = NSLineBreakByCharWrapping;
    lbl.font = font;
    lbl.text = text;
    [lbl sizeToFit];
    
    return lbl.frame.size.height;
}
/*-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
 {
 _viewHeader.frame = CGRectMake(0, 0, self.tblPost.frame.size.width, _viewHeader.frame.size.height);
 _viewHeader.backgroundColor = UIColorFromRGB(Color_NavigationBar);
 UIView *view = [[UIView alloc] init ];
 tableView.tableHeaderView = _viewHeader;
 return view;
 }*/

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.001/*self.viewheader.frame.size.height*/;
}

-(IBAction)btnBuyPlanAction:(UIButton *)sender
{
    NSString * strPrice = [[arrGetAllPlans objectAtIndex:sender.tag -100] valueForKey:@"plan_price"];
    strPlanID = [[arrGetAllPlans objectAtIndex:sender.tag -100] valueForKey:@"plan_id"];
    [self payWithAmmount:strPrice];
}

-(void)viewDidUnload {
    [[NSNotificationCenter defaultCenter]
     removeObserver:self
     name:DidReceivePlans
     object:nil];
}

-(void)didReceivePlans:(NSNotification *)notification {
    if (self.isViewLoaded && self.view.window) {
        
        NSLog(@"USER Info inside: %@",notification.userInfo);
        NSDictionary * dictDATA = [[NSDictionary alloc]init];
        dictDATA = notification.userInfo;
        arrGetAllPlans = [[NSMutableArray alloc] init];
        arrGetAllPlans = [dictDATA valueForKey:@"message"];
        NSLog(@"arrGetAllPlans:%@",arrGetAllPlans);
        [subscriptionView.tblSubscriptionPlans reloadData];
        [self showSubscription];

    }
    
}


-(void)setupPayPalConfiguration
{
    // Set up payPalConfig
    _payPalConfig = [[PayPalConfiguration alloc] init];
#if HAS_CARDIO
    // You should use the PayPal-iOS-SDK+card-Sample-App target to enable this setting.
    // For your apps, you will need to link to the libCardIO and dependent libraries. Please read the README.md
    // for more details.
    _payPalConfig.acceptCreditCards = YES;
#else
    _payPalConfig.acceptCreditCards = NO;
#endif
    _payPalConfig.merchantName = @"Extend";
    _payPalConfig.merchantPrivacyPolicyURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/privacy-full"];
    _payPalConfig.merchantUserAgreementURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/useragreement-full"];
    
    // Setting the languageOrLocale property is optional.
    //
    // If you do not set languageOrLocale, then the PayPalPaymentViewController will present
    // its user interface according to the device's current language setting.
    //
    // Setting languageOrLocale to a particular language (e.g., @"es" for Spanish) or
    // locale (e.g., @"es_MX" for Mexican Spanish) forces the PayPalPaymentViewController
    // to use that language/locale.
    //
    // For full details, including a list of available languages and locales, see PayPalPaymentViewController.h.
    
    _payPalConfig.languageOrLocale = [NSLocale preferredLanguages][0];
    
    
    // Setting the payPalShippingAddressOption property is optional.
    //
    // See PayPalConfiguration.h for details.
    
    _payPalConfig.payPalShippingAddressOption = PayPalShippingAddressOptionPayPal;
    
    // Do any additional setup after loading the view, typically from a nib.
    
    //self.successView.hidden = YES;
    
    // use default environment, should be Production in real life
    self.environment = kPayPalEnvironment;
    
    NSLog(@"PayPal iOS SDK version: %@", [PayPalMobile libraryVersion]);
    
    // Update payPalConfig re accepting credit cards.
    self.payPalConfig.acceptCreditCards = self.acceptCreditCards;
    
    
}

-(void)payWithAmmount:(NSString *)ammount
{
    // Remove our last completed payment, just for demo purposes.
    self.resultText = nil;
    
    // Note: For purposes of illustration, this example shows a payment that includes
    //       both payment details (subtotal, shipping, tax) and multiple items.
    //       You would only specify these if appropriate to your situation.
    //       Otherwise, you can leave payment.items and/or payment.paymentDetails nil,
    //       and simply set payment.amount to your total charge.
    
    // Optional: include multiple items
    PayPalItem *item1 = [PayPalItem itemWithName:@""
                                    withQuantity:1
                                       withPrice:[NSDecimalNumber decimalNumberWithString:ammount]
                                    withCurrency:@"USD"
                                         withSku:@"Hip-00037"];
    
    
    NSArray *items = @[item1];
    NSDecimalNumber *subtotal = [PayPalItem totalPriceForItems:items];
    
    // Optional: include payment details
    NSDecimalNumber *shipping = [[NSDecimalNumber alloc] initWithString:@"0.00"];
    NSDecimalNumber *tax = [[NSDecimalNumber alloc] initWithString:@"0.00"];
    PayPalPaymentDetails *paymentDetails = [PayPalPaymentDetails paymentDetailsWithSubtotal:subtotal
                                                                               withShipping:shipping
                                                                                    withTax:tax];
    
    NSDecimalNumber *total = [[subtotal decimalNumberByAdding:shipping] decimalNumberByAdding:tax];
    
    PayPalPayment *payment = [[PayPalPayment alloc] init];
    
    payment.amount = total;
    payment.currencyCode = @"USD";
    payment.shortDescription = @"Hipster clothing";
    payment.items = nil; // items;  // if not including multiple items, then leave payment.items as nil
    payment.paymentDetails = paymentDetails; // if not including payment details, then leave payment.paymentDetails as nil
    
    if (!payment.processable) {
        // This particular payment will always be processable. If, for
        // example, the amount was negative or the shortDescription was
        // empty, this payment wouldn't be processable, and you'd want
        // to handle that here.
    }
    
    
    PayPalPaymentViewController *paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment
                                                                                                configuration:self.payPalConfig
                                                                                                     delegate:self];
    [self presentViewController:paymentViewController animated:YES completion:nil];
}

- (BOOL)acceptCreditCards {
    return self.payPalConfig.acceptCreditCards;
}

- (void)setAcceptCreditCards:(BOOL)acceptCreditCards {
    self.payPalConfig.acceptCreditCards = acceptCreditCards;
}

- (void)setPayPalEnvironment:(NSString *)environment {
    self.environment = environment;
    [PayPalMobile preconnectWithEnvironment:environment];
}



#pragma mark PayPalPaymentDelegate methods

- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController didCompletePayment:(PayPalPayment *)completedPayment {
    NSLog(@"PayPal Payment Success!");
    self.resultText = [completedPayment description];
    
    
    NSDictionary * DictPayment = [[NSDictionary alloc]init];
    DictPayment = [completedPayment confirmation];
    
    NSLog(@"completedPayment id----%@",[[[completedPayment confirmation]valueForKey:@"response"] valueForKey:@"id"] );
    //[self RequestBookingTrainer];
    
    
    /*NSDictionary * DICT=[[NSDictionary alloc]init];
     DICT=@{
     @"app_token"         :[Ref valueForKey:@"apptoken"],
     @"booking_type"      :@"1",
     @"plan_id"           :strPlainID,
     @"user_id"           :[Ref valueForKey:@"userid"],
     @"trainer_id"        :_strTrainerId,
     @"plan_start_date"   :_txtDate.text,
     @"plan_start_time"   :_txtTime.text,
     @"address"           :_tvDestimationAddress.text,
     @"transaction_id"    :[[[completedPayment confirmation]valueForKey:@"response"] valueForKey:@"id"],
     @"booking_amount"    :[completedPayment amount],
     @"state"             :[[[completedPayment confirmation]valueForKey:@"response"] valueForKey:@"state"],
     @"environment"       :[[[completedPayment confirmation]valueForKey:@"client"] valueForKey:@"environment"],
     @"response_type"     :[[completedPayment confirmation]valueForKey:@"response_type"] ,
     };*/
    
    
    [self sendCompletedPaymentToServer:completedPayment]; // Payment was processed successfully; send to server for verification and fulfillment
    [self dismissViewControllerAnimated:YES completion:nil];
    [self removePopUpView];
}



- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController {
    NSLog(@"PayPal Payment Canceled");
    self.resultText = nil;
    //self.successView.hidden = YES;
    [self dismissViewControllerAnimated:YES completion:nil];
    [self removePopUpView];
}

#pragma mark Proof of payment validation

- (void)sendCompletedPaymentToServer:(PayPalPayment *)completedPayment {
    // TODO: Send completedPayment.confirmation to server
    NSLog(@"Here is your proof of payment:\n\n%@\n\nSend this to your server for confirmation and fulfillment.", completedPayment.confirmation);
    [self SelectPlanWithPlanId];
    [self removePopUpView];
}

-(void)SelectPlanWithPlanId
{//{"apptoken":"d10289185f466003c450a15d300e50d5","user_id":"54","plan_id":"2"}
    NSMutableDictionary * dictParam = [[NSMutableDictionary alloc] init];
    [dictParam setObject:AppToken forKey:@"apptoken"];
    [dictParam setObject:[[UserModel sharedSingleton] getUserID] forKey:@"user_id"];
    [dictParam setObject:strPlanID forKey:@"plan_id"];
    
    NSLog(@"Update Dict Data :%@",dictParam);
    
    
    [Networking rawJsondataTaskwithURL:SelectPlan Param:dictParam ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error) {
        if (!error)
        {
            NSLog(@"response History Data :%@",response);
            
            if ([[response valueForKey:@"status"]integerValue] == 1)
            {
                [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Success];
            }
            else
            {
                [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Error];
            }
        }
    }];
    
}


#pragma mark - Authorize Future Payments

/*- (IBAction)getUserAuthorizationForFuturePayments:(id)sender {
 
 PayPalFuturePaymentViewController *futurePaymentViewController = [[PayPalFuturePaymentViewController alloc] initWithConfiguration:self.payPalConfig delegate:self];
 [self presentViewController:futurePaymentViewController animated:YES completion:nil];
 }*/


#pragma mark PayPalFuturePaymentDelegate methods

- (void)payPalFuturePaymentViewController:(PayPalFuturePaymentViewController *)futurePaymentViewController
                didAuthorizeFuturePayment:(NSDictionary *)futurePaymentAuthorization {
    NSLog(@"PayPal Future Payment Authorization Success!");
    self.resultText = [futurePaymentAuthorization description];
    //[self showSuccess];
    
    [self sendFuturePaymentAuthorizationToServer:futurePaymentAuthorization];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)payPalFuturePaymentDidCancel:(PayPalFuturePaymentViewController *)futurePaymentViewController {
    NSLog(@"PayPal Future Payment Authorization Canceled");
    //self.successView.hidden = YES;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)sendFuturePaymentAuthorizationToServer:(NSDictionary *)authorization {
    // TODO: Send authorization to server
    NSLog(@"Here is your authorization:\n\n%@\n\nSend this to your server to complete future payment setup.", authorization);
}


#pragma mark - Authorize Profile Sharing

/*- (IBAction)getUserAuthorizationForProfileSharing:(id)sender {
 
 NSSet *scopeValues = [NSSet setWithArray:@[kPayPalOAuth2ScopeOpenId, kPayPalOAuth2ScopeEmail, kPayPalOAuth2ScopeAddress, kPayPalOAuth2ScopePhone]];
 
 PayPalProfileSharingViewController *profileSharingPaymentViewController = [[PayPalProfileSharingViewController alloc] initWithScopeValues:scopeValues configuration:self.payPalConfig delegate:self];
 [self presentViewController:profileSharingPaymentViewController animated:YES completion:nil];
 }*/


#pragma mark PayPalProfileSharingDelegate methods

- (void)payPalProfileSharingViewController:(PayPalProfileSharingViewController *)profileSharingViewController
             userDidLogInWithAuthorization:(NSDictionary *)profileSharingAuthorization {
    NSLog(@"PayPal Profile Sharing Authorization Success!");
    self.resultText = [profileSharingAuthorization description];
    //[self showSuccess];
    
    [self sendProfileSharingAuthorizationToServer:profileSharingAuthorization];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)userDidCancelPayPalProfileSharingViewController:(PayPalProfileSharingViewController *)profileSharingViewController {
    NSLog(@"PayPal Profile Sharing Authorization Canceled");
    //self.successView.hidden = YES;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)sendProfileSharingAuthorizationToServer:(NSDictionary *)authorization {
    // TODO: Send authorization to server
    NSLog(@"Here is your authorization:\n\n%@\n\nSend this to your server to complete profile sharing setup.", authorization);
}
#pragma Mark-  Commaon Alertview

-(void)showAlertMessage:(NSString*)message Title:(NSString *)title
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               {
                               }];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

@end
