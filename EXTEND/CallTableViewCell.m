//
//  CallTableViewCell.m
//  EXTEND
//
//  Created by Apple on 25/05/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "CallTableViewCell.h"

@implementation CallTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.imgProPic.layer.cornerRadius = self.imgProPic.frame.size.height/2;
    self.imgProPic.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
