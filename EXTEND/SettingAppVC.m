//
//  SettingAppVC.m
//  EXTEND
//
//  Created by Apple on 10/04/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "SettingAppVC.h"
#import "REFrostedViewController.h"

@interface SettingAppVC ()

@end

@implementation SettingAppVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBar];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setNavigationBar
{
    self.navigationController.navigationBar.hidden = NO;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(40, 0,SCREEN_MAX_LENGTH-40, 40)];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"Raleway" size:16];
    label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    label.textAlignment = NSTextAlignmentLeft;
    label.textColor =[UIColor whiteColor];
    label.text=@"Setting";
    self.navigationItem.titleView = label;
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu_icon"]style:UIBarButtonItemStylePlain target:self action:@selector(showmenu)];
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    
    /*UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"setting_icon"]style:UIBarButtonItemStylePlain target:self action:@selector(setting)];
     self.navigationItem.rightBarButtonItem = rightBarButtonItem;*/
}

- (void)showmenu
{
    [self.navigationController.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    [self.frostedViewController presentMenuViewController];
}


@end
