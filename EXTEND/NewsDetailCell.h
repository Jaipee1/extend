//
//  NewsDetailCell.h
//  EXTEND
//
//  Created by Apple on 05/04/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsDetailCell : UITableViewCell

@property (weak,nonatomic) IBOutlet UIButton * btnLikeDislike;
@property (weak,nonatomic) IBOutlet UIButton * btnShare;
@property (weak,nonatomic) IBOutlet UILabel * lblLikeCount;
@property (weak,nonatomic) IBOutlet UILabel * lblCommentCount;
@property (weak,nonatomic) IBOutlet UIButton * btnWritComment;
@property (weak,nonatomic) IBOutlet UITextView * tvNews;

@end
