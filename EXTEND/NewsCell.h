//
//  NewsCell.h
//  EXTEND
//
//  Created by Apple on 10/03/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgviewPost;
@property (weak, nonatomic) IBOutlet UILabel *lblPostTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblNewsPostCategoryName;
@property (weak, nonatomic) IBOutlet UILabel *lblNewPostDateTime;
@property (weak, nonatomic) IBOutlet UILabel *lblLikeCount;
@property (weak, nonatomic) IBOutlet UILabel *lblCommentCount;
@property (weak, nonatomic) IBOutlet UIButton *btnLike;
@property (weak, nonatomic) IBOutlet UIButton *btnComment;
@property (weak, nonatomic) IBOutlet UIButton *btnShare;
@property (weak, nonatomic) IBOutlet UILabel *lblNewsPostDescription;

@end
