//
//  PostSend_VC.h
//  EXTEND
//
//  Created by Apple on 06/03/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PostSend_VC : UIViewController

/*typedef NS_ENUM(NSInteger, PlayerStateType) {
    PlayerStateOff,
    PlayerStatePlaying,
    PlayerStatePaused
};

typedef enum playerStateTypes
{
    PLAYER_OFF,
    PLAYER_PLAYING,
    PLAYER_PAUSED
} PlayerState;

@property(assign, nonatomic) PlayerState * enumPlayer;
@property(assign, nonatomic) PlayerStateType * enumPlaye;*/

@property(retain, nonatomic) NSMutableArray * arrEditPost;
@property(assign) BOOL isPostEditing;

@end
