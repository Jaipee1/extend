//
//  NewsMoreReplies_VC.m
//  EXTEND
//
//  Created by Apple on 18/03/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "NewsMoreReplies_VC.h"

#import "CommentCell.h"
#import "Networking.h"
#import "UserModel.h"

#import <SDWebImage/UIImageView+WebCache.h>

@interface NewsMoreReplies_VC ()
{
    NSMutableArray * arrCommentData;
}
@property (strong, nonatomic) IBOutlet UITableView *tblComment;
@property (weak, nonatomic) IBOutlet UIView *viewCommentBox;
@property (weak, nonatomic) IBOutlet UITextView *tv_AddComment;


@end

@implementation NewsMoreReplies_VC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //viewheightconst.constant=50;
    
    
    
    self.tv_AddComment.layer.cornerRadius = self.tv_AddComment.frame.size.height/2;
    self.tv_AddComment.clipsToBounds = YES;
    //self.tv_AddComment.contentInset = UIEdgeInsetsMake(4,8,0,0);
    self.tv_AddComment.textContainerInset = UIEdgeInsetsMake(15, 12, 0, 0);
    //_lblLikeCount.text = _strLikeCount;
    //_lblCommentCount.text = _strCommentCount;
    
    NSLog(@"Comment ID : %@",_strCommentID);
    NSLog(@"user ID of posted comment : %@",_strReplyUserID);
    
    [self getMoreReplieswithUserID:_strReplyUserID andwithCommentID:_strCommentID];
}

-(void)getMoreReplieswithUserID:(NSString *)user_id andwithCommentID:(NSString *)comment_id
{
    //{"apptoken":"d10289185f466003c450a15d300e50d5","comment_id":"6" ,"user_id":"95","reply_user_id":"95"}

    
    
    NSMutableDictionary * dictParam =[[NSMutableDictionary alloc] init];
    [dictParam setObject:[[UserModel sharedSingleton] getUserID] forKey:@"reply_user_id"];
    [dictParam setObject:AppToken  forKey:@"apptoken"];
    [dictParam setObject:user_id forKey:@"user_id"];
    [dictParam setObject:comment_id forKey:@"comment_id"];
    
    [Networking rawJsondataTaskwithURL:ShowNewsMoreReply Param:dictParam ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error) {
        if (!error) {
            NSLog(@"All Replies Data : %@",response);
            
            if ([[response valueForKey:@"status"]integerValue] == 1)
            {
                arrCommentData = [[NSMutableArray alloc] init];
                arrCommentData = [response valueForKey:@"news_comment_reply"];
                [_tblComment reloadData];
                
            }
            else if ([[response valueForKey:@"status"]integerValue] == 0)
            {
                [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Error];
            }
            
        }
        else
        {
            
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma Mark-  Commaon Alertview

-(void)showAlertMessage:(NSString*)message Title:(NSString *)title
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               {
                               }];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arrCommentData.count;    //count number of row from counting array hear cataGorry is An Array
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"CommentCell";
    
    CommentCell *cell = (CommentCell *)  [_tblComment dequeueReusableCellWithIdentifier:MyIdentifier forIndexPath:indexPath];
    //    if (cell == nil)
    //    {
    //        cell = [[HomeTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
    //    }


    cell.viewMain.layer.cornerRadius = 27;
    cell.viewMain.clipsToBounds = YES;
    
    
    cell.imgViewMainCommenterProPic.layer.cornerRadius = cell.imgViewMainCommenterProPic.frame.size.height/2;
    cell.imgViewMainCommenterProPic.layer.borderWidth = 1;
    cell.imgViewMainCommenterProPic.layer.borderColor = UIColorFromRGB(Color_NavigationBar).CGColor;
    cell.imgViewMainCommenterProPic.clipsToBounds = YES;
    
    [cell.imgViewMainCommenterProPic sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",UserProfilePictureBaseURL,[[arrCommentData objectAtIndex:indexPath.row] valueForKey:@"image"]]] placeholderImage:[UIImage imageNamed:@"upload_icon"]];
    cell.lblMainCommenterUsername.text = [NSString stringWithFormat:@"%@ %@",[[arrCommentData objectAtIndex:indexPath.row] valueForKey:@"first_name"],[[arrCommentData objectAtIndex:indexPath.row] valueForKey:@"last_name"]];
    cell.lblCommentDateTime.text = [[arrCommentData objectAtIndex:indexPath.row] valueForKey:@"reply_time"];

    cell.lblCommentDescription.text = [[arrCommentData objectAtIndex:indexPath.row] valueForKey:@"reply"];
    
    
    if ([[[arrCommentData objectAtIndex:indexPath.row] valueForKey:@"rply_user_id"] isEqualToString:[[UserModel sharedSingleton] getUserID]])
    {
        cell.viewMain.backgroundColor = UIColorFromRGB(Color_NavigationBar);
        cell.lblCommentDateTime.textColor = cell.lblCommentDescription.textColor = cell.lblMainCommenterUsername.textColor = [UIColor whiteColor];
    }
    else
    {
        cell.viewMain.backgroundColor = [UIColor whiteColor];
        cell.lblCommentDateTime.textColor = cell.lblCommentDescription.textColor = cell.lblMainCommenterUsername.textColor = [UIColor blackColor];
    }
    
      return cell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"CommentCell";
    CommentCell *cell = [self.tblComment dequeueReusableCellWithIdentifier:MyIdentifier];
    
    CGRect newFrame = cell.lblCommentDescription.frame;
    newFrame.size.height = [self heightForLabelwithText:[[arrCommentData objectAtIndex:indexPath.row] valueForKey:@"reply"] withFont:[UIFont systemFontOfSize:cell.lblCommentDescription.font.pointSize] withWidth:cell.lblCommentDescription.frame.size.width];
    cell.lblCommentDescription.frame = newFrame;

    return  55 + [self heightForLabelwithText:[[arrCommentData objectAtIndex:indexPath.row] valueForKey:@"reply"] withFont:[UIFont systemFontOfSize:cell.lblCommentDescription.font.pointSize] withWidth:cell.lblCommentDescription.frame.size.width] + 3;
    
    
}

-(CGFloat )heightForLabelwithText:(NSString *)text withFont:(UIFont *)font withWidth:(CGFloat)width
{
    CGRect rect = CGRectMake(0, 0, width, CGFLOAT_MAX);
    
    UILabel * lbl = [[UILabel alloc] initWithFrame:rect];
    lbl.numberOfLines = 0;
    lbl.lineBreakMode = NSLineBreakByCharWrapping;
    lbl.font = font;
    lbl.text = text;
    [lbl sizeToFit];
    
    return lbl.frame.size.height;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.001/*self.viewheader.frame.size.height*/;
}




#pragma Send Comment Action And Keyboard Animation
- (IBAction)btnSendAction:(id)sender
{
    
    if (_tv_AddComment.text.length !=0 && ![_tv_AddComment.text isEqualToString:@"Write a comment..."])
    {
        [self ReplywithCommentID:_strCommentID withReplyUserID:_strReplyUserID];
        [_tv_AddComment resignFirstResponder];
        _tv_AddComment.text = nil;
    }
}

-(void)ReplywithCommentID:(NSString *)comment_id withReplyUserID:(NSString *)reply_user_id
{
    //{"apptoken":"d10289185f466003c450a15d300e50d5","comment_id":"4", "user_id":"54","reply":"hello","reply_user_id":"95"}
    
    NSMutableDictionary * dictParam =[[NSMutableDictionary alloc] init];
    [dictParam setObject:[[UserModel sharedSingleton] getUserID] forKey:@"reply_user_id"];
    [dictParam setObject:AppToken  forKey:@"apptoken"];
    [dictParam setObject:reply_user_id forKey:@"user_id"];
    [dictParam setObject:_tv_AddComment.text forKey:@"reply"];
    [dictParam setObject:comment_id forKey:@"comment_id"];
    NSLog(@"Reply Param : %@",dictParam);
    [Networking rawJsondataTaskwithURL:AddReply Param:dictParam ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error) {
        if (!error)
        {
            if ([[response valueForKey:@"status"]integerValue] == 1)
            {
                NSLog(@"Reply on Comment Data : %@",response);
                [self getMoreReplieswithUserID:reply_user_id andwithCommentID:comment_id];
            }
            else if ([[response valueForKey:@"status"]integerValue] == 0)
            {
                
                [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Error];
            }
            
            
        }
        else
        {
            
        }
    }];
    
}


/*-(void)addCommentwithComment:(NSString *)comment withNewsID:(NSString *)newsID andwithCategoryID:(NSString *)categoryID
{
    //{apptoken":"d10289185f466003c450a15d300e50d5","user_id":"2","news_id":"2","comment":"nice","cat_id":"2"}
    
    NSMutableDictionary * dictParam =[[NSMutableDictionary alloc] init];
    [dictParam setObject:[[UserModel sharedSingleton] getUserID] forKey:@"user_id"];
    [dictParam setObject:AppToken  forKey:@"apptoken"];
    [dictParam setObject:newsID forKey:@"news_id"];
    [dictParam setObject:comment forKey:@"comment"];
    [dictParam setObject:categoryID forKey:@"cat_id"];
    
    [Networking rawJsondataTaskwithURL:AddCommentsInNews Param:dictParam ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error) {
        if (!error)
        {
            if ([[response valueForKey:@"status"]integerValue] == 1)
            {
                NSLog(@"Add New Comment Data : %@",response);
                //[self getCommentswithNewsID:self.strNewsPostID];
            }
            else if ([[response valueForKey:@"status"]integerValue] == 0)
            {
                
                [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Error];
            }
            
            
        }
        else
        {
            
        }
    }];
}*/


#pragma mark - Text View delegates

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSCharacterSet *doneButtonCharacterSet = [NSCharacterSet newlineCharacterSet];
    NSRange replacementTextRange = [text rangeOfCharacterFromSet:doneButtonCharacterSet];
    NSUInteger location = replacementTextRange.location;
    
    if (textView.text.length + text.length > 500){
        if (location != NSNotFound){
            [textView resignFirstResponder];
        }
        return NO;
    }
    else if (location != NSNotFound){
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    if (textView == _tv_AddComment && [_tv_AddComment.text isEqualToString:@"Write a comment..."]) {
        _tv_AddComment.text = nil;
    }
    
    NSLog(@"Did begin editing");
    //  [self animateTextView:textView up:YES];
}

-(void)textViewDidChange:(UITextView *)textView{
    NSLog(@"Did Change");
    
    
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    NSLog(@"Did End editing");
    
    if (textView == _tv_AddComment && [_tv_AddComment.text isEqualToString:@""]) {
        _tv_AddComment.text = @"Write a comment...";
    }
    
    //[self animateTextView:textView up:NO];
}


-(BOOL)textViewShouldEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    return YES;
}

/*-(void)animateTextView:(UITextView*)textView up:(BOOL)up
 {
 
 int a;
 a= 580-(textView.frame.origin.y+400);
 
 const int movementDistance = a;
 const float movementDuration = 0.3f;
 
 int movement = (up ? movementDistance : -movementDistance);
 
 [UIView beginAnimations: @"animateTextField" context: nil];
 [UIView setAnimationBeginsFromCurrentState: YES];
 [UIView setAnimationDuration: movementDuration];
 self.view.frame = CGRectOffset(self.view.frame, 0, movement);
 [UIView commitAnimations];
 
 }*/


- (void)viewWillAppear:(BOOL)animated
{
    //[self getCommentswithNewsID:self.strNewsPostID];
    
    self.navigationController.navigationBarHidden = YES;
    
    [super viewWillAppear:(BOOL)animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillDisappear:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:(BOOL)animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Keyboard appearance/disappearance handling

- (void)keyboardWillAppear:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0);
    
    [self.tblComment setContentInset:contentInsets];
    [self.tblComment setScrollIndicatorInsets:contentInsets];
    CGSize r = _tblComment.contentSize;
    [_tblComment scrollRectToVisible:CGRectMake(0, r.height-10, r.width, 10) animated:YES];
    
    CGRect messageFrame = self.viewCommentBox.frame;
    messageFrame.origin.y -= keyboardSize.height;
    [self.viewCommentBox setFrame:messageFrame];
    NSLog(@"viewCommentBox Will apperar fram: %f",_viewCommentBox.frame.origin.y);
}

- (void)keyboardWillDisappear:(NSNotification *)notification
{
    
    NSDictionary *userInfo = [notification userInfo];
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.25];
    [self.tblComment setContentInset:UIEdgeInsetsZero];
    [UIView commitAnimations];
    [self.tblComment setScrollIndicatorInsets:UIEdgeInsetsZero];
    
    CGRect messageFrame = self.viewCommentBox.frame;
    //messageFrame.origin.y += keyboardSize.height;
    messageFrame.origin.y = self.view.frame.size.height - 60;
    [self.viewCommentBox setFrame:messageFrame];
    
    NSLog(@"viewCommentBox Will Disappear fram: %f",_viewCommentBox.frame.origin.y);
}

-(IBAction)btnCancelAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
