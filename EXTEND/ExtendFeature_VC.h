//
//  ExtendFeature_VC.h
//  EXTEND
//
//  Created by Apple on 16/03/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>
#import "PayPalMobile.h"
@interface ExtendFeature_VC : UIViewController<CLLocationManagerDelegate,PayPalPaymentDelegate, PayPalFuturePaymentDelegate, PayPalProfileSharingDelegate,UIPopoverControllerDelegate>

{
    GMSMapView *mapView;
    CLLocationManager *locationManager;
    CGFloat latitudeUser;
    CGFloat longitudeUser;
}

#pragma -------Paypal Stuff-------
@property(nonatomic, strong, readwrite) NSString *environment;
@property(nonatomic, assign, readwrite) BOOL acceptCreditCards;
@property(nonatomic, strong, readwrite) NSString *resultText;
@property(nonatomic, strong, readwrite) PayPalConfiguration *payPalConfig;



@end
