//
//  VideoAndImageTableViewCell.h
//  EXTEND
//
//  Created by Apple on 26/05/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoAndImageTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingConstraintMedia;
@property (weak, nonatomic) IBOutlet UIView *viewBG;
@property (weak, nonatomic) IBOutlet UIImageView *imgMsg;
@property (weak, nonatomic) IBOutlet UIButton *btnMediaAction;

@end
