//
//  SubscriptionTableViewCell.h
//  EXTEND
//
//  Created by Apple on 02/06/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubscriptionTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblPlanPrice;
@property (weak, nonatomic) IBOutlet UIButton *btnBuyPlan;
@property (weak, nonatomic) IBOutlet UILabel *lblPlanDescription;

@end
