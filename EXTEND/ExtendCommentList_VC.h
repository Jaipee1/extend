//
//  ExtendCommentList_VC.h
//  EXTEND
//
//  Created by Apple on 16/03/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExtendCommentList_VC : UIViewController
@property (strong, nonatomic) NSString * strUserID;
@property ( nonatomic) double latitude;
@property ( nonatomic) double longitude;
@end
