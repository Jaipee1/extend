//
//  NewsDetailsVC.h
//  EXTEND
//
//  Created by Apple on 05/04/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsModel.h"

@interface NewsDetailsVC : UIViewController
@property (strong, nonatomic) NSMutableArray * arrGetNews;
@property (assign) NSInteger index;
@end
