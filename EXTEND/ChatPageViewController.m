//
//  ChatPageViewController.m
//  EXTEND
//
//  Created by Apple on 25/05/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "ChatPageViewController.h"
#import "CallsViewController.h"
#import "ChatViewController.h"
#import "ContactViewController.h"



@interface ChatPageViewController ()
{
  NSMutableArray  * arrController;
}
//@property (strong, nonatomic) NSMutableArray  * arrController; //*<UIViewController *>*/
@end

@implementation ChatPageViewController
//@synthesize arrController;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.delegate   = self;
    //self.dataSource = self; // IF YOU WANT TO SCROLL PAGE VIEW CONTROLLER UNCOMMENT DATA SOURCE LIE CODE
    
    CallsViewController * callsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CallsViewController"];
    
    ChatViewController * chatVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatViewController"];
    
    ContactViewController * contactVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactViewController"];
    
    arrController = [[NSMutableArray alloc] init];
    
    [arrController addObject:callsVC];
    [arrController addObject:chatVC];
    [arrController addObject:contactVC];
    
    NSLog(@"count:%lu",(unsigned long)arrController.count);
    
    [self setViewControllers:@[chatVC] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)showPageWithViewcontroller:(ChildViewControllers)child withCompletion:(BOOL)boolValue
{
    NSInteger index = 0;
    
    switch (child) {
        case calls:
            index = 0;
            break;
        case chat:
            index = 1;
            break;
        case contact:
            index = 2;
            break;
            
        default:
            break;
    }
    
    UIViewController * controller = [arrController objectAtIndex:index];
    
    [self setViewControllers:@[controller] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Page View Datasource Methods
- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    /*let currentIndex = pages.index(of: viewController)!
    let previousIndex = abs((currentIndex - 1) % pages.count)*/
    
    NSInteger currentIndex = [arrController indexOfObject: viewController];
    NSInteger previousIndex = abs((currentIndex - 1) % arrController.count);
   /* --currentIndex;
    currentIndex = currentIndex % (arrController.count);*/
    return [arrController objectAtIndex:previousIndex];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    /*
     let currentIndex = pages.index(of: viewController)!
     let nextIndex = abs((currentIndex + 1) % pages.count)
     */
    
    
    NSInteger currentIndex = [arrController indexOfObject:viewController];
    NSInteger previousIndex = abs((currentIndex + 1) % arrController.count);
    
    /*++currentIndex;
    currentIndex = currentIndex % (arrController.count);*/
    return [arrController objectAtIndex:previousIndex];
}

-(UIViewController *)viewControllerAtIndex:(NSUInteger)index
{
    return arrController[index];
}




@end
