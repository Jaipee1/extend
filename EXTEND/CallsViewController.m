//
//  CallsViewController.m
//  EXTEND
//
//  Created by Apple on 25/05/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "CallsViewController.h"
#import "CallTableViewCell.h"
#import "FinalChatViewController.h"
#import "Networking.h"
#import "UserModel.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface CallsViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    CallTableViewCell * cell;
    NSMutableArray * arrRecentCalldata;
}
@property (weak, nonatomic) IBOutlet UITableView *tblCalllist;
@end

@implementation CallsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self GetResentChat];
}

-(void)GetResentChat
{
    //{"apptoken":"d10289185f466003c450a15d300e50d5", "user_id":"170"  ,"limit":"0" }
    
    NSMutableDictionary * dictParam =[[NSMutableDictionary alloc] init];
    [dictParam setObject:[[UserModel sharedSingleton] getUserID] forKey:@"user_id"];
    [dictParam setObject:AppToken  forKey:@"apptoken"];
    [dictParam setObject:[NSNumber numberWithInteger:0]  forKey:@"limit"];
    
    [Networking rawJsondataTaskwithURL:ResentCallList Param:dictParam ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error) {
        if (!error)
        {
            
            NSLog(@"ResentCallList Data : %@",response);
            
            if ([[response valueForKey:@"status"]integerValue] == 1)
            {
                arrRecentCalldata = [[NSMutableArray alloc] init];
                arrRecentCalldata = [response valueForKey:@"show_chat"];
                self.tblCalllist.delegate   = self;
                self.tblCalllist.dataSource = self;
                [self.tblCalllist reloadData];
                
            }
            else if ([[response valueForKey:@"status"]integerValue] == 0)
            {
                [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Error];
            }
            
        }
        else
        {
            
        }
    }];
    
}

#pragma Mark-  Commaon Alertview

-(void)showAlertMessage:(NSString*)message Title:(NSString *)title
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               {
                               }];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arrRecentCalldata.count;  //count number of row from counting array hear cataGorry is An Array
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"CallTableViewCell";
    
    cell = (CallTableViewCell *)  [self.tblCalllist dequeueReusableCellWithIdentifier:MyIdentifier forIndexPath:indexPath];
    
    
    cell.tag = indexPath.row;
    //objNewsModel = [self.arrNewsModel objectAtIndex:indexPath.row];
    
    [cell.imgProPic sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",UserProfilePictureBaseURL,[[arrRecentCalldata objectAtIndex:indexPath.row] valueForKey:@"profile_image"]]] placeholderImage:[UIImage imageNamed:@"upload_icon"]];
    cell.lblFullName.text = [[arrRecentCalldata objectAtIndex:indexPath.row] valueForKey:@"name"];
    cell.lblDate.text = [[arrRecentCalldata objectAtIndex:indexPath.row] valueForKey:@"call_time"];
    
    if ([[[arrRecentCalldata objectAtIndex:indexPath.row] valueForKey:@"call_type"]isEqualToString:@"audio"])
    {
        cell.imgCallOrVideo.image = [UIImage imageNamed:@"audiocall"];
    }
    else if ([[[arrRecentCalldata objectAtIndex:indexPath.row] valueForKey:@"call_type"]isEqualToString:@"video"])
    {
        cell.imgCallOrVideo.image = [UIImage imageNamed:@"videocall"];
    }
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    FinalChatViewController * objFinalChatViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"FinalChatViewController"];
    objFinalChatViewController.arrOtherUserData = [arrRecentCalldata objectAtIndex:indexPath.row];
    //objFinalChatViewController.otherPersonUserID = [[arrRecentCalldata objectAtIndex:indexPath.row] valueForKey:@"user_id"];
    
    if ([[[arrRecentCalldata objectAtIndex:indexPath.row] valueForKey:@"user_id"] isEqualToString:[[UserModel sharedSingleton] getUserID]])
    {
        objFinalChatViewController.otherPersonUserID = [[arrRecentCalldata objectAtIndex:indexPath.row] valueForKey:@"friend_id"];
    }
    else
    {
        objFinalChatViewController.otherPersonUserID = [[arrRecentCalldata objectAtIndex:indexPath.row] valueForKey:@"user_id"];
    }
    /*UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:objFinalChatViewController];
    [self presentViewController:nav animated:YES completion:nil];*/
     [self.navigationController pushViewController:objFinalChatViewController animated:YES];
    
}
/*- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
 {
 static NSString *MyIdentifier = @"NewsCell";
 NewsCell *cell = [self.tblNews dequeueReusableCellWithIdentifier:MyIdentifier];
 
 objNewsModel = [self.arrNewsModel objectAtIndex:indexPath.row];
 
 return cell.contentView.frame.size.height - 50 + [self heightForLabelwithText:objNewsModel.NewsContent withFont:[UIFont systemFontOfSize:cell.lblNewsPostDescription.font.pointSize] withWidth:cell.lblNewsPostDescription.frame.size.width] ;
 
 }*/
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.001/*self.viewheader.frame.size.height*/;
}


@end
