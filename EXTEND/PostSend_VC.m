//
//  PostSend_VC.m
//  EXTEND
//
//  Created by Apple on 06/03/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "PostSend_VC.h"

#import "Networking.h"
#import "UserModel.h"
#import "PostModel.h"

#import "UIViewController+util.h"

#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <SDWebImage/UIImageView+WebCache.h>

#import <GooglePlaces/GooglePlaces.h>

@interface PostSend_VC ()<UIGestureRecognizerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,AVAudioPlayerDelegate ,GMSAutocompleteViewControllerDelegate>
{
    UIAlertView *alertImagechoose;
    UIImagePickerController *ImgPicker;
    UIImage *IMAGE;
    NSString * strPostType;
    PostModel * objPostModel;
   // PlayerState thePlayerState;
}
@property (weak, nonatomic) IBOutlet UIImageView *imgViewProfilePicture;
@property (weak, nonatomic) IBOutlet UILabel *lblUserFullName;
@property (weak, nonatomic) IBOutlet UITextView *tvMessageCaption;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewPost;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ConstraintTvMessageCaption;
@property (strong, nonatomic) NSURL *videoURL;

@end

@implementation PostSend_VC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [UIViewController currentViewController];
    
    NSLog(@"which ViewController is this :%@",[UIViewController currentViewController]);
    
    _imgViewProfilePicture.layer.cornerRadius = self.imgViewProfilePicture.frame.size.height/2;
    _imgViewProfilePicture.clipsToBounds = YES;
    [_imgViewProfilePicture sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",UserProfilePictureBaseURL,[[UserModel sharedSingleton] getUserProfileImageUrl]]] placeholderImage:[UIImage imageNamed:@"upload_icon"]];
    _lblUserFullName.text = [NSString stringWithFormat:@"%@ %@",[[UserModel sharedSingleton] getUserFirstName],[[UserModel sharedSingleton] getUserLastName]];
    
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(ChangePicture:)];
    tap.delegate = self;
    _imgViewPost.userInteractionEnabled = YES;
    [_imgViewPost addGestureRecognizer:tap];
    
    objPostModel = [_arrEditPost objectAtIndex:0];
    
    NSLog(@"objPostModel Data:%@",objPostModel.PostName);
    
    if (objPostModel)
    {
        NSLog(@"objPostModel Data:%@",objPostModel.PostName);
        
        self.tvMessageCaption.text = objPostModel.PostName;
        if ([objPostModel.PostType isEqualToString:@"video"])
        {
            [self.imgViewPost sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",VideoPost,objPostModel.PostvideoThumbnail]] placeholderImage:[UIImage imageNamed:@"upload_icon"]];
        }
        else
        {
            [self.imgViewPost sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImagePost,objPostModel.PostContent]] placeholderImage:[UIImage imageNamed:@"upload_icon"]];
        }
        _imgViewPost.userInteractionEnabled = NO;
    }
    else
    {
        NSLog(@"objPostModel Data: nil");
    }
   /* _resultsViewController = [[GMSAutocompleteResultsViewController alloc] init];
    _resultsViewController.delegate = self;
    
    _searchController = [[UISearchController alloc]
                         initWithSearchResultsController:_resultsViewController];
    _searchController.searchResultsUpdater = _resultsViewController;
    
    // Put the search bar in the navigation bar.
    [_searchController.searchBar sizeToFit];
    self.navigationItem.titleView = _searchController.searchBar;
    
    // When UISearchController presents the results view, present it in
    // this view controller, not one further up the chain.
    self.definesPresentationContext = YES;
    
    // Prevent the navigation bar from being hidden when searching.
    _searchController.hidesNavigationBarDuringPresentation = NO;*/
}
/*
// Handle the user's selection.
- (void)resultsController:(GMSAutocompleteResultsViewController *)resultsController
 didAutocompleteWithPlace:(GMSPlace *)place {
    _searchController.active = NO;
    // Do something with the selected place.
    NSLog(@"Place name %@", place.name);
    NSLog(@"Place address %@", place.formattedAddress);
    NSLog(@"Place attributions %@", place.attributions.string);
}

- (void)resultsController:(GMSAutocompleteResultsViewController *)resultsController
didFailAutocompleteWithError:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:nil];
    // TODO: handle the error.
    NSLog(@"Error: %@", [error description]);
}

// Turn the network activity indicator on and off again.
- (void)didRequestAutocompletePredictionsForResultsController:
(GMSAutocompleteResultsViewController *)resultsController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)didUpdateAutocompletePredictionsForResultsController:
(GMSAutocompleteResultsViewController *)resultsController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}*/

-(void)ChangePicture:(UITapGestureRecognizer *)sender
{
    
    /*alertImagechoose=[[UIAlertView alloc]initWithTitle:nil message:@"Please Select Photo / Video from" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Camera",@"Gallery", nil];
    [alertImagechoose show];*/
    
    UIAlertController * view=   [UIAlertController
                                 alertControllerWithTitle:nil
                                 message:@"Please Select Photo / Video from"
                                 preferredStyle:UIAlertControllerStyleActionSheet];
    
    view.view.tintColor = UIColorFromRGB(Color_NavigationBar);
    //view.view.subviews.firstObject.backgroundColor = UIColorFromRGB(Color_NavigationBar);
    UIAlertAction* Gallery = [UIAlertAction
                              actionWithTitle:@"Camera"
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                  NSString *model = [[UIDevice currentDevice] model];
                                  if ([model isEqualToString:@"iPhone Simulator"] || [model isEqualToString:@"iPad Simulator"])
                                  {
                                      UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!!!" message:@"No Camera found in Simulator!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                      [alert show];
                                  }
                                  else
                                  {
                                      ImgPicker = [[UIImagePickerController alloc] init];
                                      ImgPicker.delegate = self;
                                      ImgPicker.allowsEditing = YES;
                                      ImgPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                                      ImgPicker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
                                      
                                      // Only For Video  MediaType
                                      
                                      //ImgPicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
                                      
                                      // Only For All (Video and Photo)  MediaType
                                      ImgPicker.mediaTypes =
                                      [UIImagePickerController availableMediaTypesForSourceType:
                                       UIImagePickerControllerSourceTypeCamera];
                                      [self presentViewController:ImgPicker animated:YES completion:nil];
                                      ImgPicker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
                                      ImgPicker.videoQuality = UIImagePickerControllerQualityTypeMedium;
                                      
                                      /*ImgPicker=[[UIImagePickerController alloc]init];
                                       ImgPicker.delegate=self;
                                       //ImgPicker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                                       ImgPicker.sourceType=UIImagePickerControllerSourceTypeSavedPhotosAlbum;
                                       ImgPicker.mediaTypes =
                                       [UIImagePickerController availableMediaTypesForSourceType:
                                       UIImagePickerControllerSourceTypeSavedPhotosAlbum];
                                       ImgPicker.allowsEditing=true;
                                       
                                       [self presentViewController:ImgPicker animated:true completion:^{
                                       NSLog(@"piker");
                                       
                                       
                                       }];*/
                                      
                                  }
                                  
                                  
                                  
                                  //Do some thing here
                                  [view dismissViewControllerAnimated:YES completion:nil];
                                  
                              }];
    
    UIAlertAction* Camera = [UIAlertAction
                             actionWithTitle:@"Gallery"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
                                     
                                     UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                                     picker.delegate = self;
                                     picker.allowsEditing = YES;
                                     //picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                     picker.sourceType=UIImagePickerControllerSourceTypeSavedPhotosAlbum;
                                     picker.mediaTypes =
                                     [UIImagePickerController availableMediaTypesForSourceType:
                                      UIImagePickerControllerSourceTypeSavedPhotosAlbum];
                                     ImgPicker.videoQuality = UIImagePickerControllerQualityTypeMedium;
                                     
                                     //picker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
                                     
                                     [self presentViewController:picker animated:YES completion:NULL];
                                 }
                                 //Do some thing here
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    
    
    
    [view addAction:Gallery];
    [view addAction:Camera];
    [view addAction:cancel];
    [self presentViewController:view animated:YES completion:nil];
    
}
-(void)imagePickerController:(UIImagePickerController *)picker
       didFinishPickingImage:(UIImage *)img
                 editingInfo:(NSDictionary *)editingInfo

{
    if ([picker isEqual:ImgPicker])
    {
        
    }
    [picker dismissModalViewControllerAnimated:YES];
    
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissModalViewControllerAnimated:YES];
    
}
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [[picker parentViewController] dismissViewControllerAnimated:YES completion:nil];
    
    //IMAGE = [info valueForKey:@"UIImagePickerControllerOriginalImage"];
    
    if([info[UIImagePickerControllerMediaType] isEqualToString:(__bridge NSString *)(kUTTypeImage)])
    {
        NSLog(@"Image");
        IMAGE=[info objectForKey:UIImagePickerControllerEditedImage];
        _imgViewPost.image=IMAGE;
        strPostType = @"image";
    }
    else
    {
        // IF VIDEO CAPTURED BY CAMERA (Not from Gallery) AND SEND TO SERVER AND YOU GOT ANY SERVER ERROR THAN PLZ
        // GOTO CAPABILITES SELECT BACKGROUND MODES TO SOLVE THIS SERVER ERROR.
        
        NSLog(@"Video");
        strPostType = @"video";
        self.videoURL = info[UIImagePickerControllerMediaURL];
        [picker dismissViewControllerAnimated:YES completion:NULL];
        [self generateThumbnailImage];
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)generateThumbnailImage
{
    AVURLAsset *asset=[[AVURLAsset alloc] initWithURL:self.videoURL options:nil];
    AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generator.appliesPreferredTrackTransform=TRUE;
    //[asset release];
    CMTime thumbTime = CMTimeMakeWithSeconds(0,30);
    
    AVAssetImageGeneratorCompletionHandler handler = ^(CMTime requestedTime, CGImageRef im, CMTime actualTime, AVAssetImageGeneratorResult result, NSError *error){
        if (result != AVAssetImageGeneratorSucceeded)
        {
            NSLog(@"couldn't generate thumbnail, error:%@", error);
        }
        else
        {
            _imgViewPost.image = [UIImage imageWithCGImage:im];
        }
    };
    
    CGSize maxSize = CGSizeMake(320, 165);
    generator.maximumSize = maxSize;
    [generator generateCGImagesAsynchronouslyForTimes:[NSArray arrayWithObject:[NSValue valueWithCMTime:thumbTime]] completionHandler:handler];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString *)validate
{
    NSString * strMsg ;
    
    if (_tvMessageCaption.text.length == 0 || [_tvMessageCaption.text isEqualToString:@"Say something about Post..."])
    {
        strMsg = @"Please Say somthing about your Post ";
    }
    else if (_imgViewPost.image == nil && self.videoURL == nil)
    {
        strMsg = @"Please Select video or image for post ";
    }
    else
    {
        strMsg = nil;
    }
    
    return strMsg;
}

#pragma Mark-  Commaon Alertview

-(void)showAlertMessage:(NSString*)message Title:(NSString *)title
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               {
                               }];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(IBAction)btnPostAction:(id)sender
{
    if ([self validate])
    {
        [self showAlertMessage:[self validate] Title:Alert_Title_Info];
    }
    else
    {
        if (self.isPostEditing)
        {
            NSLog(@"Edit Post");
            [self editPostSend];
        }
        else
        {
            NSLog(@"New Post");
            [self newPostSend];
        }
        
    }
    
}

-(void)newPostSend
{
    NSMutableDictionary * dictParam = [[NSMutableDictionary alloc] init];
    
    [dictParam setObject:[[UserModel sharedSingleton] getUserID] forKey:@"user_id"];
    [dictParam setObject:AppToken forKey:@"apptoken"];
    [dictParam setObject:_tvMessageCaption.text forKey:@"post_name"];
    [dictParam setObject:strPostType forKey:@"post_type"];
    
    
    if ([strPostType isEqualToString:@"video"])
    {
        NSMutableArray * arrVideoUrl = [[NSMutableArray alloc] init];
        NSMutableArray * arrVideoUrlParameter = [[NSMutableArray alloc] init];
        
        
        NSMutableArray * arrThumnailImage = [[NSMutableArray alloc] init];
        NSMutableArray * arrThumbnailImageParameter = [[NSMutableArray alloc] init];
        
        [arrVideoUrlParameter addObject:@"post_content"];
        [arrVideoUrl addObject:self.videoURL];
        
        [arrThumbnailImageParameter addObject:@"post_video_thumbnail"];
        [arrThumnailImage addObject:_imgViewPost.image];
        
        
        [Networking dataTaskwithURL:PostSend Param:dictParam VideoUrlArray:arrVideoUrl VideoPrameterArray:arrVideoUrlParameter ThumbnailImageArray:arrThumnailImage ThumbnaiImagePrameterArray:arrThumbnailImageParameter compilation:^(id response, NSError *error) {
            if (!error)
            {
                NSLog(@"response Post Video----%@",response);
                
                if ([[response valueForKey:@"status"]integerValue] == 1)
                {
                    [self addPostTitlewithPostID:[response valueForKey:@"insert_id"]];
                    
                }
                else if ([[response valueForKey:@"status"]integerValue] == 0)
                {
                    
                    [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Error];
                }
            }
        }];
        
        
        /*[Networking dataTaskwithURL:PostSend Param:dictParam VideoUrlArray:arrVideoUrl VideoPrameterArray:arrVideoUrlParameter compilation:^(id response, NSError *error) {
         if (!error)
         {
         NSLog(@"response Post Video----%@",response);
         
         if ([[response valueForKey:@"status"]integerValue] == 1)
         {
         [self addPostTitlewithPostID:[response valueForKey:@"insert_id"]];
         
         }
         else if ([[response valueForKey:@"status"]integerValue] == 0)
         {
         
         [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Error];
         }
         }
         }];*/
    }
    else if ([strPostType isEqualToString:@"image"])
    {
        NSMutableArray * arrVideoUrl = [[NSMutableArray alloc] init];
        NSMutableArray * arrVideoUrlParameter = [[NSMutableArray alloc] init];
        
        [arrVideoUrlParameter addObject:@"post_content"];
        [arrVideoUrl addObject:_imgViewPost.image];
        
        [Networking dataTaskwithURL:PostSend Param:dictParam ImageArray:arrVideoUrl ImageParamArray:arrVideoUrlParameter compilation:^(id response, NSError *error) {
            if (!error)
            {
                NSLog(@"response Post Video----%@",response);
                if ([[response valueForKey:@"status"]integerValue] == 1)
                {
                    [self addPostTitlewithPostID:[response valueForKey:@"insert_id"]];
                }
                else if ([[response valueForKey:@"status"]integerValue] == 0)
                {
                    [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Error];
                }
            }
        }];
    }
}


-(void)addPostTitlewithPostID:(NSString *)post_id
{
    //Parameter = {"apptoken":"d10289185f466003c450a15d300e50d5","post_id":"9", "post_title":"bigbe 😊😊😀😀   😭"}
    
    NSMutableDictionary * dictParam = [[NSMutableDictionary alloc] init];
    
    [dictParam setObject:post_id forKey:@"post_id"];
    [dictParam setObject:_tvMessageCaption.text forKey:@"post_title"];
    [dictParam setObject:AppToken forKey:@"apptoken"];

    [Networking rawJsondataTaskwithURL:AddPostTitle Param:dictParam ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error)
     {
         if (!error)
         {
             NSLog(@"response----%@",response);
             
             if ([[response valueForKey:@"status"]integerValue] == 1)
             {
                 [self dismissViewControllerAnimated:YES completion:nil];
             }
             else if ([[response valueForKey:@"status"]integerValue] == 0)
             {
                 [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Error];
             }
             
         }
     }];
}

- (IBAction)btnCloseAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma Mark: textview delegate method

-(BOOL)textView:(UITextView *)_textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    [self adjustFrames];
    
    if([text isEqualToString:@"\n"]) {
        [_textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}


-(void) adjustFrames
{
    CGRect textFrame = _tvMessageCaption.frame;
    textFrame.size.height = _tvMessageCaption.contentSize.height;
    
    if (_tvMessageCaption.text.length < 1 && _tvMessageCaption.text.length > 5)
    {
        _tvMessageCaption.frame = textFrame;
        _ConstraintTvMessageCaption.constant = 35;
    }
    
    if (_tvMessageCaption.frame.size.height < 110)
    {
        _tvMessageCaption.frame = textFrame;
        _ConstraintTvMessageCaption.constant = _tvMessageCaption.frame.size.height;
    }
    NSLog(@"_tvMessageCaption.frame.size.height---%f",_tvMessageCaption.frame.size.height);
    [self.view layoutIfNeeded];
}

/*-(CGFloat )heightForLabelwithText:(NSString *)text withFont:(UIFont *)font withWidth:(CGFloat)width
{
    CGRect rect = CGRectMake(0, 0, width, CGFLOAT_MAX);
    
    UILabel * lbl = [[UILabel alloc] initWithFrame:rect];
    lbl.numberOfLines = 0;
    lbl.lineBreakMode = NSLineBreakByCharWrapping;
    lbl.font = font;
    lbl.text = text;
    [lbl sizeToFit];
    
    return lbl.frame.size.height;
}*/

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    if (textView == _tvMessageCaption && [_tvMessageCaption.text isEqualToString:@"Say something about Post..."])
    {
        _tvMessageCaption.text = nil;
        //_tvMessageCaption.textColor = [UIColor colorWithRed:240/255.0f green:99/255.0f blue:91/255.0f alpha:1.0];
    }

    NSLog(@"Did begin editing");
}

-(void)textViewDidChange:(UITextView *)textView
{
    NSLog(@"Did Change");
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    NSLog(@"Did End editing");
    
    if (textView == _tvMessageCaption && [_tvMessageCaption.text isEqualToString:@""]) {
        _tvMessageCaption.text = @"Say something about Post...";
        //_tvDestimationAddress.textColor = [UIColor colorWithRed:199/255.0f green:199/255.0f blue:205/255.0f alpha:1.0];
    }

}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    return YES;
}

-(void)editPostSend
{
    //{apptoken:d10289185f466003c450a15d300e50d5,user_id:54,post_type:video,friend_id:94,post_video_thumbnail:””,post_content:post_id:post_name:
    


    
    NSMutableDictionary * dictParam = [[NSMutableDictionary alloc] init];
    
    [dictParam setObject:[[UserModel sharedSingleton] getUserID] forKey:@"user_id"];
    [dictParam setObject:AppToken forKey:@"apptoken"];
    [dictParam setObject:_tvMessageCaption.text forKey:@"post_name"];
    [dictParam setObject:objPostModel.PostType forKey:@"post_type"];
    [dictParam setObject:@"" forKey:@"post_video_thumbnail"];
    [dictParam setObject:@"" forKey:@"post_content"];
    [dictParam setObject:objPostModel.PostID forKey:@"post_id"];
    
    [Networking dataTaskwithURL:EditPost Param:dictParam VideoUrlArray:nil VideoPrameterArray:nil ThumbnailImageArray:nil ThumbnaiImagePrameterArray:nil compilation:^(id response, NSError *error) {
        if (!error)
        {
            NSLog(@"response Post Video----%@",response);
            
            if ([[response valueForKey:@"status"]integerValue] == 1)
            {
                [self dismissViewControllerAnimated:YES completion:nil];
            }
            else if ([[response valueForKey:@"status"]integerValue] == 0)
            {
                [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Error];
            }
        }
    }];
    
 /*   if ([strPostType isEqualToString:@"video"])
    {
        NSMutableArray * arrVideoUrl = [[NSMutableArray alloc] init];
        NSMutableArray * arrVideoUrlParameter = [[NSMutableArray alloc] init];
        
        NSMutableArray * arrThumnailImage = [[NSMutableArray alloc] init];
        NSMutableArray * arrThumbnailImageParameter = [[NSMutableArray alloc] init];
        
        [arrVideoUrlParameter addObject:@"post_content"];
        [arrVideoUrl addObject:self.videoURL];
        
        [arrThumbnailImageParameter addObject:@"post_video_thumbnail"];
        [arrThumnailImage addObject:_imgViewPost.image];
        
        [Networking dataTaskwithURL:PostSend Param:dictParam VideoUrlArray:arrVideoUrl VideoPrameterArray:arrVideoUrlParameter ThumbnailImageArray:arrThumnailImage ThumbnaiImagePrameterArray:arrThumbnailImageParameter compilation:^(id response, NSError *error) {
            if (!error)
            {
                NSLog(@"response Post Video----%@",response);
                
                if ([[response valueForKey:@"status"]integerValue] == 1)
                {
                    [self addPostTitlewithPostID:[response valueForKey:@"insert_id"]];
                }
                else if ([[response valueForKey:@"status"]integerValue] == 0)
                {
                    [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Error];
                }
            }
        }];

    }
    else if ([strPostType isEqualToString:@"image"])
    {
        NSMutableArray * arrVideoUrl = [[NSMutableArray alloc] init];
        NSMutableArray * arrVideoUrlParameter = [[NSMutableArray alloc] init];
        
        [arrVideoUrlParameter addObject:@"post_content"];
        [arrVideoUrl addObject:_imgViewPost.image];
        
        [Networking dataTaskwithURL:PostSend Param:dictParam ImageArray:arrVideoUrl ImageParamArray:arrVideoUrlParameter compilation:^(id response, NSError *error) {
            if (!error)
            {
                NSLog(@"response Post Video----%@",response);
                if ([[response valueForKey:@"status"]integerValue] == 1)
                {
                    [self addPostTitlewithPostID:[response valueForKey:@"insert_id"]];
                }
                else if ([[response valueForKey:@"status"]integerValue] == 0)
                {
                    [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Error];
                }
            }
        }];
    }*/

}


// Present the autocomplete view controller when the button is pressed.
- (IBAction)onLaunchClicked:(id)sender {
    GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
    acController.delegate = self;
    [self presentViewController:acController animated:YES completion:nil];
}

// Handle the user's selection.
- (void)viewController:(GMSAutocompleteViewController *)viewController
didAutocompleteWithPlace:(GMSPlace *)place {
    [self dismissViewControllerAnimated:YES completion:nil];
    // Do something with the selected place.
    NSLog(@"Place name %@", place.name);
    NSLog(@"Place address %@", place.formattedAddress);
    NSLog(@"Place attributions %@", place.attributions.string);
}

- (void)viewController:(GMSAutocompleteViewController *)viewController
didFailAutocompleteWithError:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:nil];
    // TODO: handle the error.
    NSLog(@"Error: %@", [error description]);
}

// User canceled the operation.
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

// Turn the network activity indicator on and off again.
- (void)didRequestAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)didUpdateAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

@end
