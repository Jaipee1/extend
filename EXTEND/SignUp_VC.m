//
//  SignUp_VC.m
//  EXTEND
//
//  Created by Apple on 02/03/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "SignUp_VC.h"
#import "HelperClass.h"
#import "Networking.h"

@interface SignUp_VC ()<UIGestureRecognizerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    UIAlertView *alertImagechoose;
    UIImagePickerController *ImgPicker;
    UIImage *IMAGE;
}
@property (weak, nonatomic) IBOutlet UIImageView *imgViewProfilePicture;
@property (weak, nonatomic) IBOutlet UITextField *txtFirstName;
@property (weak, nonatomic) IBOutlet UITextField *txtLastName;
@property (weak, nonatomic) IBOutlet UITextField *txtMobile;
@property (weak, nonatomic) IBOutlet UITextField *txtEmailAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;

@end

@implementation SignUp_VC
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = NO;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _imgViewProfilePicture.layer.cornerRadius = self.imgViewProfilePicture.frame.size.height/2;
    _imgViewProfilePicture.clipsToBounds = YES;
    
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(ChangePicture:)];
    tap.delegate = self;
    [_imgViewProfilePicture addGestureRecognizer:tap];
    
}
-(void)ChangePicture:(UITapGestureRecognizer *)sender
{
    
    alertImagechoose=[[UIAlertView alloc]initWithTitle:nil message:@"Please Select Image with" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Camera",@"Gallery", nil];
    [alertImagechoose show];
    
    //        UIView *view = sender.view;
    //        NSLog(@"%ld", (long)view.tag);//By tag, you can find out where you had tapped.
    //        //TAG_IV=(long)view.tag;
    
}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView==alertImagechoose)
    {
        
        if(buttonIndex == 1)
        {
            
            NSString *model = [[UIDevice currentDevice] model];
            if ([model isEqualToString:@"iPhone Simulator"] || [model isEqualToString:@"iPad Simulator"])
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!!!" message:@"No Camera found in Simulator!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
            else
            {
                ImgPicker = [[UIImagePickerController alloc] init];
                ImgPicker.delegate = self;
                
                ImgPicker.allowsEditing = YES;
                ImgPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                
                ImgPicker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
                [self presentViewController:ImgPicker animated:YES completion:nil];
                //   [self presentModalViewController: picker animated:YES];
                
                ImgPicker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
                
            }
            
        }
        
        if(buttonIndex == 2)
        {
            
            ImgPicker=[[UIImagePickerController alloc]init];
            ImgPicker.delegate=self;
            ImgPicker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
            ImgPicker.allowsEditing=true;
            
            [self presentViewController:ImgPicker animated:true completion:^{
                NSLog(@"piker");
                
                
            }];
            
        }
        
        
        
    }
    
}

-(void)imagePickerController:(UIImagePickerController *)picker
       didFinishPickingImage:(UIImage *)img
                 editingInfo:(NSDictionary *)editingInfo

{
    if ([picker isEqual:ImgPicker])
    {
        
    }
    [picker dismissModalViewControllerAnimated:YES];
    
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissModalViewControllerAnimated:YES];
    
}
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [[picker parentViewController] dismissViewControllerAnimated:YES completion:nil];
    
    //IMAGE = [info valueForKey:@"UIImagePickerControllerOriginalImage"];
    IMAGE=[info objectForKey:UIImagePickerControllerEditedImage];
    _imgViewProfilePicture.image=IMAGE;
    
    
    
    /*  // Get the data for the image as a PNG
     NSData* imageData = UIImagePNGRepresentation(IMAGE);
     
     // Give a name to the file
     NSString* imageName = @"MyImage.png";
     
     // Now, we have to find the documents directory so we can save it
     // Note that you might want to save it elsewhere, like the cache directory,
     // or something similar.
     NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
     NSString* documentsDirectory = [paths objectAtIndex:0];
     
     // Now we get the full path to the file
     NSString* fullPathToFile = [documentsDirectory stringByAppendingPathComponent:imageName];
     NSLog(@"-----------%@",fullPathToFile);
     // and then we write it out
     [imageData writeToFile:fullPathToFile atomically:NO];*/
    
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString *)validate
{
    NSString *strAlertMessage;
    if ([[HelperClass getValidString:self.txtFirstName.text] isEqualToString:@""])
    {
        strAlertMessage = @"Please Enter First Name";
    }
    else if ([[HelperClass getValidString:self.txtLastName.text] isEqualToString:@""])
    {
        strAlertMessage = @"Please Enter Last Name";
    }
    else if ([[HelperClass getValidString:self.txtMobile.text] isEqualToString:@""] || ![HelperClass NSStringIsValidNumber:self.txtMobile.text])
    {
        strAlertMessage = @"Please Enter Mobile Number";
    }
    else if ([[HelperClass getValidString:self.txtEmailAddress.text] isEqualToString:@""] || ![HelperClass NSStringIsValidEmail:self.txtEmailAddress.text])
    {
        strAlertMessage = @"Please Enter Email Address";
    }
    else if ([[HelperClass getValidString:self.txtPassword.text] isEqualToString:@""])
    {
        strAlertMessage = @"Please Enter Password";
    }
    
    return strAlertMessage;
}

#pragma Mark-  Commaon Alertview

-(void)showAlertMessage:(NSString*)message Title:(NSString *)title
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               {
                               }];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}



- (IBAction)btnSignUpAction:(id)sender
{
    if ([self validate])
    {
        [self showAlertMessage:[self validate] Title:Alert_Title_Info];
    }
    else
    {
        // Parameter {"profile_pic":"https://lh3.googleusercontent.com/-IrC02NTCYyE/AAAAAAAAAAI/AAAAAAAABMc/w8J-XGBL4wI/photo.jpg","first_name":"alka","last_name":"alka","email_id":"ctdeav2aaa1@gmail.com","password":"alka","phone_number":"1645255545","user_type":"normal","fb_id":"","google_id":"aaa","apptoken":"d10289185f466003c450a15d300e50d5"}
        
        NSDictionary * Dict=[[NSDictionary alloc]init];
        Dict=@{@"apptoken":AppToken,
                    @"email_id":self.txtEmailAddress.text,
                    @"password":self.txtPassword.text,
                    @"first_name":self.txtFirstName.text,
                    @"last_name":self.txtLastName.text,
                    @"phone_number":self.txtMobile.text,
                    @"user_type":@"normal",
                    @"device_token":[[NSUserDefaults standardUserDefaults]valueForKey:@"Device_token"],
                    };
        NSLog(@"DictLogin---%@",Dict);
        
        NSMutableArray * ImageArrayParameter = [[NSMutableArray alloc]init];
        NSMutableArray * ImageArray = [[NSMutableArray alloc]init];
        
        if (_imgViewProfilePicture.image !=nil)
        {
            [ImageArray addObject:_imgViewProfilePicture.image];
            [ImageArrayParameter addObject:@"profile_pic"];
        }
        
        [Networking dataTaskwithURL:SignUp Param:Dict ImageArray:ImageArray ImageParamArray:ImageArrayParameter compilation:^(id response, NSError *error) {
            if (!error)
            {
                NSLog(@"response----%@",response);
                
                if ([[response valueForKey:@"success"]integerValue] == 1)
                {
                    
                    /*
                     response----{
                     code = 1;
                     message = "Registration successful!";
                     success = 1;
                     "user_info" =     {
                     "date_of_birth" = "";
                     "device_token" = "";
                     "email_id" = "John@gmail.com";
                     "email_verification" = 0;
                     "fb_id" = 0;
                     "first_name" = John;
                     gender = "";
                     "google_id" = 0;
                     id = 101;
                     image = "5341488613709.jpg";
                     "last_name" = Doe;
                     "login_status" = 0;
                     password = e10adc3949ba59abbe56e057f20f883e;
                     "phone_number" = 123564546;
                     "user_type" = normal;
                     };
                     }

                     */
                    [self.navigationController popViewControllerAnimated:YES];
                    
                    //[self showAlertMessage:[response objectForKey:@"message"] Title:Alert_Title_Success];
                    
                    [self showAlertMessage:@"Signup Successfully & Please Check Your Mail For Account Varification" Title:Alert_Title_Success];
                    
                    /*UIAlertView * Alert = [[UIAlertView alloc]initWithTitle:@"Success" message:[response valueForKey:@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                     [Alert show];*/
                    
                    /*  UserModelObj = [UserModel sharedMySingleton];
                     
                     //NSString * StrUserID = [NSString stringWithFormat:@"%@",[[response valueForKey:@"msg"] valueForKey:@"user_id"] ];
                     [UserModelObj setUserID:[[response valueForKey:@"msg"] valueForKey:@"user_id"]];
                     [UserModelObj setContactNumber:[[response valueForKey:@"msg"] valueForKey:@"user_contact"]];
                     [UserModelObj setEmailAddress:[[response valueForKey:@"msg"] valueForKey:@"user_email"]];
                     [UserModelObj setUsername:[[response valueForKey:@"msg"] valueForKey:@"user_name"]];
                     [UserModelObj setImageUrl:[[response valueForKey:@"msg"] valueForKey:@"user_image"]];
                     [UserModelObj setUserType:[[response valueForKey:@"msg"] valueForKey:@"user_type"]];
                     [UserModelObj setAddress:[[response valueForKey:@"msg"] valueForKey:@"user_address"]];
                     [UserModelObj setIsLogedIn:YES];
                     //NSLog(@"get data---%@",[UserModelObj getUserID]);*/
                    
                    
                    /*TabBarVC * objTabBarVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBarVC"];
                     [self.navigationController pushViewController:objTabBarVC animated:YES];*/
                    
                    

                }
                else if ([[response valueForKey:@"success"]integerValue] == 0)
                {
   
                    [self showAlertMessage:[response objectForKey:@"message"] Title:Alert_Title_Error];
                }
                
            }
        }];

        
    }
}

-(IBAction)btnBackAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField;
{
    [textField resignFirstResponder];
    
    return YES;
}

@end
