//
//  PaypalPayment.m
//  EXTEND
//
//  Created by Apple on 10/05/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "PaypalPayment.h"
#import <UIKit/UIKit.h>


#define kPayPalEnvironment PayPalEnvironmentSandbox

@implementation PaypalPayment

-(instancetype)init
{
    if ((self = [super init]))
    {
        [PayPalMobile initializeWithClientIdsForEnvironments:@{PayPalEnvironmentProduction : @"YOUR_CLIENT_ID_FOR_PRODUCTION",
                                                               PayPalEnvironmentSandbox : @"AVPXTBwp4jsn_9ApozL7RegIJW1yR5RgUcRvTeqbs2hWaZbPz3RGzmwf-QTKPs4_onBpkGwMC0S5o5kh"}];
    }
    return self;
}

-(id)initWithViewController:(UIViewController *)viewcontroller andWithAmmount:(NSString *)ammount
{
    if ((self = [super init]))
    {
        self.aViewcontroller = viewcontroller;
        
        // Set up payPalConfig
        _payPalConfig = [[PayPalConfiguration alloc] init];
#if HAS_CARDIO
        // You should use the PayPal-iOS-SDK+card-Sample-App target to enable this setting.
        // For your apps, you will need to link to the libCardIO and dependent libraries. Please read the README.md
        // for more details.
        _payPalConfig.acceptCreditCards = YES;
#else
        _payPalConfig.acceptCreditCards = NO;
#endif
        _payPalConfig.merchantName = @"jai yadav";
        _payPalConfig.merchantPrivacyPolicyURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/privacy-full"];
        _payPalConfig.merchantUserAgreementURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/useragreement-full"];
        
        // Setting the languageOrLocale property is optional.
        //
        // If you do not set languageOrLocale, then the PayPalPaymentViewController will present
        // its user interface according to the device's current language setting.
        //
        // Setting languageOrLocale to a particular language (e.g., @"es" for Spanish) or
        // locale (e.g., @"es_MX" for Mexican Spanish) forces the PayPalPaymentViewController
        // to use that language/locale.
        //
        // For full details, including a list of available languages and locales, see PayPalPaymentViewController.h.
        
        _payPalConfig.languageOrLocale = [NSLocale preferredLanguages][0];
        
        
        // Setting the payPalShippingAddressOption property is optional.
        //
        // See PayPalConfiguration.h for details.
        
        _payPalConfig.payPalShippingAddressOption = PayPalShippingAddressOptionPayPal;
        
        // Do any additional setup after loading the view, typically from a nib.
        
        //self.successView.hidden = YES;
        
        // use default environment, should be Production in real life
        self.environment = kPayPalEnvironment;
        
        NSLog(@"PayPal iOS SDK version: %@", [PayPalMobile libraryVersion]);
        
        
        
        
        
        // Remove our last completed payment, just for demo purposes.
        self.resultText = nil;
        
        // Note: For purposes of illustration, this example shows a payment that includes
        //       both payment details (subtotal, shipping, tax) and multiple items.
        //       You would only specify these if appropriate to your situation.
        //       Otherwise, you can leave payment.items and/or payment.paymentDetails nil,
        //       and simply set payment.amount to your total charge.
        
        // Optional: include multiple items
        PayPalItem *item1 = [PayPalItem itemWithName:@""
                                        withQuantity:1
                                           withPrice:[NSDecimalNumber decimalNumberWithString:ammount]
                                        withCurrency:@"USD"
                                             withSku:@"Hip-00037"];
        /*    PayPalItem *item2 = [PayPalItem itemWithName:@"Free rainbow patch"
         withQuantity:1
         withPrice:[NSDecimalNumber decimalNumberWithString:@"0.00"]
         withCurrency:@"USD"
         withSku:@"Hip-00066"];
         PayPalItem *item3 = [PayPalItem itemWithName:@"Long-sleeve plaid shirt (mustache not included)"
         withQuantity:1
         withPrice:[NSDecimalNumber decimalNumberWithString:@"37.99"]
         withCurrency:@"USD"
         withSku:@"Hip-00291"];*/
        
        NSArray *items = @[item1];
        NSDecimalNumber *subtotal = [PayPalItem totalPriceForItems:items];
        
        // Optional: include payment details
        NSDecimalNumber *shipping = [[NSDecimalNumber alloc] initWithString:@"0.00"];
        NSDecimalNumber *tax = [[NSDecimalNumber alloc] initWithString:@"0.00"];
        PayPalPaymentDetails *paymentDetails = [PayPalPaymentDetails paymentDetailsWithSubtotal:subtotal
                                                                                   withShipping:shipping
                                                                                        withTax:tax];
        
        NSDecimalNumber *total = [[subtotal decimalNumberByAdding:shipping] decimalNumberByAdding:tax];
        
        PayPalPayment *payment = [[PayPalPayment alloc] init];
        
        payment.amount = total;
        payment.currencyCode = @"USD";
        payment.shortDescription = @"Hipster clothing";
        payment.items = nil; // items;  // if not including multiple items, then leave payment.items as nil
        payment.paymentDetails = paymentDetails; // if not including payment details, then leave payment.paymentDetails as nil
        
        if (!payment.processable) {
            // This particular payment will always be processable. If, for
            // example, the amount was negative or the shortDescription was
            // empty, this payment wouldn't be processable, and you'd want
            // to handle that here.
        }
        
        // Update payPalConfig re accepting credit cards.
        self.payPalConfig.acceptCreditCards = self.acceptCreditCards;
        
        
        
        paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment
                                                                                                    configuration:self.payPalConfig
                                                                                                         delegate:self];
        [viewcontroller presentViewController:paymentViewController animated:YES completion:nil];
    }
    return self;
}


- (BOOL)acceptCreditCards {
    return self.payPalConfig.acceptCreditCards;
}

- (void)setAcceptCreditCards:(BOOL)acceptCreditCards {
    self.payPalConfig.acceptCreditCards = acceptCreditCards;
}

- (void)setPayPalEnvironment:(NSString *)environment {
    self.environment = environment;
    [PayPalMobile preconnectWithEnvironment:environment];
}


#pragma mark PayPalFuturePaymentDelegate methods

- (void)payPalFuturePaymentViewController:(PayPalFuturePaymentViewController *)futurePaymentViewController
                didAuthorizeFuturePayment:(NSDictionary *)futurePaymentAuthorization {
    NSLog(@"PayPal Future Payment Authorization Success!");
    self.resultText = [futurePaymentAuthorization description];
    //[self showSuccess];
    
    [self sendFuturePaymentAuthorizationToServer:futurePaymentAuthorization];
    [self.aViewcontroller dismissViewControllerAnimated:YES completion:nil];
}

- (void)payPalFuturePaymentDidCancel:(PayPalFuturePaymentViewController *)futurePaymentViewController {
    NSLog(@"PayPal Future Payment Authorization Canceled");
    //self.successView.hidden = YES;
    [self.aViewcontroller dismissViewControllerAnimated:YES completion:nil];
}

- (void)sendFuturePaymentAuthorizationToServer:(NSDictionary *)authorization {
    // TODO: Send authorization to server
    NSLog(@"Here is your authorization:\n\n%@\n\nSend this to your server to complete future payment setup.", authorization);
}


#pragma mark PayPalProfileSharingDelegate methods

- (void)payPalProfileSharingViewController:(PayPalProfileSharingViewController *)profileSharingViewController
             userDidLogInWithAuthorization:(NSDictionary *)profileSharingAuthorization {
    NSLog(@"PayPal Profile Sharing Authorization Success!");
    self.resultText = [profileSharingAuthorization description];
    //[self showSuccess];
    
    [self sendProfileSharingAuthorizationToServer:profileSharingAuthorization];
   // [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)userDidCancelPayPalProfileSharingViewController:(PayPalProfileSharingViewController *)profileSharingViewController {
    NSLog(@"PayPal Profile Sharing Authorization Canceled");
    //self.successView.hidden = YES;
   // [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)sendProfileSharingAuthorizationToServer:(NSDictionary *)authorization {
    // TODO: Send authorization to server
    NSLog(@"Here is your authorization:\n\n%@\n\nSend this to your server to complete profile sharing setup.", authorization);
}


#pragma mark PayPalPaymentDelegate methods

- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController didCompletePayment:(PayPalPayment *)completedPayment {
    NSLog(@"PayPal Payment Success!");
    self.resultText = [completedPayment description];
    
    
    NSDictionary * DictPayment = [[NSDictionary alloc]init];
    DictPayment = [completedPayment confirmation];
    
    NSLog(@"completedPayment id----%@",[[[completedPayment confirmation]valueForKey:@"response"] valueForKey:@"id"] );
          /*  @"transaction_id"    :[[[completedPayment confirmation]valueForKey:@"response"] valueForKey:@"id"],
           @"booking_amount"    :[completedPayment amount],
           @"state"             :[[[completedPayment confirmation]valueForKey:@"response"] valueForKey:@"state"],
           @"environment"       :[[[completedPayment confirmation]valueForKey:@"client"] valueForKey:@"environment"],
           @"response_type"     :[[completedPayment confirmation]valueForKey:@"response_type"] ,*/

    
    [self sendCompletedPaymentToServer:completedPayment]; // Payment was processed successfully; send to server for verification and fulfillment
    [self.aViewcontroller dismissViewControllerAnimated:YES completion:nil];
}


- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController {
    NSLog(@"PayPal Payment Canceled");
    self.resultText = nil;
    [self.aViewcontroller dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark Proof of payment validation
- (void)sendCompletedPaymentToServer:(PayPalPayment *)completedPayment {
    // TODO: Send completedPayment.confirmation to server
    NSLog(@"Here is your proof of payment:\n\n%@\n\nSend this to your server for confirmation and fulfillment.", completedPayment.confirmation);
}



-(void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController willCompletePayment:(PayPalPayment *)completedPayment completionBlock:(PayPalPaymentDelegateCompletionBlock)completionBlock
{

}



@end

//@class PaypalPayment;
//
//@interface PaypalPayment (PayPalPaymentDelegate)
//- (void)foo;
//@end
//@implementation PaypalPayment (PayPalPaymentDelegate)
//- (void)foo
//{
//}
//@end
