//
//  ChatModel.h
//  EXTEND
//
//  Created by Apple on 26/05/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChatModel : NSObject

@property(strong, nonatomic) NSString * chat_data;
@property(strong, nonatomic) NSString * chat_datatype;
@property(strong, nonatomic) NSString * chat_date;
@property(strong, nonatomic) NSString * chat_id;
@property(strong, nonatomic) NSString * chat_msg;
@property(strong, nonatomic) NSString * chat_reciver;
@property(strong, nonatomic) NSString * chat_sender;
@property(strong, nonatomic) NSString * name;
@property(strong, nonatomic) NSString * profile_picture;
@property(strong, nonatomic) NSString * read_status;

@end
