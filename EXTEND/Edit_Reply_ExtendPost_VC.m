//
//  Edit_Reply_ExtendPost_VC.m
//  EXTEND
//
//  Created by Apple on 18/03/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "Edit_Reply_ExtendPost_VC.h"
#import "UserModel.h"
#import "Networking.h"

@interface Edit_Reply_ExtendPost_VC ()
@property (weak, nonatomic) IBOutlet UIView *viewReplyBox;
@property (weak, nonatomic) IBOutlet UITextView *tv_Reply;
@end

@implementation Edit_Reply_ExtendPost_VC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = NO;
    _tv_Reply.layer.cornerRadius = self.tv_Reply.frame.size.height/2;
    _tv_Reply.clipsToBounds = YES;
    //self.tv_Reply.contentInset = UIEdgeInsetsMake(4,8,0,0);
    self.tv_Reply.textContainerInset = UIEdgeInsetsMake(15, 12, 0, 0);
    
    if (_isReply)
    {
        NSLog(@"Yes");
    }
    else
    {
        NSLog(@"No");
        self.tv_Reply.text = self.strComment;
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barTintColor = UIColorFromRGB(Color_NavigationBar);
    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(40, 0,SCREEN_MAX_LENGTH-40, 40)];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"Raleway" size:16];
    label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    label.textAlignment = NSTextAlignmentLeft;
    label.textColor =[UIColor whiteColor];
    label.text=@"Edit";
    self.navigationItem.titleView = label;
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back_icon"]style:UIBarButtonItemStylePlain target:self action:@selector(Back)];
    self.navigationItem.leftBarButtonItem = revealButtonItem;
}

-(void)Back
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma Common Alert
-(void)showAlertMessage:(NSString*)message Title:(NSString *)title
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               {
                               }];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (IBAction)btnSendAction:(id)sender
{
    if (_tv_Reply.text.length !=0 && ![_tv_Reply.text isEqualToString:@"Write a comment..."])
    {
        if (_isReply)
        {
            [self ReplywithExtendPostID:_strExtendPostID withReplyUserID:_strReplyUserID];
        }
        else
        {
            [self editExtendPostWithExtendPostID:_strExtendPostID];
        }
    }
    
}

-(void)ReplywithExtendPostID:(NSString *)extend_podt_id withReplyUserID:(NSString *)reply_user_id
{
    //{"apptoken":"d10289185f466003c450a15d300e50d5", "extend_post_id":"7","to_user_id":"95" , "from_user_id":"97" , "post_reply":"nice" }
    
    NSMutableDictionary * dictParam =[[NSMutableDictionary alloc] init];
    [dictParam setObject:[[UserModel sharedSingleton] getUserID] forKey:@"from_user_id"];
    [dictParam setObject:AppToken  forKey:@"apptoken"];
    [dictParam setObject:reply_user_id forKey:@"to_user_id"];
    [dictParam setObject:_tv_Reply.text forKey:@"post_reply"];
    [dictParam setObject:extend_podt_id forKey:@"extend_post_id"];
    NSLog(@"Reply Param : %@",dictParam);
    [Networking rawJsondataTaskwithURL:ExtendPostReply Param:dictParam ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error) {
        if (!error)
        {
            if ([[response valueForKey:@"status"]integerValue] == 1)
            {
                NSLog(@"Reply on Comment Data : %@",response);
                [self.navigationController popViewControllerAnimated:YES];
            }
            else if ([[response valueForKey:@"status"]integerValue] == 0)
            {
                
                [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Error];
            }
            
            
        }
        else
        {
            
        }
    }];
    
}

//{"apptoken":"d10289185f466003c450a15d300e50d5","extend_post_id":"16","extend_post":"new post update "}

-(void)editExtendPostWithExtendPostID:(NSString *)post_id
{
    NSMutableDictionary * dictParam =[[NSMutableDictionary alloc] init];
    //[dictParam setObject:[[UserModel sharedSingleton] getUserID] forKey:@"user_Id"];
    [dictParam setObject:AppToken  forKey:@"apptoken"];
    [dictParam setObject:_tv_Reply.text forKey:@"extend_post"];
    [dictParam setObject:post_id forKey:@"extend_post_id"];
    NSLog(@"Edit Param : %@",dictParam);
    //return;
    [Networking rawJsondataTaskwithURL:EditExtendPost Param:dictParam ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error) {
        if (!error)
        {
            if ([[response valueForKey:@"status"]integerValue] == 1)
            {
                NSLog(@"Edit Comment Data : %@",response);
                [self.navigationController popViewControllerAnimated:YES];
            }
            else if ([[response valueForKey:@"status"]integerValue] == 0)
            {
                
                [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Error];
            }
            
            
        }
        else
        {
            
        }
    }];
    
}


#pragma mark - Text View delegates

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSCharacterSet *doneButtonCharacterSet = [NSCharacterSet newlineCharacterSet];
    NSRange replacementTextRange = [text rangeOfCharacterFromSet:doneButtonCharacterSet];
    NSUInteger location = replacementTextRange.location;
    
    if (textView.text.length + text.length > 500){
        if (location != NSNotFound){
            [textView resignFirstResponder];
        }
        return NO;
    }
    else if (location != NSNotFound){
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    if (textView == _tv_Reply && [_tv_Reply.text isEqualToString:@"Write a comment..."]) {
        _tv_Reply.text = nil;
    }
    
    NSLog(@"Did begin editing");
    //  [self animateTextView:textView up:YES];
}

-(void)textViewDidChange:(UITextView *)textView
{
    NSLog(@"Did Change");
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    NSLog(@"Did End editing");
    
    if (textView == _tv_Reply && [_tv_Reply.text isEqualToString:@""]) {
        _tv_Reply.text = @"Write a comment...";
    }
    
    //[self animateTextView:textView up:NO];
}


-(BOOL)textViewShouldEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    return YES;
}

/*-(void)animateTextView:(UITextView*)textView up:(BOOL)up
 {
 
 int a;
 a= 580-(textView.frame.origin.y+400);
 
 const int movementDistance = a;
 const float movementDuration = 0.3f;
 
 int movement = (up ? movementDistance : -movementDistance);
 
 [UIView beginAnimations: @"animateTextField" context: nil];
 [UIView setAnimationBeginsFromCurrentState: YES];
 [UIView setAnimationDuration: movementDuration];
 self.view.frame = CGRectOffset(self.view.frame, 0, movement);
 [UIView commitAnimations];
 
 }*/

@end
