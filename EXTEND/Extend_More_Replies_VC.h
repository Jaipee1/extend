//
//  Extend_More_Replies_VC.h
//  EXTEND
//
//  Created by Apple on 18/03/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Extend_More_Replies_VC : UIViewController
@property (strong, nonatomic) NSString * strExtendPostID;
@property (strong, nonatomic) NSString * strReplyUserID;
@property (strong, nonatomic) NSString * strLocation;
@end
