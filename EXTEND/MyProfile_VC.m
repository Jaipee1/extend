//
//  MyProfile_VC.m
//  EXTEND
//
//  Created by Apple on 24/03/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "MyProfile_VC.h"

#import "UserModel.h"
#import "Networking.h"
#import "HelperClass.h"

#import "OptionCollectionCell.h"
#import "NotificationListVC.h"
#import "MeetRadarAnimation_VC.h"
#import "MyPostVC.h"

#import "REFrostedViewController.h"

#import <SDWebImage/UIImageView+WebCache.h>


@interface MyProfile_VC ()<UITextFieldDelegate,UIGestureRecognizerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    NSArray * arrCollectionData;
    UIAlertView *alertImagechoose;
    UIImagePickerController *ImgPicker;
    UIImage *IMAGE;
}

@property (weak, nonatomic) IBOutlet UIImageView *imgViewCover;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewProPic;
@property (weak, nonatomic) IBOutlet UILabel *lblUserFullName;
@property (weak, nonatomic) IBOutlet UIButton *btnEdit;


@property (weak, nonatomic) IBOutlet UIView *viewOption;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionOptions;

@property (weak, nonatomic) IBOutlet UIView *viewEditDetail;

@property (weak, nonatomic) IBOutlet UITextField *txtFirstName;
@property (weak, nonatomic) IBOutlet UITextField *txtLastName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmailAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtPhoneNumber;

@property (strong, nonatomic) IBOutlet UIView *viewResetPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtOldPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtNewPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtConfirmPassword;
@end

@implementation MyProfile_VC

- (void)viewDidLoad {
    [super viewDidLoad];
    //self.navigationController.navigationBar.hidden = YES;
    
    self.imgViewProPic.layer.cornerRadius = self.imgViewProPic.frame.size.width/2;
    self.imgViewProPic.layer.borderWidth = 1.5f ;
    self.imgViewProPic.layer.borderColor = [UIColor whiteColor].CGColor;
    self.imgViewProPic.clipsToBounds = YES;
    
    [self.imgViewProPic sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",UserProfilePictureBaseURL,[[UserModel sharedSingleton] getUserProfileImageUrl]]] placeholderImage:[UIImage imageNamed:@"upload_icon"]];
    [self.imgViewCover sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",UserProfilePictureBaseURL,[[UserModel sharedSingleton] getUserProfileImageUrl]]] placeholderImage:[UIImage imageNamed:@"upload_icon"]];
    
    self.lblUserFullName.text = [NSString stringWithFormat:@"%@ %@",[[UserModel sharedSingleton] getUserFirstName],[[UserModel sharedSingleton] getUserLastName]];
    
    [self setCollectionData];
    [self setValuesInTextfields];
    
    /*UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureRecognized:)];
    [self.view addGestureRecognizer:tapRecognizer];*/
    
    
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(ChangePicture:)];
    tap.delegate = self;
    self.imgViewProPic.userInteractionEnabled = YES;
    [self.imgViewProPic addGestureRecognizer:tap];
    
}

-(void)viewWillAppear:(BOOL)animated
{
  self.navigationController.navigationBar.hidden = YES;
    
   
}
-(IBAction)btnChangePicOrCamera:(id)sender
{
    //[self.viewResetPassword removeFromSuperview];
    
    alertImagechoose=[[UIAlertView alloc]initWithTitle:nil message:@"Please Select Image with" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Camera",@"Gallery", nil];
    [alertImagechoose show];
}

-(void)ChangePicture:(UITapGestureRecognizer *)sender
{
    //[self.viewResetPassword removeFromSuperview];
    
    NotificationListVC *objMyProfile_VC = [self.storyboard instantiateViewControllerWithIdentifier:@"NotificationListVC"];
    [self.navigationController pushViewController:objMyProfile_VC animated:YES ];
    
    alertImagechoose=[[UIAlertView alloc]initWithTitle:nil message:@"Please Select Image with" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Camera",@"Gallery", nil];
    [alertImagechoose show];
    
    //        UIView *view = sender.view;
    //        NSLog(@"%ld", (long)view.tag);//By tag, you can find out where you had tapped.
    //        //TAG_IV=(long)view.tag;
    
}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView==alertImagechoose)
    {
        
        if(buttonIndex == 1)
        {
            
            NSString *model = [[UIDevice currentDevice] model];
            if ([model isEqualToString:@"iPhone Simulator"] || [model isEqualToString:@"iPad Simulator"])
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!!!" message:@"No Camera found in Simulator!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
            else
            {
                ImgPicker = [[UIImagePickerController alloc] init];
                ImgPicker.delegate = self;
                
                ImgPicker.allowsEditing = YES;
                ImgPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                
                ImgPicker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
                [self presentViewController:ImgPicker animated:YES completion:nil];
                //   [self presentModalViewController: picker animated:YES];
                
                ImgPicker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
                
            }
            
        }
        
        if(buttonIndex == 2)
        {
            
            ImgPicker=[[UIImagePickerController alloc]init];
            ImgPicker.delegate=self;
            ImgPicker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
            ImgPicker.allowsEditing=true;
            
            [self presentViewController:ImgPicker animated:true completion:^{
                NSLog(@"piker");
                
                
            }];
            
        }
        
        
        
    }
    
}

-(void)imagePickerController:(UIImagePickerController *)picker
       didFinishPickingImage:(UIImage *)img
                 editingInfo:(NSDictionary *)editingInfo

{
    if ([picker isEqual:ImgPicker])
    {
        
    }
    [picker dismissModalViewControllerAnimated:YES];
    
    
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissModalViewControllerAnimated:YES];
    
}
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [[picker parentViewController] dismissViewControllerAnimated:YES completion:nil];
    
    //IMAGE = [info valueForKey:@"UIImagePickerControllerOriginalImage"];
    IMAGE=[info objectForKey:UIImagePickerControllerEditedImage];
    self.imgViewProPic.image=IMAGE;
    self.imgViewCover.image=IMAGE;
    
    [picker dismissViewControllerAnimated:YES completion:^{
        [self updateProfilePicture];
    }];
}

/*- (void)tapGestureRecognized:(UITapGestureRecognizer *)recognizer
{
    [self.viewResetPassword removeFromSuperview];
}*/

-(void)setValuesInTextfields
{
    self.txtFirstName.text = [[UserModel sharedSingleton] getUserFirstName];
    self.txtLastName.text = [[UserModel sharedSingleton] getUserLastName];
    self.txtEmailAddress.text = [[UserModel sharedSingleton] getUserEmailAddress];
    self.txtPassword.text = [[UserModel sharedSingleton] getUserPassword];
    self.txtPhoneNumber.text = [[UserModel sharedSingleton] getUserPhoneNumber];
    
    
}

-(void)setCollectionData
{
    arrCollectionData = [[NSArray alloc] init];
    
    arrCollectionData=@[@{@"image": @"post_icon",
                          @"title":@ "My Post"
                          },
                        @{ @"image": @"account_icon",
                           @"title":@ "Account Setting"
                           },
                        @{ @"image": @"chat_icon-1",
                           @"title":@ "Chat Setting"
                           },
                        @{ @"image": @"profile_meet_icon",
                           @"title":@ "Meet"
                           },
                        @{ @"image": @"profile_notifications_icon",
                           @"title":@ "Notification"
                           },
                        @{ @"image": @"profile_contact_icon",
                           @"title":@ "Contact"
                           }];
    
    [_collectionOptions reloadData];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark CollectionView DataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return arrCollectionData.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"optionCell";
    
    OptionCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];

    cell.imgViewOptions.image = [UIImage imageNamed:[[arrCollectionData valueForKey:@"image"] objectAtIndex:indexPath.row]];
    cell.lblOptionTitle.text = [[arrCollectionData valueForKey:@"title"] objectAtIndex:indexPath.row];
    [cell setUserInteractionEnabled:YES];
    
    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.item == 0)
    {
       /* MyPostVC *objMyPostVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MyPostVC"];
        [self.navigationController pushViewController:objMyPostVC animated:YES ];*/
    }
    else if (indexPath.item == 3)
    {
        MeetRadarAnimation_VC *objMeetRadarAnimation_VC = [self.storyboard instantiateViewControllerWithIdentifier:@"MeetRadarAnimation_VC"];
        [self.navigationController pushViewController:objMeetRadarAnimation_VC animated:YES ];
    }
    else if (indexPath.item == 4)
    {
        /*NotificationListVC *objMyProfile_VC = [self.storyboard instantiateViewControllerWithIdentifier:@"NotificationListVC"];
        [self.navigationController pushViewController:objMyProfile_VC animated:YES ];*/
    }
   

}

#pragma mark – UICollectionViewDelegateFlowLayout

// 1
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    //CGSize s = CGSizeMake([[UIScreen mainScreen] bounds].size.width/2 - 13, [[UIScreen mainScreen] bounds].size.height/2-70);
    CGSize s = CGSizeMake([[UIScreen mainScreen] bounds].size.width/2 - 13, 120);
    return s;
}

// 3
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(7, 7, 7, 7);
}
- (IBAction)btnBack:(id)sender
{
    //[self.navigationController popViewControllerAnimated:YES];
    [self.navigationController.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    [self.frostedViewController presentMenuViewController];
}
- (IBAction)btnEditAction:(UIButton *)sender
{
    //[self.viewResetPassword removeFromSuperview];
    
    if (sender.isSelected)
    {
        sender.selected = NO;
        [self.view bringSubviewToFront:self.viewOption];
    }
    else
    {
        sender.selected = YES;
        [self.view bringSubviewToFront:self.viewEditDetail];
    }
}


#pragma Mark-  Commaon Alertview

-(void)showAlertMessage:(NSString*)message Title:(NSString *)title
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               {
                               }];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(NSString *)validate
{
    NSString *strAlertMessage;
    if ([[HelperClass getValidString:self.txtFirstName.text] isEqualToString:@""])
    {
        strAlertMessage = @"Please Enter First Name";
    }
    else if ([[HelperClass getValidString:self.txtLastName.text] isEqualToString:@""])
    {
        strAlertMessage = @"Please Enter Last Name";
    }
    else if ([[HelperClass getValidString:self.txtPhoneNumber.text] isEqualToString:@""] || ![HelperClass NSStringIsValidNumber:self.txtPhoneNumber.text])
    {
        strAlertMessage = @"Please Enter Mobile Number";
    }
    else if ([[HelperClass getValidString:self.txtEmailAddress.text] isEqualToString:@""] || ![HelperClass NSStringIsValidEmail:self.txtEmailAddress.text])
    {
        strAlertMessage = @"Please Enter Email Address";
    }
    /*else if ([[HelperClass getValidString:self.txtPassword.text] isEqualToString:@""])
    {
        strAlertMessage = @"Please Enter Password";
    }*/
    
    return strAlertMessage;
}


-(IBAction)btnUpdateAction:(id)sender
{
    //[self.viewResetPassword removeFromSuperview];
    
    if ([self validate])
    {
        [self showAlertMessage:[self validate] Title:Alert_Title_Info];
    }
    else
    {
        [self updateProfile];
    }
}

-(void)updateProfile
{
    //{user_id:54,first_name:ctdev21,last_name:gautam,profile_pic:,phone_number:975865865895,apptoken:d10289185f466003c450a15d300e50d5}
    
    NSMutableDictionary * dictParam = [[NSMutableDictionary alloc] init];
    
    [dictParam setObject:[[UserModel sharedSingleton] getUserID] forKey:@"user_id"];
    [dictParam setObject:AppToken forKey:@"apptoken"];
    [dictParam setObject:self.txtFirstName.text forKey:@"first_name"];
    [dictParam setObject:self.txtLastName.text forKey:@"last_name"];
    [dictParam setObject:self.txtPhoneNumber.text forKey:@"phone_number"];
    
    NSLog(@"Update Dict Data :%@",dictParam);
    
    NSMutableArray * ImageArrayParameter = [[NSMutableArray alloc]init];
    NSMutableArray * ImageArray = [[NSMutableArray alloc]init];
    
    if (self.imgViewProPic.image !=nil)
    {
        [ImageArray addObject:self.imgViewProPic.image];
        [ImageArrayParameter addObject:@"profile_pic"];
    }
    
    [Networking dataTaskwithURL:UpdateProfile Param:dictParam ImageArray:ImageArray ImageParamArray:ImageArrayParameter compilation:^(id response, NSError *error) {
        if (!error)
        {
            NSLog(@"response Update Data :%@",response);
            
            if ([[response valueForKey:@"status"]integerValue] == 1)
            {
                
                
                NSData *data = [NSKeyedArchiver archivedDataWithRootObject:response];
                [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"UserLoginData"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [[UserModel sharedSingleton] setStrUserID:[response valueForKey:@"user_id"]];
                [[UserModel sharedSingleton] setStrUserFirstName:[response valueForKey:@"first_name"]];
                [[UserModel sharedSingleton] setStrUserLastName:[response valueForKey:@"last_name"]];
                [[UserModel sharedSingleton] setStrUserProfileImageUrl:[response valueForKey:@"image"]];
                [[UserModel sharedSingleton] setStrUserEmailAddress:[response valueForKey:@"email_id"]];
                [[UserModel sharedSingleton] setStrUserPhoneNumber:[response valueForKey:@"phone_number"]];
                [[UserModel sharedSingleton] setStrUserPassword:[response valueForKey:@"password"]];
                
                NSLog(@"First name :%@",[[UserModel sharedSingleton] getUserFirstName]);
                
                self.lblUserFullName.text = [NSString stringWithFormat:@"%@ %@",[[UserModel sharedSingleton] getUserFirstName],[[UserModel sharedSingleton] getUserLastName]];
                
                [self showAlertMessage:[response objectForKey:@"message"] Title:Alert_Title_Success];
                
            }
            else if ([[response valueForKey:@"status"]integerValue] == 0)
            {
                
                [self showAlertMessage:[response objectForKey:@"message"] Title:Alert_Title_Error];
            }
            
        }
    }];
    
    
}

-(void)updateProfilePicture
{
    NSMutableDictionary * dictParam = [[NSMutableDictionary alloc] init];
    
    [dictParam setObject:[[UserModel sharedSingleton] getUserID] forKey:@"user_id"];
    [dictParam setObject:AppToken forKey:@"apptoken"];

    NSLog(@"Update Dict Data :%@",dictParam);
    
    NSMutableArray * ImageArrayParameter = [[NSMutableArray alloc]init];
    NSMutableArray * ImageArray = [[NSMutableArray alloc]init];
    
    if (self.imgViewProPic.image !=nil)
    {
        [ImageArray addObject:self.imgViewProPic.image];
        [ImageArrayParameter addObject:@"profile_pic"];
    }
    
    [Networking dataTaskwithURL:UpdateProfile Param:dictParam ImageArray:ImageArray ImageParamArray:ImageArrayParameter compilation:^(id response, NSError *error) {
        if (!error)
        {
            NSLog(@"response Update Data :%@",response);
            
            if ([[response valueForKey:@"status"]integerValue] == 1)
            {
                
                
                NSData *data = [NSKeyedArchiver archivedDataWithRootObject:response];
                [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"UserLoginData"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [[UserModel sharedSingleton] setStrUserID:[response valueForKey:@"user_id"]];
                [[UserModel sharedSingleton] setStrUserFirstName:[response valueForKey:@"first_name"]];
                [[UserModel sharedSingleton] setStrUserLastName:[response valueForKey:@"last_name"]];
                [[UserModel sharedSingleton] setStrUserProfileImageUrl:[response valueForKey:@"image"]];
                [[UserModel sharedSingleton] setStrUserEmailAddress:[response valueForKey:@"email_id"]];
                [[UserModel sharedSingleton] setStrUserPhoneNumber:[response valueForKey:@"phone_number"]];
                [[UserModel sharedSingleton] setStrUserPassword:[response valueForKey:@"password"]];
                
                NSLog(@"First name :%@",[[UserModel sharedSingleton] getUserFirstName]);
                
                self.lblUserFullName.text = [NSString stringWithFormat:@"%@ %@",[[UserModel sharedSingleton] getUserFirstName],[[UserModel sharedSingleton] getUserLastName]];
                
                [self showAlertMessage:[response objectForKey:@"message"] Title:Alert_Title_Success];
                
            }
            else if ([[response valueForKey:@"status"]integerValue] == 0)
            {
                
                [self showAlertMessage:[response objectForKey:@"message"] Title:Alert_Title_Error];
            }
            
        }
    }];

}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == self.txtOldPassword || textField == self.txtNewPassword || textField == self.txtConfirmPassword )
    {
        
    }
    else
    {
        //[self.viewResetPassword removeFromSuperview];
    }
    
    if (textField == self.txtPassword)
    {
        /*[self.view addSubview:self.viewResetPassword];
        [self.viewResetPassword setCenter:self.view.center];
        self.viewResetPassword.layer.cornerRadius = 3 ;
        self.viewResetPassword.clipsToBounds = YES;*/
        [self.txtOldPassword becomeFirstResponder];
        
    }
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField;
{
    [textField resignFirstResponder];
    
    return YES;
}

-(NSString *)ValidatePassword
{
    NSString *strAlertMessage;
    if (self.txtOldPassword.text.length == 0)
    {
        strAlertMessage = @"Please Enter Old Password";
    }
    else if (self.txtNewPassword.text.length == 0)
    {
        strAlertMessage = @"Please Enter New Password";
    }
    else if (self.txtNewPassword.text.length < 7)
    {
        strAlertMessage = @"Please Enter 8 Digit New Password";
    }
    else if (self.txtConfirmPassword.text.length == 0)
    {
        strAlertMessage = @"Please Enter Confirm Password";
    }
    else if (![self.txtNewPassword.text isEqualToString:self.txtConfirmPassword.text])
    {
        strAlertMessage = @"Password do not match";
    }
    
    return strAlertMessage;
}

- (IBAction)btnResetAction:(id)sender
{
    
    
    if ([self ValidatePassword])
    {
        [self showAlertMessage:[self ValidatePassword] Title:Alert_Title_Info];
    }
    else
    {
        [self resetPassword];
    }
}

-(void)resetPassword
{
    //{"user_id":"54","old_password":"123456789","new_password":"12345678","apptoken":"d10289185f466003c450a15d300e50d5"}
    
    NSMutableDictionary * dictParam = [[NSMutableDictionary alloc] init];
    
    [dictParam setObject:[[UserModel sharedSingleton] getUserID] forKey:@"user_id"];
    [dictParam setObject:AppToken forKey:@"apptoken"];
    [dictParam setObject:self.txtOldPassword.text forKey:@"old_password"];
    [dictParam setObject:self.txtNewPassword.text forKey:@"new_password"];
    
    [Networking rawJsondataTaskwithURL:ResetPassword Param:dictParam ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error)
     {
         if (!error)
         {
             NSLog(@"response ResetPassword Data :%@",response);
             
             if ([[response valueForKey:@"code"] intValue] == 1)
             {
                 //[self.viewResetPassword removeFromSuperview];
                 [self showAlertMessage:[response objectForKey:@"message"] Title:Alert_Title_Success];
             }
             else if ([[response valueForKey:@"code"] intValue] == 0)
             {
                 [self showAlertMessage:[response objectForKey:@"message"] Title:Alert_Title_Error];
             }
         }
     }];
    
}

@end
