//
//  Edit_Reply_ExtendPost_VC.h
//  EXTEND
//
//  Created by Apple on 18/03/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Edit_Reply_ExtendPost_VC : UIViewController
@property  BOOL isReply;
@property (strong, nonatomic) NSString * strExtendPostID;
@property (strong, nonatomic) NSString * strReplyUserID;
@property (strong, nonatomic) NSString * strComment;
@end
