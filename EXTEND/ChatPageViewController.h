//
//  ChatPageViewController.h
//  EXTEND
//
//  Created by Apple on 25/05/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum : NSUInteger {
    calls,
    chat,
    contact,
} ChildViewControllers;

@interface ChatPageViewController : UIPageViewController<UIPageViewControllerDataSource, UIPageViewControllerDelegate>

-(void)showPageWithViewcontroller:(ChildViewControllers) child withCompletion:(BOOL) boolValue;

@end
