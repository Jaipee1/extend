//
//  CommentModel.h
//  EXTEND
//
//  Created by Apple on 30/03/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommentModel : NSObject

@property (nonatomic, retain) NSString * comment;
@property (nonatomic, retain) NSString * comment_id;
@property (nonatomic, retain) NSString * email_id;
@property (nonatomic, retain) NSString * first_name;
@property (nonatomic, retain) NSString * image;
@property (nonatomic, retain) NSString * last_name;
@property (nonatomic, retain) NSString * phone_number;
@property (nonatomic, retain) NSString * post_id;
@property (nonatomic, retain) NSString * posted_Date;
@property (nonatomic, retain) NSString * user_id;



@end

/*comment = nice;
"comment_id" = 2;
"email_id" = "eramit1880@gmail.com";
"first_name" = Amt;
image = "img58a1a9fe10438.jpg";
"last_name" = Soni;
"phone_number" = 0;
"post_id" = 43;
"posted_Date" = "Mar 29 (1 days ago)";
"user_id" = 54;*/
