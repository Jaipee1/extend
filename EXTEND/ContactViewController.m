//
//  ContactViewController.m
//  EXTEND
//
//  Created by Apple on 25/05/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "ContactViewController.h"
#import "FinalChatViewController.h"
#import "ContactTableViewCell.h"
#import "Networking.h"
#import "UserModel.h"
#import <SDWebImage/UIImageView+WebCache.h>


@interface ContactViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    ContactTableViewCell * cell;
    NSMutableArray * arrFriendsData;
}


@property (weak, nonatomic) IBOutlet UITableView *tblContactlist;

@end

@implementation ContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tblContactlist.delegate   = self;
    self.tblContactlist.dataSource = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)viewWillAppear:(BOOL)animated
{
    [self GetResentChat];
}

-(void)GetResentChat
{
    //{"apptoken":"d10289185f466003c450a15d300e50d5", "user_id":"170"  ,"limit":"0" }
    
    NSMutableDictionary * dictParam =[[NSMutableDictionary alloc] init];
    [dictParam setObject:[[UserModel sharedSingleton] getUserID] forKey:@"user_id"];
    [dictParam setObject:AppToken  forKey:@"apptoken"];
    [dictParam setObject:[NSNumber numberWithInteger:0]  forKey:@"limit"];
    
    [Networking rawJsondataTaskwithURL:FriendList Param:dictParam ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error) {
        if (!error)
        {
            
            NSLog(@"FriendList Data : %@",response);
            
            if ([[response valueForKey:@"status"]integerValue] == 1)
            {
                arrFriendsData = [[NSMutableArray alloc] init];
                arrFriendsData = [response valueForKey:@"message"];
                self.tblContactlist.delegate   = self;
                self.tblContactlist.dataSource = self;
                [self.tblContactlist reloadData];
                
            }
            else if ([[response valueForKey:@"status"]integerValue] == 0)
            {
                [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Error];
            }
            
        }
        else
        {
            
        }
    }];
    
}


#pragma Mark-  Commaon Alertview

-(void)showAlertMessage:(NSString*)message Title:(NSString *)title
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               {
                               }];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arrFriendsData.count;  //count number of row from counting array hear cataGorry is An Array
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"ContactTableViewCell";
    
    cell = (ContactTableViewCell *)  [self.tblContactlist dequeueReusableCellWithIdentifier:MyIdentifier forIndexPath:indexPath];
    
    
    cell.tag = indexPath.row;
    
    [cell.imgProPic sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",UserProfilePictureBaseURL,[[arrFriendsData objectAtIndex:indexPath.row] valueForKey:@"profile_picture"]]] placeholderImage:[UIImage imageNamed:@"upload_icon"]];
    cell.lblFullName.text = [NSString stringWithFormat:@"%@ %@",[[arrFriendsData objectAtIndex:indexPath.row] valueForKey:@"first_name"],[[arrFriendsData objectAtIndex:indexPath.row] valueForKey:@"last_name"]];
    
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    FinalChatViewController * objFinalChatViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"FinalChatViewController"];
    objFinalChatViewController.otherPersonUserID = [[arrFriendsData objectAtIndex:indexPath.row] valueForKey:@"id"];
    [self.navigationController pushViewController:objFinalChatViewController animated:YES];
    
    /*UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:objFinalChatViewController];
    [self presentViewController:nav animated:YES completion:nil];*/
    
}
/*- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
 {
 static NSString *MyIdentifier = @"NewsCell";
 NewsCell *cell = [self.tblNews dequeueReusableCellWithIdentifier:MyIdentifier];
 
 objNewsModel = [self.arrNewsModel objectAtIndex:indexPath.row];
 
 return cell.contentView.frame.size.height - 50 + [self heightForLabelwithText:objNewsModel.NewsContent withFont:[UIFont systemFontOfSize:cell.lblNewsPostDescription.font.pointSize] withWidth:cell.lblNewsPostDescription.frame.size.width] ;
 
 }*/
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.001/*self.viewheader.frame.size.height*/;
}


@end
