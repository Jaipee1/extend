//
//  SubscriptionPlanView.h
//  EXTEND
//
//  Created by Apple on 08/05/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubscriptionPlanView : UIView <UITableViewDelegate,UITableViewDataSource>
/*{
    IBOutlet UIButton *btnBuySilver;
    IBOutlet UIButton *btnBuyGold;
}*/

@property(nonatomic,weak) IBOutlet UIButton *btnBuySilver;
@property(nonatomic,weak) IBOutlet UIButton *btnBuyGold;
@property(nonatomic,weak) IBOutlet UITableView *tblSubscriptionPlans;

@end
