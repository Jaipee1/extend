//
//  FinalChatViewController.h
//  EXTEND
//
//  Created by Apple on 25/05/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FinalChatViewController : UIViewController
@property (strong, nonatomic) NSArray * arrOtherUserData;
@property (strong, nonatomic) NSString * otherPersonUserID;

@end
