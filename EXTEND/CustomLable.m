//
//  CustomLable.m
//  EXTEND
//
//  Created by Apple on 09/05/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "CustomLable.h"

@implementation CustomLable


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawTextInRect:(CGRect)rect {
    UIEdgeInsets insets = {0, 0, 0, -15};
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, insets)];
    [self sizeToFit];
}


@end
