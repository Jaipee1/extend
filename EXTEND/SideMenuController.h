//
//  SideMenuController.h
//  EXTEND
//
//  Created by Apple on 06/03/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideMenuController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>

#pragma mark TableView Object-

@property (weak, nonatomic) IBOutlet UITableView *table;

#pragma mark ImageView Object-
@property (weak, nonatomic) IBOutlet UIImageView *imagebottom;
@property (weak, nonatomic) IBOutlet UIImageView *imageMiddle;

@property (weak, nonatomic) IBOutlet UIImageView *imageview;

#pragma mark UserName Object-

@property (weak, nonatomic) IBOutlet UILabel *UserName;
@property int i;

@property int request;

@property (strong, nonatomic) UIWindow *window;


+(void)logout;

@end
