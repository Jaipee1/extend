//
//  AppDelegate.m
//  EXTEND
//
//  Created by Apple on 02/03/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "AppDelegate.h"
#import "IQKeyboardManager.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <GooglePlus/GooglePlus.h>
#import <AddressBook/AddressBook.h>


#import "CallViewController.h"
#import "VideoCallViewController.h"

#import "DEMORootViewController.h"
#import "UserModel.h"

#import "PaypalPayment.h"

#import <PushKit/PushKit.h>


//NSString * const UIApplicationDidReceiveRemoteNotification = @"UIApplicationDidReceiveRemoteNotification";

@import GoogleMaps;
@import GooglePlaces;
//

#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@import UserNotifications;
#endif



// Implement UNUserNotificationCenterDelegate to receive display notification via APNS for devices
// running iOS 10 and above. Implement FIRMessagingDelegate to receive data message via FCM for
// devices running iOS 10 and above.
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@interface AppDelegate () <UNUserNotificationCenterDelegate,SINClientDelegate, SINCallClientDelegate, SINManagedPushDelegate>
@end
#endif

// Copied from Apple's header in case it is missing in some cases (e.g. pre-Xcode 8 builds).
#ifndef NSFoundationVersionNumber_iOS_9_x_Max
#define NSFoundationVersionNumber_iOS_9_x_Max 1299
#endif

@interface AppDelegate ()<GPPDeepLinkDelegate>


@end

@implementation AppDelegate

static NSString * const kClientId = @"901881984765-d33mme5u65fhqnuegi751i86ira8jd9m.apps.googleusercontent.com";


-(void)getUserLoginDetails
{
    NSData *dictionaryData = [[NSUserDefaults standardUserDefaults] objectForKey:@"UserLoginData"];
    NSDictionary *dictionary = [NSKeyedUnarchiver unarchiveObjectWithData:dictionaryData];
    NSLog(@"dictionary :%@",dictionary);
    
    [[UserModel sharedSingleton] setStrUserID:[dictionary valueForKey:@"user_id"]];
    [[UserModel sharedSingleton] setStrUserFirstName:[dictionary valueForKey:@"first_name"]];
    [[UserModel sharedSingleton] setStrUserLastName:[dictionary valueForKey:@"last_name"]];
    [[UserModel sharedSingleton] setStrUserProfileImageUrl:[dictionary valueForKey:@"image"]];
    [[UserModel sharedSingleton] setStrUserEmailAddress:[dictionary valueForKey:@"email_id"]];
    [[UserModel sharedSingleton] setStrUserPhoneNumber:[dictionary valueForKey:@"phone_number"]];
    [[UserModel sharedSingleton] setStrUserPassword:[dictionary valueForKey:@"password"]];
    
    // FOR SINCH USER REGISTRATION (AUDIO & VIDEO CALL)
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UserDidLoginNotification"
                                                        object:nil
                                                      userInfo:@{
                                                                 @"userId" : [[UserModel sharedSingleton] getUserID]
                                                                 }];
    

}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    
    //// SINCH----------------
    
    
    [self handleLocalNotification:[launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey]];
    [[NSNotificationCenter defaultCenter] addObserverForName:@"UserDidLoginNotification"
                                                      object:nil
                                                       queue:nil
                                                  usingBlock:^(NSNotification *note) {
                                                      [self initSinchClientWithUserId:note.userInfo[@"userId"]];
                                                  }];
    
   
    [self voipRegistration];
    /////--------------SINCH
    
    [PayPalMobile initializeWithClientIdsForEnvironments:@{PayPalEnvironmentProduction : @"YOUR_CLIENT_ID_FOR_PRODUCTION",
                                                           PayPalEnvironmentSandbox : @"AVPXTBwp4jsn_9ApozL7RegIJW1yR5RgUcRvTeqbs2hWaZbPz3RGzmwf-QTKPs4_onBpkGwMC0S5o5kh"}];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isLogin"] == YES)
    {
        [self getUserLoginDetails];
        UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        DEMORootViewController *root=[mainStoryboard instantiateViewControllerWithIdentifier:@"rootController"];
        [navigationController pushViewController:root animated:true];
    }
    

    
    [GMSServices provideAPIKey:@"AIzaSyBV1pMoklztNaUy7uy2v20v66gcZ6hGNpM"];
    [GMSPlacesClient provideAPIKey:@"AIzaSyBV1pMoklztNaUy7uy2v20v66gcZ6hGNpM"];
    
    [GPPSignIn sharedInstance].clientID = kClientId;
    [GPPDeepLink setDelegate:self];
    [GPPDeepLink readDeepLinkAfterInstall];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
    

    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0){
        
        UIUserNotificationType types = UIUserNotificationTypeBadge |
        UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
        
        UIUserNotificationSettings *mySettings =
        [UIUserNotificationSettings settingsForTypes:types categories:nil];
        
        [application registerUserNotificationSettings:mySettings];
        [application registerForRemoteNotifications];
        
        
    }else{
        [application registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }
    
    
    
    // Register for remote notifications
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1) {
        // iOS 7.1 or earlier. Disable the deprecation warnings.
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        UIRemoteNotificationType allNotificationTypes =
        (UIRemoteNotificationTypeSound |
         UIRemoteNotificationTypeAlert |
         UIRemoteNotificationTypeBadge);
        [application registerForRemoteNotificationTypes:allNotificationTypes];
#pragma clang diagnostic pop
    } else {
        // iOS 8 or later
        // [START register_for_notifications]
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
            UIUserNotificationType allNotificationTypes =
            (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
            UIUserNotificationSettings *settings =
            [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
            [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        } else {
            // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
            UNAuthorizationOptions authOptions =
            UNAuthorizationOptionAlert
            | UNAuthorizationOptionSound
            | UNAuthorizationOptionBadge;
            [[UNUserNotificationCenter currentNotificationCenter]
             requestAuthorizationWithOptions:authOptions
             completionHandler:^(BOOL granted, NSError * _Nullable error) {
             }
             ];
            
            // For iOS 10 display notification (sent via APNS)
            [[UNUserNotificationCenter currentNotificationCenter] setDelegate:self];
            // For iOS 10 data message (sent via FCM)
            //[[FIRMessaging messaging] setRemoteMessageDelegate:self];
#endif
        }
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        // [END register_for_notifications]
        
    }

    
    return YES;
}

-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    
    
    NSString * token = [NSString stringWithFormat:@"%@", deviceToken];
    //Format token as you need:
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@">" withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@"<" withString:@""];
   
    
    NSString * strStart = @"\n\n\n+++++++++++++++++++++++##DEVICE TOKEN##+++++++++++++++++++++++++++++++++++\n\n\n";
    NSString * strEnd   = @"\n\n\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n\n\n";
    
     NSLog(@"DEVICE TOKEN :%@ %@ %@",strStart,token, strEnd);
    
    NSString *str_token=[NSString stringWithFormat:@"%@",token];
    [[NSUserDefaults standardUserDefaults] setObject:str_token forKey:@"Device_token"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //[_client registerPushNotificationData:deviceToken];
    //[self.sinch.push application:application didRegisterForRemoteNotificationsWithDeviceToken:deviceToken ];
    
}


- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error NS_AVAILABLE_IOS(3_0)
{
    NSLog(@"Error-----%@",error.description);
    [[NSUserDefaults standardUserDefaults] setObject:@"SIMULATOR IOS" forKey:@"Device_token"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    //[self handleLocalNotification:notification];
    if ([notification sin_isSinchNotification]){
        // This will trigger -[SINClientDelegate didReceiveIncomingCall:] if the notification
        // represents a call (i.e. contrast to that it may represent an instant-message)
        id<SINNotificationResult> result = [_client relayLocalNotification:notification];
        if (result.isCall && result.callResult.isTimedOut) {
            // The notification is related to an incoming call,
            // but was too old and the call has expired.
            // The call should be treated as a missed call and appropriate
            // action should be taken to communicate that to the user.
        }
    }
    
    NSLog(@"++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n\n\n");
    NSLog(@"LOCAL NOTIFICATION");
    NSLog(@"\n\n\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
}

- (void)requestUserNotificationPermission {
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        UIUserNotificationType types = UIUserNotificationTypeAlert | UIUserNotificationTypeSound;
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    }
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation] || [GPPURLHandler handleURL:url sourceApplication:sourceApplication annotation:annotation];
    
    //return [GPPURLHandler handleURL:url sourceApplication:sourceApplication annotation:annotation];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    /*BOOL backgroundAccepted = [[UIApplication sharedApplication] setKeepAliveTimeout:600 handler:^{ [self backgroundHandler]; }];
    if (backgroundAccepted)
    {
        NSLog(@"VOIP backgrounding accepted");
    }*/
}

/*- (void)backgroundHandler {
    
    NSLog(@"### -->VOIP backgrounding callback"); // What to do here to extend timeout?
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UserDidLoginNotification"
                                                        object:nil
                                                      userInfo:@{
                                                                 @"userId" : [[UserModel sharedSingleton] getUserID]
                                                                 }];
}*/

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSDKAppEvents activateApp];
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}



// Register for VoIP notifications
- (void) voipRegistration {
    dispatch_queue_t mainQueue = dispatch_get_main_queue();
    // Create a push registry object
    PKPushRegistry * voipRegistry = [[PKPushRegistry alloc] initWithQueue: mainQueue];
    // Set the registry's delegate to self
    voipRegistry.delegate = self;
    // Set the push type to VoIP
    voipRegistry.desiredPushTypes = [NSSet setWithObject:PKPushTypeVoIP];
}

// Handle updated push credentials
- (void)pushRegistry:(PKPushRegistry *)registry didUpdatePushCredentials: (PKPushCredentials *)credentials forType:(NSString *)type {
    // Register VoIP push token (a property of PKPushCredentials) with server
    
    [_client registerPushNotificationData:credentials.token];
}

// Handle incoming pushes
- (void)pushRegistry:(PKPushRegistry *)registry didReceiveIncomingPushWithPayload:(PKPushPayload *)payload forType:(NSString *)type {
    // Process the received push
    
    //notify
    
    NSLog(@"payload.dictionaryPayload: %@",payload.dictionaryPayload);
    
    NSDictionary* dic = payload.dictionaryPayload;
    NSString* sinchinfo = [dic objectForKey:@"sin"];
    if (sinchinfo == nil)
        return;
    UILocalNotification* notif = [[UILocalNotification alloc] init];
    notif.alertBody = @"incoming call ";
    [[UIApplication sharedApplication] presentLocalNotificationNow:notif];
    dispatch_async(dispatch_get_main_queue(), ^{
        [_client relayRemotePushNotificationPayload:sinchinfo];
    });
}



#pragma mark - GPPDeepLinkDelegate

- (void)didReceiveDeepLink:(GPPDeepLink *)deepLink {
    // An example to handle the deep link data.
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"Deep-link Data"
                          message:[deepLink deepLinkID]
                          delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil];
    [alert show];
}


// SINManagedPushDelegate
- (void)managedPush:(id<SINManagedPush>)unused didReceiveIncomingPushWithPayload:(NSDictionary *)payload forType:(NSString *)pushType {
    
    id<SINClient> client; // get previously created client
    [client relayRemotePushNotification:payload];
    
    NSLog(@"JAIPEEEEEEEEEEEEEE: %@",pushType);
}

- (void)handleRemoteNotification:(NSDictionary *)userInfo {
    if (!_client) {
        NSString *userId = [[UserModel sharedSingleton] getUserID];
        if (userId) {
            [self initSinchClientWithUserId:userId];
        }
    }
    [self.client relayRemotePushNotification:userInfo];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    NSLog(@"Remote notification");
    NSLog(@"userInfo %@",userInfo);
    //[self.sinch.push application:application didReceiveRemoteNotification:userInfo];
    [self handleRemoteNotification:userInfo];
    [self.client relayRemotePushNotification:userInfo];
    
    if ([[[userInfo valueForKey:@"aps"] valueForKey:@"alert"] valueForKey:@"data"] != [NSNull null])
    {
        /*NSMutableArray * arrData = [[NSMutableArray alloc] init];
        arrData = [[[userInfo valueForKey:@"aps"] valueForKey:@"alert"] valueForKey:@"data"];*/
        
        if ([[[[[userInfo valueForKey:@"aps"] valueForKey:@"alert"] valueForKey:@"data"] valueForKey:@"type"] isEqualToString:@"chat"])
        {
            [[NSNotificationCenter defaultCenter]
             postNotificationName:UIApplicationDidReceiveRemoteNotification
             object:self
             userInfo:userInfo];
            
            
        }
        else if ([[[[[userInfo valueForKey:@"aps"] valueForKey:@"alert"] valueForKey:@"data"] valueForKey:@"type"] isEqualToString:@"call"])
        {
            NSMutableDictionary * dictCallDetail = [[[userInfo valueForKey:@"aps"] valueForKey:@"alert"] valueForKey:@"data"];
            [[NSUserDefaults standardUserDefaults]setObject:dictCallDetail forKey:@"PushCallDetail"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
        
        
    }
    
}


- (SINLocalNotification *)client:(id<SINClient>)client localNotificationForIncomingCall:(id<SINCall>)call {
    SINLocalNotification *notification = [[SINLocalNotification alloc] init];
    notification.alertAction = @"Answer";
    notification.alertBody = [NSString stringWithFormat:@"Incoming call from %@", [call remoteUserId]];
    return notification;
}


#pragma mark -

- (void)initSinchClientWithUserId:(NSString *)userId {
    if (!_client) {
        _client = [Sinch clientWithApplicationKey:@"4d9f40c4-ca77-472d-9c5b-e86402a6bd3c"
                                applicationSecret:@"T/ufKZ46GUCwmo44aLctow=="
                                  environmentHost:@"sandbox.sinch.com"
                                           userId:userId];
        
        _client.delegate = self;
        [_client enableManagedPushNotifications];
        [_client setSupportCalling:YES];
        [_client start];
        [_client startListeningOnActiveConnection];
        [_client setSupportCalling:YES];
        [_client setSupportMessaging:YES];
        self.client.callClient.delegate = self;
        
        
        self.push = [Sinch managedPushWithAPSEnvironment:SINAPSEnvironmentAutomatic];
        self.push.delegate = self;
        [self.push setDesiredPushTypeAutomatically];
        [self.push registerUserNotificationSettings]; // This can be delayed to later in the app's life-cycle, e.g. once a user logs in

        
        //#####################################################################

        
        id config = [[SinchService configWithApplicationKey:@"4d9f40c4-ca77-472d-9c5b-e86402a6bd3c"
                                         applicationSecret:@"T/ufKZ46GUCwmo44aLctow=="
                                           environmentHost:@"sandbox.sinch.com"] pushNotificationsWithEnvironment:SINAPSEnvironmentAutomatic];
        
        id<SINService> sinch = [SinchService serviceWithConfig:config];
        sinch.delegate = self;
        sinch.callClient.delegate = self;

        self.sinch = sinch;
        //call.delegate = self;

    }
}

- (void)client:(id<SINCallClient>)client didReceiveIncomingCall:(id<SINCall>)call {
    UIViewController *top = self.window.rootViewController;
    if ([call.details isVideoOffered])
    {
        VideoCallViewController *controller = [top.storyboard instantiateViewControllerWithIdentifier:@"VideoCallViewController"];
        controller.call = call;
        [self.window.rootViewController presentViewController:controller animated:YES completion:nil];
    }
    else
    {
    CallViewController *controller = [top.storyboard instantiateViewControllerWithIdentifier:@"CallViewController"];
    controller.call = call;
    [self.window.rootViewController presentViewController:controller animated:YES completion:nil];
    }
}

- (void)handleLocalNotification:(UILocalNotification *)notification {
    if (notification) {
        id<SINNotificationResult> result = [self.client relayLocalNotification:notification];
        if ([result isCall] && [[result callResult] isTimedOut]) {
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:@"Missed call"
                                  message:[NSString stringWithFormat:@"Missed call from %@", [[result callResult] remoteUserId]]
                                  delegate:nil
                                  cancelButtonTitle:nil
                                  otherButtonTitles:@"OK", nil];
            [alert show];
        }
    }
}

#pragma mark - SINClientDelegate

- (void)clientDidStart:(id<SINClient>)client {
    NSLog(@"Sinch client started successfully (version: %@)", [Sinch version]);
    ///add the VoIP registration
    [self voipRegistration];
}

- (void)clientDidFail:(id<SINClient>)client error:(NSError *)error {
    NSLog(@"Sinch client error: %@", [error localizedDescription]);
}

- (void)client:(id<SINClient>)client
    logMessage:(NSString *)message
          area:(NSString *)area
      severity:(SINLogSeverity)severity
     timestamp:(NSDate *)timestamp {
    NSLog(@"%@", message);
}



/////////////

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"VideocallView"])
    {
        VideoCallViewController *VideoCallViewController = [segue destinationViewController];
        VideoCallViewController.call = sender;
    }
    else if ([segue.identifier isEqualToString:@"callView"])
    {
        CallViewController *callViewController = [segue destinationViewController];
        callViewController.call = sender;
    }
    
    
}

#pragma mark - SINCallClientDelegate



@end
