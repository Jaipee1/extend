//
//  TextChatTableViewCell.m
//  EXTEND
//
//  Created by Apple on 26/05/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "TextChatTableViewCell.h"

@implementation TextChatTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.tvBGView.layer.cornerRadius = 15;
    self.tvBGView.clipsToBounds = YES;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
