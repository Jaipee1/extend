//
//  MapPostModel.h
//  EXTEND
//
//  Created by Apple on 09/05/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MapPostModel : NSObject

@property (nonatomic, retain) NSString * descriptionPost;
@property (nonatomic, retain) NSString * image;
@property (nonatomic, retain) NSString * post_status;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * location_latitude;
@property (nonatomic, retain) NSString * location_longitude;
@property (nonatomic, retain) NSString * postID;

@end

/*
 description = "Jakes ";
 id = 29;
 image = "7491494306250.jpg";
 "location_latitude" = "22.725481";
 "location_longitude" = "75.890823";
 "post_status" = Active;
 "sys_date" = "2017-05-09 05:04:10";
 title = "hiss ";
 "user_id" = 93;
 */
