//
//  VideoAndImageTableViewCell.m
//  EXTEND
//
//  Created by Apple on 26/05/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "VideoAndImageTableViewCell.h"

@implementation VideoAndImageTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.viewBG.layer.cornerRadius = 15;
    self.viewBG.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
