//
//  SubscriptionTableViewCell.m
//  EXTEND
//
//  Created by Apple on 02/06/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "SubscriptionTableViewCell.h"

@implementation SubscriptionTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    _btnBuyPlan.layer.cornerRadius = _btnBuyPlan.frame.size.height/2;
    _btnBuyPlan.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
