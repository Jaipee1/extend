//
//  HomeTableViewCell.h
//  EXTEND
//
//  Created by Apple on 03/03/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "TTFaveButton.h"

@interface HomeTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgViewProfilePicture;
@property (weak, nonatomic) IBOutlet UILabel *lblUsername;
@property (strong, nonatomic) IBOutlet UILabel *lblPostTime;
@property (weak, nonatomic) IBOutlet UILabel *lblMsgCaption;
@property (weak, nonatomic) IBOutlet UILabel *lblLikeCount;
@property (weak, nonatomic) IBOutlet UILabel *lblCommentCount;
@property (weak, nonatomic) IBOutlet UIImageView *ImgViewPost;
@property (weak, nonatomic) IBOutlet UIButton *btnShare;
@property (weak, nonatomic) IBOutlet UIButton *btnComment;
@property (weak, nonatomic) IBOutlet TTFaveButton *btnLike;
@property (weak, nonatomic) IBOutlet UIButton *btnPlay;
@property (retain, nonatomic) MPMoviePlayerController * movie;
@property (assign) BOOL isVideo;
@property (weak, nonatomic) IBOutlet UIButton *btnEditDeletePost;

@end
