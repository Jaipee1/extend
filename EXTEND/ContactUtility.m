//
//  ContactUtility.m
//  EXTEND
//
//  Created by Apple on 15/04/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "ContactUtility.h"

@implementation ContactUtility

+ (id)createContactWithFirst:(NSString *)firstName Last:(NSString *)lastName
                MobileNumber:(NSString *)mobileNumber HomeNumber:(NSString *)homeNumber
{
    ContactUtility *newContact = [[self alloc] init];
    [newContact setFirstName:firstName];
    [newContact setLastName:lastName];
    [newContact setMobileNumber:mobileNumber];
    [newContact setHomeNumber:homeNumber];
    return newContact;
}

@end
