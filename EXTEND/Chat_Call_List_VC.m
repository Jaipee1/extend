//
//  Chat_Call_List_VC.m
//  EXTEND
//
//  Created by Apple on 06/03/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "Chat_Call_List_VC.h"
#import "REFrostedViewController.h"
#import "ChatPageViewController.h"


@interface Chat_Call_List_VC ()
@property (weak, nonatomic) IBOutlet UIView *ContainerView;
@property (strong, nonatomic) ChatPageViewController * pageController;
@property (weak, nonatomic) IBOutlet UIView * callBGView, *ChatBGView , *contactBGView;
@end

@implementation Chat_Call_List_VC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBar];
    // Do any additional setup after loading the view.
    
    self.pageController = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatPageViewController"];
    
    //self.pageController.e
    self.pageController.view.frame = self.ContainerView.bounds;
    [self.pageController willMoveToParentViewController:self];
    //self.pageController.willMove(toParentViewController: self)
    [self.ContainerView addSubview:self.pageController.view];
    //self.containerView.addSubview(self.pageController.view)
    [self addChildViewController:self.pageController];
    //self.addChildViewController(self.pageController)
    [self.pageController didMoveToParentViewController:self];
    //self.pageController.didMove(toParentViewController: self)
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = NO;
}

-(void)setNavigationBar
{
    self.navigationController.navigationBar.hidden = NO;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(40, 0,SCREEN_MAX_LENGTH-40, 40)];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"Raleway" size:16];
    label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    label.textAlignment = NSTextAlignmentLeft;
    label.textColor =[UIColor whiteColor];
    label.text=@"Chat";
    self.navigationItem.titleView = label;
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu_icon"]style:UIBarButtonItemStylePlain target:self action:@selector(showmenu)];
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    
    /*UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"setting_icon"]style:UIBarButtonItemStylePlain target:self action:@selector(setting)];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;*/
}

-(ChildViewControllers)childViewController:(NSInteger)tag
{
    //ChildViewControllers CC;
    
    NSInteger vc = calls;
    
    switch (tag) {
        case 1:
            vc = chat;
            break;
        case 2:
            vc = contact;
            break;
            
        default:
            break;
    }
    
    return vc;

}

-(IBAction)segmentAction:(UIButton *)sender
{
    NSInteger tag = sender.tag;
    NSInteger vc = [self childViewController:tag];
    [self.pageController showPageWithViewcontroller:vc withCompletion:nil];
    [self viewBackgroundColourWithTag:tag];
    //self.pageController.showPage(vc, compilation: nil)

}

-(void)viewBackgroundColourWithTag:(NSInteger)tag
{
   //90  12  255
    UIColor * color;
    
    color = [UIColor colorWithRed:90.0/255.0 green:12.0/255.0 blue:255.0/255.0 alpha:1.0];
    self.callBGView.backgroundColor = self.ChatBGView.backgroundColor = self.contactBGView.backgroundColor = color;
    color = [UIColor colorWithRed:30.0/255.0 green:12.0/255.0 blue:255.0/255.0 alpha:1.0];
    switch (tag) {
        case 0:
            self.callBGView.backgroundColor = color;
            break;
        case 1:
            self.ChatBGView.backgroundColor = color;
            break;
        case 2:
            self.contactBGView.backgroundColor = color;
            break;
            
        default:
            break;
    }
    
    
}

- (void)showmenu
{
    [self.navigationController.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    [self.frostedViewController presentMenuViewController];
}


@end
