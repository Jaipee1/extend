//
//  Login_vc.m
//  EXTEND
//
//  Created by Apple on 02/03/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "Login_vc.h"
#import "Home_VC.h"

#import "HelperClass.h"
#import "Networking.h"
#import "UserModel.h"
#import "DEMORootViewController.h"
#import "AddressBook.h"
#import "ContactUtility.h"

#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKLoginKit/FBSDKLoginManager.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <GoogleOpenSource/GoogleOpenSource.h>
#import <GooglePlus/GooglePlus.h>
#import <AddressBook/AddressBook.h>

#import "AppDelegate.h"

@interface Login_vc ()<UITextFieldDelegate,GPPSignInDelegate>
{
    FBSDKLoginManager *FBLoginManager;
    UserModel *UserModelObj  ;
    NSMutableDictionary * dictParamSocial;
}
@property (weak, nonatomic) IBOutlet UIImageView *ImgViewProfilePicture;
@property (weak, nonatomic) IBOutlet UITextField *txtEmailAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (strong, nonatomic) IBOutlet UIView *viewForgotPassword;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollMain;
@property (weak, nonatomic) IBOutlet UITextField *txtEmailAddressForForgetPassword;

@property (strong, nonatomic) IBOutlet UIView *viewMobileNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtMobileNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtSetPassword;

@property (strong, nonatomic) IBOutlet UIView *ViewSynchonizeContact;

@end

@implementation Login_vc

static NSString * const kClientId = @"901881984765-d33mme5u65fhqnuegi751i86ira8jd9m.apps.googleusercontent.com";

-(void)viewWillAppear:(BOOL)animated
{
    
    self.navigationController.navigationBar.hidden = YES;
    self.navigationController.navigationBar.barTintColor = UIColorFromRGB(Color_NavigationBar);
    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
    [self.navigationController.navigationBar setTranslucent:NO];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UserModelObj = [UserModel sharedSingleton];
    
    // Do any additional setup after loading the view.
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
    [self.scrollMain addGestureRecognizer:singleTap];
    
    [GPPSignInButton class];
    
    GPPSignIn *signIn = [GPPSignIn sharedInstance];
    signIn.shouldFetchGooglePlusUser = YES;
    signIn.shouldFetchGoogleUserEmail = YES;
    signIn.clientID = kClientId;
    
    // Sync the current sign-in configurations to match the selected
    // app activities in the app activity picker.
    if (signIn.actions) {
        
        for (NSString *appActivity in signIn.actions) {
            
        }
    }
    
    signIn.delegate = self;
    //[signIn trySilentAuthentication];
    
    

}

- (void)singleTapGestureCaptured:(UITapGestureRecognizer *)gesture
{
    [self RemoveForgetPasswordView];
}


-(IBAction)btnGooglePlusAction:(id)sender
{
    
    /*AppDelegate *app=[UIApplication sharedApplication].delegate;
     app.flag=NO;
     hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
     hud.labelText = @"Please wait...";
     hud.dimBackground = YES;
     
     NSUserDefaults *ref=[NSUserDefaults standardUserDefaults];
     self.deviceToken = [ref valueForKey: @"deviceToken"];
     [ref synchronize];*/
    
    [self adoptUserSettings];
    [self reportAuthStatus];
}

#pragma mark - Helper methods

- (void)adoptUserSettings
{
    GPPSignIn *signIn = [GPPSignIn sharedInstance];
    
    // There should only be one selected color scheme
    // There may be multiple app activity types supported
    NSMutableArray *supportedAppActivities = [[NSMutableArray alloc] init];
    NSString *schema =
    [NSString stringWithFormat:@"http://schemas.google.com/CommentActivity"];
    [supportedAppActivities addObject:schema];
    
    //http://schemas.google.com/CommentActivity
    [signIn authenticate];
}

- (void)reportAuthStatus
{
    if ([GPPSignIn sharedInstance].authentication)
    {
        NSLog(@"Status: Authenticated");
    } else
    {
        // To authenticate, use Google+ sign-in button.
        NSLog(@"Status: Not authenticated");
    }
    [self refreshUserInfo];
}


#pragma mark - UIAlertView Delegate
- (void)finishedWithAuth:(GTMOAuth2Authentication *)auth
                   error:(NSError *)error
{
    if (error)
    {
        
        NSString *err =
        [NSString stringWithFormat:@"Status: Authentication error: %@", error];
        NSLog(@"%@",err);
        return;
    }
    [self reportAuthStatus];
}

- (void)didDisconnectWithError:(NSError *)error
{
    
    NSString *err;
    
    if (error)
    {
        err =
        [NSString stringWithFormat:@"Status: Failed to disconnect: %@", error];
    } else
    {
        err=
        [NSString stringWithFormat:@"Status: Disconnected"];
    }
    NSLog(@"%@",err);
    [self refreshUserInfo];
}

- (void)presentSignInViewController:(UIViewController *)viewController
{
    [[self navigationController] pushViewController:viewController animated:YES];
}


- (void)refreshUserInfo
{
    if ([GPPSignIn sharedInstance].authentication == nil)
    {
        //self.lbl_UserName.text = kPlaceholderUserName;
        //self.lbl_Email.text = kPlaceholderEmailAddress;
        // self.userAvatar.image = [UIImage imageNamed:kPlaceholderAvatarImageName];
        //NSLog(@"%@ %@",lbl_UserName.text,lbl_Email.text);
        return;
    }
    
    //self.lbl_Email.text = [GPPSignIn sharedInstance].userEmail;
    
    NSLog(@"[GPPSignIn sharedInstance].userEmail---%@",[GPPSignIn sharedInstance].userEmail);
    
    // The googlePlusUser member will be populated only if the appropriate
    // scope is set when signing in.
    GTLPlusPerson *person = [GPPSignIn sharedInstance].googlePlusUser;
    if (person == nil)
    {
        return;
    }
    GTLPlusPerson *userProfile = person;
    // Get what we want
    NSArray * userEmails = userProfile.emails;
    NSString * email = ((GTLPlusPersonEmailsItem *)[userEmails objectAtIndex:0]).value;
    NSString * username1 = userProfile.displayName;
    NSString * profileId = userProfile.identifier;
    
    dictParamSocial =[[NSMutableDictionary alloc] init];
    [dictParamSocial setObject:email  forKey:@"email_id"];
    [dictParamSocial setObject:username1 forKey:@"first_name"];
    [dictParamSocial setObject:@"" forKey:@"last_name"];
    [dictParamSocial setObject:@"" forKey:@"fb_id"];
    [dictParamSocial setObject:profileId forKey:@"google_id"];
    [dictParamSocial setObject:person.image.url forKey:@"social_profile_pic"];
    
    [self checkEmailwithEmailID:email];

    
    NSLog(@"userProfile---%@",userProfile.description);
    NSLog(@"userProfile---%@",profileId);
    
   /* dispatch_queue_t backgroundQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(backgroundQueue, ^{
        NSData *avatarData = nil;
        NSString *imageURLString = person.image.url;
        if (imageURLString) {
            NSURL *imageURL = [NSURL URLWithString:imageURLString];
            avatarData = [NSData dataWithContentsOfURL:imageURL];
        }
        
        if (avatarData) {
            // Update UI from the main thread when available
            dispatch_async(dispatch_get_main_queue(), ^{
                //  self.userAvatar.image = [UIImage imageWithData:avatarData];
                [[GPPSignIn sharedInstance] signOut];
                
                //return ;
                
                
                
                NSDictionary * DICT=[[NSDictionary alloc]init];
                DICT=@{@"app_token":AppToken,
                       @"user_email":email,
                       @"user_name":username1,
                       @"google_id":profileId,
                       @"social_login_type":@"3",
                       @"device_token":[[NSUserDefaults standardUserDefaults]valueForKey:@"Device_token"],
                       @"os_type":@"ios",
                       };
                
                UIImage * image = [UIImage imageWithData:avatarData];
                
                NSMutableArray * arrImage = [[NSMutableArray alloc] init];
                [arrImage addObject:image];
                
                NSMutableArray * arrImageParam = [[NSMutableArray alloc] init];
                [arrImageParam addObject:@"user_image"];
                
                
                
                
                //TabBarVC * objTabBarVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBarVC"];
                //[self.navigationController pushViewController:objTabBarVC animated:YES];
            });
        }
    });*/
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString *)validate
{
    NSString *strAlertMessage;
    
    if ([[HelperClass getValidString:self.txtEmailAddress.text] isEqualToString:@""] || ![HelperClass NSStringIsValidEmail:self.txtEmailAddress.text])
    {
        strAlertMessage = @"Please Enter Email Address";
    }
    else if ([[HelperClass getValidString:self.txtPassword.text] isEqualToString:@""])
    {
        strAlertMessage = @"Please Enter Password";
    }
    
    return strAlertMessage;
}

#pragma Mark-  Commaon Alertview

-(void)showAlertMessage:(NSString*)message Title:(NSString *)title
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               {
                               }];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (IBAction)btnLoginAction:(id)sender
{
    if ([self validate])
    {
        [self showAlertMessage:[self validate] Title:Alert_Title_Info];
    }
    else
    {
        // Parameter  {"email_id":"tensakjh@gmail.com","password":"123456","apptoken":"d10289185f466003c450a15d300e50d5"}
       // device_token
        [self loginwithEmailAddress:self.txtEmailAddress.text withPassword:self.txtPassword.text];
    }
        
}

-(void)loginwithEmailAddress:(NSString *)emailAddress withPassword:(NSString *)password
{
    NSDictionary * DictLogin=[[NSDictionary alloc]init];
    DictLogin=@{@"apptoken":AppToken,
                @"email_id":emailAddress,
                @"password":password,
                @"ios_device_token":[[NSUserDefaults standardUserDefaults]valueForKey:@"Device_token"],
                @"device_token":@"",
                };
    NSLog(@"DictLogin---%@",DictLogin);
    
    [Networking rawJsondataTaskwithURL:Login Param:DictLogin ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error)
     {
         if (!error)
         {
             NSLog(@"response----%@",response);
             
             if ([[response valueForKey:@"success"]integerValue] == 1)
             {
                 
                 
                 [UserModelObj setStrUserID:[response valueForKey:@"user_id"]];
                 [UserModelObj setStrUserFirstName:[response valueForKey:@"first_name"]];
                 [UserModelObj setStrUserLastName:[response valueForKey:@"last_name"]];
                 [UserModelObj setStrUserProfileImageUrl:[response valueForKey:@"image"]];
                 [UserModelObj setStrUserEmailAddress:[response valueForKey:@"email_id"]];
                 [UserModelObj setStrUserPhoneNumber:[response valueForKey:@"phone_number"]];
                 [[UserModel sharedSingleton] setStrUserPassword:[response valueForKey:@"password"]];
                 
                 [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isLogin"];
                 
                 NSLog(@"get user id---%@",[UserModelObj getUserID]);
                 
                 NSData *data = [NSKeyedArchiver archivedDataWithRootObject:response];
                 [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"UserLoginData"];
                 [[NSUserDefaults standardUserDefaults] synchronize];
                 
                 
                 // FOR SINCH USER REGISTRATION (AUDIO & VIDEO CALL)
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"UserDidLoginNotification"
                                                                     object:nil
                                                                   userInfo:@{
                                                                              @"userId" : [response valueForKey:@"user_id"]
                                                                              }];
                 
                 DEMORootViewController *root=[self.storyboard instantiateViewControllerWithIdentifier:@"rootController"];
                 [self.navigationController pushViewController:root animated:true];
                 
                 /* Home_VC * objHome_VC = [self.storyboard instantiateViewControllerWithIdentifier:@"Home_VC"];
                  [self.navigationController pushViewController:objHome_VC animated:YES];*/
                 
             }
             else if ([[response valueForKey:@"success"]integerValue] == 0)
             {
                 
                 [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Error];
             }
             
         }
     }];
    
}




- (IBAction)btnSignUpAction:(id)sender
{
    
}

- (IBAction)btnForgotPasswordAction:(id)sender
{
    [self.viewMobileNumber removeFromSuperview];
    [self.ViewSynchonizeContact removeFromSuperview];
    
    [self.view addSubview:self.viewForgotPassword];
    [self.viewForgotPassword setCenter:self.view.center];
    self.viewForgotPassword.layer.cornerRadius = 3 ;
    self.viewForgotPassword.clipsToBounds = YES;
    [self.txtEmailAddressForForgetPassword becomeFirstResponder];
    
}

-(NSString *)ValidateEmailAddressForForgetPassword
{
    NSString *strAlertMessage;
    
    if ([[HelperClass getValidString:self.txtEmailAddressForForgetPassword.text] isEqualToString:@""] || ![HelperClass NSStringIsValidEmail:self.txtEmailAddressForForgetPassword.text])
    {
        strAlertMessage = @"Please Enter Email Address";
    }
    return strAlertMessage;

}

- (IBAction)btnOkAction:(id)sender
{
    if ([self ValidateEmailAddressForForgetPassword])
    {
        [self showAlertMessage:[self ValidateEmailAddressForForgetPassword] Title:Alert_Title_Info];
    }
    else
    {
        
        // Parameter  {"email_id":"ctdev21@gmail.com","apptoken":"d10289185f466003c450a15d300e50d5"}
        
        NSDictionary * Dict=[[NSDictionary alloc]init];
        Dict=@{@"apptoken":AppToken,
                    @"email_id":self.txtEmailAddressForForgetPassword.text,
                    };
        
        [Networking rawJsondataTaskwithURL:ForgotPassword Param:Dict ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error){
            if (!error)
            {
                NSLog(@"response----%@",response);
                
                if ([[response valueForKey:@"status"]integerValue] == 1)
                {
                    /*UIAlertView * Alert = [[UIAlertView alloc]initWithTitle:@"Success" message:[response valueForKey:@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                     [Alert show];*/
                    
                    /*  UserModelObj = [UserModel sharedMySingleton];
                     
                     //NSString * StrUserID = [NSString stringWithFormat:@"%@",[[response valueForKey:@"msg"] valueForKey:@"user_id"] ];
                     [UserModelObj setUserID:[[response valueForKey:@"msg"] valueForKey:@"user_id"]];
                     [UserModelObj setContactNumber:[[response valueForKey:@"msg"] valueForKey:@"user_contact"]];
                     [UserModelObj setEmailAddress:[[response valueForKey:@"msg"] valueForKey:@"user_email"]];
                     [UserModelObj setUsername:[[response valueForKey:@"msg"] valueForKey:@"user_name"]];
                     [UserModelObj setImageUrl:[[response valueForKey:@"msg"] valueForKey:@"user_image"]];
                     [UserModelObj setUserType:[[response valueForKey:@"msg"] valueForKey:@"user_type"]];
                     [UserModelObj setAddress:[[response valueForKey:@"msg"] valueForKey:@"user_address"]];
                     [UserModelObj setIsLogedIn:YES];
                     //NSLog(@"get data---%@",[UserModelObj getUserID]);*/
                    
                    
                    /*TabBarVC * objTabBarVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBarVC"];
                     [self.navigationController pushViewController:objTabBarVC animated:YES];*/
                    
                    [self showAlertMessage:@"Please Chaeck Your Mail For Update Password" Title:Alert_Title_Success];
                }
                else if ([[response valueForKey:@"status"]integerValue] == 2)
                {
                    //                    UIAlertView * Alert = [[UIAlertView alloc]initWithTitle:@"Error" message:[response valueForKey:@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    //                    [Alert show];
                    
                    [self showAlertMessage:[self validate] Title:Alert_Title_Error];
                }
                
            }
        }];
        [self RemoveForgetPasswordView];
    }
    
}

-(void)RemoveForgetPasswordView
{
    [self.viewForgotPassword removeFromSuperview];
    [self.ViewSynchonizeContact removeFromSuperview];
    [self.viewMobileNumber removeFromSuperview];
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField;
{
    [textField resignFirstResponder];
    
    return YES;
}

#pragma  FB Method

-(IBAction)btnFacbookAction:(id)sender
{
    [self ConnectToFB];
}

-(void)ConnectToFB
{
    
    FBLoginManager = [[FBSDKLoginManager alloc] init];
    [FBLoginManager logOut];
    
    [FBLoginManager logInWithReadPermissions:@[@"public_profile",@"email",@"user_friends"] fromViewController:nil handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error)
        {
            UIAlertView * Alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Please Try Again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [Alert show];
        }
        else if (result.isCancelled)
        {
            UIAlertView * Alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Please Try Again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [Alert show];
        }
        else
        {
            NSLog(@"FBSDKLoginManagerLoginResult----%@ ",result);
            
            if ([result.grantedPermissions containsObject:@"email"])
            {
                
                if ([FBSDKAccessToken currentAccessToken])
                {
                    
                    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{ @"fields": @"id,birthday,first_name,last_name,email,location,gender,picture"}]
                     
                     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                         
                         if (!error)
                         {
                             NSLog(@"fb user %@", result);
                             //responseID=[result valueForKey:@"id"];
                             //responseFullName=[result valueForKey:@"first_name"];
                             NSLog(@"%@",[result valueForKey:@"id"]);
                             NSLog(@"%@",[result valueForKey:@"first_name"]);
                             NSLog(@"Image:%@",[[[result valueForKey:@"picture"] valueForKey:@"data"] valueForKey:@"url"]);
                             //NSString *strEmail;
                             //UIImage * image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[[[result valueForKey:@"picture"] valueForKey:@"data"] valueForKey:@"url"]]]];
                             NSString * strEmail;
                             NSString * strFirstName;
                             NSString * strLastName;
                             if ([result[@"email"] isEqualToString:@""] || result[@"email"] == nil)
                             {
                                 strEmail = @"";
                             }
                             else
                             {
                                 strEmail = result[@"email"];
                             }
                             
                             if ([result[@"first_name"] isEqualToString:@""] || result[@"first_name"] == nil)
                             {
                                 strFirstName = @"";
                             }
                             else
                             {
                                 strFirstName = result[@"first_name"];
                             }
                             
                             if ([result[@"last_name"] isEqualToString:@""] || result[@"last_name"] == nil)
                             {
                                 strLastName = @"";
                             }
                             else
                             {
                                 strLastName = result[@"last_name"];
                             }
                             
                             //return ;
                             
                             dictParamSocial =[[NSMutableDictionary alloc] init];
                             [dictParamSocial setObject:strEmail  forKey:@"email_id"];
                             [dictParamSocial setObject:strFirstName forKey:@"first_name"];
                             [dictParamSocial setObject:strLastName forKey:@"last_name"];
                             [dictParamSocial setObject:[result valueForKey:@"id"] forKey:@"fb_id"];
                             [dictParamSocial setObject:@"" forKey:@"google_id"];
                             [dictParamSocial setObject:[[[result valueForKey:@"picture"] valueForKey:@"data"] valueForKey:@"url"] forKey:@"social_profile_pic"];
                             
                             [self checkEmailwithEmailID:strEmail];
                             
                            
                         }
                         else
                         {
                             //[MBProgressHUD hideHUDForView:self.view animated:YES];
                             
                         }
                         //1072438042806942
                     }];
                }
            }
            
            
        }
    }];
    
    
}


-(void)checkEmailwithEmailID:(NSString *)email_id
{
    //{"apptoken":"d10289185f466003c450a15d300e50d5","email_id":"savitakeshav35@gmail.com"}
    
    NSMutableDictionary * dictParam =[[NSMutableDictionary alloc] init];
    [dictParam setObject:AppToken  forKey:@"apptoken"];
    [dictParam setObject:email_id  forKey:@"email_id"];

    [Networking rawJsondataTaskwithURL:CheckEmail Param:dictParam ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error)
    {
        if (!error) {
            
            if ([[response valueForKey:@"code"] intValue] == 1)
            {
                [self loginwithEmailAddress:email_id withPassword:[response valueForKey:@"password"]];
            }
            else if ([[response valueForKey:@"code"] intValue] == 0)
            {
                [self.ViewSynchonizeContact removeFromSuperview];
                [self.viewForgotPassword removeFromSuperview];
                
                [self.view addSubview:self.viewMobileNumber];
                [self.viewMobileNumber setCenter:self.view.center];
                self.viewMobileNumber.layer.cornerRadius = 3 ;
                self.viewMobileNumber.clipsToBounds = YES;
                [self.txtMobileNumber becomeFirstResponder];
            }
        }
    }];
}

-(NSString *)validateMobileNumberAndPassword
{
    NSString * strMsg;
    
    if (_txtMobileNumber.text.length == 0)
    {
        strMsg = @"Please Enter Mobile Number";
    }
    else if (_txtSetPassword.text.length == 0)
    {
        strMsg = @"Please Enter Password";
    }
    else
    {
        strMsg = nil;
    }
    
    return strMsg;
}

-(IBAction)btnOkActionOnMobileNumberView:(id)sender
{
    if ([self validateMobileNumberAndPassword])
    {
        [self showAlertMessage:[self validateMobileNumberAndPassword] Title:Alert_Title_Info];
    }
    else
    {
        [self checkMobileNumberwithMobileNumber:_txtMobileNumber.text];
    }
}


-(void)checkMobileNumberwithMobileNumber:(NSString *)mobileNo
{
    //{"apptoken":"d10289185f466003c450a15d300e50d5","phone_number":"123456789"}
    
    NSMutableDictionary * dictParam =[[NSMutableDictionary alloc] init];
    [dictParam setObject:AppToken  forKey:@"apptoken"];
    [dictParam setObject:mobileNo  forKey:@"phone_number"];
    
    [Networking rawJsondataTaskwithURL:CheckPhoneNumber Param:dictParam ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error)
     {
         if (!error) {
             
             if ([[response valueForKey:@"code"] intValue] == 1)
             {
                 [_txtMobileNumber becomeFirstResponder];
                 [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Info];
             }
             else if ([[response valueForKey:@"code"] intValue] == 0)
             {
                 // Code For Social Sign Up
                 
                 [self SocialSignupwithwithMobileNumber:_txtMobileNumber.text andWithPassword:_txtSetPassword.text];
             }
         }
     }];
}

-(void)SocialSignupwithwithMobileNumber:(NSString *)mobileNo andWithPassword:(NSString *)password
{
    //{"apptoken":"d10289185f466003c450a15d300e50d5","first_name":"nre" ,"last_name":"dsf" ,"email_id":"besavitakeshav35@gmail.com" ,"password":"123456" ,"phone_number":"9845685952" ,"fb_id":"2656546464" ,"google_id":"" ,"social_profile_pic":"https://www.google.co.in/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&uact=8&ved=0ahUKEwi2qbnAjODSAhUBuY8KHUefCWsQjRwIBw&url=http%3A%2F%2Fstackoverflow.com%2Fquestions%2F26881219%2Fhow-to-place-an-image-on-top-left-of-another-image-without-using-javascript&psig=AFQjCNF-iZl0vQgPb1ni0ob5XYI8fO-jZA&ust=1489928008325177" ,"user_type":"facebook" ,"device_token":""}

    
    /*NSMutableDictionary * dictParam =[[NSMutableDictionary alloc] init];*/
    [dictParamSocial setObject:AppToken  forKey:@"apptoken"];
    [dictParamSocial setObject:mobileNo forKey:@"phone_number"];
    [dictParamSocial setObject:password forKey:@"password"];
    

    
    NSLog(@"Social Param : %@",dictParamSocial);
    
    
    
    [Networking rawJsondataTaskwithURL:SocialSignUp Param:dictParamSocial ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error)
     {
         if (!error) {
             NSLog(@"social response : %@",response);
             if ([[response valueForKey:@"code"] intValue] == 1)
             {
                 //[self loginwithEmailAddress:email_id withPassword:[response valueForKey:@"password"]];
                 [self.viewMobileNumber removeFromSuperview];
                 [self.viewForgotPassword removeFromSuperview];
                 
                 [self.view addSubview:self.ViewSynchonizeContact];
                 [self.ViewSynchonizeContact setCenter:self.view.center];
                 self.ViewSynchonizeContact.layer.cornerRadius = 3 ;
                 self.ViewSynchonizeContact.clipsToBounds = YES;
                 
                 [UserModelObj setStrUserID:[response valueForKey:@"user_id"]];
                 [UserModelObj setStrUserFirstName:[response valueForKey:@"first_name"]];
                 [UserModelObj setStrUserLastName:[response valueForKey:@"last_name"]];
                 [UserModelObj setStrUserProfileImageUrl:[response valueForKey:@"image"]];
                 [UserModelObj setStrUserEmailAddress:[response valueForKey:@"email_id"]];
                 [UserModelObj setStrUserPhoneNumber:[response valueForKey:@"phone_number"]];
                 [[UserModel sharedSingleton] setStrUserPassword:[response valueForKey:@"password"]];
                 
                 [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isLogin"];
                 
                 NSLog(@"get user id---%@",[UserModelObj getUserID]);
                 
                 NSData *data = [NSKeyedArchiver archivedDataWithRootObject:response];
                 [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"UserLoginData"];
                 [[NSUserDefaults standardUserDefaults] synchronize];
                 
                 [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Success];
                 
                 // FOR SINCH USER REGISTRATION (AUDIO & VIDEO CALL)
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"UserDidLoginNotification"
                                                                     object:nil
                                                                   userInfo:@{
                                                                              @"userId" : [response valueForKey:@"user_id"]
                                                                              }];

                 
                /* DEMORootViewController *root=[self.storyboard instantiateViewControllerWithIdentifier:@"rootController"];
                 [self.navigationController pushViewController:root animated:true];*/
                 
             }
             else if ([[response valueForKey:@"code"] intValue] == 0)
             {
                 [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Error];
             }
         }
     }];
}

- (IBAction)btnAllowSkipAction:(UIButton *)sender
{
    if (sender.tag == 1001)
    {
        /*[AppDelegate getContactAuthorizationFromUser];
        [AppDelegate getContactAuthorizationFromUser];
        NSMutableArray *addressbookConacts = [AppDelegate getContacts];
        [self saveContactList];*/
        
        
        DEMORootViewController *root=[self.storyboard instantiateViewControllerWithIdentifier:@"rootController"];
        [self.navigationController pushViewController:root animated:true];
    }
    else if (sender.tag == 1002)
    {
        DEMORootViewController *root=[self.storyboard instantiateViewControllerWithIdentifier:@"rootController"];
        [self.navigationController pushViewController:root animated:true];
    }
}


-(void)saveContactList
{
    AddressBook * objAddressBook = [[AddressBook alloc] initWithComplitionHandler:^(id ContactResponse, NSError *error) {
        NSLog(@"objAddressBook.arrContact :%@",ContactResponse);
        
        if (!error)
        {
            NSMutableArray * arr = [[NSMutableArray alloc]init];
            arr = [ContactResponse copy];
            
            [ContactResponse isKindOfClass:[NSMutableArray class]];
            
            NSMutableArray * arrContact = [[NSMutableArray alloc]init];
            for (int i = 0; i<arr.count; i++)
            {
                ContactUtility * utility = [arr objectAtIndex:i];
                NSMutableDictionary * dictContact = [[NSMutableDictionary alloc] init];
                if (![utility.firstName isEqual:[NSNull null]] || utility.firstName != nil)
                {
                    [dictContact setObject:utility.firstName forKey:@"contact_name"];
                    [dictContact setObject:utility.mobileNumber forKey:@"contact_no"];
                }
                
                [arrContact addObject:dictContact];
            }
            
            NSLog(@"arrContact :%@",arrContact);
            
            NSMutableDictionary * dictParam = [[NSMutableDictionary alloc] init];
            
            [dictParam setObject:AppToken  forKey:@"apptoken"];
            [dictParam setObject:[[UserModel sharedSingleton] getUserID] forKey:@"user_id"];
            [dictParam setObject:[[UserModel sharedSingleton] getUserFirstName] forKey:@"c_name"];
            
            //NSMutableArray *addressbookConacts = [AppDelegate getContacts];
            
            [dictParam setObject:arrContact forKey:@"contact_list"];
            
            //{"apptoken":"d10289185f466003c450a15d300e50d5","user_id":"54","c_name":"alka","contact_list":
            
            NSLog(@"ContactList Param : %@",dictParam);
            
            [Networking rawJsondataTaskwithURL:SaveContactList Param:dictParam ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error)
             {
                 if (!error) {
                     NSLog(@"ContactList response : %@",response);
                     if ([[response valueForKey:@"code"] intValue] == 1)
                     {
                         //[self loginwithEmailAddress:email_id withPassword:[response valueForKey:@"password"]];
                         
                         [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Success];
                     }
                     else if ([[response valueForKey:@"code"] intValue] == 0)
                     {
                         [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Error];
                     }
                 }
             }];

        }
        

    }];

}


@end
