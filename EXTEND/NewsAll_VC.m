//
//  NewsAll_VC.m
//  EXTEND
//
//  Created by Apple on 10/03/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "NewsAll_VC.h"
#import "NewsCell.h"
#import "NewsCommentList_VC.h"
#import "NewsDetailsVC.h"

#import "REFrostedViewController.h"
#import "Networking.h"
#import "UserModel.h"
#import "NewsModel.h"

#import <SDWebImage/UIImageView+WebCache.h>

@interface NewsAll_VC ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray * arrCategoryListData;
    NSMutableArray * arrNewsData;
    NewsModel * objNewsModel;
    //NSMutableArray * arrNewsModel;
}

@property (retain, nonatomic) NSMutableArray * arrNewsModel;

@property (strong ,nonatomic) IBOutlet UIView * viewHeader;
@property (weak ,nonatomic) IBOutlet UIImageView * imgViewCategory;
@property (weak ,nonatomic) IBOutlet UILabel * lblCategoryName;
@property (strong ,nonatomic) IBOutlet UIButton * btnCategory;

@property (weak ,nonatomic) IBOutlet UIScrollView * scrlHeader;
@property (strong ,nonatomic) IBOutlet UITableView * tblNews;
@end

@implementation NewsAll_VC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self LoadScroll];
    [self getNewswithCategory_ID:@""];
}

-(void)LoadScroll
{
    
    NSMutableDictionary * dictParam = [[NSMutableDictionary alloc] init];
    [dictParam setObject:AppToken forKey:@"apptoken"];
    
    [Networking rawJsondataTaskwithURL:CategoryList Param:dictParam ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error) {
        if (!error) {
            //NSLog(@"Category List Data : %@",response);
            
            arrCategoryListData = [[NSMutableArray alloc] init];
            arrCategoryListData = [response valueForKey:@"post_info"];
            
            
            for (int i = 0; i<arrCategoryListData.count; i++)
            {
                NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"HeaderView" owner:self options:nil];
                UIView *mainView = [subviewArray objectAtIndex:0];
                
                _viewHeader.frame=CGRectMake(i * _viewHeader.frame.size.width, 0, 60, _viewHeader.frame.size.height);
                _imgViewCategory.layer.cornerRadius = self.imgViewCategory.frame.size.height/2;
                _imgViewCategory.layer.borderWidth = 1 ;
                _imgViewCategory.layer.borderColor = UIColorFromRGB(Color_NavigationBar).CGColor;
                _imgViewCategory.clipsToBounds = YES;
                
                [_imgViewCategory sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",AdminNewsCategoriesImageBaseUrl,[[arrCategoryListData objectAtIndex:i] valueForKey:@"cat_image"]]] placeholderImage:[UIImage imageNamed:@"upload_icon"]];
                _lblCategoryName.text = [[arrCategoryListData objectAtIndex:i] valueForKey:@"categoryName"];
                
                _btnCategory.tag = i+1000;
                _lblCategoryName.tag = i+10000;
                _imgViewCategory.tag = i+100000;
                
                [_btnCategory addTarget:self action:@selector(btnCategorySelect:) forControlEvents:UIControlEventTouchUpInside];
                
                [_scrlHeader addSubview:mainView];
                
            }
            _scrlHeader.contentSize = CGSizeMake(arrCategoryListData.count * _viewHeader.frame.size.width , _scrlHeader.frame.size.height);
            NSLog(@"_scrlHeader.contentSize : %f",_scrlHeader.contentSize.width);
        }
        else
        {
        
        }
    }];
    
 
}

-(void)btnCategorySelect:(UIButton *)sender
{
    for (int i=0; i < self.scrlHeader.subviews.count; i++)
    {
        UILabel * lbl = (UILabel *) [self.scrlHeader viewWithTag:i+10000];
        UIImageView *  Imgview = (UIImageView *) [self.scrlHeader viewWithTag:i+100000];
        
        if (sender.tag + 9000 == lbl.tag && sender.tag+99000 == Imgview.tag )
        {
            [lbl setTextColor:UIColorFromRGB(Color_NavigationBar)];
            Imgview.layer.borderWidth = 4 ;
            Imgview.clipsToBounds = YES;
        }
        else
        {
            [lbl setTextColor:[UIColor blackColor]];
            Imgview.layer.borderWidth = 1 ;
            Imgview.clipsToBounds = YES;
        }
        
    }
    
    [self getNewswithCategory_ID:[[arrCategoryListData objectAtIndex:sender.tag - 1000] valueForKey:@"c_Id"]];
    
}


-(void)getNewswithCategory_ID:(NSString *)categoryID
{
   // {"apptoken":"d10289185f466003c450a15d300e50d5","cat_id":"5","user_id":"95"}
    
    NSMutableDictionary * dictParam =[[NSMutableDictionary alloc] init];
    [dictParam setObject:[[UserModel sharedSingleton] getUserID] forKey:@"user_id"];
    [dictParam setObject:AppToken  forKey:@"apptoken"];
    [dictParam setObject:categoryID forKey:@"cat_id"];
    
    NSLog(@"News Param: %@",dictParam);
    
    [Networking rawJsondataTaskwithURL:NewsByCategory Param:dictParam ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error) {
        if (!error) {
            NSLog(@"News Data : %@",response);
            
            
            if ([[response valueForKey:@"status"]integerValue] == 1)
            {
                arrNewsData = [[NSMutableArray alloc] init];
                arrNewsData = [response valueForKey:@"news_info"];
                
                self.arrNewsModel = [NSMutableArray new];
                
                for (int i=0; i<arrNewsData.count; i++)
                {
                    objNewsModel = [NewsModel new];
                    
                    objNewsModel.NewsTitle = [[arrNewsData objectAtIndex:i] valueForKey:@"title"];
                    objNewsModel.Category_Name = [[arrNewsData objectAtIndex:i] valueForKey:@"categoryName"];
                    objNewsModel.NewsContent = [[arrNewsData objectAtIndex:i] valueForKey:@"content"];
                    objNewsModel.PostDate = [[arrNewsData objectAtIndex:i] valueForKey:@"add_Date"];
                    objNewsModel.News_ID = [[arrNewsData objectAtIndex:i] valueForKey:@"id"];
                    objNewsModel.Category_ID = [[arrNewsData objectAtIndex:i] valueForKey:@"categories_Id"];
                    objNewsModel.NewsImage = [NSString stringWithFormat:@"%@%@",AdminNewsPostImageBaseUrl,[[arrNewsData objectAtIndex:i] valueForKey:@"feature_Img"]];
                    objNewsModel.LikeCount = [NSString stringWithFormat:@"%d",[[[arrNewsData objectAtIndex:i] valueForKey:@"like_count"] intValue]];
                    objNewsModel.CommentCount = [NSString stringWithFormat:@"%d",[[[arrNewsData objectAtIndex:i] valueForKey:@"comment_count"] intValue]];
                    objNewsModel.liked = [[[arrNewsData valueForKey:@"like_show"] objectAtIndex:i] intValue];
                    
                    
                    
                    [self.arrNewsModel addObject:objNewsModel];
                    
                }
                //NSLog(@"_arr:%@",self.arrNewsModel);
                
                //[_tblNews reloadData];
            }
            else if ([[response valueForKey:@"status"]integerValue] == 0)
            {
                arrNewsData = nil;
                self.arrNewsModel = [NSMutableArray new];
                [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Info];
            }
            
            [_tblNews reloadData];
        }
        else
        {
            
        }
    }];

}

#pragma mark-  Commaon Alertview

-(void)showAlertMessage:(NSString*)message Title:(NSString *)title
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               {
                               }];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = NO;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(40, 0,SCREEN_MAX_LENGTH-40, 40)];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"Raleway" size:16];
    label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    label.textAlignment = NSTextAlignmentLeft;
    label.textColor =[UIColor whiteColor];
    label.text=@"News";
    self.navigationItem.titleView = label;
    
    /*UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back_icon"]style:UIBarButtonItemStylePlain target:self action:@selector(Back)];
    self.navigationItem.leftBarButtonItem = revealButtonItem;*/
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu_icon"]style:UIBarButtonItemStylePlain target:self action:@selector(showmenu)];
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    
}

- (void)showmenu
{
    [self.navigationController.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    [self.frostedViewController presentMenuViewController];
}

-(void)Back
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.arrNewsModel.count; //arrNewsData.count;    //count number of row from counting array hear cataGorry is An Array
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"NewsCell";
    
    NewsCell *cell = (NewsCell *)  [_tblNews dequeueReusableCellWithIdentifier:MyIdentifier forIndexPath:indexPath];
    //    if (cell == nil)
    //    {
    //        cell = [[HomeTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
    //                                        reuseIdentifier:MyIdentifier];
    //    }
    
    cell.tag = indexPath.row;
    objNewsModel = [self.arrNewsModel objectAtIndex:indexPath.row];
    
    cell.imgviewPost.clipsToBounds = YES;
    [cell.imgviewPost sd_setImageWithURL:[NSURL URLWithString:objNewsModel.NewsImage] placeholderImage:[UIImage imageNamed:@"upload_icon"]];
    /*CAGradientLayer *gradientMask = [CAGradientLayer layer];
    gradientMask.frame = cell.imgviewPost.bounds;
    gradientMask.colors = @[(id)[UIColor clearColor].CGColor,
                            (id)[UIColor whiteColor].CGColor,
                            (id)[UIColor blackColor].CGColor,
                            (id)[UIColor clearColor].CGColor];
    gradientMask.locations = @[@0.0, @0.10, @0.60, @1.0];
    cell.imgviewPost.layer.mask = gradientMask;*/
    
    cell.lblPostTitle.text = objNewsModel.NewsTitle;
    cell.lblNewsPostCategoryName.text = objNewsModel.Category_Name;
    cell.lblNewsPostDescription.text = objNewsModel.NewsContent;
    cell.lblNewPostDateTime.text = objNewsModel.PostDate;
    cell.lblLikeCount.text = objNewsModel.LikeCount;
    cell.lblCommentCount.text = objNewsModel.CommentCount;
    
    cell.btnLike.tag = 100000+indexPath.row;
    cell.btnComment.tag = 1000+indexPath.row;
    cell.btnShare.tag = 100+indexPath.row;
    [cell.btnLike addTarget:self action:@selector(btnLikeAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnComment addTarget:self action:@selector(btnCommentAction:) forControlEvents:UIControlEventTouchUpInside];
    
    if (objNewsModel.liked == 0)
     {
     [cell.btnLike setImage:[UIImage imageNamed:@"like_icon-1"] forState:UIControlStateNormal];
     }
     else if (objNewsModel.liked == 1)
     {
     [cell.btnLike setImage:[UIImage imageNamed:@"like_hover_news"] forState:UIControlStateNormal];
     }
    
    [cell.btnShare addTarget:self action:@selector(btnShareAction:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NewsDetailsVC * objNewsDetailsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsDetailsVC"];
    objNewsDetailsVC.arrGetNews = [self.arrNewsModel copy];
    objNewsDetailsVC.index      = indexPath.row;
    NSLog(@"objNewsDetailsVC.arrGetNews DATA:%@",objNewsDetailsVC.arrGetNews);
    [self.navigationController pushViewController:objNewsDetailsVC animated:YES];
    
}

-(void)btnShareAction:(UIButton *)sender
{
    objNewsModel = [self.arrNewsModel objectAtIndex:sender.tag - 100];
    
    NSURL *SharingUrl;
    
    SharingUrl = [NSURL URLWithString:objNewsModel.NewsImage];
    
    
    NSArray *objectsToShare = @[SharingUrl];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:nil];
}


/*- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"NewsCell";
    
    NewsCell *cell = (NewsCell *)  [_tblNews dequeueReusableCellWithIdentifier:MyIdentifier forIndexPath:indexPath];
    //    if (cell == nil)
    //    {
    //        cell = [[HomeTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
    //                                        reuseIdentifier:MyIdentifier];
    //    }
    
    
    objNewsModel = [self.arrNewsModel objectAtIndex:indexPath.row];
    
    cell.tag = indexPath.row;

    cell.imgviewPost.clipsToBounds = YES;
    [cell.imgviewPost sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",AdminNewsPostImageBaseUrl,[[arrNewsData objectAtIndex:indexPath.row] valueForKey:@"feature_Img"]]] placeholderImage:[UIImage imageNamed:@"upload_icon"]];
    NSLog(@"ImageUrl : %@",[NSString stringWithFormat:@"%@%@",AdminNewsPostImageBaseUrl,[[arrNewsData objectAtIndex:indexPath.row] valueForKey:@"feature_Img"]]);
    cell.lblPostTitle.text = [[arrNewsData objectAtIndex:indexPath.row] valueForKey:@"title"];
    cell.lblNewsPostCategoryName.text = [[arrNewsData objectAtIndex:indexPath.row] valueForKey:@"categoryName"];
    cell.lblNewsPostDescription.text = [[arrNewsData objectAtIndex:indexPath.row] valueForKey:@"content"];
    cell.lblNewPostDateTime.text = [[arrNewsData objectAtIndex:indexPath.row] valueForKey:@"add_Date"];
    cell.lblLikeCount.text = [NSString stringWithFormat:@"%d",[[[arrNewsData objectAtIndex:indexPath.row] valueForKey:@"like_count"] intValue]];
    cell.lblCommentCount.text = [NSString stringWithFormat:@"%d",[[[arrNewsData objectAtIndex:indexPath.row] valueForKey:@"comment_count"] intValue]];
    
    cell.btnLike.tag = 100000+indexPath.row;
    cell.btnComment.tag = 1000+indexPath.row;
    [cell.btnLike addTarget:self action:@selector(btnLikeAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnComment addTarget:self action:@selector(btnCommentAction:) forControlEvents:UIControlEventTouchUpInside];
    

    
    return cell;
}*/



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"NewsCell";
    NewsCell *cell = [self.tblNews dequeueReusableCellWithIdentifier:MyIdentifier];
    
    objNewsModel = [self.arrNewsModel objectAtIndex:indexPath.row];
    
    return cell.contentView.frame.size.height - 50 + [self heightForLabelwithText:objNewsModel.NewsContent withFont:[UIFont systemFontOfSize:cell.lblNewsPostDescription.font.pointSize] withWidth:cell.lblNewsPostDescription.frame.size.width] ;
    
}

-(CGFloat )heightForLabelwithText:(NSString *)text withFont:(UIFont *)font withWidth:(CGFloat)width
{
    CGRect rect = CGRectMake(0, 0, width, CGFLOAT_MAX);
    
    UILabel * lbl = [[UILabel alloc] initWithFrame:rect];
    lbl.numberOfLines = 0;
    lbl.lineBreakMode = NSLineBreakByCharWrapping;
    lbl.font = font;
    lbl.text = text;
    [lbl sizeToFit];
    
    return lbl.frame.size.height;
}

/**-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    _viewHeader.frame = CGRectMake(0, 0, self.tblNews.frame.size.width, _viewHeader.frame.size.height);
    //_viewHeader.backgroundColor = UIColorFromRGB(Color_NavigationBar);
    UIView *view = [[UIView alloc] init ];
    tableView.tableHeaderView = _viewHeader;
    return view;
    
}*/

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.001/*self.viewheader.frame.size.height*/;
}

-(void)btnLikeAction:(UIButton *)sender
{
    
    // {"apptoken":"d10289185f466003c450a15d300e50d5", "user_id":"54" ,"news_id":"2","like":"1"}
    
    objNewsModel = [self.arrNewsModel objectAtIndex:sender.tag - 100000];
    
    NSMutableDictionary * dictParam =[[NSMutableDictionary alloc] init];
    [dictParam setObject:AppToken  forKey:@"apptoken"];
    [dictParam setObject:[[UserModel sharedSingleton] getUserID]  forKey:@"user_id"];
    [dictParam setObject:objNewsModel.News_ID  forKey:@"news_id"];
    
    if (objNewsModel.liked == 1)
    {
        [dictParam setObject:@"0"  forKey:@"like"];
    }
    else
    {
        [dictParam setObject:@"1"  forKey:@"like"];
    }

    NSLog(@"Dict Like:%@",dictParam);
    
    [Networking rawJsondataTaskwithURL:NewsLikeDislike Param:dictParam ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error)
     {
         if (!error) {
             
             NSLog(@"Like Unlike News Data :%@",response);
             if ([[response valueForKey:@"code"] intValue] == 1)
             {
                 
                 NSLog(@"Liked Value: %d",objNewsModel.liked);
                 if (objNewsModel.liked == 1)
                 {
                     objNewsModel.liked = 0;
                     objNewsModel.LikeCount = [NSString stringWithFormat:@"%d",[objNewsModel.LikeCount intValue] - 1];
                     [sender setImage:[UIImage imageNamed:@"like_icon-1"] forState:UIControlStateNormal];
                 }
                 else
                 {
                     objNewsModel.liked = 1;
                     objNewsModel.LikeCount = [NSString stringWithFormat:@"%d",[objNewsModel.LikeCount intValue] + 1];
                     [sender setImage:[UIImage imageNamed:@"like_hover_news"] forState:UIControlStateNormal];
                 }
                 NSLog(@"Update Liked Value: %d",objNewsModel.liked);
                 
                 [self.tblNews reloadData];
             }
             else if ([[response valueForKey:@"code"] intValue] == 0)
             {
                 
             }
         }
     }];


}

-(void)btnCommentAction:(UIButton *)sender
{
    
    //UIViewController *viewController = (UIViewController *)[storyboard instantiateViewControllerWithIdentifier:@"generaC"];
    
    objNewsModel = [self.arrNewsModel objectAtIndex:sender.tag - 1000];
    
    NewsCommentList_VC * objNewsCommentList_VC = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsCommentList_VC"];
    
    objNewsCommentList_VC.strNewsPostID = objNewsModel.News_ID;
    objNewsCommentList_VC.strNewsCategoryID = objNewsModel.Category_ID;
    objNewsCommentList_VC.strCommentCount = objNewsModel.CommentCount;
    objNewsCommentList_VC.strLikeCount = objNewsModel.LikeCount;
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:objNewsCommentList_VC];
    [self presentViewController:nav animated:YES completion:nil];
}

@end
