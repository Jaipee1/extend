//
//  SidMenu_Cell.m
//  Events
//
//  Created by Genuine Soft Technologies on 12/21/15.
//  Copyright © 2015 Genuine Soft Technologies. All rights reserved.
//

#import "SidMenu_Cell.h"

@implementation SidMenu_Cell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
