//
//  UserModel.m
//  EXTEND
//
//  Created by Apple on 04/03/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "UserModel.h"

@implementation UserModel

//@synthesize strUserID,strUserLastName,strUserFirstName,strUserPhoneNumber,strUserEmailAddress,strUserProfileImageUrl;

static UserModel* _sharedSingleton = nil;

+(UserModel*)sharedSingleton
{
    static UserModel *sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedInstance = [[UserModel alloc] init];
    });
    return sharedInstance;
}

//-(void)setUserID:(NSString *)strUserID
//{
//    self.strUserID = strUserID;
//}

-(NSString *)getUserID
{
    return self.strUserID;
}

-(NSString *)getUserFirstName
{
    return self.strUserFirstName;
}

-(NSString *)getUserLastName
{
    return self.strUserLastName;
}

-(NSString *)getUserPhoneNumber
{
    return self.strUserPhoneNumber;
}

-(NSString *)getUserEmailAddress
{
    return self.strUserEmailAddress;
}

-(NSString *)getUserProfileImageUrl
{
    return self.strUserProfileImageUrl;
}

-(NSString *)getUserPassword
{
    return self.strUserPassword;
}

@end
