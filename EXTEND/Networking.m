//
//  Networking.m
//  Patral
//
//  Created by GST on 27/12/16.
//  Copyright © 2016 GST. All rights reserved.
//

#import "Networking.h"
#import <AFNetworking.h>
#import "SVProgressHUD.h"

@implementation Networking
+(void)dataTaskwithURL:(NSString *)url Param:(NSDictionary*)param ImageArray:(NSMutableArray *)imageArray ImageParamArray:(NSMutableArray *)imageParamArray compilation:(void (^) (id response, NSError* error))handler
{
    [self IsInternet];
    
    [SVProgressHUD showWithStatus:@"Loading.."];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager POST:url parameters:param constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData)
    {
        if (imageArray.count>0)
        {
            for (int i=0; i<imageArray.count; i++)
            {
                
                NSData *imageData = UIImageJPEGRepresentation([imageArray objectAtIndex:i],0.001);
                [formData appendPartWithFileData:imageData name:[imageParamArray objectAtIndex:i] fileName:@"Image.jpg" mimeType:@"image/jpg"];
            }
        }
        
    } progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
    {
        handler(responseObject, nil);
        [SVProgressHUD dismiss];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
    {
        NSLog(@"Error: %@", error);
        [SVProgressHUD dismiss];
        handler(nil, error);
        
    }];
    

}

#pragma Mark : For Viedo Upload
+(void)dataTaskwithURL:(NSString *)url Param:(NSDictionary*)param VideoUrlArray:(NSMutableArray *)videoUrlArray VideoPrameterArray:(NSMutableArray *)videoParameterArray compilation:(void (^) (id response, NSError* error))handler
{
    [self IsInternet];
    
    [SVProgressHUD showWithStatus:@"Loading.."];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager POST:url parameters:param constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData)
     {
         if (videoUrlArray.count>0)
         {
             for (int i=0; i<videoUrlArray.count; i++)
             {
                 
                 //NSData *imageData = UIImageJPEGRepresentation([imageArray objectAtIndex:i],0.001);
                 NSData *videoData = [NSData  dataWithContentsOfURL:[videoUrlArray objectAtIndex:i]];
                 [formData appendPartWithFileData:videoData name:[videoParameterArray objectAtIndex:i] fileName:@"VideoIOS.mov" mimeType:@"video/quicktime"];
             }
         }
         
         
         
     } progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     {
         handler(responseObject, nil);
         [SVProgressHUD dismiss];
         
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
     {
         NSLog(@"Error: %@", error);
         [SVProgressHUD dismiss];
         handler(nil, error);
         
     }];
    
    
}


#pragma mark : For Viedo Upload with Thumbnail Image
+(void)dataTaskwithURL:(NSString *)url Param:(NSDictionary*)param VideoUrlArray:(NSMutableArray *)videoUrlArray VideoPrameterArray:(NSMutableArray *)videoParameterArray
   ThumbnailImageArray:(NSMutableArray  *)thumbnailimageArray ThumbnaiImagePrameterArray:(NSMutableArray *)thumbnailImageParameterArray compilation:(void (^) (id response, NSError* error))handler
{
    [self IsInternet];
    
    [SVProgressHUD showWithStatus:@"Loading.."];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager POST:url parameters:param constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData)
     {
         if (videoUrlArray.count>0)
         {
             for (int i=0; i<videoUrlArray.count; i++)
             {
                 
                 //NSData *imageData = UIImageJPEGRepresentation([imageArray objectAtIndex:i],0.001);
                 NSData *videoData = [NSData  dataWithContentsOfURL:[videoUrlArray objectAtIndex:i]];
                 [formData appendPartWithFileData:videoData name:[videoParameterArray objectAtIndex:i] fileName:@"VideoIOS.mov" mimeType:@"video/quicktime"];
             }
         }
         
         if (thumbnailimageArray.count>0)
         {
             for (int i=0; i<thumbnailimageArray.count; i++)
             {
                 
                 NSData *imageData = UIImageJPEGRepresentation([thumbnailimageArray objectAtIndex:i],0.001);
                 [formData appendPartWithFileData:imageData name:[thumbnailImageParameterArray objectAtIndex:i] fileName:@"Image.jpg" mimeType:@"image/jpg"];
             }
         }
         
     } progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     {
         handler(responseObject, nil);
         [SVProgressHUD dismiss];
         
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
     {
         NSLog(@"Error: %@", error);
         [SVProgressHUD dismiss];
         handler(nil, error);
         
     }];

}



# pragma Mark: For Raw Json Data Post
+(void)rawJsondataTaskwithURL:(NSString *)url Param:(NSDictionary *)param ImageArray:(NSMutableArray *)imageArray ImageParamArray:(NSMutableArray *)imageParamArray compilation:(void (^)(id, NSError *))handler
{
    
    NSLog(@"url:%@",url);
    
    [self IsInternet];

    [SVProgressHUD showWithStatus:@"Loading.."];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];

   /* NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:url parameters:param error:nil];
    req.timeoutInterval = 30;
    [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];*/
    
    //manager.responseSerializer.acceptableContentTypes= [NSSet setWithObjects:@"text/html",@"application/json", nil];
    
     [[manager dataTaskWithRequest:[[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:url parameters:param error:nil] completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
         
         NSLog(@"Get response");
     if (!error)
     {
         //NSLog(@"%@", responseObject);
         handler(responseObject, nil);
         [SVProgressHUD dismiss];
     }
     else
     {
         //NSLog(@"Error: %@, %@, %@", error.description, response, responseObject);
         NSLog(@"Error: %@", error.description.description);
         [SVProgressHUD dismiss];
         handler(nil, error);
         
     }
         
     }] resume];
    
   
}

# pragma Mark: For Raw Json Data Post With Hud Animation on Your Need
+(void)rawJsondataTaskwithURL:(NSString *)url Param:(NSDictionary *)param ImageArray:(NSMutableArray *)imageArray ImageParamArray:(NSMutableArray *)imageParamArray WithHudAnimation:(BOOL)animation compilation:(void (^)(id, NSError *))handler
{
    
    NSLog(@"url:%@",url);
    
    [self IsInternet];
    if (animation)
    {
        [SVProgressHUD showWithStatus:@"Loading.."];
    }
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    /* NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:url parameters:param error:nil];
     req.timeoutInterval = 30;
     [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
     [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];*/
    
    //manager.responseSerializer.acceptableContentTypes= [NSSet setWithObjects:@"text/html",@"application/json", nil];
    
    [[manager dataTaskWithRequest:[[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:url parameters:param error:nil] completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        
        NSLog(@"Get response");
        if (!error)
        {
            //NSLog(@"%@", responseObject);
            handler(responseObject, nil);
            [SVProgressHUD dismiss];
        }
        else
        {
            //NSLog(@"Error: %@, %@, %@", error.description, response, responseObject);
            NSLog(@"Error: %@", error.description.description);
            [SVProgressHUD dismiss];
            handler(nil, error);
            
        }
        
    }] resume];
    
    
}



+(void)IsInternet
{
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        
        NSLog([[AFNetworkReachabilityManager sharedManager] isReachable] ? @"YES" : @"NO");
        
        switch (status) {
            case AFNetworkReachabilityStatusReachableViaWWAN:
                NSLog(@"WWN");
                break;
            case AFNetworkReachabilityStatusReachableViaWiFi:
                NSLog(@"WiFi");
                break;
            case AFNetworkReachabilityStatusUnknown:
                NSLog(@"Unknown");
                break;
            case AFNetworkReachabilityStatusNotReachable:
                NSLog(@"Not Reachable");
                [self showAlertMessage:@"No internet connection" Title:@"Info"];
                break;
            default:
                break;
        }
    }];
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
}

#pragma Mark-  Commaon Alertview

+(void)showAlertMessage:(NSString*)message Title:(NSString *)title
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               {
                               }];
    [alert addAction:okAction];
    [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alert animated:YES completion:nil];
    
}


#pragma Mark : For Viedo Upload
+(void)dataTaskForChatwithURL:(NSString *)url Param:(NSDictionary*)param MediaArray:(NSMutableArray *)mediaUrlArray MediaPrameterArray:(NSMutableArray *)mediaParameterArray MediaType:(NSString*)mediaType compilation:(void (^) (id response, NSError* error))handler
{
    [self IsInternet];
    
    [SVProgressHUD showWithStatus:@"Loading.."];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager POST:url parameters:param constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData)
     {
         if (mediaUrlArray.count>0)
         {
             for (int i=0; i<mediaUrlArray.count; i++)
             {
                 if ([mediaType isEqualToString:@"video"])
                 {
                     NSData *videoData = [NSData  dataWithContentsOfURL:[mediaUrlArray objectAtIndex:i]];
                     [formData appendPartWithFileData:videoData name:[mediaParameterArray objectAtIndex:i] fileName:@"VideoIOS.mov" mimeType:@"video/quicktime"];
                 }
                 else if ([mediaType isEqualToString:@"image"])
                 {
                     NSData *imageData = UIImageJPEGRepresentation([mediaUrlArray objectAtIndex:i],0.001);
                     [formData appendPartWithFileData:imageData name:[mediaParameterArray objectAtIndex:i] fileName:@"Image.jpg" mimeType:@"image/jpg"];
                 }
                 else if ([mediaType isEqualToString:@"audio"])
                 {
                     /*NSData *imageData = UIImageJPEGRepresentation([mediaUrlArray objectAtIndex:i],0.001);
                     [formData appendPartWithFileData:imageData name:[mediaParameterArray objectAtIndex:i] fileName:@"Image.jpg" mimeType:@"image/jpg"];*/
                 }
                 
             }
         }
         
         
         
     } progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     {
         handler(responseObject, nil);
         [SVProgressHUD dismiss];
         
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
     {
         NSLog(@"Error: %@", error);
         [SVProgressHUD dismiss];
         handler(nil, error);
         
     }];
    
    
}



@end
