//
//  ChatViewController.m
//  EXTEND
//
//  Created by Apple on 25/05/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "ChatViewController.h"
#import "FinalChatViewController.h"
#import "ChatListTableViewCell.h"
#import "Networking.h"
#import "UserModel.h"
#import <SDWebImage/UIImageView+WebCache.h>


@interface ChatViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    ChatListTableViewCell * cell;
    
    NSMutableArray * arrRecentChatData;
}
@property (weak, nonatomic) IBOutlet UITableView *tblChatlist;

@end

@implementation ChatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self GetResentChat];
}


-(void)GetResentChat
{
    //{"apptoken":"d10289185f466003c450a15d300e50d5", "user_id":"170"  ,"limit":"0" }
    
    NSMutableDictionary * dictParam =[[NSMutableDictionary alloc] init];
    [dictParam setObject:[[UserModel sharedSingleton] getUserID] forKey:@"user_id"];
    [dictParam setObject:AppToken  forKey:@"apptoken"];
    [dictParam setObject:[NSNumber numberWithInteger:0]  forKey:@"limit"];
    
    [Networking rawJsondataTaskwithURL:ResentChatList Param:dictParam ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error) {
        if (!error)
        {
            
            NSLog(@"ResentChatList Data : %@",response);
            
            if ([[response valueForKey:@"status"]integerValue] == 1)
            {
                arrRecentChatData = [[NSMutableArray alloc] init];
                arrRecentChatData = [response valueForKey:@"show_chat"];
                self.tblChatlist.delegate   = self;
                self.tblChatlist.dataSource = self;
                [self.tblChatlist reloadData];
                
            }
            else if ([[response valueForKey:@"status"]integerValue] == 0)
            {
                [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Error];
            }
            
        }
        else
        {
            
        }
    }];

}

#pragma Mark-  Commaon Alertview

-(void)showAlertMessage:(NSString*)message Title:(NSString *)title
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               {
                               }];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arrRecentChatData.count;  //count number of row from counting array hear cataGorry is An Array
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"ChatListTableViewCell";
    
    cell = (ChatListTableViewCell *)  [self.tblChatlist dequeueReusableCellWithIdentifier:MyIdentifier forIndexPath:indexPath];

    
    cell.tag = indexPath.row;
    //objNewsModel = [self.arrNewsModel objectAtIndex:indexPath.row];
    
    [cell.imgProPic sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",UserProfilePictureBaseURL,[[arrRecentChatData objectAtIndex:indexPath.row] valueForKey:@"profile_image"]]] placeholderImage:[UIImage imageNamed:@"upload_icon"]];
    cell.lblFullName.text = [[arrRecentChatData objectAtIndex:indexPath.row] valueForKey:@"name"];
    cell.lblDate.text = [[arrRecentChatData objectAtIndex:indexPath.row] valueForKey:@"chat_time"];
    
    cell.lblMessage.text = [[arrRecentChatData objectAtIndex:indexPath.row] valueForKey:@"message"];
    
    if ([[[arrRecentChatData objectAtIndex:indexPath.row] valueForKey:@"chat_datatype"]isEqualToString:@"audio"])
    {
        cell.lblMessage.text = [[arrRecentChatData objectAtIndex:indexPath.row] valueForKey:@"chat_datatype"];
    }
    else if ([[[arrRecentChatData objectAtIndex:indexPath.row] valueForKey:@"chat_datatype"]isEqualToString:@"video"])
    {
        cell.lblMessage.text = [[arrRecentChatData objectAtIndex:indexPath.row] valueForKey:@"chat_datatype"];
    }
    else if ([[[arrRecentChatData objectAtIndex:indexPath.row] valueForKey:@"chat_datatype"]isEqualToString:@"image"])
    {
        cell.lblMessage.text = [[arrRecentChatData objectAtIndex:indexPath.row] valueForKey:@"chat_datatype"];
    }
    
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    FinalChatViewController * objFinalChatViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"FinalChatViewController"];
    objFinalChatViewController.arrOtherUserData = [arrRecentChatData objectAtIndex:indexPath.row];
    if ([[[arrRecentChatData objectAtIndex:indexPath.row] valueForKey:@"user_id"] isEqualToString:[[UserModel sharedSingleton] getUserID]])
    {
        objFinalChatViewController.otherPersonUserID = [[arrRecentChatData objectAtIndex:indexPath.row] valueForKey:@"friend_id"];
    }
    else
    {
        objFinalChatViewController.otherPersonUserID = [[arrRecentChatData objectAtIndex:indexPath.row] valueForKey:@"user_id"];
    }
    /*UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:objFinalChatViewController];
    [self presentViewController:nav animated:YES completion:nil];*/
    [self.navigationController pushViewController:objFinalChatViewController animated:YES];
    
}
/*- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"NewsCell";
    NewsCell *cell = [self.tblNews dequeueReusableCellWithIdentifier:MyIdentifier];
    
    objNewsModel = [self.arrNewsModel objectAtIndex:indexPath.row];
    
    return cell.contentView.frame.size.height - 50 + [self heightForLabelwithText:objNewsModel.NewsContent withFont:[UIFont systemFontOfSize:cell.lblNewsPostDescription.font.pointSize] withWidth:cell.lblNewsPostDescription.frame.size.width] ;
    
}*/
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.001/*self.viewheader.frame.size.height*/;
}

@end
