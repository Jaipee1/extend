//
//  Home_VC.m
//  EXTEND
//
//  Created by Apple on 03/03/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "Home_VC.h"
#import "HomeTableViewCell.h"
#import "PostSend_VC.h"
#import "NewsAll_VC.h"
#import "Chat_Call_List_VC.h"
#import "ExtendFeature_VC.h"
#import "EXPhotoViewer.h"
#import "TTFaveButton.h"
#import "PostModel.h"
#import "Networking.h"
#import "UserModel.h"
#import "REFrostedViewController.h"
#import "SideMenuController.h"
#import "MeetRadarAnimation_VC.h"
#import "ShowAllPostCommentsVC.h"
#import "SettingAppVC.h"

#import "UIViewController+util.h"

#import <SDWebImage/UIImageView+WebCache.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>

#import "PaypalPayment.h"

@interface Home_VC ()<UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate,UIActionSheetDelegate>
{
    //NSMutableArray * arr;
    NSMutableArray * arrPostData;
    //MPMoviePlayerController * moviePlayer;
    MPMoviePlayerViewController * moviePlayer;
    int last_RecordCount;
    PostModel * objpost;
    
    NSInteger selectedIndex;
    
    AVPlayerItem *playerItem;
    
}
@property (nonatomic,strong) IBOutlet UITableView * tblPost;
@property (strong, nonatomic) IBOutlet UIView *viewHeader;
@property (retain, nonatomic) NSMutableArray * arr;
@end

@implementation Home_VC

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    // FOR SINCH USER REGISTRATION (AUDIO & VIDEO CALL)
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"UserDidLoginNotification"
//                                                        object:nil
//                                                      userInfo:@{
//                                                                 @"userId" : [[UserModel sharedSingleton] getUserID]
//                                                                 }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(MPMoviePlayerDidExitFullscreen:) name:MPMoviePlayerDidExitFullscreenNotification object:nil];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor clearColor];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.tblPost addSubview:refreshControl];
    
    //PaypalPayment * pay = [[PaypalPayment alloc] initWithViewController:self andWithAmmount:@"10"];
    
    
}

- (void)refresh:(UIRefreshControl *)refreshControl {
    // Do your job, when done:
    
    last_RecordCount = 0;
    _arr = [NSMutableArray new];
    arrPostData=[[NSMutableArray alloc]init];
    [self.tblPost reloadData];
    //[self showPostwithLastRecored:last_RecordCount];
    
    [refreshControl endRefreshing];
}



- (void)MPMoviePlayerDidExitFullscreen:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerDidExitFullscreenNotification
                                                  object:nil];
    
    //[moviePlayer.view removeFromSuperview];
}


-(void)showPostwithLastRecored:(int)last_recored
{
    NSDictionary * dictPram=[[NSDictionary alloc]init];
    
    // {"apptoken":"d10289185f466003c450a15d300e50d5","user_id":"95","last_record":"10"}
    
    dictPram=@{@"apptoken":AppToken,
               @"user_id":[[UserModel sharedSingleton] getUserID],
               @"last_record":[NSNumber numberWithInteger:last_recored],
               };
    NSLog(@"DictLogin---%@",dictPram);
    
    [Networking rawJsondataTaskwithURL:ShowPost Param:dictPram ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error)
     {
         if (!error)
         {
             //NSLog(@"response----%@",response);
             
             if ([[response valueForKey:@"status"]integerValue] == 1)
             {
                 //arrPostData = [[NSMutableArray alloc] init];
                 arrPostData = [response valueForKey:@"post_info"];
                 
                 for (int i=0; i<arrPostData.count; i++)
                 {
                     objpost                = [PostModel new];
                     
                     objpost.Firstname      = [[arrPostData valueForKey:@"first_name"] objectAtIndex:i];
                     objpost.Lastname       = [[arrPostData valueForKey:@"last_name"] objectAtIndex:i];
                     objpost.PostContent    = [[arrPostData valueForKey:@"post_content"] objectAtIndex:i];
                     objpost.PostName       = [[arrPostData valueForKey:@"post_name"] objectAtIndex:i];
                     objpost.PostType       = [[arrPostData valueForKey:@"post_type"] objectAtIndex:i];
                     objpost.UserImageUrl   = [[arrPostData valueForKey:@"image"] objectAtIndex:i];
                     objpost.UserIDofPost   = [[arrPostData valueForKey:@"posted_by"] objectAtIndex:i];
                     objpost.likeCount      = [NSString stringWithFormat:@"%d",[[[arrPostData valueForKey:@"like_count"] objectAtIndex:i] intValue]];
                     objpost.CommentCount   = [NSString stringWithFormat:@"%d",[[[arrPostData valueForKey:@"comment_count"] objectAtIndex:i] intValue]];
                     objpost.shareCount     = [NSString stringWithFormat:@"%d",[[[arrPostData valueForKey:@"share_count"] objectAtIndex:i] intValue]];
                     objpost.PostID         = [[arrPostData valueForKey:@"post_id"] objectAtIndex:i];
                     objpost.PostvideoThumbnail = [[arrPostData valueForKey:@"post_video_thumbnail"] objectAtIndex:i];
                     objpost.TagFriends     = [[arrPostData valueForKey:@"tage_friend_id"] objectAtIndex:i];
                     objpost.PostTime       = [[arrPostData valueForKey:@"posted_time"] objectAtIndex:i];
                     objpost.liked          = [[[arrPostData valueForKey:@"login_user_like"] objectAtIndex:i] intValue];
                     
                     [_arr addObject:objpost];
                     
                 }
                 NSLog(@"_arr:%@",_arr);
                 [self.tblPost reloadData];
                 
                 last_RecordCount += 10;
             }
             else if ([[response valueForKey:@"status"]integerValue] == 0)
             {
                 
                 //[self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Error];
             }
             
         }
     }];
    
}

#pragma mark-  Commaon Alertview

-(void)showAlertMessage:(NSString*)message Title:(NSString *)title
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               {
                               }];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)showmenu
{
    [self.navigationController.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    [UIViewController currentViewController];
    
    NSLog(@"which ViewController is this :%@",[UIViewController currentViewController]);
    [self.frostedViewController presentMenuViewController];
}

-(void)viewWillAppear:(BOOL)animated
{
    last_RecordCount = 0;
    _arr = [NSMutableArray new];
    arrPostData=[[NSMutableArray alloc]init];
    [self showPostwithLastRecored:last_RecordCount];
    
    self.navigationController.navigationBar.hidden = NO;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(40, 0,SCREEN_MAX_LENGTH-40, 40)];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"Raleway" size:16];
    label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    label.textAlignment = NSTextAlignmentLeft;
    label.textColor =[UIColor whiteColor];
    label.text=@"Home";
    self.navigationItem.titleView = label;
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu_icon"]style:UIBarButtonItemStylePlain target:self action:@selector(showmenu)];
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    
}

-(void)Back
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)btnpostUpload:(id)sender
{
    PostSend_VC * objPostSend_VC = [self.storyboard instantiateViewControllerWithIdentifier:@"PostSend_VC"];
    objPostSend_VC.isPostEditing = NO;
    //objPostSend_VC.enumPlayer = PLAYER_OFF;
    [self.navigationController presentViewController:objPostSend_VC animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _arr.count;//arrPostData.count;    //count number of row from counting array hear cataGorry is An Array
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"ImageCell";
    
    HomeTableViewCell *cell = (HomeTableViewCell *)  [_tblPost dequeueReusableCellWithIdentifier:@"ImageCell" forIndexPath:indexPath];
    
    objpost = [_arr objectAtIndex:indexPath.row];
    //cell.tag = indexPath.row;
    cell.lblUsername.text = [NSString stringWithFormat:@"%@ %@",objpost.Firstname,objpost.Lastname];
    cell.lblMsgCaption.text = objpost.PostName;
    cell.lblPostTime.text = objpost.PostTime;
    cell.lblLikeCount.text = [NSString stringWithFormat:@"%@ Like",objpost.likeCount];
    cell.lblCommentCount.text = [NSString stringWithFormat:@"%@ Comment",objpost.CommentCount];
    cell.imgViewProfilePicture.layer.cornerRadius = cell.imgViewProfilePicture.frame.size.height/2;
    cell.imgViewProfilePicture.clipsToBounds = YES;
    [cell.imgViewProfilePicture sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",UserProfilePictureBaseURL,objpost.UserImageUrl]] placeholderImage:[UIImage imageNamed:@"upload_icon"]];
    cell.ImgViewPost.clipsToBounds = YES;
    cell.btnPlay.hidden = YES;
    
    cell.tag                    = indexPath.row + 1;
    cell.ImgViewPost.tag        = 10001 + indexPath.row;
    cell.btnLike.tag            = 10000 + indexPath.row;
    cell.btnComment.tag         = 100000 + indexPath.row;
    cell.btnShare.tag           = 1000000 + indexPath.row;
    cell.btnEditDeletePost.tag  = indexPath.row + 1000000000;
    
    [cell.btnLike addTarget:self action:@selector(btnLikeAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnShare addTarget:self action:@selector(btnShareAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnEditDeletePost addTarget:self action:@selector(btnEditDeleteAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnComment addTarget:self action:@selector(btnCommentAction:) forControlEvents:UIControlEventTouchUpInside];
    
    if (objpost.liked == 0)
    {
        [cell.btnLike setImage:[UIImage imageNamed:@"like_icon"] forState:UIControlStateNormal];
    }
    else if (objpost.liked == 1)
    {
        [cell.btnLike setImage:[UIImage imageNamed:@"like_hover"] forState:UIControlStateNormal];
    }
    
    if ([objpost.PostType isEqualToString:@"video"] && cell.tag == indexPath.row + 1)
    {
        cell.btnPlay.hidden = NO;
        cell.btnPlay.tag = indexPath.row;
        [cell.btnPlay addTarget:self action:@selector(playButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.ImgViewPost sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",VideoPost,objpost.PostvideoThumbnail]] placeholderImage:[UIImage imageNamed:@"upload_icon"]];
        cell.ImgViewPost.userInteractionEnabled = NO;
    }
    else
    {
        [cell.ImgViewPost sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImagePost,objpost.PostContent]] placeholderImage:[UIImage imageNamed:@"upload_icon"]];
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(zoomPicture:)];
        
        tap.delegate = self;
        cell.ImgViewPost.userInteractionEnabled = YES;
        [cell.ImgViewPost addGestureRecognizer:tap];
    }
    
    
    return cell;
}

-(IBAction)btnEditDeleteAction:(UIButton *)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"What do you want to do with the post?"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:@"Delete it"
                                                    otherButtonTitles:@"Edit it", nil];
    actionSheet.tag = 100;
    selectedIndex = sender.tag - 1000000000;
    
    [actionSheet showInView:self.view];
    
    /*NSIndexPath *myIP = [NSIndexPath indexPathForRow:sender.tag - 100  inSection:0];
     //Type cast it to CustomCell
     HomeTableViewCell *cell = (HomeTableViewCell*)[self.tblPost cellForRowAtIndexPath:myIP];*/
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (actionSheet.tag == 100) {
        if (buttonIndex == 0)
        {
            NSLog(@"selectedIndex = %ld",(long)selectedIndex);
            
            objpost = [_arr objectAtIndex:selectedIndex];
            [self deletePostWithPostID:objpost.PostID andIndex:selectedIndex];
            
        }
        else if (buttonIndex == 1)
        {
            NSLog(@"selectedIndex = %ld",(long)selectedIndex);
            
            PostSend_VC * objPostSend_VC    = [self.storyboard instantiateViewControllerWithIdentifier:@"PostSend_VC"];
            objPostSend_VC.isPostEditing    = YES;
            objpost                         = [_arr objectAtIndex:selectedIndex];
            objPostSend_VC.arrEditPost      = [NSMutableArray new];
            [objPostSend_VC.arrEditPost addObject:objpost];
            // NSLog(@"objPostSend_VC.arrEditPost :%@",objpost);
            
            [self.navigationController presentViewController:objPostSend_VC animated:YES completion:nil];
        }
    }
    
    
    NSLog(@"Index = %ld - Title = %@", (long)buttonIndex, [actionSheet buttonTitleAtIndex:buttonIndex]);
}

-(void)deletePostWithPostID:(NSString *)post_id andIndex:(NSInteger) selected_Index
{
    // {"apptoken":"d10289185f466003c450a15d300e50d5","post_id":"47"}
    
    NSMutableDictionary * dictParam =[[NSMutableDictionary alloc] init];
    //[dictParam setObject:[[UserModel sharedSingleton] getUserID] forKey:@"user_id"];
    [dictParam setObject:AppToken  forKey:@"apptoken"];
    [dictParam setObject:post_id forKey:@"post_id"];
    
    [Networking rawJsondataTaskwithURL:DeletePost Param:dictParam ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error) {
        if (!error) {
            NSLog(@"Delete Post Data : %@",response);
            
            
            if ([[response valueForKey:@"status"]integerValue] == 1)
            {
                [self.arr removeObjectAtIndex:selected_Index];
                [self.tblPost reloadData];
            }
            else if ([[response valueForKey:@"status"]integerValue] == 0)
            {
                [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Error];
            }
            
            
        }
        else
        {
            [self showAlertMessage:@"Server Error please try after sometime" Title:Alert_Title_Error];
        }
    }];
    
}

-(void)zoomPicture:(UITapGestureRecognizer *)sender
{
    UIView * view = sender.view;
    NSLog(@"%ld", (long)view.tag);
    
    NSIndexPath *myIP = [NSIndexPath indexPathForRow:sender.view.tag - 10001 inSection:0];
    //Type cast it to CustomCell
    HomeTableViewCell *cell = (HomeTableViewCell*)[self.tblPost cellForRowAtIndexPath:myIP];
    [EXPhotoViewer showImageFrom:cell.ImgViewPost];
}

-(void)btnCommentAction:(UIButton *)sender
{
    objpost = [self.arr objectAtIndex:sender.tag - 100000];
    
    ShowAllPostCommentsVC * objShowAllPostCommentsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ShowAllPostCommentsVC"];
    
    objShowAllPostCommentsVC.strPostID          = objpost.PostID;
    objShowAllPostCommentsVC.strCommentCount    = objpost.CommentCount;
    objShowAllPostCommentsVC.strLikeCount       = objpost.likeCount;
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:objShowAllPostCommentsVC];
    [self presentViewController:nav animated:YES completion:nil];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    NSInteger currentOffset = scrollView.contentOffset.y;
    NSInteger maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
    if (maximumOffset - currentOffset <= -40) {
        NSLog(@"reload");
        [self showPostwithLastRecored:last_RecordCount];
    }
}


-(void)playButtonClicked:(UIButton*)sender
{
    objpost = [_arr objectAtIndex:sender.tag];
    
    NSString *path = [NSString stringWithFormat:@"%@",objpost.PostContent];
    // http://techslides.com/demos/sample-videos/small.mp4
    NSURL *videoURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",VideoPost,path]];
    /*moviePlayer = [[MPMoviePlayerViewController alloc] initWithContentURL:videoURL];
     [moviePlayer.moviePlayer prepareToPlay];
     [self presentMoviePlayerViewControllerAnimated:moviePlayer];
     [moviePlayer.moviePlayer play];*/
    
    AVPlayer *avPlayer = [AVPlayer playerWithURL:videoURL];
    avPlayer.automaticallyWaitsToMinimizeStalling = true;
    
    AVPlayerLayer *layer = [AVPlayerLayer playerLayerWithPlayer:avPlayer];
    avPlayer.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    layer.frame = self.view.bounds;
    //[self.view.layer addSublayer: layer];
    
    // create a player view controller
    AVPlayerViewController *controller = [[AVPlayerViewController alloc]init];
    controller.view.frame = self.view.frame;
    controller.player = avPlayer;
    //[self.view addSubview:controller.view];
    
    AVAsset *avAsset = [AVAsset assetWithURL:videoURL];
    playerItem = [[AVPlayerItem alloc]initWithAsset:avAsset];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:controller.player.currentItem];
    
    [self presentViewController:controller animated:YES completion:nil];
    [controller.player play];
    
}

/*- (NSTimeInterval) availableDuration;
{
    NSArray *loadedTimeRanges = [[self.player currentItem] loadedTimeRanges];
    CMTimeRange timeRange = [[loadedTimeRanges objectAtIndex:0] CMTimeRangeValue];
    Float64 startSeconds = CMTimeGetSeconds(timeRange.start);
    Float64 durationSeconds = CMTimeGetSeconds(timeRange.duration);
    NSTimeInterval result = startSeconds + durationSeconds;
    return result;
}*/

-(void)itemDidFinishPlaying:(NSNotification *) notification {
    
    NSLog(@"Finish Video");
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:playerItem];
}

-(void)btnLikeAction:(TTFaveButton *)sender
{
    // {"apptoken":"d10289185f466003c450a15d300e50d5","user_id":"95","post_id":"2"}
    PostModel *objPostGetData = [_arr objectAtIndex:sender.tag - 10000];
    
    NSMutableDictionary * dictParam =[[NSMutableDictionary alloc] init];
    [dictParam setObject:AppToken  forKey:@"apptoken"];
    [dictParam setObject:[[UserModel sharedSingleton] getUserID]  forKey:@"user_id"];
    [dictParam setObject:objPostGetData.PostID  forKey:@"post_id"];
    
    [Networking rawJsondataTaskwithURL:LikeHomePost Param:dictParam ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error)
     {
         if (!error) {
             
             NSLog(@"LIke Unlike Post Data :%@",response);
             if ([[response valueForKey:@"code"] intValue] == 1)
             {
                 NSLog(@"Liked Value: %d",objPostGetData.liked);
                 if (objPostGetData.liked == 1)
                 {
                     objPostGetData.liked = 0;
                     objPostGetData.likeCount = [NSString stringWithFormat:@"%d",[objPostGetData.likeCount intValue] - 1];
                     [sender setImage:[UIImage imageNamed:@"like_icon"] forState:UIControlStateNormal];
                 }
                 else
                 {
                     objPostGetData.liked = 1;
                     objPostGetData.likeCount = [NSString stringWithFormat:@"%d",[objPostGetData.likeCount intValue] + 1];
                     [sender setImage:[UIImage imageNamed:@"like_hover"] forState:UIControlStateNormal];
                 }
                 NSLog(@"Update Liked Value: %d",objPostGetData.liked);
                 [self.tblPost reloadData];
                 
             }
             else if ([[response valueForKey:@"code"] intValue] == 0)
             {
                 
             }
         }
     }];
    
}

-(void)btnShareAction:(UIButton *)sender
{
    objpost = [_arr objectAtIndex:sender.tag - 1000000];
    
    NSURL *SharingUrl;
    if ([objpost.PostType isEqualToString:@"video"])
    {
        NSLog(@"Video Sharing");
        SharingUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",VideoPost,objpost.PostContent]];
    }
    else
    {
        NSLog(@"Image Sharing");
        SharingUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImagePost,objpost.PostContent]];
    }
    
    NSArray *objectsToShare = @[SharingUrl];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:nil];
}

-(void)doneButtonClick:(NSNotification*)aNotification{
    NSNumber *reason = [aNotification.userInfo objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
    
    if ([reason intValue] == MPMovieFinishReasonUserExited) {
        // Your done button action here
        [moviePlayer.view removeFromSuperview] ;
        [self dismissMoviePlayerViewControllerAnimated];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"ImageCell";
    HomeTableViewCell *cell = [self.tblPost dequeueReusableCellWithIdentifier:MyIdentifier];
    objpost = [_arr objectAtIndex:indexPath.row];
    return 230 + [self heightForLabelwithText:objpost.PostName withFont:[UIFont systemFontOfSize:15.0] withWidth:cell.lblMsgCaption.frame.size.width] ;
    
}

-(CGFloat )heightForLabelwithText:(NSString *)text withFont:(UIFont *)font withWidth:(CGFloat)width
{
    CGRect rect = CGRectMake(0, 0, width, CGFLOAT_MAX);
    
    UILabel * lbl = [[UILabel alloc] initWithFrame:rect];
    lbl.numberOfLines = 0;
    lbl.lineBreakMode = NSLineBreakByCharWrapping;
    lbl.font = font;
    lbl.text = text;
    [lbl sizeToFit];
    
    return lbl.frame.size.height;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    _viewHeader.frame = CGRectMake(0, 0, self.tblPost.frame.size.width, _viewHeader.frame.size.height);
    _viewHeader.backgroundColor = UIColorFromRGB(Color_NavigationBar);
    UIView *view = [[UIView alloc] init ];
    tableView.tableHeaderView = _viewHeader;
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.001/*self.viewheader.frame.size.height*/;
}


#pragma mark Header Buttons Action

-(IBAction)btnHeaderActions:(UIButton *)sender
{
    if (sender.tag == 1001)
    {
        [self viewDidLoad];
    }
    else if (sender.tag == 1002)
    {
        Chat_Call_List_VC * objChat_Call_List_VC = [self.storyboard instantiateViewControllerWithIdentifier:@"Chat_Call_List_VC"];
        [self.navigationController pushViewController:objChat_Call_List_VC animated:YES];
    }
    else if (sender.tag == 1003)
    {
        MeetRadarAnimation_VC * objMeetRadarAnimation_VC = [self.storyboard instantiateViewControllerWithIdentifier:@"MeetRadarAnimation_VC"];
        [self.navigationController pushViewController:objMeetRadarAnimation_VC animated:YES];
        
    }
    else if (sender.tag == 1004)
    {
        ExtendFeature_VC * objExtendFeature_VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ExtendFeature_VC"];
        [self.navigationController pushViewController:objExtendFeature_VC animated:YES];
    }
    else if (sender.tag == 1005)
    {
        NewsAll_VC * objNewsAll_VC = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsAll_VC"];
        [self.navigationController pushViewController:objNewsAll_VC animated:YES];
    }
    else if (sender.tag == 1006)
    {
        SettingAppVC * objSettingAppVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingAppVC"];
        [self.navigationController pushViewController:objSettingAppVC animated:YES];
    }
    else if (sender.tag == 1007)
    {
        /*[[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isLogin"];
         [[NSUserDefaults standardUserDefaults] synchronize];*/
        [SideMenuController logout];
    }
}


@end
