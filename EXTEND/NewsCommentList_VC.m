//
//  NewsCommentList_VC.m
//  EXTEND
//
//  Created by Apple on 14/03/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "NewsCommentList_VC.h"
#import "CommentCell.h"
#import "Edit_Reply_Comment_VC.h"
#import "NewsMoreReplies_VC.h"

#import "Networking.h"
#import "UserModel.h"

#import <SDWebImage/UIImageView+WebCache.h>


@interface NewsCommentList_VC ()
{
    NSMutableArray * arrCommentData;
}
@property (strong, nonatomic) IBOutlet UITableView *tblComment;
@property (weak, nonatomic) IBOutlet UIView *viewCommentBox;
@property (weak, nonatomic) IBOutlet UITextView *tv_AddComment;
@property (weak, nonatomic) IBOutlet UILabel * lblLikeCount;
@property (weak, nonatomic) IBOutlet UILabel * lblCommentCount;

@end

@implementation NewsCommentList_VC
@synthesize viewheightconst;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //viewheightconst.constant=50;
    
    self.view.layer.cornerRadius = 10;
    self.view.backgroundColor = [UIColor blackColor];
    self.view.clipsToBounds = YES;
    
    
    self.tv_AddComment.layer.cornerRadius = self.tv_AddComment.frame.size.height/2;
    self.tv_AddComment.clipsToBounds = YES;
    
    //self.tv_AddComment.contentInset = UIEdgeInsetsMake(4,8,0,0);
    self.tv_AddComment.textContainerInset = UIEdgeInsetsMake(15, 12, 0, 0);
    
    _lblLikeCount.text = _strLikeCount;
    _lblCommentCount.text = _strCommentCount;
   
    NSLog(@"News Post ID : %@",_strNewsPostID);
    
}

-(void)getCommentswithNewsID:(NSString *)NewsID
{
    //{"apptoken":"d10289185f466003c450a15d300e50d5","news_id":"2"}


    NSMutableDictionary * dictParam =[[NSMutableDictionary alloc] init];
    //[dictParam setObject:[[UserModel sharedSingleton] getUserID] forKey:@"user_id"];
    [dictParam setObject:AppToken  forKey:@"apptoken"];
    [dictParam setObject:NewsID forKey:@"news_id"];
    
    [Networking rawJsondataTaskwithURL:ShowCommentsNews Param:dictParam ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error) {
        if (!error) {
            NSLog(@"All Comment Data : %@",response);
            
            if ([[response valueForKey:@"status"]integerValue] == 1)
            {
                arrCommentData = [[NSMutableArray alloc] init];
                
                if ([response valueForKey:@"news_comment"] == [NSNull null])
                {
                    return ;
                }
                
                arrCommentData = [response valueForKey:@"news_comment"];
                [_tblComment reloadData];

            }
            else if ([[response valueForKey:@"status"]integerValue] == 0)
            {
                
                [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Error];
            }
            
        }
        else
        {
            
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma Mark-  Commaon Alertview

-(void)showAlertMessage:(NSString*)message Title:(NSString *)title
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               {
                               }];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arrCommentData.count;    //count number of row from counting array hear cataGorry is An Array
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"CommentCell";
    
    CommentCell *cell = (CommentCell *)  [_tblComment dequeueReusableCellWithIdentifier:MyIdentifier forIndexPath:indexPath];
    //    if (cell == nil)
    //    {
    //        cell = [[HomeTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
    //                                        reuseIdentifier:MyIdentifier];
    //    }
//    CGRect rect = CGRectMake(cell.viewMain.frame.origin.x, cell.viewMain.frame.origin.y, cell.viewMain.frame.size.width, 50);
//    cell.viewMain.frame = rect;
    
    
    /*cell.constraintHeightMainView.constant = 50;
    [cell setNeedsLayout];*/
    cell.viewMain.layer.cornerRadius = 25;
    cell.viewMain.clipsToBounds = YES;
    
    
    cell.imgViewMainCommenterProPic.layer.cornerRadius = cell.imgViewMainCommenterProPic.frame.size.height/2;
    cell.imgViewMainCommenterProPic.layer.borderWidth = 1;
    cell.imgViewMainCommenterProPic.layer.borderColor = UIColorFromRGB(Color_NavigationBar).CGColor;
    cell.imgViewMainCommenterProPic.clipsToBounds = YES;
    
    cell.imgViewFirstReplyerProPic.layer.cornerRadius = cell.imgViewFirstReplyerProPic.frame.size.height/2;
    cell.imgViewFirstReplyerProPic.layer.borderWidth = 1;
    cell.imgViewFirstReplyerProPic.layer.borderColor = UIColorFromRGB(Color_NavigationBar).CGColor;
    cell.imgViewFirstReplyerProPic.clipsToBounds = YES;
    
    cell.imgViewSecondReplyerProPic.layer.cornerRadius = cell.imgViewSecondReplyerProPic.frame.size.height/2;
    cell.imgViewSecondReplyerProPic.layer.borderWidth = 1;
    cell.imgViewSecondReplyerProPic.layer.borderColor = UIColorFromRGB(Color_NavigationBar).CGColor;
    cell.imgViewSecondReplyerProPic.clipsToBounds = YES;
    
    [cell.imgViewMainCommenterProPic sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",UserProfilePictureBaseURL,[[arrCommentData objectAtIndex:indexPath.row] valueForKey:@"image"]]] placeholderImage:[UIImage imageNamed:@"upload_icon"]];
    cell.lblMainCommenterUsername.text = [NSString stringWithFormat:@"%@ %@",[[arrCommentData objectAtIndex:indexPath.row] valueForKey:@"first_name"],[[arrCommentData objectAtIndex:indexPath.row] valueForKey:@"last_name"]];
    cell.lblCommentDateTime.text = [[arrCommentData objectAtIndex:indexPath.row] valueForKey:@"posted_Date"];
    if ([[[arrCommentData objectAtIndex:indexPath.row] valueForKey:@"count"] intValue] != 0)
    {
        cell.lblReplyCount.text = [NSString stringWithFormat:@"%d",[[[arrCommentData objectAtIndex:indexPath.row] valueForKey:@"count"] intValue] ];
        cell.btnMore.hidden = YES;
        if ([[[arrCommentData objectAtIndex:indexPath.row] valueForKey:@"count"] intValue] > 2)
        {
            cell.btnMore.hidden = NO;
            [cell.btnMore addTarget:self action:@selector(btnMoreAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        
    }
    else
    {
        cell.lblReplyCount.text = nil;
        cell.btnMore.hidden = YES;
    }
    
    
    cell.lblCommentDescription.text = [[arrCommentData objectAtIndex:indexPath.row] valueForKey:@"comment"];
    
    cell.btnReply.tag = 1000000 + indexPath.row;
    cell.btnMore.tag = 10000 + indexPath.row;
    
    if ([[[arrCommentData objectAtIndex:indexPath.row] valueForKey:@"user_Id"] isEqualToString:[[UserModel sharedSingleton] getUserID]])
    {
        cell.viewMain.backgroundColor = UIColorFromRGB(Color_NavigationBar);
        cell.lblReplyerSecondReplyDescription.textColor = cell.lblReplyCount.textColor = cell.lblCommentDateTime.textColor = cell.lblCommentDescription.textColor = cell.lblReplyerFirstUsername.textColor = cell.lblMainCommenterUsername.textColor = cell.lblReplyerSecondUsername.textColor = cell.lblReplyerFirstReplyDescription.textColor = [UIColor whiteColor];
        [cell.btnReply setImage:[UIImage imageNamed:@"edit_comment_icon"] forState:UIControlStateNormal];
        [cell.btnReply addTarget:self action:@selector(btnEditAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        cell.viewMain.backgroundColor = [UIColor whiteColor];
        cell.lblReplyerSecondReplyDescription.textColor = cell.lblReplyCount.textColor = cell.lblCommentDateTime.textColor = cell.lblCommentDescription.textColor = cell.lblReplyerFirstUsername.textColor = cell.lblMainCommenterUsername.textColor = cell.lblReplyerSecondUsername.textColor = cell.lblReplyerFirstReplyDescription.textColor = [UIColor blackColor];
        
        [cell.btnReply setImage:[UIImage imageNamed:@"reply_icon"] forState:UIControlStateNormal];
        [cell.btnReply addTarget:self action:@selector(btnReplyAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    
    if ([[[arrCommentData objectAtIndex:indexPath.row] valueForKey:@"rply_data"] count] > 0)
    {
        if ([[[arrCommentData objectAtIndex:indexPath.row] valueForKey:@"rply_data"] count] > 1)
        {
            /// For First Reply
            
            cell.viewReplyFirst.hidden = NO;
            cell.viewReplySecond.hidden = NO;
            
            NSLog(@"UserReply 2: %@ ",[[[[arrCommentData valueForKey:@"rply_data"] objectAtIndex:indexPath.row] objectAtIndex:0]valueForKey:@"reply"] );
            
            [cell.imgViewFirstReplyerProPic sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",UserProfilePictureBaseURL,[[[[arrCommentData valueForKey:@"rply_data"] objectAtIndex:indexPath.row] objectAtIndex:0] valueForKey:@"image"]]] placeholderImage:[UIImage imageNamed:@"upload_icon"]];
            cell.lblReplyerFirstUsername.text = [NSString stringWithFormat:@"%@ %@",[[[[arrCommentData valueForKey:@"rply_data"] objectAtIndex:indexPath.row] objectAtIndex:0] valueForKey:@"first_name"],[[[[arrCommentData valueForKey:@"rply_data"] objectAtIndex:indexPath.row] objectAtIndex:0] valueForKey:@"last_name"]];
            NSLog(@"UserReply: %@ ",[[[arrCommentData valueForKey:@"rply_data"] objectAtIndex:0] valueForKey:@"reply"]);
            
            cell.lblReplyerFirstReplyDescription.text = [[[[arrCommentData valueForKey:@"rply_data"] objectAtIndex:indexPath.row] objectAtIndex:0] valueForKey:@"reply"];
            
            
            
            // For Second Reply
            [cell.imgViewSecondReplyerProPic sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",UserProfilePictureBaseURL,[[[[arrCommentData valueForKey:@"rply_data"] objectAtIndex:indexPath.row] objectAtIndex:1] valueForKey:@"image"]]] placeholderImage:[UIImage imageNamed:@"upload_icon"]];
            cell.lblReplyerSecondUsername.text = [NSString stringWithFormat:@"%@ %@",[[[[arrCommentData valueForKey:@"rply_data"] objectAtIndex:indexPath.row] objectAtIndex:1] valueForKey:@"first_name"],[[[[arrCommentData valueForKey:@"rply_data"] objectAtIndex:indexPath.row] objectAtIndex:1] valueForKey:@"last_name"]];
            cell.lblReplyerSecondReplyDescription.text = [[[[arrCommentData valueForKey:@"rply_data"] objectAtIndex:indexPath.row] objectAtIndex:1] valueForKey:@"reply"];
            
        }
        else //if ([[[arrCommentData objectAtIndex:indexPath.row] valueForKey:@"rply_data"] count] > 1)
        {
            cell.viewReplySecond.hidden = YES;
            cell.viewReplyFirst.hidden = NO;
         
            [cell.imgViewFirstReplyerProPic sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",UserProfilePictureBaseURL,[[[[arrCommentData valueForKey:@"rply_data"] objectAtIndex:indexPath.row] objectAtIndex:0] valueForKey:@"image"]]] placeholderImage:[UIImage imageNamed:@"upload_icon"]];
            cell.lblReplyerFirstUsername.text = [NSString stringWithFormat:@"%@ %@",[[[[arrCommentData valueForKey:@"rply_data"] objectAtIndex:indexPath.row] objectAtIndex:0] valueForKey:@"first_name"],[[[[arrCommentData valueForKey:@"rply_data"] objectAtIndex:indexPath.row] objectAtIndex:0] valueForKey:@"last_name"]];
            
            cell.lblReplyerFirstReplyDescription.text = [[[[arrCommentData valueForKey:@"rply_data"] objectAtIndex:indexPath.row] objectAtIndex:0] valueForKey:@"reply"];
            
            
            NSLog(@"UserReply 1: %@ ",[[[[arrCommentData valueForKey:@"rply_data"] objectAtIndex:indexPath.row] objectAtIndex:0]valueForKey:@"reply"] );
            
            
        }
        
    }
    else
    {
        cell.viewReplyFirst.hidden = YES;
        cell.viewReplySecond.hidden = YES;
    }
    return cell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"CommentCell";
    CommentCell *cell = [self.tblComment dequeueReusableCellWithIdentifier:MyIdentifier];
    
    CGRect newFrame = cell.lblCommentDescription.frame;
    newFrame.size.height = [self heightForLabelwithText:[[arrCommentData objectAtIndex:indexPath.row] valueForKey:@"comment"] withFont:[UIFont systemFontOfSize:cell.lblCommentDescription.font.pointSize] withWidth:cell.lblCommentDescription.frame.size.width];
    cell.lblCommentDescription.frame = newFrame;
    
    if ([[[arrCommentData objectAtIndex:indexPath.row] valueForKey:@"rply_data"] count]==0)
    {
        return  55 + [self heightForLabelwithText:[[arrCommentData objectAtIndex:indexPath.row] valueForKey:@"comment"] withFont:[UIFont systemFontOfSize:cell.lblCommentDescription.font.pointSize] withWidth:cell.lblCommentDescription.frame.size.width] + 5 ;
    }
    else if ([[[arrCommentData objectAtIndex:indexPath.row] valueForKey:@"rply_data"] count]==1)
    {
        CGRect newFrame = cell.lblReplyerFirstReplyDescription.frame;
        newFrame.size.height = [self heightForLabelwithText:[[[[arrCommentData valueForKey:@"rply_data"] objectAtIndex:indexPath.row] objectAtIndex:0] valueForKey:@"reply"] withFont:[UIFont systemFontOfSize:cell.lblReplyerFirstReplyDescription.font.pointSize] withWidth:cell.lblReplyerFirstReplyDescription.frame.size.width];
        cell.lblReplyerFirstReplyDescription.frame = newFrame;
        cell.viewReplyFirst.frame = CGRectMake(cell.viewReplyFirst.frame.origin.x, cell.viewReplyFirst.frame.origin.y, cell.viewReplyFirst.frame.size.width,  cell.lblReplyerFirstReplyDescription.frame.size.height);
        
        return 110 + cell.viewReplyFirst.frame.size.height;
    }
    else{
        
        CGRect newFrame = cell.lblReplyerFirstReplyDescription.frame;
        newFrame.size.height = [self heightForLabelwithText:[[[[arrCommentData valueForKey:@"rply_data"] objectAtIndex:indexPath.row] objectAtIndex:0] valueForKey:@"reply"] withFont:[UIFont systemFontOfSize:cell.lblReplyerFirstReplyDescription.font.pointSize] withWidth:cell.lblReplyerFirstReplyDescription.frame.size.width];
        cell.lblReplyerFirstReplyDescription.frame = newFrame;
        cell.viewReplyFirst.frame = CGRectMake(cell.viewReplyFirst.frame.origin.x, cell.viewReplyFirst.frame.origin.y, cell.viewReplyFirst.frame.size.width,  cell.lblReplyerFirstReplyDescription.frame.size.height);
        
        
        /////
        CGRect newFramelblSecond = cell.lblReplyerSecondReplyDescription.frame;
        newFramelblSecond.size.height = [self heightForLabelwithText:[[[[arrCommentData valueForKey:@"rply_data"] objectAtIndex:indexPath.row] objectAtIndex:1] valueForKey:@"reply"] withFont:[UIFont systemFontOfSize:cell.lblReplyerSecondReplyDescription.font.pointSize] withWidth:cell.lblReplyerSecondReplyDescription.frame.size.width];
        cell.lblReplyerSecondReplyDescription.frame = newFramelblSecond;
        cell.viewReplySecond.frame = CGRectMake(cell.viewReplySecond.frame.origin.x, cell.viewReplySecond.frame.origin.y, cell.viewReplySecond.frame.size.width,  cell.lblReplyerSecondReplyDescription.frame.size.height);
        
        //NSLog(@"Reply First View Height : %f",cell.viewReplyFirst.frame.size.height);
        return 160 + cell.viewReplyFirst.frame.size.height + cell.viewReplySecond.frame.size.height;
    }
    
    return cell.contentView.frame.size.height ;//- 50 + [self heightForLabelwithText:[[arrNewsData objectAtIndex:indexPath.row] valueForKey:@"content"] withFont:[UIFont systemFontOfSize:cell.lblNewsPostDescription.font.pointSize] withWidth:cell.lblNewsPostDescription.frame.size.width] ;
    
}

-(CGFloat )heightForLabelwithText:(NSString *)text withFont:(UIFont *)font withWidth:(CGFloat)width
{
    CGRect rect = CGRectMake(0, 0, width, CGFLOAT_MAX);
    
    UILabel * lbl = [[UILabel alloc] initWithFrame:rect];
    lbl.numberOfLines = 0;
    lbl.lineBreakMode = NSLineBreakByCharWrapping;
    lbl.font = font;
    lbl.text = text;
    [lbl sizeToFit];
    
    return lbl.frame.size.height;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.001/*self.viewheader.frame.size.height*/;
}

#pragma Reply & Edit Actions

-(IBAction)btnReplyAction:(UIButton *)sender
{
    Edit_Reply_Comment_VC * objEdit_Reply_Comment_VC = [self.storyboard instantiateViewControllerWithIdentifier:@"Edit_Reply_Comment_VC"];
    objEdit_Reply_Comment_VC.isReply = YES;
    objEdit_Reply_Comment_VC.strCommentID = [[arrCommentData objectAtIndex:sender.tag - 1000000] valueForKey:@"Id"];
    objEdit_Reply_Comment_VC.strReplyUserID = [[arrCommentData objectAtIndex:sender.tag - 1000000] valueForKey:@"id"];
    [self.navigationController pushViewController:objEdit_Reply_Comment_VC animated:YES];
}

-(IBAction)btnEditAction:(UIButton *)sender
{
    Edit_Reply_Comment_VC * objEdit_Reply_Comment_VC = [self.storyboard instantiateViewControllerWithIdentifier:@"Edit_Reply_Comment_VC"];
    objEdit_Reply_Comment_VC.isReply = NO;
    objEdit_Reply_Comment_VC.strCommentID = [[arrCommentData objectAtIndex:sender.tag - 1000000] valueForKey:@"Id"];
    objEdit_Reply_Comment_VC.strComment = [[arrCommentData objectAtIndex:sender.tag - 1000000] valueForKey:@"comment"];
    NSLog(@"get Comment from Array:%@",[[arrCommentData objectAtIndex:sender.tag - 1000000] valueForKey:@"comment"]);

    [self.navigationController pushViewController:objEdit_Reply_Comment_VC animated:YES];
}

-(void)btnMoreAction:(UIButton *)sender
{
    NewsMoreReplies_VC * objNewsMoreReplies_VC = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsMoreReplies_VC"];
    objNewsMoreReplies_VC.strCommentID = [[arrCommentData objectAtIndex:sender.tag - 10000] valueForKey:@"Id"];
    objNewsMoreReplies_VC.strReplyUserID = [[arrCommentData objectAtIndex:sender.tag - 10000] valueForKey:@"id"];
    [self.navigationController pushViewController:objNewsMoreReplies_VC animated:YES];

}

#pragma Send Comment Action And Keyboard Animation
- (IBAction)btnSendAction:(id)sender
{
    if (_tv_AddComment.text.length !=0 && ![_tv_AddComment.text isEqualToString:@"Write a comment..."]  && _strNewsPostID != nil && _strNewsCategoryID != nil)
    {
        [self addCommentwithComment:_tv_AddComment.text withNewsID:_strNewsPostID andwithCategoryID:_strNewsCategoryID];
        
        [_tv_AddComment resignFirstResponder];
        _tv_AddComment.text = nil;
    }
}

-(void)addCommentwithComment:(NSString *)comment withNewsID:(NSString *)newsID andwithCategoryID:(NSString *)categoryID
{
    //{apptoken":"d10289185f466003c450a15d300e50d5","user_id":"2","news_id":"2","comment":"nice","cat_id":"2"}
    
    NSMutableDictionary * dictParam =[[NSMutableDictionary alloc] init];
    [dictParam setObject:[[UserModel sharedSingleton] getUserID] forKey:@"user_id"];
    [dictParam setObject:AppToken  forKey:@"apptoken"];
    [dictParam setObject:newsID forKey:@"news_id"];
    [dictParam setObject:comment forKey:@"comment"];
    [dictParam setObject:categoryID forKey:@"cat_id"];
    
    [Networking rawJsondataTaskwithURL:AddCommentsInNews Param:dictParam ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error) {
        if (!error)
        {
            if ([[response valueForKey:@"status"]integerValue] == 1)
            {
                NSLog(@"Add New Comment Data : %@",response);
                [self getCommentswithNewsID:self.strNewsPostID];
            }
            else if ([[response valueForKey:@"status"]integerValue] == 0)
            {
                
                [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Error];
            }
            
            
        }
        else
        {
            
        }
    }];
}


#pragma mark - Text View delegates

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSCharacterSet *doneButtonCharacterSet = [NSCharacterSet newlineCharacterSet];
    NSRange replacementTextRange = [text rangeOfCharacterFromSet:doneButtonCharacterSet];
    NSUInteger location = replacementTextRange.location;
    
    if (textView.text.length + text.length > 500){
        if (location != NSNotFound){
            [textView resignFirstResponder];
        }
        return NO;
    }
    else if (location != NSNotFound){
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    if (textView == _tv_AddComment && [_tv_AddComment.text isEqualToString:@"Write a comment..."]) {
        _tv_AddComment.text = nil;
    }
    
    NSLog(@"Did begin editing");
    //  [self animateTextView:textView up:YES];
}

-(void)textViewDidChange:(UITextView *)textView{
    NSLog(@"Did Change");
    
    
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    NSLog(@"Did End editing");
    
    if (textView == _tv_AddComment && [_tv_AddComment.text isEqualToString:@""]) {
        _tv_AddComment.text = @"Write a comment...";
    }
    
    //[self animateTextView:textView up:NO];
}


-(BOOL)textViewShouldEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    return YES;
}

/*-(void)animateTextView:(UITextView*)textView up:(BOOL)up
{
    
    int a;
    a= 580-(textView.frame.origin.y+400);
    
    const int movementDistance = a;
    const float movementDuration = 0.3f;
    
    int movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
    
}*/


- (void)viewWillAppear:(BOOL)animated
{
    [self getCommentswithNewsID:self.strNewsPostID];
    
    self.navigationController.navigationBarHidden = YES;
    
    [super viewWillAppear:(BOOL)animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillDisappear:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:(BOOL)animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Keyboard appearance/disappearance handling

- (void)keyboardWillAppear:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0);
    
    [self.tblComment setContentInset:contentInsets];
    [self.tblComment setScrollIndicatorInsets:contentInsets];
    CGSize r = _tblComment.contentSize;
    [_tblComment scrollRectToVisible:CGRectMake(0, r.height-10, r.width, 10) animated:YES];
    
    CGRect messageFrame = self.viewCommentBox.frame;
    messageFrame.origin.y -= keyboardSize.height;
    [self.viewCommentBox setFrame:messageFrame];
    NSLog(@"viewCommentBox Will apperar fram: %f",_viewCommentBox.frame.origin.y);
}

- (void)keyboardWillDisappear:(NSNotification *)notification
{
    
    NSDictionary *userInfo = [notification userInfo];
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.25];
    [self.tblComment setContentInset:UIEdgeInsetsZero];
    [UIView commitAnimations];
    [self.tblComment setScrollIndicatorInsets:UIEdgeInsetsZero];
    
    CGRect messageFrame = self.viewCommentBox.frame;
    //messageFrame.origin.y += keyboardSize.height;
    messageFrame.origin.y = self.view.frame.size.height - 60;
    [self.viewCommentBox setFrame:messageFrame];
    
    NSLog(@"viewCommentBox Will Disappear fram: %f",_viewCommentBox.frame.origin.y);
}

-(IBAction)btnCancelAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
