//
//  NewsCommentList_VC.h
//  EXTEND
//
//  Created by Apple on 14/03/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsCommentList_VC : UIViewController
@property (strong, nonatomic) NSString * strNewsPostID;
@property (strong, nonatomic) NSString * strNewsCategoryID;
@property (strong, nonatomic) NSString * strLikeCount;
@property (strong, nonatomic) NSString * strCommentCount;


@property(nonatomic,weak) IBOutlet NSLayoutConstraint *viewheightconst;
@end
