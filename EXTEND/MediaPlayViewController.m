//
//  MediaPlayViewController.m
//  EXTEND
//
//  Created by Apple on 29/05/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "MediaPlayViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVKit/AVKit.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface MediaPlayViewController ()<AVAudioPlayerDelegate, NSURLSessionDelegate,NSURLSessionDataDelegate,NSURLSessionDownloadDelegate>
{
    AVAudioPlayer *player;
    AVPlayer * videoPlayer;
}
@property (strong, nonatomic) AVPlayer * songPlayer;
@property (strong, nonatomic) AVPlayerViewController * avPlayerViewcontroller;
@property (weak, nonatomic) IBOutlet UIImageView *imgviewPost;
@property (weak, nonatomic) IBOutlet UIView *viewAudio;
@property (weak, nonatomic) IBOutlet UIButton *btnPlayPause;
@property (weak, nonatomic) IBOutlet UISlider *slideRange;
@property (weak, nonatomic) IBOutlet UILabel *lblLoading;
@end

@implementation MediaPlayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    if ([self.MediaType isEqualToString:@"audio"])
    {
        [self.view bringSubviewToFront:self.viewAudio];
        /*AVPlayer *player = [[AVPlayer alloc]initWithURL:[NSURL URLWithString:self.MediaURL]];
         self.songPlayer = player;
         if (self.songPlayer !=nil)
         {
         [self.songPlayer removeObserver:self forKeyPath:@"status"];
         }
         
         [[NSNotificationCenter defaultCenter] addObserver:self
         selector:@selector(playerItemDidReachEnd:)
         name:AVPlayerItemDidPlayToEndTimeNotification
         object:[_songPlayer currentItem]];
         [self.songPlayer addObserver:self forKeyPath:@"status" options:0 context:nil];
         //[NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateProgress:) userInfo:nil repeats:YES];*/
        
        
        
        //dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        
        
        
        self.btnPlayPause.alpha = 0.5;
        self.btnPlayPause.userInteractionEnabled = NO;
        
        NSString *dataUrl = self.MediaURL;
        NSURL *url = [NSURL URLWithString:dataUrl];
        
        // 2
        NSURLSessionDataTask *downloadTask = [[NSURLSession sharedSession]
                                              dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                  // 4: Handle response here
                                                  
                                                  if (!error)
                                                  {
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          // code here
                                                          
                                                          self.btnPlayPause.alpha = 1.0;
                                                          self.btnPlayPause.userInteractionEnabled = YES;
                                                          
                                                          [self.view layoutIfNeeded];
                                                          self.lblLoading.text = nil;
                                                          NSError *errorr;
                                                          //NSData *audioData = [NSData dataWithContentsOfURL:[NSURL URLWithString:self.MediaURL]];
                                                          player = [[AVAudioPlayer alloc] initWithData:data error:&errorr];
                                                          player.delegate = self;
                                                          
                                                          self.slideRange.maximumValue = [player duration];
                                                          //_lblTimeaudio.text = [NSString stringWithFormat:@"%.2f",slider.maximumValue];
                                                          self.slideRange.value = 0.0;
                                                          [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTime:) userInfo:nil repeats:YES];
                                                          [player play];
                                                          AVAudioSession *audioSession = [AVAudioSession sharedInstance];
                                                          [audioSession setActive:YES error:nil];
                                                      });
                                                      
                                                      
                                                  }
                                                  else
                                                  {
                                                      self.lblLoading.text = @"Can't Play this Audio";
                                                  }
                                                  
                                                  
                                              }];
        
        // 3
        [downloadTask resume];
        
        
        
        
        //});
        
        
    }
    else if ([self.MediaType isEqualToString:@"video"])
    {
        [self.viewAudio removeFromSuperview];
        AVPlayerViewController *playerViewController = [[AVPlayerViewController alloc] init];
        playerViewController.player = [AVPlayer playerWithURL:[NSURL URLWithString:self.MediaURL]];
        self.avPlayerViewcontroller = playerViewController;
        [self addChildViewController:self.avPlayerViewcontroller];
        [self.view addSubview:self.avPlayerViewcontroller.view];
        self.avPlayerViewcontroller.view.frame = self.view.frame;
        self.view.autoresizesSubviews = TRUE;
        [self.avPlayerViewcontroller.player play];
        
        
    }
    else if ([self.MediaType isEqualToString:@"image"])
    {
        [self.view bringSubviewToFront:self.imgviewPost];
        [self.imgviewPost sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",self.MediaURL]] placeholderImage:[UIImage imageNamed:@"upload_icon"]];
    }
    
    [self.view layoutIfNeeded];
    
}

- (void)updateTime:(NSTimer *)timer {
    [self.view layoutIfNeeded];
    self.slideRange.value = player.currentTime;
    //_lblTimeraudioelapse.text = [NSString stringWithFormat:@"%.2f",slider.value];
    
}

-(IBAction)btnPlayPauseAction:(UIButton *)sender
{
    if (sender.isSelected == YES)
    {
        sender.selected = NO;
        [player play];
        [self.btnPlayPause setTitle:@"Play" forState:UIControlStateNormal];
    }
    else
    {
        sender.selected = YES;
        [player pause];
        [self.btnPlayPause setTitle:@"Pause" forState:UIControlStateNormal];
    }
}


-(void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data
{
    NSLog(@"\n\n11111\n\n");
}

-(void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didBecomeDownloadTask:(NSURLSessionDownloadTask *)downloadTask
{
    NSLog(@"\n\n22222\n\n");
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = NO;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    
    if (object == _songPlayer && [keyPath isEqualToString:@"status"]) {
        if (_songPlayer.status == AVPlayerStatusFailed) {
            NSLog(@"AVPlayer Failed");
            
        } else if (_songPlayer.status == AVPlayerStatusReadyToPlay) {
            NSLog(@"AVPlayerStatusReadyToPlay");
            [self.songPlayer play];
            
        } else if (_songPlayer.status == AVPlayerItemStatusUnknown) {
            NSLog(@"AVPlayer Unknown");
            
        }
    }
}

- (void)playerItemDidReachEnd:(NSNotification *)notification {
    
    //  code here to play next sound file
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidDisappear:(BOOL)animated
{
    //self.navigationController.navigationBarHidden = YES;
}

-(IBAction)btnBack:(id)sender
{
    /*[self.songPlayer pause];
     self.songPlayer = nil;
     [self.songPlayer removeObserver:self forKeyPath:@"status" context:nil];
     [[NSNotificationCenter defaultCenter] removeObserver:self];*/
    [player stop];
    player = nil;
    [self.navigationController popViewControllerAnimated:YES];
}

@end
