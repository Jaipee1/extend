//
//  CommentCell.h
//  EXTEND
//
//  Created by Apple on 14/03/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *viewMain;
@property (weak, nonatomic) IBOutlet UIView *viewReplyFirst;
@property (weak, nonatomic) IBOutlet UIView *viewReplySecond;


///////
@property (weak, nonatomic) IBOutlet UIImageView *imgViewMainCommenterProPic;
@property (weak, nonatomic) IBOutlet UILabel *lblMainCommenterUsername;
@property (weak, nonatomic) IBOutlet UILabel *lblCommentDateTime;
@property (weak, nonatomic) IBOutlet UILabel *lblReplyCount;
@property (weak, nonatomic) IBOutlet UILabel *lblCommentDescription;

//////

@property (weak, nonatomic) IBOutlet UIImageView *imgViewFirstReplyerProPic;
@property (weak, nonatomic) IBOutlet UILabel *lblReplyerFirstUsername;
@property (weak, nonatomic) IBOutlet UILabel *lblReplyerFirstReplyDescription;


//////

@property (weak, nonatomic) IBOutlet UIImageView *imgViewSecondReplyerProPic;
@property (weak, nonatomic) IBOutlet UILabel *lblReplyerSecondUsername;
@property (weak, nonatomic) IBOutlet UILabel *lblReplyerSecondReplyDescription;


/////
@property (weak, nonatomic) IBOutlet UIButton *btnReply;
@property (weak, nonatomic) IBOutlet UIButton *btnMore;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightMainView;

@end
