//
//  UserModel.h
//  EXTEND
//
//  Created by Apple on 04/03/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserModel : NSObject

+(UserModel *)sharedSingleton;

@property (nonatomic, strong) NSString * strUserID;
@property (nonatomic, strong) NSString * strUserFirstName;
@property (nonatomic, strong) NSString * strUserLastName;
@property (nonatomic, strong) NSString * strUserEmailAddress;
@property (nonatomic, strong) NSString * strUserPhoneNumber;
@property (nonatomic, strong) NSString * strUserProfileImageUrl;
@property (nonatomic, strong) NSString * strUserPassword;
@property  BOOL UserLogin;

//-(void)setUserID:(NSString *)strUserID;
-(NSString *)getUserID;
-(NSString *)getUserFirstName;
-(NSString *)getUserLastName;
-(NSString *)getUserEmailAddress;
-(NSString *)getUserPhoneNumber;
-(NSString *)getUserProfileImageUrl;
-(NSString *)getUserPassword;

@end
