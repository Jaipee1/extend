//
//  ExtendFeature_VC.m
//  EXTEND
//
//  Created by Apple on 16/03/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "ExtendFeature_VC.h"
#import "ExtendCommentList_VC.h"
#import "ExtendSetting_VC.h"
#import "REFrostedViewController.h"
#import "UserModel.h"
#import "Networking.h"
#import "HelperClass.h"
#import "MyMarker.h"
#import "SubscriptionPlanView.h"
#import "LocationManagerModel.h"
#import "MapPostHistoryVC.h"
#import "SubscriptionTableViewCell.h"


#import <SDWebImage/UIImageView+WebCache.h>
#import <MobileCoreServices/MobileCoreServices.h>

#define kPayPalEnvironment PayPalEnvironmentSandbox

@interface ExtendFeature_VC ()<GMSMapViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate, UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray * arrAllUserData;
    NSMutableArray * arrGetAllPlans;
    GMSMarker *markerForPost;
    UIButton * btnBackground;
    
    UIAlertView *alertImagechoose;
    UIImagePickerController *ImgPicker;
    UIImage *IMAGE;
    
    NSString * latitudePost;
    NSString * longitudePost;
    
    SubscriptionPlanView * subscriptionView;
    
    NSString * strPlanID;
}
//@property (strong, nonatomic) IBOutlet UIView *markerView;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewUser;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewPin;
@property (retain, nonatomic) IBOutlet UIView *viewPopupExtend;
@property (weak, nonatomic) IBOutlet UIImageView *ImageViewExtendPost;
@property (weak, nonatomic) IBOutlet UIImageView *imageviewPropic;
@property (weak, nonatomic) IBOutlet UILabel *lblUserFullName;
@property (weak, nonatomic) IBOutlet UITextField *txtPostTitle;
@property (weak, nonatomic) IBOutlet UITextView *tvPostDescription;
@property (weak, nonatomic) IBOutlet UIButton *btnPostAndCancle;
@property (weak, nonatomic) IBOutlet UIButton *btnCameraShutter;
@property (weak, nonatomic) IBOutlet UILabel *lblLocationTap;

@end

@implementation ExtendFeature_VC


- (void)loadView
{
    // Create a GMSCameraPosition that tells the map to display the
    // coordinate -33.86,151.20 at zoom level 6.
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:-33.86
                                                            longitude:151.20
                                                                 zoom:15];
    mapView = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    mapView.myLocationEnabled = YES;
    mapView.delegate = self;
    self.view = mapView;
    
    mapView.camera = [GMSCameraPosition cameraWithLatitude:[[[LocationManagerModel sharedSingleton] locationManager] location].coordinate.latitude longitude:[[[LocationManagerModel sharedSingleton] locationManager] location].coordinate.longitude zoom:15];
    
 }

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupPayPalConfiguration];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [locationManager requestAlwaysAuthorization];
    }
    
    [locationManager startUpdatingLocation];
    
    // Preconnect to PayPal early
    [self setPayPalEnvironment:self.environment];
    
    [self setNavigationBar];
    [self removeGMSBlockingGestureRecognizerFromMapView:mapView];
    
}

-(void)setNavigationBar
{
    self.navigationController.navigationBar.hidden = NO;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(40, 0,SCREEN_MAX_LENGTH-40, 40)];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"Raleway" size:16];
    label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    label.textAlignment = NSTextAlignmentLeft;
    label.textColor =[UIColor whiteColor];
    label.text=@"Extend";
    self.navigationItem.titleView = label;
    
    /*UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back_icon"]style:UIBarButtonItemStylePlain target:self action:@selector(Back)];
    self.navigationItem.leftBarButtonItem = revealButtonItem;*/
    
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"setting_icon"]style:UIBarButtonItemStylePlain target:self action:@selector(setting)];
    //self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    UIBarButtonItem *btnShare = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"history"]style:UIBarButtonItemStylePlain target:self action:@selector(History)];
    
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:btnShare, rightBarButtonItem, nil]];
    
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu_icon"]style:UIBarButtonItemStylePlain target:self action:@selector(showmenu)];
    self.navigationItem.leftBarButtonItem = revealButtonItem;

}

- (void)showmenu
{
    [self.navigationController.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    [self.frostedViewController presentMenuViewController];
}

-(void)Back
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setting
{
    
    ExtendSetting_VC * objExtendSetting_VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ExtendSetting_VC"];
    [self.navigationController pushViewController:objExtendSetting_VC animated:YES];
    //[self presentViewController:objExtendSetting_VC animated:YES completion:nil];
}

-(void)History
{
    MapPostHistoryVC * objMapPostHistoryVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MapPostHistoryVC"];
    [self.navigationController pushViewController:objMapPostHistoryVC animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        NSString * strLong  = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        NSString * strLat  = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        NSLog(@"Lat : %@ \n Long: %@",strLat,strLong);
        [locationManager stopUpdatingLocation];
        [self setUserLocationWithLatitude:strLat withLongitude:strLong];
    }
}

-(void)loadMapViewwithLatitude:(CGFloat)latitude andLongitude:(CGFloat)longitude
{
    NSLog(@"load");
    mapView.camera = [GMSCameraPosition cameraWithLatitude:latitude longitude:longitude zoom:15];
    
}

-(BOOL)mapView:(GMSMapView *)mapVieww didTapMarker:(MyMarker *)marker
{
    NSLog(@"Did Tap On Marker");
    NSLog(@"User ID On Tap : %d",marker.iconView.tag);
    //NSString * str = [ExtendFeature_VC getAddressFromLatLon:marker.position.latitude withLongitude:marker.position.longitude];
    /*ExtendCommentList_VC * objExtendCommentList_VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ExtendCommentList_VC"];
    objExtendCommentList_VC.strUserID = [NSString stringWithFormat:@"%d",marker.iconView.tag];
    objExtendCommentList_VC.latitude = marker.position.latitude;
    objExtendCommentList_VC.longitude = marker.position.longitude;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:objExtendCommentList_VC];
    [self presentViewController:nav animated:YES completion:nil];*/
    
    NSLog(@"MY MARKER title:%@",marker.TitlePost);
    
    btnBackground = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, mapVieww.frame.size.width, mapVieww.frame.size.height)];
    btnBackground.backgroundColor = [UIColor blackColor];
    btnBackground.alpha = 0.4f;
    [btnBackground addTarget:self action:@selector(removePopUpView) forControlEvents:UIControlEventTouchUpInside];
    [mapView addSubview:btnBackground];
    
    NSArray *subviewArray       = [[NSBundle mainBundle] loadNibNamed:@"CustomPopupView" owner:self options:nil];
    self.viewPopupExtend        = [subviewArray objectAtIndex:0];
    self.viewPopupExtend.layer.cornerRadius = 10;
    self.viewPopupExtend.clipsToBounds = YES;
    //[self.viewPopupExtend setCenter:mapView.center];
    self.viewPopupExtend.frame  = CGRectOffset( self.viewPopupExtend.frame, (self.view.frame.size.width - self.viewPopupExtend.frame.size.width)/2, (self.view.frame.size.height - self.viewPopupExtend.frame.size.height)/6 );
    self.viewPopupExtend.backgroundColor = [UIColor whiteColor];
    self.viewPopupExtend.transform       = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    
    self.imageviewPropic.layer.cornerRadius = self.imageviewPropic.frame.size.height/2;
    self.imageviewPropic.clipsToBounds = YES;
    
    self.btnCameraShutter.hidden = true;
    self.btnPostAndCancle.tag = 10001;
    [self.btnPostAndCancle setImage:[UIImage imageNamed:@"cancel"] forState:UIControlStateNormal];
    [self.imageviewPropic sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",UserProfilePictureBaseURL,marker.urlPropic]] placeholderImage:[UIImage imageNamed:@"upload_icon"]];
    self.lblUserFullName.text   = marker.Fullname;
    self.txtPostTitle.text      = marker.TitlePost;
    self.tvPostDescription.text = marker.DescriptionPost;
    
    [self.ImageViewExtendPost sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",MapImagePost,marker.urlPost]] placeholderImage:[UIImage imageNamed:@"upload_icon"]];
    
    self.txtPostTitle.enabled = NO;
    self.tvPostDescription.editable = NO;
    self.tvPostDescription.selectable = NO;
    
    [HelperClass getAddressFromLatLon:marker.position.latitude withLongitude:marker.position.longitude compilation:^(id response, NSError *error) {
        
        if (!error) {
            self.lblLocationTap.text =  response;
        }
        else
        {
            self.lblLocationTap.text = nil;
        }
        
    }];
    
    [mapView addSubview:self.viewPopupExtend];
    
    [UIView animateWithDuration:0.3/1.5 animations:^{
        
        self.viewPopupExtend.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0);
        
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3/2 animations:^{
            
            self.viewPopupExtend.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
            
        } completion:^(BOOL finished) {
            
            [UIView animateWithDuration:0.3/2 animations:^{
                self.viewPopupExtend.transform = CGAffineTransformIdentity;
                [self.viewPopupExtend layoutIfNeeded];
                
                
            }];
        }];
    }];

    
    return true;
}

- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker {
    // your code
    
    NSLog(@"Did Tap InfoWindow Of Marker");
    
}


-(void)setUserLocationWithLatitude:(NSString *)latitude withLongitude:(NSString *)longitude
{
    //{"apptoken":"d10289185f466003c450a15d300e50d5", "user_id":"54","location_latitude":"646fdgdfgdfg464","location_longitude":"95dsdsfds3416", "show_distance":"5"}
    
    NSMutableDictionary * dictParam =[[NSMutableDictionary alloc] init];
    [dictParam setObject:[[UserModel sharedSingleton] getUserID] forKey:@"user_id"];
    [dictParam setObject:AppToken  forKey:@"apptoken"];
    [dictParam setObject:latitude  forKey:@"location_latitude"];
    [dictParam setObject:longitude  forKey:@"location_longitude"];
    [dictParam setObject:@"25"  forKey:@"show_distance"];
    
    [Networking rawJsondataTaskwithURL:UserLocation Param:dictParam ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error) {
        if (!error)
        {
            
            NSLog(@"User Location Data : %@",response);
            
            if ([[response valueForKey:@"status"]integerValue] == 1)
            {
                //[self getAllUsersNearByUserLocationWithLatitude:latitude withLongitude:longitude];
                [self getAllExtendPostNearByUserLocationWithLatitude:latitude withLongitude:longitude];
            }
            else if ([[response valueForKey:@"status"]integerValue] == 0)
            {
                
            }
            
        }
        else
        {
            
        }
    }];
}

-(void)getAllExtendPostNearByUserLocationWithLatitude:(NSString *)latitude withLongitude:(NSString *)longitude
{
    //{"apptoken":"d10289185f466003c450a15d300e50d5", "location_latitude":"22.7234483" , "location_longitude":"75.886481" ,"distance":"5" }
    
    NSMutableDictionary * dictParam =[[NSMutableDictionary alloc] init];
    //[dictParam setObject:[[UserModel sharedSingleton] getUserID] forKey:@"user_id"];
    [dictParam setObject:AppToken  forKey:@"apptoken"];
    [dictParam setObject:latitude  forKey:@"location_latitude"];
    [dictParam setObject:longitude  forKey:@"location_longitude"];
    [dictParam setObject:@"25"  forKey:@"distance"];
    NSLog(@"dictParam DATA: %@",dictParam);
    [Networking rawJsondataTaskwithURL:ShowExtendPostOnMap Param:dictParam ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error) {
        if (!error)
        {
            NSLog(@"All Extend Post Near by User Data : %@",response);
            if ([[response valueForKey:@"status"]integerValue] == 1)
            {
                arrAllUserData = [[NSMutableArray alloc] init];
                arrAllUserData = [response valueForKey:@"user_info"];
                [self createMarkerWithUserList:arrAllUserData];
            }
            else if ([[response valueForKey:@"status"]integerValue] == 0)
            {
                
            }
        }
        else
        {
            
        }
    }];
    
}



-(void)getAllUsersNearByUserLocationWithLatitude:(NSString *)latitude withLongitude:(NSString *)longitude
{
    //{"apptoken":"d10289185f466003c450a15d300e50d5", "location_latitude":"22.7234483" , "location_longitude":"75.886481" ,"distance":"5" }
    
    NSMutableDictionary * dictParam =[[NSMutableDictionary alloc] init];
    //[dictParam setObject:[[UserModel sharedSingleton] getUserID] forKey:@"user_id"];
    [dictParam setObject:AppToken  forKey:@"apptoken"];
    [dictParam setObject:latitude  forKey:@"location_latitude"];
    [dictParam setObject:longitude  forKey:@"location_longitude"];
    [dictParam setObject:@"25"  forKey:@"distance"];
    
    [Networking rawJsondataTaskwithURL:GetAllUsersNearByUser Param:dictParam ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error) {
        if (!error)
        {
            NSLog(@"All Users Near by User Data : %@",response);
            if ([[response valueForKey:@"status"]integerValue] == 1)
            {
                arrAllUserData = [[NSMutableArray alloc] init];
                arrAllUserData = [response valueForKey:@"user_info"];
                [self createMarkerWithUserList:arrAllUserData];
            }
            else if ([[response valueForKey:@"status"]integerValue] == 0)
            {
                
            }
        }
        else
        {
            
        }
    }];

}

-(void)createMarkerWithUserList:(NSMutableArray *)array
{
    [mapView clear];

    /*for (int i = 0; i<array.count; i++)
    {
        // Creates a marker in the center of the map.
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position = CLLocationCoordinate2DMake([[[array objectAtIndex:i] valueForKey:@"location_latitude"] floatValue], [[[array objectAtIndex:i] valueForKey:@"location_longitude"] floatValue]);
        
        marker.iconView = [[[NSBundle mainBundle] loadNibNamed:@"CustomMarker" owner:self options:nil] objectAtIndex:0];
        [_imageViewUser sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",UserProfilePictureBaseURL,[[array objectAtIndex:i] valueForKey:@"profile_picture"]]] placeholderImage:[UIImage imageNamed:@"upload_icon"]];
        
        if ([[[array objectAtIndex:i] valueForKey:@"id"] isEqualToString:[[UserModel sharedSingleton] getUserID]])
        {
            _imageViewPin.image = [UIImage imageNamed:@"pin_green"];
        }
        else
        {
            _imageViewPin.image = [UIImage imageNamed:@"pin"];
        }
        _imageViewUser.layer.cornerRadius = self.imageViewUser.frame.size.width/2;
        _imageViewUser.clipsToBounds = YES;
        
        marker.iconView.tag = [[[array objectAtIndex:i] valueForKey:@"id"] integerValue];
        marker.map = mapView;
    }*/
    
    
    for (int i = 0; i<array.count; i++)
    {
        // Creates a marker in the center of the map.
        MyMarker * mark = [[MyMarker alloc] initWithProfilePictureURL:[[array objectAtIndex:i] valueForKey:@"profile_picture"]];
        mark.position = CLLocationCoordinate2DMake([[[array objectAtIndex:i] valueForKey:@"location_latitude"] floatValue], [[[array objectAtIndex:i] valueForKey:@"location_longitude"] floatValue]);
        
        
        [mark setFullname:  [[array objectAtIndex:i] valueForKey:@"first_name"]];
        [mark setLatitude:  [[[array objectAtIndex:i] valueForKey:@"location_latitude"] floatValue]];
        [mark setLongitude: [[[array objectAtIndex:i] valueForKey:@"location_longitude"] floatValue]];
        [mark setUrlPropic: [[array objectAtIndex:i] valueForKey:@"profile_picture"]];
        [mark setUrlPost:   [[array objectAtIndex:i] valueForKey:@"image"]];
        [mark setTitlePost: [[array objectAtIndex:i] valueForKey:@"title"]];
        [mark setDescriptionPost:   [[array objectAtIndex:i] valueForKey:@"description"]];
        [mark setUserID:    [[array objectAtIndex:i] valueForKey:@"id"]];
        
        /*mark.iconView = [[[NSBundle mainBundle] loadNibNamed:@"CustomMarker" owner:self options:nil] objectAtIndex:0];
        [mark.imageViewUser sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",UserProfilePictureBaseURL,[[array objectAtIndex:i] valueForKey:@"profile_picture"]]] placeholderImage:[UIImage imageNamed:@"upload_icon"]];*/
        
        if ([[[array objectAtIndex:i] valueForKey:@"id"] isEqualToString:[[UserModel sharedSingleton] getUserID]])
        {
            _imageViewPin.image = [UIImage imageNamed:@"pin_green"];
        }
        else
        {
            _imageViewPin.image = [UIImage imageNamed:@"pin"];
        }
        _imageViewUser.layer.cornerRadius = self.imageViewUser.frame.size.width/2;
        _imageViewUser.clipsToBounds = YES;
        
        mark.iconView.tag = [[[array objectAtIndex:i] valueForKey:@"id"] integerValue];
        mark.map = mapView;
    
    }

    
    
}



- (void)mapView:(GMSMapView *)mapView1 didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    
    [self showSubscription];
    return;
    NSMutableDictionary * dictParam =[[NSMutableDictionary alloc] init];
    [dictParam setObject:[[UserModel sharedSingleton] getUserID] forKey:@"user_id"];
    [dictParam setObject:AppToken  forKey:@"apptoken"];
    [Networking rawJsondataTaskwithURL:CountUserMapPost Param:dictParam ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error) {
        if (!error)
        {
            
            
            NSLog(@"Count OF post : %@",response);
            if ([[response valueForKey:@"status"]integerValue] == 1)
            {
    
                btnBackground = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, mapView1.frame.size.width, mapView1.frame.size.height)];
                btnBackground.backgroundColor = [UIColor blackColor];
                btnBackground.alpha = 0.4f;
                [btnBackground addTarget:self action:@selector(removePopUpView) forControlEvents:UIControlEventTouchUpInside];
                [mapView1 addSubview:btnBackground];
                
                markerForPost = [[GMSMarker alloc] init];
                markerForPost.userData = @"NewPost";
                markerForPost.iconView.tag = 1000000000;
                markerForPost.position = CLLocationCoordinate2DMake(coordinate.latitude, coordinate.longitude);
                
                markerForPost.iconView = [[[NSBundle mainBundle] loadNibNamed:@"CustomMarker" owner:self options:nil] objectAtIndex:0];
                [_imageViewUser sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",UserProfilePictureBaseURL,[[UserModel sharedSingleton] getUserID]]] placeholderImage:[UIImage imageNamed:@"upload_icon"]];
                _imageViewPin.image = [UIImage imageNamed:@"pin_green"];
                _imageViewUser.layer.cornerRadius = self.imageViewUser.frame.size.width/2;
                _imageViewUser.clipsToBounds = YES;
                
                markerForPost.map = mapView;
                
                NSArray *subviewArray       = [[NSBundle mainBundle] loadNibNamed:@"CustomPopupView" owner:self options:nil];
                self.viewPopupExtend        = [subviewArray objectAtIndex:0];
                self.viewPopupExtend.layer.cornerRadius = 10;
                self.viewPopupExtend.clipsToBounds = YES;
                //[self.viewPopupExtend setCenter:mapView.center];
                self.viewPopupExtend.frame  = CGRectOffset( self.viewPopupExtend.frame, (self.view.frame.size.width - self.viewPopupExtend.frame.size.width)/2, (self.view.frame.size.height - self.viewPopupExtend.frame.size.height)/6 );
                self.viewPopupExtend.backgroundColor = [UIColor whiteColor];
                self.viewPopupExtend.transform       = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
                self.btnPostAndCancle.tag = 10000;
                [mapView1 addSubview:self.viewPopupExtend];
                self.imageviewPropic.layer.cornerRadius = self.imageviewPropic.frame.size.height/2;
                self.imageviewPropic.clipsToBounds = YES;
                [self.imageviewPropic sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",UserProfilePictureBaseURL,[[UserModel sharedSingleton] getUserProfileImageUrl]]] placeholderImage:[UIImage imageNamed:@"upload_icon"]];
                self.lblUserFullName.text = [NSString stringWithFormat:@"%@ %@",[[UserModel sharedSingleton] getUserFirstName],[[UserModel sharedSingleton] getUserLastName]];
                [HelperClass getAddressFromLatLon:coordinate.latitude withLongitude:coordinate.longitude compilation:^(id response, NSError *error) {
                    
                    if (!error) {
                        self.lblLocationTap.text =  response;
                    }
                    else
                    {
                        self.lblLocationTap.text = nil;
                    }
                    
                }];
                
                [UIView animateWithDuration:0.3/1.5 animations:^{
                    
                    self.viewPopupExtend.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0);
                    
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:0.3/2 animations:^{
                        
                        self.viewPopupExtend.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
                        
                    } completion:^(BOOL finished) {
                        
                        [UIView animateWithDuration:0.3/2 animations:^{
                            self.viewPopupExtend.transform = CGAffineTransformIdentity;
                            [self.viewPopupExtend layoutIfNeeded];
                            
                        }];
                    }];
                }];
                
                latitudePost    = [NSString stringWithFormat:@"%f",coordinate.latitude];
                longitudePost   = [NSString stringWithFormat:@"%f",coordinate.longitude];
                
                mapView.delegate = nil;

            }
            else if ([[response valueForKey:@"status"]integerValue] == 0)
            {
                [self showSubscription];
            }
        }
        else
        {
            
        }
    }];

    
}

-(IBAction)btnCameraShutterAction:(UIButton *)sender
{
    [self ChangePicture];
}

-(IBAction)btnPostAndCancleAction:(UIButton *)sender
{
    if (sender.tag == 10001)
    {
        [self removePopUpView];
    }
    if (sender.tag == 10000)
    {
        if ([self validate])
        {
            [self showAlertMessage:[self validate] Title:Alert_Title_Info];
        }
        else
        {
            [self postExtendWithLatitude:latitudePost andLongitude:longitudePost];
        }
    }
    
    
}

-(void)removePopUpView
{
    mapView.delegate = self;
    [btnBackground removeFromSuperview];
    [subscriptionView removeFromSuperview];
    [self.viewPopupExtend removeFromSuperview];
    markerForPost.map = nil;
    mapView.userInteractionEnabled = YES;
}


-(void)ChangePicture
{

    UIAlertController * view=   [UIAlertController
                                 alertControllerWithTitle:nil
                                 message:@"Please Select Image for Post"
                                 preferredStyle:UIAlertControllerStyleActionSheet];
    
    view.view.tintColor = UIColorFromRGB(Color_NavigationBar);
    UIAlertAction* Gallery = [UIAlertAction
                              actionWithTitle:@"Camera"
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                  NSString *model = [[UIDevice currentDevice] model];
                                  if ([model isEqualToString:@"iPhone Simulator"] || [model isEqualToString:@"iPad Simulator"])
                                  {
                                      UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!!!" message:@"No Camera found in Simulator!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                      [alert show];
                                  }
                                  else
                                  {
                                      ImgPicker = [[UIImagePickerController alloc] init];
                                      ImgPicker.delegate = self;
                                      ImgPicker.allowsEditing = YES;
                                      ImgPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                                      ImgPicker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
                                      
                                      // Only For Video  MediaType
                                      
                                      //ImgPicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
                                      
                                      // Only For All (Video and Photo)  MediaType
                                      //ImgPicker.mediaTypes =
                                      //[UIImagePickerController availableMediaTypesForSourceType:
                                      // UIImagePickerControllerSourceTypeCamera];
                                      [self presentViewController:ImgPicker animated:YES completion:nil];
                                      ImgPicker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
                                      //ImgPicker.videoQuality = UIImagePickerControllerQualityTypeMedium;

                                      
                                  }
                                  
                                  
                                  
                                  //Do some thing here
                                  [view dismissViewControllerAnimated:YES completion:nil];
                                  
                              }];
    
    UIAlertAction* Camera = [UIAlertAction
                             actionWithTitle:@"Gallery"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
                                     
                                     UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                                     picker.delegate = self;
                                     picker.allowsEditing = YES;
                                     picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                     picker.sourceType=UIImagePickerControllerSourceTypeSavedPhotosAlbum;
                                     /*picker.mediaTypes =
                                     [UIImagePickerController availableMediaTypesForSourceType:
                                      UIImagePickerControllerSourceTypeSavedPhotosAlbum];*/
                                     //ImgPicker.videoQuality = UIImagePickerControllerQualityTypeMedium;
                                     
                                     //picker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
                                     
                                     [self presentViewController:picker animated:YES completion:NULL];
                                 }
                                 //Do some thing here
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    
    
    
    [view addAction:Gallery];
    [view addAction:Camera];
    [view addAction:cancel];
    [self presentViewController:view animated:YES completion:nil];
    
}
-(void)imagePickerController:(UIImagePickerController *)picker
       didFinishPickingImage:(UIImage *)img
                 editingInfo:(NSDictionary *)editingInfo

{
    if ([picker isEqual:ImgPicker])
    {
        
    }
    [picker dismissModalViewControllerAnimated:YES];
    
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissModalViewControllerAnimated:YES];
    
}
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [[picker parentViewController] dismissViewControllerAnimated:YES completion:nil];
    
    //IMAGE = [info valueForKey:@"UIImagePickerControllerOriginalImage"];
    
    if([info[UIImagePickerControllerMediaType] isEqualToString:(__bridge NSString *)(kUTTypeImage)])
    {
        NSLog(@"Image");
        IMAGE=[info objectForKey:UIImagePickerControllerOriginalImage];
        self.ImageViewExtendPost.image = IMAGE;
    }

    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)removeGMSBlockingGestureRecognizerFromMapView:(GMSMapView *)mapView2
{
    if([mapView2.settings respondsToSelector:@selector(consumesGesturesInView)]) {
        mapView2.settings.consumesGesturesInView = NO;
    }
    else {
        for (id gestureRecognizer in mapView2.gestureRecognizers)
        {
            if (![gestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]])
            {
                [mapView2 removeGestureRecognizer:gestureRecognizer];
            }
        }
    }
}


-(void)postExtendWithLatitude:(NSString *)latitude andLongitude:(NSString *)longitude
{
    
    NSMutableDictionary * dictParam =[[NSMutableDictionary alloc] init];
    [dictParam setObject:[[UserModel sharedSingleton] getUserID] forKey:@"user_id"];
    [dictParam setObject:AppToken                     forKey:@"apptoken"];
    [dictParam setObject:latitude                     forKey:@"location_latitude"];
    [dictParam setObject:longitude                    forKey:@"location_longitude"];
    [dictParam setObject:self.txtPostTitle.text       forKey:@"title"];
    [dictParam setObject:self.tvPostDescription.text  forKey:@"description"];
    
    NSMutableArray *arrImage        = [[NSMutableArray alloc] init];
    NSMutableArray *arrImageParam   = [[NSMutableArray alloc] init];
    
    [arrImage      addObject:self.ImageViewExtendPost.image];
    [arrImageParam addObject:@"image"];
    
    [Networking dataTaskwithURL:ExtendAddPostOnMap Param:dictParam ImageArray:arrImage ImageParamArray:arrImageParam compilation:^(id response, NSError *error) {
        if (!error)
        {
            
            NSLog(@"Post Location Data : %@",response);
            
            if ([[response valueForKey:@"status"]integerValue] == 1)
            {
                [self removePopUpView];
                [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Success];
                [self viewDidLoad];
            }
            else if ([[response valueForKey:@"status"]integerValue] == 0)
            {
                [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Error];
            }
            
        }
        else
        {
            
        }

    }];
    

}

-(NSString *)validate
{
    NSString * msg;
    
    if (self.ImageViewExtendPost.image == nil)
    {
        msg = @"Please Select Image for Post";
    }
    else if (self.txtPostTitle.text.length == 0)
    {
        msg = @"Please Enter Post Title";
    }
    else if (self.tvPostDescription.text.length == 0 || [self.tvPostDescription.text isEqualToString:@"Post Description"])
    {
        msg = @"Please Enter Post Description";
    }
 
    return msg;
}

#pragma Mark-  Commaon Alertview

-(void)showAlertMessage:(NSString*)message Title:(NSString *)title
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               {
                               }];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

#pragma Mark: textfield delegate
-(BOOL)textFieldShouldReturn:(UITextField*)textField;
{
    [textField resignFirstResponder];
    
    return YES;
}

#pragma Mark: textview delegate method

-(BOOL)textView:(UITextView *)_textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [_textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    if (textView == _tvPostDescription && [_tvPostDescription.text isEqualToString:@"Post Description"])
    {
        _tvPostDescription.text = nil;
        //_tvMessageCaption.textColor = [UIColor colorWithRed:240/255.0f green:99/255.0f blue:91/255.0f alpha:1.0];
    }
    
    NSLog(@"Did begin editing");
}

-(void)textViewDidChange:(UITextView *)textView
{
    NSLog(@"Did Change");
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    NSLog(@"Did End editing");
    
    if (textView == _tvPostDescription && [_tvPostDescription.text isEqualToString:@""]) {
        _tvPostDescription.text = @"Post Description";
        //_tvDestimationAddress.textColor = [UIColor colorWithRed:199/255.0f green:199/255.0f blue:205/255.0f alpha:1.0];
    }
    
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    return YES;
}

-(void)showSubscription
{
    
    btnBackground = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, mapView.frame.size.width, mapView.frame.size.height)];
    btnBackground.backgroundColor = [UIColor blackColor];
    btnBackground.alpha = 0.4f;
    [btnBackground addTarget:self action:@selector(removePopUpView) forControlEvents:UIControlEventTouchUpInside];
    [mapView addSubview:btnBackground];
    
    subscriptionView = [[SubscriptionPlanView alloc] initWithFrame:CGRectMake(0, 0, 300, 450)];
    [subscriptionView.tblSubscriptionPlans registerNib:[UINib nibWithNibName:@"SubscriptionTableViewCell" bundle:nil] forCellReuseIdentifier:@"SubscriptionCell"];
    
    subscriptionView.layer.cornerRadius = 10;
    subscriptionView.clipsToBounds = YES;
    
    subscriptionView.frame  = CGRectOffset( subscriptionView.frame, (self.view.frame.size.width - subscriptionView.frame.size.width)/2, (self.view.frame.size.height - subscriptionView.frame.size.height)/6 );
    subscriptionView.backgroundColor = [UIColor whiteColor];
    subscriptionView.transform       = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    
    /*[subscriptionView.btnBuyGold addTarget:self action:@selector(btnGoldBuyAction) forControlEvents:UIControlEventTouchUpInside];
    [subscriptionView.btnBuySilver addTarget:self action:@selector(btnSilverBuyAction) forControlEvents:UIControlEventTouchUpInside];*/
    
    [mapView addSubview:subscriptionView];
    
    [UIView animateWithDuration:0.3/1.5 animations:^{
        
        subscriptionView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0);
        
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3/2 animations:^{
            
            subscriptionView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
            
        } completion:^(BOOL finished) {
            
            [UIView animateWithDuration:0.3/2 animations:^{
                subscriptionView.transform = CGAffineTransformIdentity;
                [subscriptionView layoutIfNeeded];
                
                
            }];
        }];
    }];
    
    subscriptionView.tblSubscriptionPlans.delegate = self;
    subscriptionView.tblSubscriptionPlans.dataSource = self;
    [self GetAllPlans];
}

-(void)btnGoldBuyAction
{
    NSLog(@"btnGoldBuyAction");
    [self payWithAmmount:@"20"];
}

-(void)btnSilverBuyAction
{
    NSLog(@"btnSilverBuyAction");
    [self payWithAmmount:@"10"];
}


-(void)setupPayPalConfiguration
{
    // Set up payPalConfig
    _payPalConfig = [[PayPalConfiguration alloc] init];
#if HAS_CARDIO
    // You should use the PayPal-iOS-SDK+card-Sample-App target to enable this setting.
    // For your apps, you will need to link to the libCardIO and dependent libraries. Please read the README.md
    // for more details.
    _payPalConfig.acceptCreditCards = YES;
#else
    _payPalConfig.acceptCreditCards = NO;
#endif
    _payPalConfig.merchantName = @"Extend";
    _payPalConfig.merchantPrivacyPolicyURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/privacy-full"];
    _payPalConfig.merchantUserAgreementURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/useragreement-full"];
    
    // Setting the languageOrLocale property is optional.
    //
    // If you do not set languageOrLocale, then the PayPalPaymentViewController will present
    // its user interface according to the device's current language setting.
    //
    // Setting languageOrLocale to a particular language (e.g., @"es" for Spanish) or
    // locale (e.g., @"es_MX" for Mexican Spanish) forces the PayPalPaymentViewController
    // to use that language/locale.
    //
    // For full details, including a list of available languages and locales, see PayPalPaymentViewController.h.
    
    _payPalConfig.languageOrLocale = [NSLocale preferredLanguages][0];
    
    
    // Setting the payPalShippingAddressOption property is optional.
    //
    // See PayPalConfiguration.h for details.
    
    _payPalConfig.payPalShippingAddressOption = PayPalShippingAddressOptionPayPal;
    
    // Do any additional setup after loading the view, typically from a nib.
    
    //self.successView.hidden = YES;
    
    // use default environment, should be Production in real life
    self.environment = kPayPalEnvironment;
    
    NSLog(@"PayPal iOS SDK version: %@", [PayPalMobile libraryVersion]);
    
    // Update payPalConfig re accepting credit cards.
    self.payPalConfig.acceptCreditCards = self.acceptCreditCards;
    
   
}

-(void)payWithAmmount:(NSString *)ammount
{
    // Remove our last completed payment, just for demo purposes.
    self.resultText = nil;
    
    // Note: For purposes of illustration, this example shows a payment that includes
    //       both payment details (subtotal, shipping, tax) and multiple items.
    //       You would only specify these if appropriate to your situation.
    //       Otherwise, you can leave payment.items and/or payment.paymentDetails nil,
    //       and simply set payment.amount to your total charge.
    
    // Optional: include multiple items
    PayPalItem *item1 = [PayPalItem itemWithName:@""
                                    withQuantity:1
                                       withPrice:[NSDecimalNumber decimalNumberWithString:ammount]
                                    withCurrency:@"USD"
                                         withSku:@"Hip-00037"];

    
    NSArray *items = @[item1];
    NSDecimalNumber *subtotal = [PayPalItem totalPriceForItems:items];
    
    // Optional: include payment details
    NSDecimalNumber *shipping = [[NSDecimalNumber alloc] initWithString:@"0.00"];
    NSDecimalNumber *tax = [[NSDecimalNumber alloc] initWithString:@"0.00"];
    PayPalPaymentDetails *paymentDetails = [PayPalPaymentDetails paymentDetailsWithSubtotal:subtotal
                                                                               withShipping:shipping
                                                                                    withTax:tax];
    
    NSDecimalNumber *total = [[subtotal decimalNumberByAdding:shipping] decimalNumberByAdding:tax];
    
    PayPalPayment *payment = [[PayPalPayment alloc] init];
    
    payment.amount = total;
    payment.currencyCode = @"USD";
    payment.shortDescription = @"Hipster clothing";
    payment.items = nil; // items;  // if not including multiple items, then leave payment.items as nil
    payment.paymentDetails = paymentDetails; // if not including payment details, then leave payment.paymentDetails as nil
    
    if (!payment.processable) {
        // This particular payment will always be processable. If, for
        // example, the amount was negative or the shortDescription was
        // empty, this payment wouldn't be processable, and you'd want
        // to handle that here.
    }
    
    
    PayPalPaymentViewController *paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment
                                                                                                configuration:self.payPalConfig
                                                                                                     delegate:self];
    [self presentViewController:paymentViewController animated:YES completion:nil];
}

- (BOOL)acceptCreditCards {
    return self.payPalConfig.acceptCreditCards;
}

- (void)setAcceptCreditCards:(BOOL)acceptCreditCards {
    self.payPalConfig.acceptCreditCards = acceptCreditCards;
}

- (void)setPayPalEnvironment:(NSString *)environment {
    self.environment = environment;
    [PayPalMobile preconnectWithEnvironment:environment];
}



#pragma mark PayPalPaymentDelegate methods

- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController didCompletePayment:(PayPalPayment *)completedPayment {
    NSLog(@"PayPal Payment Success!");
    self.resultText = [completedPayment description];
    
    
    NSDictionary * DictPayment = [[NSDictionary alloc]init];
    DictPayment = [completedPayment confirmation];
    
    NSLog(@"completedPayment id----%@",[[[completedPayment confirmation]valueForKey:@"response"] valueForKey:@"id"] );
    //[self RequestBookingTrainer];

    
    /*NSDictionary * DICT=[[NSDictionary alloc]init];
    DICT=@{
           @"app_token"         :[Ref valueForKey:@"apptoken"],
           @"booking_type"      :@"1",
           @"plan_id"           :strPlainID,
           @"user_id"           :[Ref valueForKey:@"userid"],
           @"trainer_id"        :_strTrainerId,
           @"plan_start_date"   :_txtDate.text,
           @"plan_start_time"   :_txtTime.text,
           @"address"           :_tvDestimationAddress.text,
           @"transaction_id"    :[[[completedPayment confirmation]valueForKey:@"response"] valueForKey:@"id"],
           @"booking_amount"    :[completedPayment amount],
           @"state"             :[[[completedPayment confirmation]valueForKey:@"response"] valueForKey:@"state"],
           @"environment"       :[[[completedPayment confirmation]valueForKey:@"client"] valueForKey:@"environment"],
           @"response_type"     :[[completedPayment confirmation]valueForKey:@"response_type"] ,
           };*/
    
    
    [self sendCompletedPaymentToServer:completedPayment]; // Payment was processed successfully; send to server for verification and fulfillment
    [self dismissViewControllerAnimated:YES completion:nil];
}



- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController {
    NSLog(@"PayPal Payment Canceled");
    self.resultText = nil;
    //self.successView.hidden = YES;
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark Proof of payment validation

- (void)sendCompletedPaymentToServer:(PayPalPayment *)completedPayment {
    // TODO: Send completedPayment.confirmation to server
    NSLog(@"Here is your proof of payment:\n\n%@\n\nSend this to your server for confirmation and fulfillment.", completedPayment.confirmation);
    [self SelectPlanWithPlanId];
}

-(void)SelectPlanWithPlanId
{//{"apptoken":"d10289185f466003c450a15d300e50d5","user_id":"54","plan_id":"2"}
    NSMutableDictionary * dictParam = [[NSMutableDictionary alloc] init];
    [dictParam setObject:AppToken forKey:@"apptoken"];
    [dictParam setObject:[[UserModel sharedSingleton] getUserID] forKey:@"user_id"];
    [dictParam setObject:strPlanID forKey:@"plan_id"];
    
    NSLog(@"Update Dict Data :%@",dictParam);
    
    
    [Networking rawJsondataTaskwithURL:SelectPlan Param:dictParam ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error) {
        if (!error)
        {
            NSLog(@"response History Data :%@",response);
            
            if ([[response valueForKey:@"status"]integerValue] == 1)
            {
                [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Success];
            }
            else
            {
                [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Error];
            }
        }
    }];

}


#pragma mark - Authorize Future Payments

/*- (IBAction)getUserAuthorizationForFuturePayments:(id)sender {
 
 PayPalFuturePaymentViewController *futurePaymentViewController = [[PayPalFuturePaymentViewController alloc] initWithConfiguration:self.payPalConfig delegate:self];
 [self presentViewController:futurePaymentViewController animated:YES completion:nil];
 }*/


#pragma mark PayPalFuturePaymentDelegate methods

- (void)payPalFuturePaymentViewController:(PayPalFuturePaymentViewController *)futurePaymentViewController
                didAuthorizeFuturePayment:(NSDictionary *)futurePaymentAuthorization {
    NSLog(@"PayPal Future Payment Authorization Success!");
    self.resultText = [futurePaymentAuthorization description];
    //[self showSuccess];
    
    [self sendFuturePaymentAuthorizationToServer:futurePaymentAuthorization];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)payPalFuturePaymentDidCancel:(PayPalFuturePaymentViewController *)futurePaymentViewController {
    NSLog(@"PayPal Future Payment Authorization Canceled");
    //self.successView.hidden = YES;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)sendFuturePaymentAuthorizationToServer:(NSDictionary *)authorization {
    // TODO: Send authorization to server
    NSLog(@"Here is your authorization:\n\n%@\n\nSend this to your server to complete future payment setup.", authorization);
}


#pragma mark - Authorize Profile Sharing

/*- (IBAction)getUserAuthorizationForProfileSharing:(id)sender {
 
 NSSet *scopeValues = [NSSet setWithArray:@[kPayPalOAuth2ScopeOpenId, kPayPalOAuth2ScopeEmail, kPayPalOAuth2ScopeAddress, kPayPalOAuth2ScopePhone]];
 
 PayPalProfileSharingViewController *profileSharingPaymentViewController = [[PayPalProfileSharingViewController alloc] initWithScopeValues:scopeValues configuration:self.payPalConfig delegate:self];
 [self presentViewController:profileSharingPaymentViewController animated:YES completion:nil];
 }*/


#pragma mark PayPalProfileSharingDelegate methods

- (void)payPalProfileSharingViewController:(PayPalProfileSharingViewController *)profileSharingViewController
             userDidLogInWithAuthorization:(NSDictionary *)profileSharingAuthorization {
    NSLog(@"PayPal Profile Sharing Authorization Success!");
    self.resultText = [profileSharingAuthorization description];
    //[self showSuccess];
    
    [self sendProfileSharingAuthorizationToServer:profileSharingAuthorization];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)userDidCancelPayPalProfileSharingViewController:(PayPalProfileSharingViewController *)profileSharingViewController {
    NSLog(@"PayPal Profile Sharing Authorization Canceled");
    //self.successView.hidden = YES;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)sendProfileSharingAuthorizationToServer:(NSDictionary *)authorization {
    // TODO: Send authorization to server
    NSLog(@"Here is your authorization:\n\n%@\n\nSend this to your server to complete profile sharing setup.", authorization);
}


-(void)GetAllPlans
{
    //{"apptoken":"d10289185f466003c450a15d300e50d5"}
    NSMutableDictionary * dictParam = [[NSMutableDictionary alloc] init];
    [dictParam setObject:AppToken forKey:@"apptoken"];
    
    NSLog(@"Update Dict Data :%@",dictParam);

    
    [Networking rawJsondataTaskwithURL:GetAllPlansList Param:dictParam ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error) {
        if (!error)
        {
            NSLog(@"response History Data :%@",response);
            
            if ([[response valueForKey:@"status"]integerValue] == 1)
            {
                arrGetAllPlans = [[NSMutableArray alloc] init];
                arrGetAllPlans = [response valueForKey:@"message"];
                [subscriptionView.tblSubscriptionPlans reloadData];
                
            }
            else
            {
                //[self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Error];
            }
        }
    }];


}


#pragma Mark: Tableview Delegate & datasource Method

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arrGetAllPlans.count;    //count number of row from counting array hear cataGorry is An Array
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SubscriptionTableViewCell *cell = (SubscriptionTableViewCell *)  [subscriptionView.tblSubscriptionPlans dequeueReusableCellWithIdentifier:@"SubscriptionCell" forIndexPath:indexPath];
    cell.lblPlanPrice.text = [NSString stringWithFormat:@"%@ - $%@",[[arrGetAllPlans objectAtIndex:indexPath.row] valueForKey:@"plan_name"],[[arrGetAllPlans objectAtIndex:indexPath.row] valueForKey:@"plan_price"]];
     NSString * strNew= [[arrGetAllPlans objectAtIndex:indexPath.row] valueForKey:@"plan_description"];
    
    strNew = [strNew stringByReplacingOccurrencesOfString:@"-" withString:@"\n-"];
    cell.lblPlanDescription.text = strNew;
    
    cell.btnBuyPlan.tag = 100 + indexPath.row;
    [cell.btnBuyPlan addTarget:self action:@selector(btnBuyPlanAction:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"SubscriptionCell";
    //SubscriptionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    return 150 ;//+ [self heightForLabelwithText:objpost.PostName withFont:[UIFont systemFontOfSize:15.0] withWidth:cell.lblMsgCaption.frame.size.width] ;
    
}

-(CGFloat )heightForLabelwithText:(NSString *)text withFont:(UIFont *)font withWidth:(CGFloat)width
{
    CGRect rect = CGRectMake(0, 0, width, CGFLOAT_MAX);
    
    UILabel * lbl = [[UILabel alloc] initWithFrame:rect];
    lbl.numberOfLines = 0;
    lbl.lineBreakMode = NSLineBreakByCharWrapping;
    lbl.font = font;
    lbl.text = text;
    [lbl sizeToFit];
    
    return lbl.frame.size.height;
}
/*-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
 {
 _viewHeader.frame = CGRectMake(0, 0, self.tblPost.frame.size.width, _viewHeader.frame.size.height);
 _viewHeader.backgroundColor = UIColorFromRGB(Color_NavigationBar);
 UIView *view = [[UIView alloc] init ];
 tableView.tableHeaderView = _viewHeader;
 return view;
 }*/

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.001/*self.viewheader.frame.size.height*/;
}

-(IBAction)btnBuyPlanAction:(UIButton *)sender
{
    NSString * strPrice = [[arrGetAllPlans objectAtIndex:sender.tag -100] valueForKey:@"plan_price"];
    strPlanID = [[arrGetAllPlans objectAtIndex:sender.tag -100] valueForKey:@"plan_id"];
    [self payWithAmmount:strPrice];
}


@end
