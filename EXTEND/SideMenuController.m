//
//  SideMenuController.m
//  EXTEND
//
//  Created by Apple on 06/03/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "SideMenuController.h"
#import "UIViewController+REFrostedViewController.h"
#import "DEMONavigationController.h"
#import "SidMenu_Cell.h"
#import "AppDelegate.h"
#import "Home_VC.h"
#import "Chat_Call_List_VC.h"
#import "NewsAll_VC.h"
#import "MeetRadarAnimation_VC.h"
#import "ExtendFeature_VC.h"
#import "Login_vc.h"
#import "MyProfile_VC.h"
#import "SettingAppVC.h"

#import "UserModel.h"
#import "Networking.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <Sinch/Sinch.h>



@interface SideMenuController ()<UIGestureRecognizerDelegate,SINCallClientDelegate>
{
    NSMutableArray *resul_arr;
    NSArray *arr_result;
    SidMenu_Cell *cell;
    AppDelegate *app;
}
@end

@implementation SideMenuController

#pragma mark Sythesize Object-

@synthesize table,imageview,UserName,i;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _imagebottom.layer.cornerRadius = _imagebottom.frame.size.width/2;
    _imagebottom.clipsToBounds = YES;
    
    _imageMiddle.layer.cornerRadius = _imageMiddle.frame.size.width/2;
    _imageMiddle.clipsToBounds = YES;
    
    imageview.layer.cornerRadius = imageview.frame.size.width/2;
    imageview.layer.borderWidth = 2.0f;
    imageview.layer.borderColor = [UIColor whiteColor].CGColor;
    imageview.clipsToBounds = YES;
    
    [imageview sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",UserProfilePictureBaseURL,[[UserModel sharedSingleton] getUserProfileImageUrl]]] placeholderImage:[UIImage imageNamed:@"upload_icon"]];
    NSLog(@"image url : %@",[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",UserProfilePictureBaseURL,[[UserModel sharedSingleton] getUserProfileImageUrl]]]);
    UserName.text = [NSString stringWithFormat:@"%@ %@",[[UserModel sharedSingleton] getUserFirstName],[[UserModel sharedSingleton] getUserLastName]];
    
    arr_result=@[@{@"cat_name": @"Home",
                   @"catgory_image":@ "home_icon",
                   @"line_image":@ "vertical_icon"
                   },
                 @{ @"cat_name": @"Chat",
                    @"catgory_image":@ "chat_icon",
                    @"line_image":@ "vertical_icon"
                    },
                 @{ @"cat_name": @"Meet",
                    @"catgory_image":@ "meet_icon",
                    @"line_image":@ "vertical_icon"
                    },
                 @{ @"cat_name": @"Extend",
                    @"catgory_image":@ "extend_icon",
                    @"line_image":@ "vertical_icon"
                    },
                 @{ @"cat_name": @"News",
                    @"catgory_image":@ "newsfeed_icon",
                    @"line_image":@ "vertical_icon"
                    },
                 @{ @"cat_name": @"Setting",
                    @"catgory_image":@ "setting_icon",
                    @"line_image":@ "vertical_icon"
                    },
                 @{ @"cat_name": @"Logout",
                    @"catgory_image":@ "logout_icon",
                    @"line_image":@ "bottom_line"
                    }];
    
    [table setBackgroundColor:[UIColor clearColor]];
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(MyProfile:)];
    tap.delegate = self;
    imageview.userInteractionEnabled = YES;
    [imageview addGestureRecognizer:tap];
    
}
-(void)MyProfile:(UITapGestureRecognizer *)sender
{
    MyProfile_VC *objMyProfile_VC = [self.storyboard instantiateViewControllerWithIdentifier:@"MyProfile_VC"];
    /*[self.navigationController pushViewController:objMyProfile_VC animated:YES ];
    [self.frostedViewController hideMenuViewController];*/
    
    DEMONavigationController *navigationController = [self.storyboard instantiateViewControllerWithIdentifier:@"contentController"];
    
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    //objChat_Call_List_VC = [self.storyboard instantiateViewControllerWithIdentifier: @"Chat_Call_List_VC"];
    navigationController.viewControllers = @[objMyProfile_VC];
    
    self.frostedViewController.contentViewController = navigationController;
    [self.frostedViewController hideMenuViewController];
}

- (void)viewDidAppear:(BOOL)animated {
    [self.table reloadData];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark tableView Delegate -


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    [imageview sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",UserProfilePictureBaseURL,[[UserModel sharedSingleton] getUserProfileImageUrl]]] placeholderImage:[UIImage imageNamed:@"upload_icon"]];
    NSLog(@"image url : %@",[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",UserProfilePictureBaseURL,[[UserModel sharedSingleton] getUserProfileImageUrl]]]);
    UserName.text = [NSString stringWithFormat:@"%@ %@",[[UserModel sharedSingleton] getUserFirstName],[[UserModel sharedSingleton] getUserLastName]];
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arr_result.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger rowOfTheCell = [indexPath row];
    cell =[table dequeueReusableCellWithIdentifier:@"cell"];
    [cell setBackgroundColor:[UIColor clearColor]];
    
    cell.label.text=[[arr_result objectAtIndex:indexPath.row] objectForKey:@"cat_name"];
    cell.image.image=[UIImage imageNamed:[[arr_result objectAtIndex:rowOfTheCell]objectForKey:@"catgory_image"]];
    cell.imageLine.image=[UIImage imageNamed:[[arr_result objectAtIndex:rowOfTheCell]objectForKey:@"line_image"]];
    return cell;
}
#pragma mark - UITableView Delegates

- (void)tableView:(UITableView* )tableView willDisplayCell:(UITableViewCell *)cell1 forRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Ref = [NSUserDefaults standardUserDefaults];
    //[imageview sd_setImageWithURL:[NSURL URLWithString:[Ref valueForKey:@"userprofilepic"]] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    
    //[cell1 setBackgroundColor:[UIColor clearColor]];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Ref = [NSUserDefaults standardUserDefaults];
    //[imageview sd_setImageWithURL:[NSURL URLWithString:[Ref valueForKey:@"userprofilepic"]] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    
    NSInteger rowOfTheCell = [indexPath row];
    NSLog(@"rowofthecell %ld", (long)rowOfTheCell);
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    DEMONavigationController *navigationController = [self.storyboard instantiateViewControllerWithIdentifier:@"contentController"];
    
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    
    Home_VC *objHome_VC ;
    Chat_Call_List_VC * objChat_Call_List_VC;
    NewsAll_VC * objNewsAll_VC;
    MeetRadarAnimation_VC * objMeetRadarAnimation_VC;
    ExtendFeature_VC * objExtendFeature_VC;
    SettingAppVC * objSettingAppVC;
//    UserBookingHistoryVC * UserBookingHistoryVC;
//    HomeTrainerVC * HomeTrainer;
//    MyProfileTrainerVC * MyProfileTrainer;
//    MyBootCampsVC * MyBootCamps;
//    MyplansVC * Myplans;
//    MyServicesVC * MyServices;
//    SettingVC * Setting;
//    ReminderVC * Reminder;
//    SettingTrainerVC * SettingTrainer;
//    UserAllChatVC * UserAllChat;
//    TrainerAllChatVC * TrainerAllChat;
//    if ([[Ref valueForKey:@"USER"] boolValue] == true)
//    {
        switch (indexPath.row)
        {
            case 0:
                
                objHome_VC = [self.storyboard instantiateViewControllerWithIdentifier:@"Home_VC"];
                navigationController.viewControllers = @[objHome_VC];
                break;
                
            case 1:
                objChat_Call_List_VC = [self.storyboard instantiateViewControllerWithIdentifier: @"Chat_Call_List_VC"];
                navigationController.viewControllers = @[objChat_Call_List_VC];
                break;
                
            case 2:
                
                objMeetRadarAnimation_VC = [self.storyboard instantiateViewControllerWithIdentifier: @"MeetRadarAnimation_VC"];
                navigationController.viewControllers = @[objMeetRadarAnimation_VC];
                break;
            case 3:
                objExtendFeature_VC = [self.storyboard instantiateViewControllerWithIdentifier: @"ExtendFeature_VC"];
                navigationController.viewControllers = @[objExtendFeature_VC];
                break;
            case 4:
                objNewsAll_VC = [self.storyboard instantiateViewControllerWithIdentifier: @"NewsAll_VC"];
                navigationController.viewControllers = @[objNewsAll_VC];
                break;
            case 5:
                objSettingAppVC = [mainStoryboard instantiateViewControllerWithIdentifier: @"SettingAppVC"];
                navigationController.viewControllers = @[objSettingAppVC];
                break;
                
            case 6:
               /* [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
                [self.navigationController popToRootViewControllerAnimated:YES];*/
                [SideMenuController logout];
                break;
        }
   // }
    
    self.frostedViewController.contentViewController = navigationController;
    [self.frostedViewController hideMenuViewController];
    
    
}

+(void)logout
{
    //{"user_id":"54","apptoken":"d10289185f466003c450a15d300e50d5"}
    
    NSMutableDictionary * dictParam =[[NSMutableDictionary alloc] init];
    [dictParam setObject:AppToken  forKey:@"apptoken"];
    [dictParam setObject:[[UserModel sharedSingleton] getUserID]  forKey:@"user_id"];
    
    [Networking rawJsondataTaskwithURL:Logout Param:dictParam ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error)
     {
         if (!error) {
             
             if ([[response valueForKey:@"code"] intValue] == 1)
             {
                 /*[_txtMobileNumber becomeFirstResponder];
                 [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Info];*/
                 [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isLogin"];
                 [[NSUserDefaults standardUserDefaults] synchronize];
                 
                 [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Success];
                 
                 AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
                 UIWindow *mainWindow = appDelegate.window;
                 UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
                 Login_vc * objLogin_vc=[mainStoryboard instantiateViewControllerWithIdentifier:@"Login_vc"];
                 UINavigationController *navigationObject = [[UINavigationController alloc] initWithRootViewController:objLogin_vc];
                 [mainWindow setRootViewController:navigationObject];
                 [navigationObject popToRootViewControllerAnimated:YES];
                 
                 [appDelegate.client  unregisterPushNotificationDeviceToken];

             }
             else if ([[response valueForKey:@"code"] intValue] == 0)
             {
                 // Code For Social Sign Up
                 
                 [self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Error];
                 
             }
         }
     }];
}


#pragma Mark-  Commaon Alertview

+(void)showAlertMessage:(NSString*)message Title:(NSString *)title
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               {
                               }];
    [alert addAction:okAction];
    [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alert animated:YES completion:nil];
    
}

@end
