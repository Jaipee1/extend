//
//  PostModel.h
//  EXTEND
//
//  Created by Apple on 21/03/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PostModel : NSObject

@property (nonatomic, retain) NSString * Firstname;
@property (nonatomic, retain) NSString * Lastname;
@property (nonatomic, retain) NSString * UserIDofPost;
@property (nonatomic, retain) NSString * PostName;
@property (nonatomic, retain) NSString * PostType;
@property (nonatomic, retain) NSString * PostvideoThumbnail;
@property (nonatomic, retain) NSString * PostID;
@property (nonatomic, retain) NSString * PostContent;
@property (nonatomic, retain) NSString * PostTime;
@property (nonatomic, retain) NSString * likeCount;
@property (nonatomic, retain) NSString * CommentCount;
@property (nonatomic, retain) NSString * shareCount;
@property (nonatomic, retain) NSString * TagFriends;
@property (nonatomic, retain) NSString * UserImageUrl;
@property (nonatomic, assign) int liked;


@end

/*
 "first_name" = CTinfotech;
 image = "img58a1b116bddff.jpg";
 "last_name" = Indore;
 "like_count" = 0;
 "login_user_like" = 0;
 "post_content" = "973-IMG_20170227_153947.jpg";
 "post_id" = 26;
 "post_name" = "nvchchdj\Ud83d\Ude0a\Ud83d\Ude0a\Ud83d\Ude0a\Ud83d\Ude0a";
 "post_type" = image;
 "post_video_thumbnail" = "";
 "posted_by" = 93;
 "posted_time" = "2017-03-01 00:00:00";
 "share_count" = 0;
 "tage_friend_id" = "";
 */
