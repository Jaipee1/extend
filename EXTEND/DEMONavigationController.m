//
//  DEMONavigationController.m
//  REFrostedViewControllerStoryboards
//
//  Created by Roman Efimov on 10/9/13.
//  Copyright (c) 2013 Roman Efimov. All rights reserved.
//

#import "DEMONavigationController.h"
#import "Chat_Call_List_VC.h"

@interface DEMONavigationController ()<UIGestureRecognizerDelegate>

@end

@implementation DEMONavigationController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationBar.barTintColor = UIColorFromRGB(Color_NavigationBar);
    self.navigationBar.tintColor=[UIColor whiteColor];
    [self.navigationBar setTranslucent:NO];
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    [self.view addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureRecognized:)]];
    //[self Navigate];
}

-(void)Navigate
{
    //self = (UINavigationController *)self.window.rootViewController;
    Chat_Call_List_VC * mediaPlay = [self.storyboard instantiateViewControllerWithIdentifier:@"Chat_Call_List_VC"];
    [self pushViewController:mediaPlay animated:YES];
}

#pragma mark -
#pragma mark Gesture recognizer

- (void)panGestureRecognized:(UIPanGestureRecognizer *)sender
{
    // Dismiss keyboard (optional)
    //
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    // Present the view controller
    //
    //[self.frostedViewController panGestureRecognized:sender]; Stop By Jai
}

@end
