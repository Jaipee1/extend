//
//  OtherUserModel.m
//  EXTEND
//
//  Created by Apple on 01/04/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "OtherUserModel.h"


@implementation OtherUserModel

@synthesize email_id, first_name, image, last_name, location_latitude, location_longitude, login_status, phone_number, user_id;

@end
