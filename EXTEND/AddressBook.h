//
//  AddressBook.h
//  EXTEND
//
//  Created by Apple on 15/04/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AddressBook/AddressBook.h>

@interface AddressBook : NSObject
@property (retain, nonatomic) NSMutableArray * arrContact;
- (void)fillContacts;
- (instancetype)initWithComplitionHandler:(void (^) (id ContactResponse, NSError* error))handler;
@end
