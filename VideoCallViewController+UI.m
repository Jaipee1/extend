//
//  VideoCallViewController+UI.m
//  EXTEND
//
//  Created by Apple on 09/06/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "VideoCallViewController+UI.h"

@implementation VideoCallViewController (UIAdjustments)
- (void)setCallStatusText:(NSString *)text {
    self.callStateLabel.text = text;
}

#pragma mark - Buttons

- (void)showButtons:(EButtonsBarV)buttons {
    if (buttons == kButtonsAnswerDeclineV) {
        self.answerButton.hidden = NO;
        self.declineButton.hidden = NO;
        self.endCallButton.hidden = YES;
    } else if (buttons == kButtonsHangupV) {
        self.endCallButton.hidden = NO;
        self.answerButton.hidden = YES;
        self.declineButton.hidden = YES;
    }
}

#pragma mark - Duration

- (void)setDuration:(NSInteger)seconds {
    [self setCallStatusText:[NSString stringWithFormat:@"%02d:%02d", (int)(seconds / 60), (int)(seconds % 60)]];
}

- (void)internal_updateDuration:(NSTimer *)timer {
    SEL selector = NSSelectorFromString([timer userInfo]);
    if ([self respondsToSelector:selector]) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [self performSelector:selector withObject:timer];
#pragma clang diagnostic pop
    }
}

- (void)startCallDurationTimerWithSelector:(SEL)sel {
    NSString *selectorAsString = NSStringFromSelector(sel);
    self.durationTimer = [NSTimer scheduledTimerWithTimeInterval:0.5
                                                          target:self
                                                        selector:@selector(internal_updateDuration:)
                                                        userInfo:selectorAsString
                                                         repeats:YES];
}

- (void)stopCallDurationTimer {
    [self.durationTimer invalidate];
    self.durationTimer = nil;
}

@end
