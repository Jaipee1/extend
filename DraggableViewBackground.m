//
//  DraggableViewBackground.m
//  testing swiping
//
//  Created by Richard Kim on 8/23/14.
//  Copyright (c) 2014 Richard Kim. All rights reserved.
//

#import "DraggableViewBackground.h"

#import "UserModel.h"
#import "OtherUserModel.h"
#import "Networking.h"
#import "MeetRadarAnimation_VC.h"

#import <SDWebImage/UIImageView+WebCache.h>
typedef NS_ENUM(NSUInteger, MeetLikeDislikeSuperlike) {
    Dislike,
    Like,
    Superlike,
};
@implementation DraggableViewBackground{
    NSInteger cardsLoadedIndex; //%%% the index of the card you have loaded into the loadedCards array last
    NSMutableArray *loadedCards; //%%% the array of card loaded (change max_buffer_size to increase or decrease the number of cards this holds)
    
    UIButton* menuButton;
    UIButton* messageButton;
    UIButton* checkButton;
    UIButton* xButton;
    UIButton* heartButton;
    
    OtherUserModel * objOtherUserModel;
    
    int superLikeCount;
}
//this makes it so only two cards are loaded at a time to
//avoid performance and memory costs
static const int MAX_BUFFER_SIZE = 2; //%%% max number of cards loaded at any given time, must be greater than 1
static const float CARD_HEIGHT = 430;

// Edited By Jai
//static const float CARD_HEIGHT = 290;//%%% height of the draggable card

static const float CARD_WIDTH = 300; //%%% width of the draggable card

@synthesize exampleCardLabels; //%%% all the labels I'm using as example data at the moment
@synthesize allCards;//%%% all the cards

//- (id)initWithFrame:(CGRect)frame
//{
//    self = [super initWithFrame:frame];
//    if (self) {
//        [super layoutSubviews];
//        [self setupView];
//        exampleCardLabels = [[NSArray alloc]initWithObjects:@"first",@"second",@"third",@"fourth",@"last", nil]; //%%% placeholder for card-specific information
//        loadedCards = [[NSMutableArray alloc] init];
//        allCards = [[NSMutableArray alloc] init];
//        cardsLoadedIndex = 0;
//        [self loadCards];
//    }
//    return self;
//}


- (id)initWithFrame:(CGRect)frame andWithCardArray:(NSArray *)array
{
    self = [super initWithFrame:frame];
    if (self) {
        [super layoutSubviews];
        
        // Get Super like Count Api Call
        
        [self GetSuperlikeCount];
        [self setupView];
        
        
        //exampleCardLabels = [[NSArray alloc]initWithObjects:@"first",@"second",@"third",@"fourth",@"last", nil]; //%%% placeholder for card-specific information
        exampleCardLabels = [[NSArray alloc] init];
        
        exampleCardLabels = array;
        loadedCards = [[NSMutableArray alloc] init];
        allCards = [[NSMutableArray alloc] init];
        cardsLoadedIndex = 0;
        [self loadCards];
    }
    return self;
}


//%%% sets up the extra buttons on the screen
-(void)setupView
{
#warning customize all of this.  These are just place holders to make it look pretty
    self.backgroundColor = [UIColor colorWithRed:.92 green:.93 blue:.95 alpha:1]; //the gray background colors
    menuButton = [[UIButton alloc]initWithFrame:CGRectMake(17, 34, 22, 15)];
    [menuButton setImage:[UIImage imageNamed:@"menuButton"] forState:UIControlStateNormal];
    menuButton.backgroundColor = [UIColor redColor];
    
    messageButton = [[UIButton alloc]initWithFrame:CGRectMake(284, 34, 18, 18)];
    [messageButton setImage:[UIImage imageNamed:@"messageButton"] forState:UIControlStateNormal];
    messageButton.backgroundColor = [UIColor redColor];
    
    xButton = [[UIButton alloc]initWithFrame:CGRectMake(60, self.frame.size.height - 60, 59, 59)];
    [xButton setImage:[UIImage imageNamed:@"meet_cancel_icon"] forState:UIControlStateNormal];
    //xButton.backgroundColor = [UIColor redColor];
    [xButton addTarget:self action:@selector(swipeLeft) forControlEvents:UIControlEventTouchUpInside];
    
    checkButton = [[UIButton alloc]initWithFrame:CGRectMake(130, self.frame.size.height - 60, 59, 59)];
    [checkButton setImage:[UIImage imageNamed:@"meet_favourite_icon"] forState:UIControlStateNormal];
    //checkButton.backgroundColor = [UIColor redColor];
    [checkButton addTarget:self action:@selector(swipeTop) forControlEvents:UIControlEventTouchUpInside];
    
    
    heartButton = [[UIButton alloc]initWithFrame:CGRectMake(200, self.frame.size.height - 60, 59, 59)];
    [heartButton setImage:[UIImage imageNamed:@"meet_like_icon"] forState:UIControlStateNormal];
    //heartButton.backgroundColor = [UIColor redColor];
    [heartButton addTarget:self action:@selector(swipeRight) forControlEvents:UIControlEventTouchUpInside];
    
    //[self addSubview:menuButton];
    //[self addSubview:messageButton];
    [self addSubview:xButton];
    [self addSubview:checkButton];
    [self addSubview:heartButton];
}

#warning include own card customization here!
//%%% creates a card and returns it.  This should be customized to fit your needs.
// use "index" to indicate where the information should be pulled.  If this doesn't apply to you, feel free
// to get rid of it (eg: if you are building cards from data from the internet)
-(DraggableView *)createDraggableViewWithDataAtIndex:(NSInteger)index
{
   // DraggableView *draggableView = [[DraggableView alloc]initWithFrame:CGRectMake((self.frame.size.width - CARD_WIDTH)/2, (self.frame.size.height - CARD_HEIGHT)/2, CARD_WIDTH, CARD_HEIGHT)];
    
    // Edited By Jai
    //DraggableView *draggableView = [[DraggableView alloc]initWithFrame:CGRectMake((self.frame.size.width - CARD_WIDTH)/2, (self.frame.size.height - CARD_HEIGHT)/5, CARD_WIDTH, CARD_HEIGHT)];
    DraggableView *draggableView = [[DraggableView alloc]initWithFrame:CGRectMake((self.frame.size.width - CARD_WIDTH)/2, 10, CARD_WIDTH, CARD_HEIGHT)];
    
    //draggableView.information.text = [exampleCardLabels objectAtIndex:index]; //%%% placeholder for card-specific information
    objOtherUserModel = [exampleCardLabels objectAtIndex:index];
    
    NSLog(@"Image:%@",objOtherUserModel.image);
    
    [draggableView.imgviewUser sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",UserProfilePictureBaseURL,objOtherUserModel.image]] placeholderImage:[UIImage imageNamed:@"upload_icon"]];
    [draggableView.lblUsername setText:[NSString  stringWithFormat:@"%@ %@",objOtherUserModel.first_name,objOtherUserModel.last_name]];
    [draggableView.lblAge setText:[NSString  stringWithFormat:@"Age: %@",objOtherUserModel.date_of_birth]];
    draggableView.delegate = self;
    return draggableView;
}

//%%% loads all the cards and puts the first x in the "loaded cards" array
-(void)loadCards
{
    loadedCards = [[NSMutableArray alloc] init];
    allCards = [[NSMutableArray alloc] init];
    
    if([exampleCardLabels count] > 0) {
        NSInteger numLoadedCardsCap =(([exampleCardLabels count] > MAX_BUFFER_SIZE)?MAX_BUFFER_SIZE:[exampleCardLabels count]);
        //%%% if the buffer size is greater than the data size, there will be an array error, so this makes sure that doesn't happen
        
        //%%% loops through the exampleCardsLabels array to create a card for each label.  This should be customized by removing "exampleCardLabels" with your own array of data
        for (int i = 0; i<[exampleCardLabels count]; i++) {
            DraggableView* newCard = [self createDraggableViewWithDataAtIndex:i];
            newCard.tag = i ;
            [allCards addObject:newCard];
            
            if (i<numLoadedCardsCap) {
                //%%% adds a small number of cards to be loaded
                [loadedCards addObject:newCard];
            }
        }
        
        //%%% displays the small number of loaded cards dictated by MAX_BUFFER_SIZE so that not all the cards
        // are showing at once and clogging a ton of data
        for (int i = 0; i<[loadedCards count]; i++) {
            if (i>0) {
                [self insertSubview:[loadedCards objectAtIndex:i] belowSubview:[loadedCards objectAtIndex:i-1]];
            } else {
                [self addSubview:[loadedCards objectAtIndex:i]];
            }
            cardsLoadedIndex++; //%%% we loaded a card into loaded cards, so we have to increment
        }
    }
}

#warning include own action here!
//%%% action called when the card goes to the left.
// This should be customized with your own action
-(void)cardSwipedLeft:(UIView *)card;
{
   
    
    //do whatever you want with the card that was swiped
    //    DraggableView *c = (DraggableView *)card;
    NSLog(@"##### Networking Dislike Or Nope #####");
    NSLog(@"ViewTag :%ld",(long)card.tag);
    [self LikeDislikeSuperlikeOnOtherUsersaAtIndex:card.tag withLikeStatusType:0];
    
    [loadedCards removeObjectAtIndex:0]; //%%% card was swiped, so it's no longer a "loaded card"
    
    if (cardsLoadedIndex < [allCards count]) { //%%% if we haven't reached the end of all cards, put another into the loaded cards
        [loadedCards addObject:[allCards objectAtIndex:cardsLoadedIndex]];
        cardsLoadedIndex++;//%%% loaded a card, so have to increment count
        [self insertSubview:[loadedCards objectAtIndex:(MAX_BUFFER_SIZE-1)] belowSubview:[loadedCards objectAtIndex:(MAX_BUFFER_SIZE-2)]];
        
        NSLog(@"%ld :: %d",(long)cardsLoadedIndex,MAX_BUFFER_SIZE );
        

    }
    
    if ([loadedCards count] == 0 )
    {
        [self removeFromSuperview];
    }
}


#warning include own action here!
//%%% action called when the card goes to the right.
// This should be customized with your own action
-(void)cardSwipedRight:(UIView *)card
{
    //do whatever you want with the card that was swiped
    //    DraggableView *c = (DraggableView *)card;
    NSLog(@"##### Networking  Like #####");
    NSLog(@"ViewTag :%ld",(long)card.tag);
    [self LikeDislikeSuperlikeOnOtherUsersaAtIndex:card.tag withLikeStatusType:1];
    
    [loadedCards removeObjectAtIndex:0];
    
    for (int i = 0; i < loadedCards.count; i++)
    {
        DraggableView * newCard = [loadedCards objectAtIndex: i];
        
        if (i == 0)
        {
            newCard.userInteractionEnabled = YES;
        }
        else
        {
            newCard.userInteractionEnabled = NO;
        }
        
        // now either check the tag property of view or however else you know
        // it's the one you want, and then change the userInteractionEnabled property.
    }
    
     //%%% card was swiped, so it's no longer a "loaded card"
    
    if (cardsLoadedIndex < [allCards count]) { //%%% if we haven't reached the end of all cards, put another into the loaded cards
        [loadedCards addObject:[allCards objectAtIndex:cardsLoadedIndex]];
        cardsLoadedIndex++;//%%% loaded a card, so have to increment count
        [self insertSubview:[loadedCards objectAtIndex:(MAX_BUFFER_SIZE-1)] belowSubview:[loadedCards objectAtIndex:(MAX_BUFFER_SIZE-2)]];
        
    }
    
    if ([loadedCards count] == 0 )
    {
        [self removeFromSuperview];
    }

}




-(void)cardSwipedTop:(UIView *)card
{
    //do whatever you want with the card that was swiped
    //    DraggableView *c = (DraggableView *)card;
    NSLog(@"##### Networking SuperLike #####");
    NSLog(@"ViewTag :%ld",(long)card.tag);
    [self LikeDislikeSuperlikeOnOtherUsersaAtIndex:card.tag withLikeStatusType:2];
    
    [loadedCards removeObjectAtIndex:0]; //%%% card was swiped, so it's no longer a "loaded card"
    
    if (cardsLoadedIndex < [allCards count]) { //%%% if we haven't reached the end of all cards, put another into the loaded cards
        [loadedCards addObject:[allCards objectAtIndex:cardsLoadedIndex]];
        cardsLoadedIndex++;//%%% loaded a card, so have to increment count
        [self insertSubview:[loadedCards objectAtIndex:(MAX_BUFFER_SIZE-1)] belowSubview:[loadedCards objectAtIndex:(MAX_BUFFER_SIZE-2)]];
        
    }
    
    if ([loadedCards count] == 0 )
    {
        [self removeFromSuperview];
    }
    
}


//%%% when you hit the right button, this is called and substitutes the swipe
-(void)swipeRight
{
    NSLog(@"##### Networking #####");
    DraggableView *dragView = [loadedCards firstObject];
    dragView.overlayView.mode = GGOverlayViewModeRight;
    [UIView animateWithDuration:0.2 animations:^{
        dragView.overlayView.alpha = 1;
    }];
    [dragView rightClickAction];
}

//%%% when you hit the left button, this is called and substitutes the swipe
-(void)swipeLeft
{
    NSLog(@"##### Networking #####");
    DraggableView *dragView = [loadedCards firstObject];
    dragView.overlayView.mode = GGOverlayViewModeLeft;
    [UIView animateWithDuration:0.2 animations:^{
        dragView.overlayView.alpha = 1;
    }];
    [dragView leftClickAction];
}

//////// Edited By JAi

-(void)swipeTop
{
    NSLog(@"##### Networking #####");
    
    DraggableView *dragView = [loadedCards firstObject];
    dragView.overlayView.mode = GGOverlayViewModeLeft;
    [UIView animateWithDuration:0.2 animations:^{
        dragView.overlayView.alpha = 1;
    }];
    [dragView topClickAction];
}
////////


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)LikeDislikeSuperlikeOnOtherUsersaAtIndex:(NSInteger)index withLikeStatusType:(int)liketype
{
    //{"apptoken":"d10289185f466003c450a15d300e50d5","user_id":"951","friend_id":"","like_status":"2"}
    
    objOtherUserModel = [exampleCardLabels objectAtIndex:index];
    
    NSMutableDictionary * dictParam =[[NSMutableDictionary alloc] init];
    [dictParam setObject:AppToken  forKey:@"apptoken"];
    [dictParam setObject:[[UserModel sharedSingleton] getUserID]  forKey:@"user_id"];
    [dictParam setObject:objOtherUserModel.user_id  forKey:@"friend_id"];
    [dictParam setObject:[NSNumber numberWithInteger:liketype]  forKey:@"like_status"];

    NSLog(@"Dict Tinder Like:%@",dictParam);
    
    [Networking rawJsondataTaskwithURL:MeetLikeSuperLikeDislikeTinder Param:dictParam ImageArray:nil ImageParamArray:nil  WithHudAnimation:NO  compilation:^(id response, NSError *error)
     {
         if (!error) {
             
             NSLog(@"Tinder Like Dislike Data :%@",response);
             if ([[response valueForKey:@"code"] intValue] == 1)
             {
                 
                 
             }
             else if ([[response valueForKey:@"code"] intValue] == 0)
             {
                 
             }
         }
     }];
    
}


-(void)GetSuperlikeCount
{
    //{"apptoken":"d10289185f466003c450a15d300e50d5", "user_id":"170" }
    NSMutableDictionary * dictParam = [[NSMutableDictionary alloc] init];
    [dictParam setObject:AppToken forKey:@"apptoken"];
    [dictParam setObject:[[UserModel sharedSingleton] getUserID]  forKey:@"user_id"];
    
    NSLog(@"Update Dict Data :%@",dictParam);
    
    
    [Networking rawJsondataTaskwithURL:GetSuperLikeCount Param:dictParam ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error) {
        if (!error)
        {
            NSLog(@"response History Data :%@",response);
            superLikeCount = 0;
            if ([[response valueForKey:@"status"]integerValue] == 1)
            {
                /*arrGetAllPlans = [[NSMutableArray alloc] init];
                 arrGetAllPlans = [response valueForKey:@"message"];
                 [subscriptionView.tblSubscriptionPlans reloadData];*/
                
                
                
            }
            else
            {
                //[self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Error];
            }
        }
    }];
    
    
}



-(void)GetAllPlans
{
    //{"apptoken":"d10289185f466003c450a15d300e50d5"}
    NSMutableDictionary * dictParam = [[NSMutableDictionary alloc] init];
    [dictParam setObject:AppToken forKey:@"apptoken"];
    
    NSLog(@"Update Dict Data :%@",dictParam);
    
    
    [Networking rawJsondataTaskwithURL:GetAllPlansList Param:dictParam ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error) {
        if (!error)
        {
            NSLog(@"response History Data :%@",response);
            
            if ([[response valueForKey:@"status"]integerValue] == 1)
            {
                /*arrGetAllPlans = [[NSMutableArray alloc] init];
                arrGetAllPlans = [response valueForKey:@"message"];
                [subscriptionView.tblSubscriptionPlans reloadData];*/
                
                MeetRadarAnimation_VC * meet = [[MeetRadarAnimation_VC alloc] init];
                [meet GetPlanData:response compilation:^(id response, NSError *error) {
                    
                }];
            }
            else
            {
                //[self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Error];
            }
        }
    }];
    
    
}



@end
