//
//  DraggableView.m
//  testing swiping
//
//  Created by Richard Kim on 5/21/14.
//  Copyright (c) 2014 Richard Kim. All rights reserved.
//
//  @cwRichardKim for updates and requests

#define ACTION_MARGIN 120 //%%% distance from center where the action applies. Higher = swipe further in order for the action to be called
#define SCALE_STRENGTH 4 //%%% how quickly the card shrinks. Higher = slower shrinking
#define SCALE_MAX .93 //%%% upper bar for how much the card shrinks. Higher = shrinks less
#define ROTATION_MAX 1 //%%% the maximum rotation allowed in radians.  Higher = card can keep rotating longer
#define ROTATION_STRENGTH 320 //%%% strength of rotation. Higher = weaker rotation
#define ROTATION_ANGLE M_PI/8 //%%% Higher = stronger rotation angle


#import "DraggableView.h"
#import "Networking.h"
#import "UserModel.h"
#import "MeetRadarAnimation_VC.h"

@implementation DraggableView {
    CGFloat xFromCenter;
    CGFloat yFromCenter;
}

//delegate is instance of ViewController
@synthesize delegate;

@synthesize panGestureRecognizer;
@synthesize information;
@synthesize overlayView,overlayViewRight,overlayViewTop;
@synthesize imgviewUser;
@synthesize lblUsername;
@synthesize lblAge;
@synthesize bottomView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupView];
        
        self.layer.cornerRadius = 10;
        self.clipsToBounds = YES;
        
#warning placeholder stuff, replace with card-specific information {
        information = [[UILabel alloc]initWithFrame:CGRectMake(0, 50, self.frame.size.width, 100)];
        information.text = @"no info given";
        [information setTextAlignment:NSTextAlignmentCenter];
        information.textColor = [UIColor blackColor];
        
        self.backgroundColor = [UIColor whiteColor];
#warning placeholder stuff, replace with card-specific information }
        
        
        // IMAGE VIEW STUFF ----------------------
        
        imgviewUser = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width , self.frame.size.height-90)];
        imgviewUser.backgroundColor = [UIColor blackColor];
        
        bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height-90, self.frame.size.width, 90)];
        bottomView.backgroundColor = UIColorFromRGB(Color_NavigationBar);
        
        
        lblUsername = [[UILabel alloc] initWithFrame:CGRectMake(8, 0, self.frame.size.width-16, 45)];
        lblUsername.textColor = [UIColor whiteColor];
        [lblUsername setFont:[UIFont fontWithName:@"HelveticaNeue-CondensedBold" size:21.0]];
        
        lblAge = [[UILabel alloc] initWithFrame:CGRectMake(8, 46, self.frame.size.width-16, 40)];
        lblAge.textColor = [UIColor whiteColor];
        [lblAge setFont:[UIFont fontWithName:@"HelveticaNeue-CondensedBold" size:18.0]];
        
        panGestureRecognizer = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(beingDragged:)];
        
        [self addGestureRecognizer:panGestureRecognizer];
        //[self addSubview:information];
        [self addSubview:imgviewUser];
        [self addSubview:bottomView];
        [bottomView addSubview:lblUsername];
        [bottomView addSubview:lblAge];
        
        overlayView = [[OverlayView alloc]initWithFrame:CGRectMake(20, 20, 150, 100)];
        
        overlayView.alpha = 0;
        [self addSubview:overlayView];
        
        overlayViewRight = [[OverlayView alloc]initWithFrame:CGRectMake(self.frame.size.width-170, 20, 150, 100)];
        overlayViewRight.alpha = 0;
        [self addSubview:overlayViewRight];
        
        overlayViewTop = [[OverlayView alloc]initWithFrame:CGRectMake(self.frame.size.width/2-75, self.frame.size.height-200, 150, 70)];
        overlayViewTop.alpha = 0;
        [self addSubview:overlayViewTop];
    }
    return self;
}

-(void)setupView
{
    self.layer.cornerRadius = 4;
    self.layer.shadowRadius = 3;
    self.layer.shadowOpacity = 0.2;
    self.layer.shadowOffset = CGSizeMake(1, 1);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

//%%% called when you move your finger across the screen.
// called many times a second
-(void)beingDragged:(UIPanGestureRecognizer *)gestureRecognizer
{
    //%%% this extracts the coordinate data from your swipe movement. (i.e. How much did you move?)
    xFromCenter = [gestureRecognizer translationInView:self].x; //%%% positive for right swipe, negative for left
    yFromCenter = [gestureRecognizer translationInView:self].y; //%%% positive for up, negative for down
    
    //%%% checks what state the gesture is in. (are you just starting, letting go, or in the middle of a swipe?)
    switch (gestureRecognizer.state) {
            //%%% just started swiping
        case UIGestureRecognizerStateBegan:{
            self.originalPoint = self.center;
            break;
        };
            //%%% in the middle of a swipe
        case UIGestureRecognizerStateChanged:{
            //%%% dictates rotation (see ROTATION_MAX and ROTATION_STRENGTH for details)
            CGFloat rotationStrength = MIN(xFromCenter / ROTATION_STRENGTH, ROTATION_MAX);
            
            //%%% degree change in radians
            CGFloat rotationAngel = (CGFloat) (ROTATION_ANGLE * rotationStrength);
            
            //%%% amount the height changes when you move the card up to a certain point
            CGFloat scale = MAX(1 - fabsf(rotationStrength) / SCALE_STRENGTH, SCALE_MAX);
            
            //%%% move the object's center by center + gesture coordinate
            self.center = CGPointMake(self.originalPoint.x + xFromCenter, self.originalPoint.y + yFromCenter);
            
            //%%% rotate by certain amount
            CGAffineTransform transform = CGAffineTransformMakeRotation(rotationAngel);
            
            //%%% scale by certain amount
            CGAffineTransform scaleTransform = CGAffineTransformScale(transform, scale, scale);
            
            //%%% apply transformations
            self.transform = scaleTransform;
            //[self updateOverlay:xFromCenter];
            [self updateOverlayWithXDistance:xFromCenter WithYDistance:yFromCenter];
            NSLog(@"xFromCenter :%f",xFromCenter);
            NSLog(@"yFromCenter :%f",yFromCenter);
            
            
            break;
        };
            //%%% let go of the card
        case UIGestureRecognizerStateEnded: {
            [self afterSwipeAction];
            break;
        };
        case UIGestureRecognizerStatePossible:break;
        case UIGestureRecognizerStateCancelled:break;
        case UIGestureRecognizerStateFailed:break;
    }
}

//%%% checks to see if you are moving right or left and applies the correct overlay image
-(void)updateOverlay:(CGFloat)distance
{
    if (distance > 0) {
        overlayView.mode = GGOverlayViewModeRight;
    } else {
        overlayView.mode = GGOverlayViewModeLeft;
    }
    
    overlayView.alpha = MIN(fabsf(distance)/100, 0.4);
}

//%%% called when the card is let go

//********************* EDITED BY JAI
-(void)updateOverlayWithXDistance:(CGFloat)xdistance WithYDistance:(CGFloat)ydistance
{
    if (xdistance > 50 && (-119 < ydistance || ydistance < 119)) {
        overlayView.mode = GGOverlayViewModeLeft;
        NSLog(@"Right Mode");
        //overlayView.alpha = MIN(fabsf(xdistance)/100, 0.4);
        overlayView.alpha = 1;
        overlayViewRight.alpha = MIN(fabsf(0.0f)/100, 0.4);
        overlayViewTop.alpha = MIN(fabsf(0.0f)/100, 0.4);
    }
    else if (xdistance < -50 && (-119 < ydistance || ydistance < 119))
    {
        overlayViewRight.mode = GGOverlayViewModeRight;
        NSLog(@"Left Mode");
        //overlayViewRight.alpha = MIN(fabsf(xdistance)/100, 0.4);
        overlayViewRight.alpha = 1;
        overlayViewTop.alpha = MIN(fabsf(0.0f)/100, 0.4);
        overlayView.alpha = MIN(fabsf(0.0f)/100, 0.4);
    }
    else if (ydistance < 0 && (-119 < xdistance || xdistance < 119))
    {
        overlayViewTop.mode = GGOverlayViewModeTop;
        NSLog(@"Top Mode");
        //overlayViewTop.alpha = MIN(fabsf(ydistance)/100, 0.4);
        overlayViewTop.alpha = 1;
        overlayViewRight.alpha = MIN(fabsf(0.0f)/100, 0.4);
        overlayView.alpha = MIN(fabsf(0.0f)/100, 0.4);
    }
    
//    overlayView.alpha = MIN(fabsf(xdistance)/100, 0.4);
}

- (void)afterSwipeAction
{
    if (xFromCenter > ACTION_MARGIN) {
        [self rightAction];
    } else if (xFromCenter < -ACTION_MARGIN) {
        [self leftAction];
    }
    //////////////  Edited By Jai
    else if (yFromCenter < - 180) {
        [self topAction];
    }
    //////////////
    else { //%%% resets the card
        [UIView animateWithDuration:0.3
                         animations:^{
                             self.center = self.originalPoint;
                             self.transform = CGAffineTransformMakeRotation(0);
                             overlayView.alpha = overlayViewRight.alpha =  overlayViewTop.alpha = 0;
                         }];
    }
}

//%%% called when a swipe exceeds the ACTION_MARGIN to the right
-(void)rightAction
{
    CGPoint finishPoint = CGPointMake(500, 2*yFromCenter +self.originalPoint.y);
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.center = finishPoint;
                     }completion:^(BOOL complete){
                         [self removeFromSuperview];
                     }];
    
    [delegate cardSwipedRight:self];
    
    NSLog(@"YES");
}

//%%% called when a swip exceeds the ACTION_MARGIN to the left
-(void)leftAction
{
    CGPoint finishPoint = CGPointMake(-500, 2*yFromCenter +self.originalPoint.y);
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.center = finishPoint;
                     }completion:^(BOOL complete){
                         [self removeFromSuperview];
                     }];
    
    [delegate cardSwipedLeft:self];
    
    NSLog(@"NO");
}

-(void)rightClickAction
{
    overlayView.alpha = 1;
    CGPoint finishPoint = CGPointMake(600, self.center.y);
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.center = finishPoint;
                         self.transform = CGAffineTransformMakeRotation(1);
                     }completion:^(BOOL complete){
                         [self removeFromSuperview];
                     }];
    
    [delegate cardSwipedRight:self];
    
    NSLog(@"YES");
}

-(void)leftClickAction
{
    overlayViewRight.alpha = 1;
    CGPoint finishPoint = CGPointMake(-600, self.center.y);
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.center = finishPoint;
                         self.transform = CGAffineTransformMakeRotation(-1);
                     }completion:^(BOOL complete){
                         [self removeFromSuperview];
                     }];
    
    [delegate cardSwipedLeft:self];
    
    NSLog(@"NO");
}


/////////  EDITED BY JAI : For Top Action

//%%% called when a swipe exceeds the ACTION_MARGIN to the right
-(void)topAction
{
    [self GetSuperLikeCountWithComplitionHandler:^(id response, NSError *error) {
        
        if ([response isEqualToString:@"No"])
        {
            [UIView animateWithDuration:0.3
                             animations:^{
                                 self.center = self.originalPoint;
                                 self.transform = CGAffineTransformMakeRotation(0);
                                 overlayView.alpha = overlayViewRight.alpha =  overlayViewTop.alpha = 0;
                             }];
        }
        else if ([response isEqualToString:@"Yes"])
        {
            CGPoint finishPoint = CGPointMake(self.originalPoint.x, -500);
            [UIView animateWithDuration:0.3
                             animations:^{
                                 self.center = finishPoint;
                             }completion:^(BOOL complete){
                                 [self removeFromSuperview];
                             }];
            
            [delegate cardSwipedTop:self];
            
            NSLog(@"Super YES");
        }
        
    }];
    
}


-(void)topClickAction
{
    overlayView.alpha = overlayViewRight.alpha =  overlayViewTop.alpha = 0;
    overlayViewTop.alpha = 1;
    [self GetSuperLikeCountWithComplitionHandler:^(id response, NSError *error) {
        
        if ([response isEqualToString:@"No"])
        {
            /*[UIView animateWithDuration:0.3
                             animations:^{
                                 self.center = self.originalPoint;
                                 self.transform = CGAffineTransformMakeRotation(0);
                                 overlayView.alpha = overlayViewRight.alpha =  overlayViewTop.alpha = 0;
                             }];*/
            
            overlayView.alpha = overlayViewRight.alpha =  overlayViewTop.alpha = 0;
        }
        else if ([response isEqualToString:@"Yes"])
        {
            overlayViewTop.alpha = 1;
            CGPoint finishPoint = CGPointMake(self.center.x, -600);
            [UIView animateWithDuration:0.3
                             animations:^{
                                 self.center = finishPoint;
                                 self.transform = CGAffineTransformMakeRotation(-1);
                             }completion:^(BOOL complete){
                                 [self removeFromSuperview];
                             }];
            
            [delegate cardSwipedTop:self];
            
            NSLog(@"Super yes");
        }
        
    }];
    

}


-(void)GetSuperLikeCountWithComplitionHandler:(void (^) (id response, NSError* error))complitionHandler
{
    NSMutableDictionary * dictParamSuperlike = [[NSMutableDictionary alloc] init];
    [dictParamSuperlike setObject:AppToken forKey:@"apptoken"];
    [dictParamSuperlike setObject:[[UserModel sharedSingleton] getUserID]  forKey:@"user_id"];
    
    NSLog(@"dictParamSuperlike Data :%@",dictParamSuperlike);
    
    
    [Networking rawJsondataTaskwithURL:GetSuperLikeCount Param:dictParamSuperlike ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error) {
        if (!error)
        {
            NSLog(@"Super Like Data :%@",response);
            if ([[response valueForKey:@"status"]integerValue] == 1)
            {
                complitionHandler(@"Yes" , nil);
            }
            else
            {
                [self GetAllPlans];
                complitionHandler(@"No" , nil);
            }
        }
        else
        {
            complitionHandler(@"No" , nil);
        }
    }];

}




-(void)GetAllPlans
{
    //{"apptoken":"d10289185f466003c450a15d300e50d5"}
    NSMutableDictionary * dictParam = [[NSMutableDictionary alloc] init];
    [dictParam setObject:AppToken forKey:@"apptoken"];
    
    NSLog(@"Plan Dict :%@",dictParam);
    
    
    [Networking rawJsondataTaskwithURL:GetAllPlansList Param:dictParam ImageArray:nil ImageParamArray:nil compilation:^(id response, NSError *error) {
        if (!error)
        {
            NSLog(@"Plan Data :%@",response);
            
            if ([[response valueForKey:@"status"]integerValue] == 1)
            {

                
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:DidReceivePlans
                 object:self
                 userInfo:response];
                
//                __weak MeetRadarAnimation_VC * meet = [[MeetRadarAnimation_VC alloc] init];
//                [meet GetPlanData:response compilation:^(id response, NSError *error) {
//                    
//                }];
            }
            else
            {
                //[self showAlertMessage:[response valueForKey:@"message"] Title:Alert_Title_Error];
            }
        }
    }];
    
    
}



@end




